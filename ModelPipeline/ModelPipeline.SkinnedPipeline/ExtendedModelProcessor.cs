﻿using Microsoft.Xna.Framework.Content.Pipeline.Processors;
using Microsoft.Xna.Framework.Content.Pipeline.Graphics;
using Microsoft.Xna.Framework.Content.Pipeline;
using System.ComponentModel;
using Microsoft.Xna.Framework;

namespace SkinnedModelPipeline
{
    [ContentProcessor(DisplayName = "Extended Model Processor")]
    class ExtendedModelProcessor : ModelProcessor
    {
        [Browsable(false)]
        public override bool GenerateTangentFrames
        {
            get => true;
            set { }
        }

        [Browsable(false)]
        public override bool GenerateMipmaps
        {
            get => true;
            set { }
        }

        [Browsable(false)]
        public override MaterialProcessorDefaultEffect DefaultEffect
        {
            get => new MaterialProcessorDefaultEffect();
            set { }
        }

        protected override MaterialContent ConvertMaterial(MaterialContent material, ContentProcessorContext context)
        {
            OpaqueDataDictionary parameters = new OpaqueDataDictionary
            {
                {"ColorKeyColor", ColorKeyColor},
                {"ColorKeyEnabled", ColorKeyEnabled},
                {"GenerateMipmaps", GenerateMipmaps},
                {"PremultiplyTextureAlpha", PremultiplyTextureAlpha},
                {"ResizeTexturesToPowerOfTwo", ResizeTexturesToPowerOfTwo},
                {"TextureFormat", TextureFormat},
            };

            EffectMaterialContent extendedMaterial = new EffectMaterialContent {Effect = new ExternalReference<EffectContent>("Effect/ExtendedEffect.fx")};
            extendedMaterial.CompiledEffect = context.BuildAsset<EffectContent, CompiledEffectContent>(extendedMaterial.Effect, "EffectProcessor");

            //Transfer opaque data
            extendedMaterial.OpaqueData.Add("DiffuseColor",
                material.OpaqueData.ContainsKey("DiffuseColor") ? material.OpaqueData["DiffuseColor"] : Vector3.One);
            extendedMaterial.OpaqueData.Add("Alpha", material.OpaqueData.ContainsKey("Alpha") ? material.OpaqueData["Alpha"] : 1.0f);
            extendedMaterial.OpaqueData.Add("SpecularColor",
                material.OpaqueData.ContainsKey("SpecularColor") ? material.OpaqueData["SpecularColor"] : Vector3.One);
            extendedMaterial.OpaqueData.Add("SpecularPower", material.OpaqueData.ContainsKey("SpecularPower") ? material.OpaqueData["SpecularPower"] : 16.0f);

            if (material.Textures.ContainsKey("Texture"))
            {
                extendedMaterial.Textures.Add("Texture", material.Textures["Texture"]);
            }

            if (material.Textures.ContainsKey("Specular"))
            {
                extendedMaterial.Textures.Add("SpecularMap", material.Textures["Specular"]);
            }
            
            if (material.Textures.ContainsKey("Normal"))
            {
                extendedMaterial.Textures.Add("NormalMap", material.Textures["Normal"]);
            }
            if (material.Textures.ContainsKey("Transparency"))
            {
                extendedMaterial.Textures.Add("TransparencyMap", material.Textures["Transparency"]);
            }


            return context.Convert<MaterialContent, MaterialContent>(extendedMaterial, "MaterialProcessor", parameters);
        }
    }
}
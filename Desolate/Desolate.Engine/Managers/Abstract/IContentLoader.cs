﻿using Microsoft.Xna.Framework.Content;

namespace Desolate.Engine.Managers.Abstract
{
    public interface IContentLoader
    {
        void LoadContent(int sceneId);
        void Init(ContentManager contentManager);
    }
}
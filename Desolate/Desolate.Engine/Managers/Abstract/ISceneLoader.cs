﻿using Desolate.Core;
using Microsoft.Xna.Framework;

namespace Desolate.Engine.Managers.Abstract
{
    public interface ISceneLoader
    {
        void Init(Game game);
        Scene LoadScene(int sceneId, bool isDebug = false);
        Scene ReloadCurrentScene(bool isDebug = false);
    }
}
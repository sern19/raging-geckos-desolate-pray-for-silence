﻿using System;
using System.Collections.Generic;
using Desolate.Core;
using Desolate.Core.Components.Rendering;
using Desolate.Engine.Managers.Abstract;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace Desolate.Engine.Managers
{
    public class ContentLoader : IContentLoader
    {
        private ContentManager contentManager;
        private static Type meshRendererType;

        public static Dictionary<string, Model> Models { get; private set; }
        public static Dictionary<string, SpriteFont> SpriteFonts { get; private set; }
        public static Dictionary<string, Texture2D> Textures2D { get; private set; }
        public static Dictionary<string, Model> NavMeshModels { get; private set; }
        public static Dictionary<string, SoundEffect> Sounds { get; private set; }

        public static Type MeshRendererType => meshRendererType;

        public void Init(ContentManager contentManager)
        {
            this.contentManager = contentManager;
        }

        private void LoadDeferredShaders()
        {
            //Load shaders
            try
            {
                DeferredMeshRenderer.ClearEffect = contentManager.Load<Effect>("Effect/Clear");
                DeferredMeshRenderer.GBufferEffect = contentManager.Load<Effect>("Effect/GBuffer");
                DeferredMeshRenderer.GBufferSkinnedEffect = contentManager.Load<Effect>("Effect/GBufferSkinned");
                DeferredMeshRenderer.AmbientLightEffect = contentManager.Load<Effect>("Effect/AmbientLight");
                DeferredMeshRenderer.DirectionalLightEffect = contentManager.Load<Effect>("Effect/DirectionalLight");
//                DeferredMeshRenderer.PointLightEffect = contentManager.Load<Effect>("Effect/PointLight");
//                DeferredMeshRenderer.SpotLightEffect = contentManager.Load<Effect>("Effect/SpotLight");
                DeferredMeshRenderer.CompositionEffect = contentManager.Load<Effect>("Effect/Composition");
                DeferredMeshRenderer.DepthWriterEffect = contentManager.Load<Effect>("Effect/DepthWriter");
                DeferredMeshRenderer.DepthWriterSkinnedEffect =
                    contentManager.Load<Effect>("Effect/DepthWriterSkinned");
                DeferredMeshRenderer.RestoreDepthEffect = contentManager.Load<Effect>("Effect/RestoreDepth");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            //Set active technique
            DeferredMeshRenderer.ClearEffect.CurrentTechnique = DeferredMeshRenderer.ClearEffect.Techniques[0];
            DeferredMeshRenderer.GBufferEffect.CurrentTechnique = DeferredMeshRenderer.GBufferEffect.Techniques[0];
            DeferredMeshRenderer.GBufferSkinnedEffect.CurrentTechnique =
                DeferredMeshRenderer.GBufferSkinnedEffect.Techniques[0];
            DeferredMeshRenderer.AmbientLightEffect.CurrentTechnique =
                DeferredMeshRenderer.AmbientLightEffect.Techniques[0];
            DeferredMeshRenderer.DirectionalLightEffect.CurrentTechnique =
                DeferredMeshRenderer.DirectionalLightEffect.Techniques[0];
//            DeferredMeshRenderer.PointLightEffect.CurrentTechnique = DeferredMeshRenderer.PointLightEffect.Techniques[0];
//            DeferredMeshRenderer.SpotLightEffect.CurrentTechnique = DeferredMeshRenderer.SpotLightEffect.Techniques[0];
            DeferredMeshRenderer.CompositionEffect.CurrentTechnique =
                DeferredMeshRenderer.CompositionEffect.Techniques[0];
            DeferredMeshRenderer.DepthWriterEffect.CurrentTechnique =
                DeferredMeshRenderer.DepthWriterEffect.Techniques[0];
            DeferredMeshRenderer.DepthWriterSkinnedEffect.CurrentTechnique =
                DeferredMeshRenderer.DepthWriterSkinnedEffect.Techniques[0];
            DeferredMeshRenderer.RestoreDepthEffect.CurrentTechnique =
                DeferredMeshRenderer.RestoreDepthEffect.Techniques[0];
        }

        public void LoadContent(int sceneId)
        {
            //Init shaders
            if (GameManager.IsDeferred)
                LoadDeferredShaders();
            //else LoadForwardShaders

            meshRendererType = GameManager.IsDeferred ? typeof(DeferredMeshRenderer) : typeof(ForwardMeshRenderer);

            //todo content should be configured per scene
            Models = new Dictionary<string, Model>();
            NavMeshModels = new Dictionary<string, Model>();
            SpriteFonts = new Dictionary<string, SpriteFont>();
            Textures2D = new Dictionary<string, Texture2D>();
            Sounds = new Dictionary<string, SoundEffect>();
            try
            {

                NavMeshModels.Add("room1", contentManager.Load<Model>("NavMeshes/room1"));
                NavMeshModels.Add("room2", contentManager.Load<Model>("NavMeshes/room2"));
                NavMeshModels.Add("room3", contentManager.Load<Model>("NavMeshes/room3"));
                NavMeshModels.Add("room4", contentManager.Load<Model>("NavMeshes/room4"));
                NavMeshModels.Add("room5", contentManager.Load<Model>("NavMeshes/room5"));

                Models.Add("karaczan", contentManager.Load<Model>("tester"));
                Models.Add("room0", contentManager.Load<Model>("Rooms/Room0"));
                Models.Add("room1", contentManager.Load<Model>("Rooms/Room1"));
                Models.Add("room2", contentManager.Load<Model>("Rooms/Room 2 without doors"));
                Models.Add("room3", contentManager.Load<Model>("Rooms/Room3"));
                Models.Add("room4", contentManager.Load<Model>("Rooms/Room4"));
                Models.Add("room5", contentManager.Load<Model>("Rooms/Room5"));
                Models.Add("room6", contentManager.Load<Model>("Rooms/Room6"));
                Models.Add("jamb1", contentManager.Load<Model>("Rooms/Door's surrounding Rooms 1&2"));
                Models.Add("doors", contentManager.Load<Model>("Rooms/source/Just door"));
                Models.Add("jamb0", contentManager.Load<Model>("Rooms/Door's surrounding Rooms 0&1"));
                Models.Add("jamb2", contentManager.Load<Model>("Rooms/Door's surrounding Rooms 2&3"));
                Models.Add("jamb3", contentManager.Load<Model>("Rooms/Door's surrounding Rooms 3&4"));
                Models.Add("jamb4", contentManager.Load<Model>("Rooms/Door's surrounding Rooms 4&5"));
                Models.Add("jamb5", contentManager.Load<Model>("Rooms/Door's surrounding Rooms 5&6"));
                Models.Add("jamb6", contentManager.Load<Model>("Rooms/Door's surrounding Rooms 6&1"));
                Models.Add("box", contentManager.Load<Model>("kostka"));
                Models.Add("plane", contentManager.Load<Model>("Plane"));
                Models.Add("gravityGun", contentManager.Load<Model>("gravityGun"));
                Models.Add("lure", contentManager.Load<Model>("lure"));
                Models.Add("taser", contentManager.Load<Model>("TaserGun"));
                Models.Add("2b", contentManager.Load<Model>("2B/source/2B"));
                Models.Add("Vent", contentManager.Load<Model>("vent"));
                Models.Add("Screw", contentManager.Load<Model>("screw"));
                Models.Add("ScrewCarpet", contentManager.Load<Model>("screw Carpet"));

                Models.Add("tanks", contentManager.Load<Model>("containers/Tanks"));
                Models.Add("container1", contentManager.Load<Model>("containers/container1"));
                Models.Add("container2", contentManager.Load<Model>("containers/container2"));
                Models.Add("case1", contentManager.Load<Model>("cases/case1"));
                Models.Add("case2", contentManager.Load<Model>("cases/case2"));
                Models.Add("crate1", contentManager.Load<Model>("crate/crate1"));
                Models.Add("crate2", contentManager.Load<Model>("crate/crate2"));
                Models.Add("barrel1", contentManager.Load<Model>("hazardBarrel/Barrel"));
                Models.Add("locker", contentManager.Load<Model>("locker/locker"));
                Models.Add("monitor", contentManager.Load<Model>("monitor/monitor"));
                Models.Add("barrel2", contentManager.Load<Model>("radioactiveBarrels/Barrel1"));
                Models.Add("barrel3", contentManager.Load<Model>("radioactiveBarrels/Barrel2"));
                Models.Add("trolley1", contentManager.Load<Model>("Trolley/Trolley"));
                Models.Add("trolley2", contentManager.Load<Model>("Trolley/Trolley2"));

                Models.Add("animations", contentManager.Load<Model>("karaczanMerged")); // FBX with Karaczan animations !!

                Textures2D.Add("gravityGunUI", contentManager.Load<Texture2D>("gravityGunUI"));
                Textures2D.Add("lureUI", contentManager.Load<Texture2D>("lureUI"));
                Textures2D.Add("taserUI", contentManager.Load<Texture2D>("taserUI"));
                Textures2D.Add("background", contentManager.Load<Texture2D>("background"));
                Textures2D.Add("ventUI", contentManager.Load<Texture2D>("ventUI"));
                Textures2D.Add("vent1", contentManager.Load<Texture2D>("vent1"));
                Textures2D.Add("vent2", contentManager.Load<Texture2D>("vent2"));
                Textures2D.Add("vent3", contentManager.Load<Texture2D>("vent3"));
                Textures2D.Add("bar", contentManager.Load<Texture2D>("Sprites/bar"));
                Textures2D.Add("barNoise", contentManager.Load<Texture2D>("Sprites/barNoise"));
                Textures2D.Add("standing", contentManager.Load<Texture2D>("Sprites/Standing"));
                Textures2D.Add("crouching", contentManager.Load<Texture2D>("Sprites/Crouching"));
                Textures2D.Add("running", contentManager.Load<Texture2D>("Sprites/Running"));
                Textures2D.Add("crosshair", contentManager.Load<Texture2D>("Sprites/SimpleCrosshair"));
                Textures2D.Add("aimRun", contentManager.Load<Texture2D>("aimRunning"));
                Textures2D.Add("aimCrouch", contentManager.Load<Texture2D>("aimCrouch"));
                Textures2D.Add("aimStand", contentManager.Load<Texture2D>("aimStanding"));
                Textures2D.Add("noiseIndicator", contentManager.Load<Texture2D>("Sprites/NoiseIndicator"));

                SpriteFonts.Add("TobagoPoster", contentManager.Load<SpriteFont>("Sprites/Tobago"));
                SpriteFonts.Add("defaultFont", contentManager.Load<SpriteFont>("Sprites/Default"));

                Sounds.Add("open", contentManager.Load<SoundEffect>("Audio/Doors/Open"));
                Sounds.Add("close", contentManager.Load<SoundEffect>("Audio/Doors/Close"));
                Sounds.Add("pull", contentManager.Load<SoundEffect>("Audio/GravityGun/GravityGunPull"));
                Sounds.Add("push", contentManager.Load<SoundEffect>("Audio/GravityGun/GravityGunPush"));
                Sounds.Add("idleA", contentManager.Load<SoundEffect>("Audio/Karaczan/IdleA"));
                Sounds.Add("idleB", contentManager.Load<SoundEffect>("Audio/Karaczan/IdleB"));
                Sounds.Add("movementA", contentManager.Load<SoundEffect>("Audio/Karaczan/MovementA"));
                Sounds.Add("movementB", contentManager.Load<SoundEffect>("Audio/Karaczan/MovementB"));
                Sounds.Add("paralyzedA", contentManager.Load<SoundEffect>("Audio/Karaczan/ParalyzedA"));
                Sounds.Add("paralyzedB", contentManager.Load<SoundEffect>("Audio/Karaczan/ParalyzedB"));
                Sounds.Add("triggerA", contentManager.Load<SoundEffect>("Audio/Karaczan/TriggerA"));
                Sounds.Add("triggerB", contentManager.Load<SoundEffect>("Audio/Karaczan/TriggerB"));
                Sounds.Add("screw", contentManager.Load<SoundEffect>("Audio/NoiseMakers/ScrewFalling"));
                Sounds.Add("lure", contentManager.Load<SoundEffect>("Audio/NoiseMakers/Lure"));
                Sounds.Add("crouching", contentManager.Load<SoundEffect>("Audio/Player/Crouching"));
                Sounds.Add("running", contentManager.Load<SoundEffect>("Audio/Player/Running"));
                Sounds.Add("walking", contentManager.Load<SoundEffect>("Audio/Player/Walking"));
                Sounds.Add("taser", contentManager.Load<SoundEffect>("Taser"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

        }
    }
}
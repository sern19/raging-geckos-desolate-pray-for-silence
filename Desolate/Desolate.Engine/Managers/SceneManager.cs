﻿using System;
using System.Collections.Generic;
using System.Linq;
using Desolate.Core;
using Desolate.Core.AI.Navigation;
using Desolate.Core.Components;
using Desolate.Core.Components.AI;
using Desolate.Core.Components.Physics;
using Desolate.Core.Components.Rendering;
using Desolate.Core.Components.Rendering.Animation;
using Desolate.Core.Components.Rendering.Animation.TransformAnimation;
using Desolate.Core.Components.Rendering.Model;
using Desolate.Core.Managers;
using Desolate.Core.Scripts;
using Desolate.Core.Scripts.AccessControl;
using Desolate.Core.Scripts.Enemy;
using Desolate.Core.Scripts.Noise;
using Desolate.Core.Scripts.PlayerBehaviour;
using Desolate.Core.Scripts.UI;
using Desolate.Core.Scripts.Weapons;
using Desolate.Core.Components.Audio;
using Desolate.Engine.Managers.Abstract;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;
using SkinnedModel;
using AnimationClip = Desolate.Core.Components.Rendering.Animation.TransformAnimation.AnimationClip;
using DirectionalLight = Desolate.Core.Components.Rendering.Lightning.DirectionalLight;

namespace Desolate.Engine.Managers
{
    public class SceneManager : ISceneLoader
    {
        #region Initialization

        private Dictionary<string, Model> Models => ContentLoader.Models;
        private Dictionary<string, SpriteFont> SpriteFonts => ContentLoader.SpriteFonts;
        private Dictionary<string, Texture2D> Textures2D => ContentLoader.Textures2D;
        private Dictionary<string, Model> NavMeshModels => ContentLoader.NavMeshModels;
        private Dictionary<string, SoundEffect> Sounds => ContentLoader.Sounds;

        private List<Func<bool, Scene>> sceneTemplates;

        private Game game;

        private int currentSceneIndex = 0;

        private Type MeshRendererType => ContentLoader.MeshRendererType;

        public SceneManager()
        {
            sceneTemplates =
                new List<Func<bool, Scene>> {MainScene, FirstScene, SecondScene, ThirdScene, FourthScene, FifthScene};
        }

        public void Init(Game game)
        {
            this.game = game;
        }

        #endregion

        #region Scene Loading

        public Scene LoadScene(int sceneId, bool isDebug = false)
        {
            //todo check if content is loaded for scene
            currentSceneIndex = sceneId;

            var scene = sceneTemplates[currentSceneIndex](isDebug);
            scene.Initialize();
            return scene;
        }

        public Scene ReloadCurrentScene(bool isDebug = false) => LoadScene(currentSceneIndex, isDebug);

        #endregion

        #region SceneTemplates

        private Scene MainScene(bool isDebug)
        {
            Scene scene = new Scene(game, isDebug);

            Physic.SetLayersCollisionInteraction(PhysicLayer.UserDefined_4, PhysicLayer.UserDefined_3, false);
            Physic.SetLayersCollisionInteraction(PhysicLayer.Pullable, PhysicLayer.Player, false);
            Physic.SetLayersCollisionInteraction(PhysicLayer.Throwable, PhysicLayer.Player, false);

            #region Camera

            Camera mainCamera = new Camera(new Vector3(0, 180, 0))
            {
                Transform =
                {
                    LocalPosition = new Vector3(-1.5f, 1.75f, 1.5f)
                },
                Layer = PhysicLayer.Player
            };

            var cameraController = new CameraController(mainCamera);

            if (GameManager.IsDebug)
            {
                cameraController.LockY = false;
                cameraController.MoveSpeed = 10f;
                cameraController.MouseRotationSpeed = 3f;
            }

            mainCamera.AddComponent(new PlayerAccessTracker(mainCamera)); //DO NOT REMOVE :)

            mainCamera.AddComponent(cameraController);
            mainCamera.AddComponent(new BoxCollider(mainCamera)
            {
                Size = new Vector3(0.5f, 0.5f, 0.5f),
                Center = new Vector3(0, -0.875f, 0)
            });
            mainCamera.AddComponent(new SphereCollider(mainCamera) {Radius = 2.5f, IsTrigger = true});
            mainCamera.AddComponent(new Rigidbody(mainCamera)
            {
                IsAffectedByGravity = false,
                IsStatic = false,
                IsKinematic = true
            });

            GameObject gravityGun = new GameObject
            {
                Transform = {LocalPosition = new Vector3(-0.9f, -0.5f, -0.6f)}
            };
            gravityGun.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, gravityGun, Models["gravityGun"]));
            gravityGun.AddComponent(new GravityGun(gravityGun));
            gravityGun.Transform.Rotate(new Vector3(90, 90, 0));
            gravityGun.Transform.Scale(new Vector3(2, 2, 2));
            mainCamera.AddChild(gravityGun);

            GameObject taser = new GameObject
            {
                Transform = {LocalPosition = new Vector3(0.3f, -0.5f, -1.1f)}
            };
            taser.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, taser, Models["taser"]));
            taser.AddComponent(new Taser(taser));
            taser.Transform.Rotate(new Vector3(0, 90, 180));
            taser.Transform.Scale(new Vector3(2, 2, 2));
            mainCamera.AddChild(taser);

            GameObject lure = new GameObject
            {
                Transform = {LocalPosition = new Vector3(0.2f, -0.3f, -0.6f)}
            };
            lure.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, lure, Models["lure"]));

            lure.Transform.Scale(new Vector3(1, 1, 1));

            mainCamera.AddChild(lure);

            GameObject cameraHolder = new GameObject();

            cameraHolder.AddChild(mainCamera);

            PlayerController playerController = new PlayerController(cameraHolder);
            playerController.GameObject.AddComponent(new NoiseGenerator(playerController.GameObject));


            cameraHolder.AddComponent(playerController);
            if (GameManager.IsDebug)
            {
                playerController.IsEnabled = false;
            }

            var playerComponent = new Player(mainCamera) {room = new Room()};
            mainCamera.AddComponent(playerComponent);


            //weapon selector UI 

            //selected weapon UI
            GameObject selectedWeaponUI = new GameObject();
            selectedWeaponUI.AddComponent(
                new ImageRenderer(selectedWeaponUI, Textures2D["background"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0.5f, 0.5f)
                });
            selectedWeaponUI.Transform.Scale(new Vector3(0.018f, 0.018f, 0.018f));
            selectedWeaponUI.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 110,
                GameManager.GraphicsDevice.Viewport.Height - 50, 0));
            scene.AddGameObject(selectedWeaponUI);

            //gravity gun
            GameObject gravityGunUI = new GameObject();
            gravityGunUI.AddComponent(
                new ImageRenderer(gravityGunUI, Textures2D["gravityGunUI"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0.5f, 0.5f),
                    Color = new Color(70, 70, 70)
                });
            gravityGunUI.Transform.Scale(new Vector3(0.012f, 0.012f, 0.012f));
            gravityGunUI.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 190,
                GameManager.GraphicsDevice.Viewport.Height - 50, 0));
            scene.AddGameObject(gravityGunUI);

            // taser
            GameObject taserUI = new GameObject();
            taserUI.AddComponent(
                new ImageRenderer(taserUI, Textures2D["taserUI"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0.5f, 0.5f)
                });
            taserUI.Transform.Scale(new Vector3(0.012f, 0.012f, 0.012f));
            taserUI.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 110,
                GameManager.GraphicsDevice.Viewport.Height - 50, 0));
            scene.AddGameObject(taserUI);

            // lure 
            GameObject lureUI = new GameObject();
            lureUI.AddComponent(
                new ImageRenderer(lureUI, Textures2D["lureUI"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0.5f, 0.5f),
                    Color = new Color(70, 70, 70)
                });
            lureUI.Transform.Scale(new Vector3(0.012f, 0.012f, 0.012f));
            lureUI.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 30,
                GameManager.GraphicsDevice.Viewport.Height - 50, 0));

            scene.AddGameObject(lureUI);

            var weaponSelectorComponent = new WeaponSelector(cameraHolder)
            {
                GravityGun = new WeaponOption() {Weapon = gravityGun, WeaponUI = gravityGunUI},
                Taser = new WeaponOption() {Weapon = taser, WeaponUI = taserUI},
                LureThrower = new WeaponOption() {Weapon = lure, WeaponUI = lureUI},
                WeaponSelectorBackground = selectedWeaponUI,
            };
            cameraHolder.AddComponent(weaponSelectorComponent);

            #endregion

            scene.AddGameObject(cameraHolder);

            #region Rooms

            #region Room_1

            GameObject roomObject = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject, Models["room1"]));
            roomObject.GetComponent<MeshRenderer>().CastsShadow = false;
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(0, -0.1f, 0),
                Size = new Vector3(50, 0.1f, 50)
            });
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(0, 2.7f, 0),
                Size = new Vector3(50, 0.1f, 50)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //right wall
            {
                Center = new Vector3(2f, 0, 6.5f),
                Size = new Vector3(1, 10, 25)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //front wall
            {
                Center = new Vector3(0, 0, -2),
                Size = new Vector3(10, 10, 2)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //left wall
            {
                Center = new Vector3(-5, 0, -1.0f),
                Size = new Vector3(2, 10, 10)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //backleft wall
            {
                Center = new Vector3(-5, 0, 15.0f),
                Size = new Vector3(2, 10, 16)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //back wall
            {
                Center = new Vector3(-1.0f, 0, 23.0f),
                Size = new Vector3(5, 10, 2)
            });
            roomObject.AddComponent(new Rigidbody(roomObject)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });

            PremadeNavMesh room1NavMesh = PremadeNavMesh.FromTriangles(NavMeshModels["room1"].GetTriangles().ToArray());

            GameObject player = new GameObject() {Transform = {LocalPosition = new Vector3(0, 0, 0)}}; //for tests

            scene.Player = cameraHolder;

            Room firstRoom = new Room() {model = Models["room1"], player = mainCamera};
            firstRoom.AddChild(roomObject);

            #region Enemy

            #region karaczan - Off

            SkinningData skinningData = Models["karaczan"].Tag as SkinningData;
            GameObject pandaObject = new GameObject
            {
                Transform =
                {
                    LocalScale = new Vector3(0.02f, 0.02f, 0.02f),
                    LocalPosition = new Vector3(0, 0.3f, -5)
                }
            };
            pandaObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, pandaObject, Models["karaczan"]));

            AnimationClip pandaRotateClip
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(pandaObject)
                            {
                                LocalScale = pandaObject.Transform.LocalScale,
                                LocalPosition = pandaObject.Transform.LocalPosition,
                                LocalEulerAngles = pandaObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(pandaObject)
                            {
                                LocalScale = pandaObject.Transform.LocalScale,
                                LocalPosition = pandaObject.Transform.LocalPosition,
                                LocalEulerAngles = new Vector3(0, 180, 0)
                            },
                            Time = TimeSpan.FromSeconds(2.5)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(pandaObject)
                            {
                                LocalScale = pandaObject.Transform.LocalScale,
                                LocalPosition = pandaObject.Transform.LocalPosition,
                                LocalEulerAngles = new Vector3(0, 360, 0)
                            },
                            Time = TimeSpan.FromSeconds(5)
                        }
                    },
                };

            Animator pandaAnimator = new Animator(pandaObject) {IsLooped = true};
            pandaAnimator.AnimationClips.Add("Rotate", pandaRotateClip);
            pandaAnimator.Play("Rotate");
            pandaObject.AddComponent(pandaAnimator);

            SkinnedAnimator skinnedPandaAnimator = new SkinnedAnimator(pandaObject) {IsLooped = true};
            skinnedPandaAnimator.InitializeTransforms(skinningData);
            skinnedPandaAnimator.Play("Take 001");
            pandaObject.AddComponent(skinnedPandaAnimator);

            #endregion

            scene.AddGameObject(pandaObject);


            GameObject enemyObject = new GameObject
            {
                Transform =
                {
                    LocalPosition = new Vector3(5.5f, 0, 20),
                    LocalScale = new Vector3(0.02f, 0.02f, 0.02f)
                },
                Layer = PhysicLayer.Enemy
            };
            enemyObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, enemyObject, Models["karaczan"]));
            enemyObject.AddComponent(new SphereCollider(enemyObject)
            {
                Center = new Vector3(0, 20f, 0),
                Radius = 10f,
                IsTrigger = true
            });
            enemyObject.AddComponent(new Rigidbody(enemyObject)
            {
                IsStatic = false,
                IsAffectedByGravity = false,
                IsKinematic = true,
            });
            enemyObject.AddComponent(new NavAgent(enemyObject)
            {
                PathCalculator = room1NavMesh,
                Speed = 1.0f
            });
            enemyObject.AddComponent(new EnemyMovement(enemyObject)
            {
                WanderRange = 6.5f
            });
            enemyObject.AddComponent(new Enemy(enemyObject)
            {
                PlayerComponent = playerComponent
            });

            var enemyAnimator = new SkinnedAnimator(enemyObject) {IsLooped = true};
            enemyAnimator.InitializeTransforms(skinningData);
            enemyAnimator.Play("Take 001");
            enemyObject.AddComponent(enemyAnimator);

            #endregion

            scene.AddGameObject(enemyObject);

            #endregion

            #region Room_2

            GameObject roomObject2 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject2.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject2, Models["room2"]));
            roomObject2.GetComponent<MeshRenderer>().CastsShadow = false;
            roomObject2.AddComponent(new Rigidbody(roomObject2)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });
            roomObject2.AddComponent(new BoxCollider(roomObject) //MIDDLE
            {
                Center = new Vector3(-17.25f, 0.0f, 3),
                Size = new Vector3(19.0f, 10.0f, 3.5f)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(-17.25f, 0.0f, -2.0f),
                Size = new Vector3(25.0f, 10.0f, 1.0f)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(-5, 0, -1.0f),
                Size = new Vector3(2, 10, 10)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(-5, 0, 15.0f),
                Size = new Vector3(2, 10, 10)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject) // left from door
            {
                Center = new Vector3(-14.25f, 0.0f, 10.5f),
                Size = new Vector3(19.0f, 10.0f, 1.0f)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(-30, 0, 5.0f),
                Size = new Vector3(2, 10, 12)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject) // left from door
            {
                Center = new Vector3(-29.0f, 0.0f, 10.5f),
                Size = new Vector3(6.0f, 10.0f, 1.0f)
            });

            scene.AddGameObject(roomObject2);

            Room secondRoom = new Room() {model = Models["room2"], player = mainCamera, name = "A"};
            secondRoom.AddChild(roomObject2);

            PremadeNavMesh room2NavMesh = PremadeNavMesh.FromTriangles(NavMeshModels["room2"].GetTriangles().ToArray());

            #endregion

            #endregion

            scene.AddGameObject(roomObject);

            #region Doors

            GameObject doorsParent = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                //Transform = { LocalPosition = new Vector3(-4.5f, -1.0f, 5.5f) }
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };

            GameObject doorsObject = new GameObject
            {
                Layer = PhysicLayer.Enviroment
            };
            doorsObject.Transform.Translate(new Vector3(-4.5f, -1.0f, 5.5f));
            doorsObject.Transform.Rotate(new Vector3(0, 90, 0));
            doorsObject.Transform.LocalScale = new Vector3(2, 3, 3);
            doorsObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsObject, Models["doors"]));
            doorsObject.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject.AddComponent(new BoxCollider(doorsObject)
            {
                Center = new Vector3(0.0f, 0.0f, 0.25f),
                Size = new Vector3(0.75f, 10f, 2.0f),
                IsTrigger = false
            });
            doorsParent.AddComponent(new SphereCollider(doorsParent)
            {
                Center = new Vector3(-4.5f, -1.0f, 5.5f),
                Radius = 2.5f,
                IsTrigger = true
            });
            doorsParent.AddComponent(new DoorsOpenClose(doorsParent) {accessLevelRequired = AccessLevel.Basic});
            doorsParent.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsParent, Models["jamb1"]));
            doorsParent.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject.AddComponent(new Rigidbody(doorsObject)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false
            });

            #region AnimationClips

            AnimationClip doorsOpenClip
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 3f),
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            AnimationClip doorsCloseClip
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 3f),
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        //new AnimationFrame
                        //{
                        //    Transform = new Transform(doorsObject)
                        //    {
                        //        LocalScale = doorsObject.Transform.LocalScale,
                        //        LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 0.2f),
                        //        LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                        //    },
                        //    Time = TimeSpan.FromSeconds(0.5)
                        //},
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            Animator doorsAnimator = new Animator(doorsObject) {IsLooped = false};
            doorsAnimator.AnimationClips.Add("Open", doorsOpenClip);
            doorsAnimator.AnimationClips.Add("Close", doorsCloseClip);
            doorsObject.AddComponent(doorsAnimator);

            doorsParent.AddChild(doorsObject);

            #endregion

            #region DoorsText

            GameObject doorsTextObject = new GameObject();

            doorsTextObject.AddComponent(
                new TextRenderer(doorsTextObject, SpriteFonts["TobagoPoster"])
                {
                    IsOverlay = false,
                    Text = "Doors",
                    Color = Color.Red,
                    Pivot = new Vector2(0.5f, 1)
                });
            doorsTextObject.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            doorsTextObject.Transform.Translate(new Vector3(-4, 2, 5.75f));
            doorsTextObject.Transform.Rotate(new Vector3(0, 90, 0));

            #endregion

            #endregion

            scene.AddGameObject(doorsParent);
            scene.AddGameObject(doorsTextObject);


            //firstRoom.AddChild(doorsObject);
            firstRoom.AddDoorsAndRoom(doorsParent, secondRoom);
            secondRoom.AddDoorsAndRoom(doorsParent, firstRoom);

            scene.AddRoom(firstRoom);
            scene.AddRoom(secondRoom);

            #region Card

            GameObject boxObject = new GameObject() {Layer = PhysicLayer.Enviroment};
            boxObject.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, boxObject, Models["box"]));
            boxObject.AddComponent(new BoxCollider(boxObject) {Size = new Vector3(20f, 20f, 20f), IsTrigger = true});
            boxObject.Transform.Translate(new Vector3(-1f, 1f, 20f));
            boxObject.Transform.Scale(new Vector3(0.5f, 1f, 0.1f));
            boxObject.AddComponent(new AccessCard(boxObject) {AccessLevel = AccessLevel.Medium, scene = scene});

            #endregion

            scene.AddGameObject(boxObject);

            #region YouDiedText

            GameObject youDiedTextObject = new GameObject();

            youDiedTextObject.AddComponent(new TextRenderer(youDiedTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Text = "You died",
                Pivot = new Vector2(0, 1)
            });
            youDiedTextObject.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            youDiedTextObject.Transform.Translate(
                new Vector3(200, GameManager.GraphicsDevice.Viewport.Height / 2.0f, 0));

            #endregion

            scene.AddGameObject(youDiedTextObject);


            #region PoisonCountText

            GameObject poisonCountTextObject = new GameObject();

            poisonCountTextObject.AddComponent(new TextRenderer(poisonCountTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Pivot = new Vector2(0, 1)
            });
            poisonCountTextObject.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));
            poisonCountTextObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 110, 95,
                0));

            scene.AddGameObject(poisonCountTextObject);

            #endregion

            #region PoisonCountUI

            GameObject poisonVentsUIObject = new GameObject();

            poisonVentsUIObject.AddComponent(new ImageRenderer(poisonVentsUIObject, Textures2D["ventUI"])
            {
                IsOverlay = true,
                Pivot = new Vector2(0.5f, 0.5f)
            });


            poisonVentsUIObject.Transform.Scale(new Vector3(0.018f, 0.018f, 0.018f));
            poisonVentsUIObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 60, 50, 0));
            scene.AddGameObject(poisonVentsUIObject);

            #endregion

            #region YouWonText

            GameObject youWonTextObject = new GameObject();

            youWonTextObject.AddComponent(new TextRenderer(youWonTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Text = "You won",
                Pivot = new Vector2(0, 1)
            });
            youWonTextObject.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            youWonTextObject.Transform.Translate(new Vector3(200, GameManager.GraphicsDevice.Viewport.Height / 2.0f,
                0));

            #endregion

            scene.AddGameObject(youWonTextObject);

            #region HUD

            GameObject hudObject = new GameObject();

            hudObject.AddComponent(new HeadUpDisplay(hudObject)
            {
                youDiedText = youDiedTextObject,
                youWonText = youWonTextObject,
                VentText = poisonCountTextObject,
                ventsPoisonedCountToWin = 3,
                VentUI = poisonVentsUIObject,
                ventImages = new List<Texture2D>()
                {
                    Textures2D["vent1"],
                    Textures2D["vent2"],
                    Textures2D["vent3"]
                }
            });

            #endregion

            scene.AddGameObject(hudObject);

            playerComponent.GameObject.GetComponent<Player>().headUpDisplay = hudObject.GetComponent<HeadUpDisplay>();


            #region Standing

            GameObject standingObject = new GameObject();

            standingObject.AddComponent(
                new ImageRenderer(standingObject, Textures2D["standing"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            standingObject.Transform.Scale(new Vector3(0.25f, 0.25f, 0.25f));
            standingObject.Transform.Translate(new Vector3(0, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            scene.AddGameObject(standingObject);


            #region Crouching

            GameObject crouchingObject = new GameObject();

            crouchingObject.AddComponent(
                new ImageRenderer(crouchingObject, Textures2D["crouching"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            crouchingObject.Transform.Scale(new Vector3(0.25f, 0.25f, 0.25f));
            crouchingObject.Transform.Translate(new Vector3(0, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            scene.AddGameObject(crouchingObject);


            #region Running

            GameObject runningObject = new GameObject();

            runningObject.AddComponent(
                new ImageRenderer(runningObject, Textures2D["running"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            runningObject.Transform.Scale(new Vector3(0.25f, 0.25f, 0.25f));
            runningObject.Transform.Translate(new Vector3(0, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            scene.AddGameObject(runningObject);


            #region PlayerStateIndicator

            GameObject playerStateIndicatorObject = new GameObject();

            playerStateIndicatorObject.AddComponent(
                new PlayerStateIndicator(playerStateIndicatorObject)
                {
                    crouchingImage = crouchingObject,
                    runningImage = runningObject,
                    walkingImage = standingObject,
                    PlayerState = playerController.MovementState
                }
            );

            #endregion

            scene.AddGameObject(playerStateIndicatorObject);

//            playerController.playerStateIndicator = playerStateIndicatorObject.GetComponent<PlayerStateIndicator>();


            #region Crosshair

            GameObject crosshairObject = new GameObject();

            crosshairObject.AddComponent(
                new ImageRenderer(crosshairObject, Textures2D["aimStand"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0.5f, 0.5f)
                });
            crosshairObject.Transform.Scale(new Vector3(0.005f, 0.005f, 0.005f));
            crosshairObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2.0f,
                GameManager.GraphicsDevice.Viewport.Height / 2.0f, 0));
            crosshairObject.AddComponent(
                new CrosshairTargetIndicator(crosshairObject));

            #endregion

            scene.AddGameObject(crosshairObject);

            playerController.ActiveCrosshair = crosshairObject;
            playerController.crosshairs =
                new List<Texture2D> {Textures2D["aimStand"], Textures2D["aimRun"], Textures2D["aimCrouch"]};


            #region Vent

            GameObject ventTextObject = new GameObject();

            ventTextObject.AddComponent(new TextRenderer(ventTextObject, SpriteFonts["TobagoPoster"])
            {
                IsEnabled = false,
                IsOverlay = true,
                Text = "VENT",
                Color = Color.Red,
                Pivot = new Vector2(0, 0)
            });
            ventTextObject.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));

            ventTextObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2,
                GameManager.GraphicsDevice.Viewport.Height / 2, 0));

            GameObject ventObject = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            ventObject.Transform.Translate(new Vector3(-15, 0, 5));
            ventObject.Transform.Scale(new Vector3(0.5f, 1.0f, 0.5f));

            ventObject.Transform.Rotate(new Vector3(0.0f, 90, 0.0f));
            ventObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, ventObject, Models["Vent"]));
            ventObject.AddComponent(new SphereCollider(ventObject) {Radius = 1.0f, IsTrigger = true});
            ventObject.AddComponent(new Vent(ventObject)
            {
                ventText = ventTextObject,
                headUpDisplay = hudObject.GetComponent<HeadUpDisplay>()
            });

            #endregion

            scene.AddGameObject(ventObject);
            scene.AddGameObject(ventTextObject);


            #region WinComputer

            GameObject winComputerTextObject = new GameObject();

            winComputerTextObject.AddComponent(new TextRenderer(winComputerTextObject, SpriteFonts["TobagoPoster"])
            {
                IsEnabled = false,
                IsOverlay = true,
                Text = "WIN COMPUTER",
                Color = Color.Blue,
                Pivot = new Vector2(0, 0)
            });
            winComputerTextObject.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));

            winComputerTextObject.Transform.Translate(new Vector3((GameManager.GraphicsDevice.Viewport.Width / 2 + 2),
                (GameManager.GraphicsDevice.Viewport.Height / 2 + 2), 0));

            GameObject winComputerObject = new GameObject()
            {
                Layer = PhysicLayer.UserDefined_3
            };
            winComputerObject.Transform.Translate(new Vector3(-20, 0, 5));
            winComputerObject.Transform.Scale(new Vector3(0.5f, 1.0f, 0.5f));
            winComputerObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, winComputerObject, Models["box"]));
            winComputerObject.AddComponent(new SphereCollider(winComputerObject) {Radius = 1.0f, IsTrigger = true});
            winComputerObject.AddComponent(
                new WinComputer(winComputerObject) {headUpDisplay = hudObject.GetComponent<HeadUpDisplay>()});

            #endregion

            scene.AddGameObject(winComputerObject);
            scene.AddGameObject(winComputerTextObject);


            #region ScrewCarpet 

            GameObject carpet = new GameObject() //simulation of screws
            {
                Layer = PhysicLayer.Pullable
            };
            carpet.Transform.Translate(new Vector3(-1.0f, 2f, 0.0f));
            carpet.Transform.Scale(new Vector3(1.5f, 1.0f, 1.5f));
            carpet.AddComponent(new BoxCollider(carpet)
            {
                IsTrigger = true,
                Size = new Vector3(3.0f, 1.0f, 3.0f),
                Center = new Vector3(0, 2, 13)
            });
            carpet.AddComponent(new BoxCollider(carpet)
            {
                IsTrigger = false,
                Size = new Vector3(3.0f, 1.0f, 3.0f),
                Center = new Vector3(0, 2, 13)
            });
            carpet.AddComponent(new Rigidbody(carpet)
            {
                IsKinematic = true,
                IsAffectedByGravity = true,
                Bounciness = 1
            });
            carpet.AddComponent(new NoiseGenerator(carpet));
            carpet.AddComponent(new SoundEmitter(carpet, Sounds));
            carpet.AddComponent(new ScrewsSimulation(carpet));
            carpet.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, carpet, Models["Screw"]));
            //carpet.AddChild(CreateScrew(new Vector3(0.0f, 0.0f, 0.0f), carpet, scene));

            #endregion

            scene.AddGameObject(carpet);

            scene.AddDefaultLights();


            return scene;
        }

        private Scene FirstScene(bool isDebug)
        {
            Scene scene = new Scene(game, isDebug);

            #region Camera

            Camera mainCamera = new Camera(new Vector3(0, 180, 0))
            {
                Transform =
                {
                    LocalPosition = new Vector3(-1.5f, 1.75f, 1.5f)
                }
            };

            var cameraController = new CameraController(mainCamera);
            if (GameManager.IsDebug)
            {
                cameraController.LockY = false;
                cameraController.MoveSpeed = 10f;
                cameraController.MouseRotationSpeed = 3f;
            }

            mainCamera.AddComponent(cameraController);
            mainCamera.AddComponent(new BoxCollider(mainCamera)
            {
                Size = new Vector3(0.5f, 0.5f, 0.5f),
                Center = new Vector3(0, -0.875f, 0)
            });
            mainCamera.AddComponent(new SphereCollider(mainCamera) {Radius = 2.5f, IsTrigger = true});
            mainCamera.AddComponent(new Rigidbody(mainCamera)
            {
                IsAffectedByGravity = false,
                IsStatic = false,
                IsKinematic = true
            });

            GameObject gravityGun = new GameObject
            {
                Transform = {LocalPosition = new Vector3(-0.9f, -0.5f, -0.6f)}
            };
            gravityGun.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, gravityGun, Models["gravityGun"]));
            gravityGun.AddComponent(new GravityGun(gravityGun));
            gravityGun.Transform.Rotate(new Vector3(90, 90, 0));
            gravityGun.Transform.Scale(new Vector3(2, 2, 2));
            mainCamera.AddChild(gravityGun);

            GameObject taser = new GameObject
            {
                Transform = {LocalPosition = new Vector3(0.3f, -0.5f, -1.1f)}
            };
            taser.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, taser, Models["taser"]));

            taser.AddComponent(new Taser(taser));
            taser.Transform.Rotate(new Vector3(0, 90, 180));
            taser.Transform.Scale(new Vector3(2, 2, 2));
            mainCamera.AddChild(taser);

            GameObject lure = new GameObject
            {
                Transform = {LocalPosition = new Vector3(0.2f, -0.3f, -0.6f)}
            };
            lure.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, lure, Models["lure"]));

            lure.Transform.Scale(new Vector3(1, 1, 1));

            mainCamera.AddChild(lure);

            GameObject cameraHolder = new GameObject();

            cameraHolder.AddChild(mainCamera);

            PlayerController playerController = new PlayerController(cameraHolder);
            playerController.GameObject.AddComponent(new NoiseGenerator(playerController.GameObject));


            cameraHolder.AddComponent(playerController);

            var playerComponent = new Player(mainCamera);
            mainCamera.AddComponent(playerComponent);

            if (GameManager.IsDebug)
            {
                playerComponent.IsEnabled = false;
            }


            ///// WEAPON SELECTOR 

            #endregion

            scene.AddGameObject(cameraHolder);


            #region Rooms

            #region Room_1

            GameObject roomObject = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject, Models["room1"]));
            roomObject.GetComponent<MeshRenderer>().CastsShadow = false;
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(0, -0.1f, 0),
                Size = new Vector3(50, 0.1f, 50)
            });
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(0, 2.7f, 0),
                Size = new Vector3(50, 0.1f, 50)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //right wall
            {
                Center = new Vector3(2f, 0, 6.5f),
                Size = new Vector3(1, 10, 25)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //front wall
            {
                Center = new Vector3(0, 0, -2),
                Size = new Vector3(10, 10, 2)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //left wall
            {
                Center = new Vector3(-5, 0, -1.0f),
                Size = new Vector3(2, 10, 10)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //backleft wall
            {
                Center = new Vector3(-5, 0, 15.0f),
                Size = new Vector3(2, 10, 16)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //back wall
            {
                Center = new Vector3(-1.0f, 0, 23.0f),
                Size = new Vector3(5, 10, 2)
            });
            roomObject.AddComponent(new Rigidbody(roomObject)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });

            GameObject player = new GameObject() {Transform = {LocalPosition = new Vector3(0, 0, 0)}}; //for tests

            scene.Player = cameraHolder;

            Room firstRoom = new Room() {model = Models["room1"], player = mainCamera};
            firstRoom.AddChild(roomObject);

            #endregion

            #region Room_2

            GameObject roomObject2 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject2.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject2, Models["room2"]));
            roomObject2.GetComponent<MeshRenderer>().CastsShadow = false;
            roomObject2.AddComponent(new Rigidbody(roomObject2)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });
            scene.AddGameObject(roomObject2);

            Room secondRoom = new Room() {model = Models["room2"], player = mainCamera};
            secondRoom.AddChild(roomObject2);

            #endregion

            #endregion

            scene.AddGameObject(roomObject);

            #region Doors

            GameObject doorsParent = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(-4.5f, -1.0f, 5.5f)}
            };

            GameObject doorsObject = new GameObject
            {
                Layer = PhysicLayer.Enviroment
            };
            doorsObject.Transform.Rotate(new Vector3(0, 90, 0));
            doorsObject.Transform.LocalScale = new Vector3(2, 3, 3);
            doorsObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsObject, Models["doors"]));
            doorsObject.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject.AddComponent(new BoxCollider(doorsObject) {Size = new Vector3(0.75f, 1.75f, 0.75f)});
            doorsParent.AddComponent(new SphereCollider(doorsParent) {Radius = 0.5f, IsTrigger = true});
            doorsParent.AddComponent(new DoorsOpenClose(doorsParent));
            doorsObject.AddComponent(new Rigidbody(doorsObject)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false
            });

            #region AnimationClips

            AnimationClip doorsOpenClip
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 3f),
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            AnimationClip doorsCloseClip
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 3f),
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        //new AnimationFrame
                        //{
                        //    Transform = new Transform(doorsObject)
                        //    {
                        //        LocalScale = doorsObject.Transform.LocalScale,
                        //        LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 0.2f),
                        //        LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                        //    },
                        //    Time = TimeSpan.FromSeconds(0.5)
                        //},
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            Animator doorsAnimator = new Animator(doorsObject) {IsLooped = false};
            doorsAnimator.AnimationClips.Add("Open", doorsOpenClip);
            doorsAnimator.AnimationClips.Add("Close", doorsCloseClip);
            doorsObject.AddComponent(doorsAnimator);

            doorsParent.AddChild(doorsObject);

            #endregion

            #region DoorsText

            GameObject doorsTextObject = new GameObject();

            doorsTextObject.AddComponent(
                new TextRenderer(doorsTextObject, SpriteFonts["TobagoPoster"])
                {
                    IsOverlay = false,
                    Text = "Doors",
                    Color = Color.Red,
                    Pivot = new Vector2(0.5f, 1)
                });
            doorsTextObject.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            doorsTextObject.Transform.Translate(new Vector3(-4, 2, 5.75f));
            doorsTextObject.Transform.Rotate(new Vector3(0, 90, 0));

            #endregion

            #endregion


            scene.AddGameObject(doorsParent);
            scene.AddGameObject(doorsTextObject);


            //firstRoom.AddChild(doorsObject);
            firstRoom.AddDoorsAndRoom(doorsParent, secondRoom);
            secondRoom.AddDoorsAndRoom(doorsParent, firstRoom);

            scene.AddRoom(firstRoom);
            scene.AddRoom(secondRoom);


            #region karaczan - Off

            SkinningData skinningData = Models["karaczan"].Tag as SkinningData;
            GameObject pandaObject = new GameObject
            {
                Transform =
                {
                    LocalScale = new Vector3(0.02f, 0.02f, 0.02f),
                    LocalPosition = new Vector3(0, 0.3f, -5)
                }
            };
            pandaObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, pandaObject, Models["karaczan"]));

            AnimationClip pandaRotateClip
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(pandaObject)
                            {
                                LocalScale = pandaObject.Transform.LocalScale,
                                LocalPosition = pandaObject.Transform.LocalPosition,
                                LocalEulerAngles = pandaObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(pandaObject)
                            {
                                LocalScale = pandaObject.Transform.LocalScale,
                                LocalPosition = pandaObject.Transform.LocalPosition,
                                LocalEulerAngles = new Vector3(0, 180, 0)
                            },
                            Time = TimeSpan.FromSeconds(2.5)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(pandaObject)
                            {
                                LocalScale = pandaObject.Transform.LocalScale,
                                LocalPosition = pandaObject.Transform.LocalPosition,
                                LocalEulerAngles = new Vector3(0, 360, 0)
                            },
                            Time = TimeSpan.FromSeconds(5)
                        }
                    },
                };

            Animator pandaAnimator = new Animator(pandaObject) {IsLooped = true};
            pandaAnimator.AnimationClips.Add("Rotate", pandaRotateClip);
            pandaAnimator.Play("Rotate");
            pandaObject.AddComponent(pandaAnimator);

            SkinnedAnimator skinnedPandaAnimator = new SkinnedAnimator(pandaObject) {IsLooped = true};
            skinnedPandaAnimator.InitializeTransforms(skinningData);
            skinnedPandaAnimator.Play("Take 001");
            pandaObject.AddComponent(skinnedPandaAnimator);

            #endregion

            scene.AddGameObject(pandaObject);


            #region YouDiedText

            GameObject youDiedTextObject = new GameObject();

            youDiedTextObject.AddComponent(new TextRenderer(youDiedTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Pivot = new Vector2(0, 1)
            });
            youDiedTextObject.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));
            youDiedTextObject.Transform.Translate(new Vector3(0, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            scene.AddGameObject(youDiedTextObject);


            #region PoisonCountText

            GameObject poisonCountTextObject = new GameObject();

            poisonCountTextObject.AddComponent(new TextRenderer(poisonCountTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Pivot = new Vector2(0, 1)
            });
            poisonCountTextObject.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));
            poisonCountTextObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 200, 30,
                0));

            #endregion

            scene.AddGameObject(poisonCountTextObject);

            #region YouWonText

            GameObject youWonTextObject = new GameObject();

            youWonTextObject.AddComponent(new TextRenderer(youWonTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Pivot = new Vector2(0, 1)
            });
            youWonTextObject.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));
            youWonTextObject.Transform.Translate(new Vector3(0, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            scene.AddGameObject(youWonTextObject);

            #region HUD

            GameObject hudObject = new GameObject();

            hudObject.AddComponent(new HeadUpDisplay(hudObject)
            {
                youDiedText = youDiedTextObject,
                youWonText = youWonTextObject,
                VentText = poisonCountTextObject
            });

            #endregion

            scene.AddGameObject(hudObject);

            playerComponent.GameObject.AddComponent(hudObject.GetComponent<HeadUpDisplay>());


            #region Standing

            GameObject standingObject = new GameObject();

            standingObject.AddComponent(
                new ImageRenderer(standingObject, Textures2D["standing"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            standingObject.Transform.Scale(new Vector3(0.25f, 0.25f, 0.25f));
            standingObject.Transform.Translate(new Vector3(0, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            scene.AddGameObject(standingObject);

            #region StandingText

            GameObject standingTextObject = new GameObject();

            standingTextObject.AddComponent(new TextRenderer(standingTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Text = "Standing",
                Pivot = new Vector2(0, 1)
            });
            standingTextObject.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));
            standingTextObject.Transform.Translate(new Vector3(Textures2D["standing"].Width * 0.25f, 0, 0));
            standingTextObject.Transform.Translate(new Vector3(0, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            //scene.AddGameObject(standingTextObject);


            #region Crouching

            GameObject crouchingObject = new GameObject();

            crouchingObject.AddComponent(
                new ImageRenderer(crouchingObject, Textures2D["crouching"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            crouchingObject.Transform.Scale(new Vector3(0.25f, 0.25f, 0.25f));
            crouchingObject.Transform.Translate(new Vector3(0, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            scene.AddGameObject(crouchingObject);


            #region Running

            GameObject runningObject = new GameObject();

            runningObject.AddComponent(
                new ImageRenderer(runningObject, Textures2D["running"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            runningObject.Transform.Scale(new Vector3(0.25f, 0.25f, 0.25f));
            runningObject.Transform.Translate(new Vector3(0, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            scene.AddGameObject(runningObject);


            #region PlayerStateIndicator

            GameObject playerStateIndicatorObject = new GameObject();

            playerStateIndicatorObject.AddComponent(
                new PlayerStateIndicator(playerStateIndicatorObject)
                {
                    crouchingImage = crouchingObject,
                    runningImage = runningObject,
                    walkingImage = standingObject,
                    PlayerState = playerController.MovementState
                }
            );

            #endregion

            scene.AddGameObject(playerStateIndicatorObject);

            // playerController.playerStateIndicator = playerStateIndicatorObject.GetComponent<PlayerStateIndicator>();


            #region Crosshair

            GameObject crosshairObject = new GameObject();

            crosshairObject.AddComponent(
                new ImageRenderer(crosshairObject, Textures2D["crosshair"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0.5f, 0.5f)
                });
            crosshairObject.Transform.Scale(new Vector3(0.005f, 0.005f, 0.005f));
            crosshairObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2.0f,
                GameManager.GraphicsDevice.Viewport.Height / 2.0f, 0));
            crosshairObject.AddComponent(
                new CrosshairTargetIndicator(crosshairObject));

            #endregion

            scene.AddGameObject(crosshairObject);

            #region Box

            GameObject boxObject = new GameObject();
            boxObject.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, boxObject, Models["box"]));
            boxObject.AddComponent(new BoxCollider(boxObject) {Center = Vector3.Up * 0.25f});
            boxObject.AddComponent(new Rigidbody(boxObject)
            {
                IsAffectedByGravity = true,
                IsStatic = false,
                IsKinematic = false,
                Velocity = Vector3.Left * 0.5f,
                Bounciness = 1.1f
            });
            boxObject.Transform.Translate(new Vector3(0f, 1.5f, 15f));

            #endregion

            scene.AddGameObject(boxObject);

            #region Vent

            GameObject ventTextObject = new GameObject();

            ventTextObject.AddComponent(new TextRenderer(ventTextObject, SpriteFonts["TobagoPoster"])
            {
                IsEnabled = false,
                IsOverlay = true,
                Text = "VENT",
                Color = Color.Red,
                Pivot = new Vector2(0, 0)
            });
            ventTextObject.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));
            ;
            ventTextObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2,
                GameManager.GraphicsDevice.Viewport.Height / 2, 0));

            GameObject ventObject = new GameObject();
            ventObject.Transform.Translate(new Vector3(-10, 0, 5));
            ventObject.Transform.Scale(new Vector3(0.5f, 1.0f, 0.5f));
            ventObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, ventObject, Models["box"]));
            ventObject.AddComponent(new SphereCollider(ventObject) {Radius = 1.0f, IsTrigger = true});
            ventObject.AddComponent(new Vent(ventObject) {ventText = ventTextObject});

            #endregion

            scene.AddGameObject(ventObject);
            scene.AddGameObject(ventTextObject);


            var triangles = NavMeshModels["nav_room1"].GetTriangles().ToArray();

            PremadeNavMesh navMeshTess = PremadeNavMesh.FromTriangles(triangles);

            try
            {
                //PremadeNavMesh.LoadBakedObj("nav_room1.obj", navMeshTess);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                //PremadeNavMesh.BakeToObj("nav_room1.obj", navMeshTess);
            }

            #region Enemy

            GameObject enemyObject = new GameObject
            {
                Transform =
                {
                    LocalPosition = new Vector3(5.5f, 0, 20),
                    LocalScale = new Vector3(0.02f, 0.02f, 0.02f)
                },
                Layer = PhysicLayer.Enemy
            };
            enemyObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, enemyObject, Models["karaczan"]));
            enemyObject.AddComponent(new SphereCollider(enemyObject)
            {
                Center = new Vector3(0, 75f, 0),
                Radius = 37.5f,
                IsTrigger = true
            });
            enemyObject.AddComponent(new NavAgent(enemyObject)
            {
                PathCalculator = navMeshTess,
                Speed = 1.0f
            });
            enemyObject.AddComponent(new EnemyMovement(enemyObject)
            {
                WanderRange = 6.5f
            });
            enemyObject.AddComponent(new Enemy(enemyObject)
            {
                PlayerComponent = playerComponent
            });

            var enemyAnimator = new SkinnedAnimator(enemyObject) {IsLooped = true};
            enemyAnimator.InitializeTransforms(skinningData);
            enemyAnimator.Play("Take 001");
            enemyObject.AddComponent(enemyAnimator);

            #endregion

            //scene.AddGameObject(enemyObject);

            #region Bestest karaczan

            GameObject toBe = new GameObject
            {
                Transform =
                {
                    LocalEulerAngles = new Vector3(0, 180, 0),
                    LocalScale = new Vector3(0.009f),
                    LocalPosition = new Vector3(-1.5f, 0.05f, 15)
                }
            };
            toBe.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, toBe, Models["2b"]));
            toBe.AddComponent(new BoxCollider(toBe, Models["2b"].CreateBoundingBox(toBe.Transform.WorldScale)));
            toBe.AddComponent(new Rigidbody(toBe)
            {
                //                Velocity = Vector3.Down,
                //                Bounciness = 1.05f,
                //                IsAffectedByGravity = true,
                IsAffectedByGravity = false,
                IsStatic = true,
                IsKinematic = false
            });

            #endregion

            scene.AddGameObject(toBe);

            scene.AddDefaultLights();
            return scene;
        }

        private Scene SecondScene(bool isDebug)
        {
            Scene scene = new Scene(game, isDebug);

            #region Camera

            Camera mainCamera = new Camera();

            CameraController cameraController = new CameraController(mainCamera);
            if (GameManager.IsDebug)
            {
                cameraController.LockY = false;
                cameraController.MoveSpeed = 10f;
                cameraController.MouseRotationSpeed = 3f;
            }

            mainCamera.AddComponent(cameraController);

            GameObject cameraHolder = new GameObject();

            cameraHolder.AddChild(mainCamera);

            #endregion

            scene.AddGameObject(cameraHolder);

            GameObject toBe = new GameObject
            {
                Transform =
                {
                    LocalScale = new Vector3(0.025f),
                    LocalPosition = new Vector3(0, -1, -1)
                }
            };
            toBe.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, toBe, Models["2b"]));

            GameObject basicLightning = new GameObject();
            basicLightning.AddComponent(
                new DirectionalLight(basicLightning) {Color = Color.Red, Direction = Vector3.Left});
            basicLightning.AddComponent(
                new DirectionalLight(basicLightning) {Color = Color.Green, Direction = Vector3.Right});
//            basicLightning.AddComponent(new DirectionalLight(basicLightning) {Color = Color.White, Direction = Vector3.Down});

            scene.AddGameObject(toBe);
            scene.AddGameObject(basicLightning);

            scene.Initialize();
            return scene;
        }

        private Scene ThirdScene(bool isDebug)
        {
            Scene scene = new Scene(game, isDebug);

            return scene;
        }

        private Scene FourthScene(bool isDebug)
        {
            Scene scene = new Scene(game, isDebug);

            Physic.SetLayersCollisionInteraction(PhysicLayer.UserDefined_4, PhysicLayer.UserDefined_3, false);
            Physic.SetLayersCollisionInteraction(PhysicLayer.Enemy, PhysicLayer.Pullable, false);
            Physic.SetLayersCollisionInteraction(PhysicLayer.Enemy, PhysicLayer.Throwable, false);
            Physic.SetLayersCollisionInteraction(PhysicLayer.Enemy, PhysicLayer.Enviroment, false);

            #region Camera

            Camera mainCamera = new Camera(new Vector3(0, 180, 0))
            {
                Transform =
                {
                    LocalPosition = new Vector3(-1.5f, 1.75f, 1.5f)
                },
                Layer = PhysicLayer.Player
            };

            var cameraController = new CameraController(mainCamera);

            if (GameManager.IsDebug)
            {
                cameraController.LockY = false;
                cameraController.MoveSpeed = 10f;
                cameraController.MouseRotationSpeed = 3f;
            }

            mainCamera.AddComponent(new PlayerAccessTracker(mainCamera)); //DO NOT REMOVE :)

            mainCamera.AddComponent(new SoundListener(mainCamera));

            mainCamera.AddComponent(cameraController);
            mainCamera.AddComponent(new BoxCollider(mainCamera)
            {
                Size = new Vector3(0.5f, 0.5f, 0.5f),
                Center = new Vector3(0, -0.875f, 0)
            });
            mainCamera.AddComponent(new SphereCollider(mainCamera) {Radius = 2.5f, IsTrigger = true});
            mainCamera.AddComponent(new Rigidbody(mainCamera)
            {
                IsAffectedByGravity = false,
                IsStatic = false,
                IsKinematic = true
            });

            GameObject gravityGun = new GameObject
            {
                Transform = {LocalPosition = new Vector3(-0.9f, -0.5f, -0.6f)},
            };
            gravityGun.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, gravityGun, Models["gravityGun"]));
            gravityGun.AddComponent(new GravityGun(gravityGun){speedOfThrow = 5, model = Models["Screw"], scene = scene, sounds = Sounds});
            gravityGun.Transform.Rotate(new Vector3(90, 90, 0));
            gravityGun.Transform.Scale(new Vector3(2, 2, 2));
            mainCamera.AddChild(gravityGun);


            GameObject taserLoadingBar = new GameObject();

            taserLoadingBar.AddComponent(
                new ImageRenderer(taserLoadingBar, Textures2D["bar"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1),
                    DrawOrder = 5
                });
            taserLoadingBar.Transform.Scale(new Vector3(0.024f, 0.005f, 0.01f));
            taserLoadingBar.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 300,
                GameManager.GraphicsDevice.Viewport.Height - 80, 0));
          


            scene.AddGameObject(taserLoadingBar);


            GameObject taser = new GameObject
            {
                Transform = {LocalPosition = new Vector3(0.3f, -0.5f, -1.1f)}
            };
            taser.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, taser, Models["taser"]));

            taser.AddComponent(new Taser(taser) {LoadingBar = taserLoadingBar});
            taser.AddComponent(new AudioSource(taser, Sounds));
            taser.Transform.Rotate(new Vector3(0, 90, 180));
            taser.Transform.Scale(new Vector3(2, 2, 2));
            mainCamera.AddChild(taser);

            #region Throwable lure

            GameObject throwableLure = new GameObject
            {
                Transform = {LocalPosition = new Vector3(0.0f, 0.0f, 0.0f)}
            };
            throwableLure.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, throwableLure, Models["lure"]));
            //            throwableLure.AddComponent(new SphereCollider(throwableLure){Radius = 1.0f});
            //            throwableLure.AddComponent(new Rigidbody(throwableLure)
            //            {
            //                IsStatic = false,
            //                IsAffectedByGravity = true,
            //                IsKinematic = true
            //            });

            #endregion


            GameObject lureCounterText = new GameObject();

            lureCounterText.AddComponent(new TextRenderer(lureCounterText, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Pivot = new Vector2(0, 1)
            });
            lureCounterText.Transform.Scale(new Vector3(0.0765f, 0.0765f, 0.0765f));
            lureCounterText.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 115,
                GameManager.GraphicsDevice.Viewport.Height - 60, 0));

            scene.AddGameObject(lureCounterText);


            GameObject lure = new GameObject
            {
                Transform = {LocalPosition = new Vector3(0.2f, -0.3f, -0.6f)}
            };
            lure.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, lure, Models["lure"]));
            lure.AddComponent(new Lure(lure)
            {
                lurePrefab = throwableLure,
                scene = scene,
                model = Models["lure"],
                sounds = Sounds,
                textCounter = lureCounterText
            });

            lure.Transform.Scale(new Vector3(1, 1, 1));

            mainCamera.AddChild(lure);

            GameObject cameraHolder = new GameObject();

            cameraHolder.AddChild(mainCamera);

            PlayerController playerController = new PlayerController(cameraHolder);
            playerController.GameObject.AddComponent(new NoiseGenerator(playerController.GameObject));
            playerController.GameObject.AddComponent(new AudioSource(playerController.GameObject, Sounds));


            cameraHolder.AddComponent(playerController);
            cameraHolder.AddComponent(new NoiseGenerator(cameraHolder));
            if (GameManager.IsDebug)
            {
                playerController.IsEnabled = false;
            }

            var playerComponent = new Player(mainCamera) {room = new Room()};
            mainCamera.AddComponent(playerComponent);

            //weapon selector UI 
            
            //selected weapon UI
            GameObject selectedWeaponUI = new GameObject();
            selectedWeaponUI.AddComponent(
                new ImageRenderer(selectedWeaponUI, Textures2D["background"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0.5f, 0.5f)
                });
            selectedWeaponUI.Transform.Scale(new Vector3(0.035f, 0.035f, 0.035f));
            selectedWeaponUI.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 110,
                GameManager.GraphicsDevice.Viewport.Height - 40, 0));
            scene.AddGameObject(selectedWeaponUI);

            //gravity gun
            GameObject gravityGunUI = new GameObject();
            gravityGunUI.AddComponent(
                new ImageRenderer(gravityGunUI, Textures2D["gravityGunUI"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0.5f, 0.5f),
                    Color = new Color(70, 70, 70)
                });
            gravityGunUI.Transform.Scale(new Vector3(0.1f, 0.1f, 0.025f));
            gravityGunUI.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 400,
                GameManager.GraphicsDevice.Viewport.Height - 120, 0));
            scene.AddGameObject(gravityGunUI);

            // taser
            GameObject taserUI = new GameObject();
            taserUI.AddComponent(
                new ImageRenderer(taserUI, Textures2D["taserUI"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0.5f, 0.5f)
                });
            taserUI.Transform.Scale(new Vector3(0.1f, 0.1f, 0.025f));
            taserUI.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 250,
                GameManager.GraphicsDevice.Viewport.Height - 120, 0));
            scene.AddGameObject(taserUI);

            // lure 
            GameObject lureUI = new GameObject();
            lureUI.AddComponent(
                new ImageRenderer(lureUI, Textures2D["lureUI"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0.5f, 0.5f),
                    Color = new Color(70, 70, 70)
                });
            lureUI.Transform.Scale(new Vector3(0.1f, 0.1f, 0.025f));
            lureUI.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 100,
                GameManager.GraphicsDevice.Viewport.Height - 120, 0));

            scene.AddGameObject(lureUI);

            var weaponSelectorComponent = new WeaponSelector(cameraHolder)
            {
                GravityGun = new WeaponOption() {Weapon = gravityGun, WeaponUI = gravityGunUI},
                Taser = new WeaponOption() {Weapon = taser, WeaponUI = taserUI},
                LureThrower = new WeaponOption() {Weapon = lure, WeaponUI = lureUI},
                WeaponSelectorBackground = selectedWeaponUI,
            };
            cameraHolder.AddComponent(weaponSelectorComponent);

            #endregion

            scene.AddGameObject(cameraHolder);

            #region Rooms

            #region Room_0

            GameObject roomObject0 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject0.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject0, Models["room0"]));
            roomObject0.GetComponent<MeshRenderer>().CastsShadow = false;
            roomObject0.AddComponent(new BoxCollider(roomObject0) //front wall
            {
                Center = new Vector3(-2, 0, -2),
                Size = new Vector3(8, 10, 2)
            });
            roomObject0.AddComponent(new BoxCollider(roomObject0)
            {
                Center = new Vector3(10, 0, -2),
                Size = new Vector3(10, 10, 2)
            });
            roomObject0.AddComponent(new BoxCollider(roomObject0)
            {
                Center = new Vector3(14, 0, -8.0f),
                Size = new Vector3(2, 10, 10)
            });
            roomObject0.AddComponent(new BoxCollider(roomObject0)
            {
                Center = new Vector3(-2, 0, -8.0f),
                Size = new Vector3(2, 10, 10)
            });
            roomObject0.AddComponent(new BoxCollider(roomObject0)
            {
                Center = new Vector3(5, 0, -12),
                Size = new Vector3(25, 10, 2)
            });
            roomObject0.AddComponent(new BoxCollider(roomObject0)
            {
                Center = new Vector3(0, -0.1f, 0),
                Size = new Vector3(50, 0.1f, 50)
            });
            roomObject0.AddComponent(new Rigidbody(roomObject0)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });

            Room zeroRoom = new Room() {model = Models["room0"], player = mainCamera};
            zeroRoom.AddChild(roomObject0);


            scene.AddGameObject(roomObject0);

            #endregion

            #region Room_1

            GameObject roomObject = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject, Models["room1"]));
            roomObject.GetComponent<MeshRenderer>().CastsShadow = false;
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(0, -0.1f, 0),
                Size = new Vector3(50, 0.1f, 50)
            });
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(0, 2.7f, 0),
                Size = new Vector3(50, 0.1f, 50)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //right wall
            {
                Center = new Vector3(2f, 0, 12.0f),
                Size = new Vector3(1, 10, 15.5f)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //front wall
            {
                Center = new Vector3(-2, 0, -2),
                Size = new Vector3(8, 10, 2)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //left wall
            {
                Center = new Vector3(-5, 0, -1.0f),
                Size = new Vector3(2, 10, 10)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //backleft wall
            {
                Center = new Vector3(-5, 0, 15.0f),
                Size = new Vector3(2, 10, 16)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //back wall
            {
                Center = new Vector3(-1.0f, 0, 23.0f),
                Size = new Vector3(5, 10, 2)
            });
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(7, 0, 5),
                Size = new Vector3(10, 10, 2)
            });
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(8, 0, 3.0f),
                Size = new Vector3(2, 10, 8)
            });
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(8, 0, -2),
                Size = new Vector3(8, 10, 2)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //right wall
            {
                Center = new Vector3(2f, 0, 27.0f),
                Size = new Vector3(1, 10, 9.5f)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //backleft wall
            {
                Center = new Vector3(11, 0, 22.0f),
                Size = new Vector3(2, 10, 16)
            });
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(7, 0, 13),
                Size = new Vector3(10, 10, 2)
            });
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(12.5f, 0, 29),
                Size = new Vector3(10, 10, 2)
            });
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(3.75f, 0, 29),
                Size = new Vector3(3, 10, 2)
            });
            roomObject.AddComponent(new Rigidbody(roomObject)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });

            PremadeNavMesh room1NavMesh = PremadeNavMesh.FromTriangles(NavMeshModels["room1"].GetTriangles().ToArray());

            GameObject player = new GameObject() {Transform = {LocalPosition = new Vector3(0, 0, 0)}}; //for tests

            scene.Player = cameraHolder;

            Room firstRoom = new Room() {model = Models["room1"], player = mainCamera, name = "first"};
            firstRoom.AddChild(roomObject);

            #region Enemy

            #region karaczan - Off

            SkinningData skinningData = Models["animations"].Tag as SkinningData;
            GameObject pandaObject = new GameObject
            {
                Transform =
                {
                    LocalScale = new Vector3(0.02f, 0.02f, 0.02f),
                    LocalPosition = new Vector3(0, 0.3f, -5)
                }
            };
            pandaObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, pandaObject, Models["animations"]));

            AnimationClip pandaRotateClip
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(pandaObject)
                            {
                                LocalScale = pandaObject.Transform.LocalScale,
                                LocalPosition = pandaObject.Transform.LocalPosition,
                                LocalEulerAngles = pandaObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(pandaObject)
                            {
                                LocalScale = pandaObject.Transform.LocalScale,
                                LocalPosition = pandaObject.Transform.LocalPosition,
                                LocalEulerAngles = new Vector3(0, 180, 0)
                            },
                            Time = TimeSpan.FromSeconds(2.5)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(pandaObject)
                            {
                                LocalScale = pandaObject.Transform.LocalScale,
                                LocalPosition = pandaObject.Transform.LocalPosition,
                                LocalEulerAngles = new Vector3(0, 360, 0)
                            },
                            Time = TimeSpan.FromSeconds(5)
                        }
                    },
                };

            Animator pandaAnimator = new Animator(pandaObject) {IsLooped = true};
            pandaAnimator.AnimationClips.Add("Rotate", pandaRotateClip);
            pandaAnimator.Play("Rotate");
            pandaObject.AddComponent(pandaAnimator);

            SkinnedAnimator skinnedPandaAnimator = new SkinnedAnimator(pandaObject) {IsLooped = true};
            skinnedPandaAnimator.InitializeTransforms(skinningData);
            pandaObject.AddComponent(skinnedPandaAnimator);

            #endregion

            //scene.AddGameObject(pandaObject);

            SkinningData skinningData2 = Models["karaczan"].Tag as SkinningData;

            GameObject enemyObject = new GameObject
            {
                Transform =
                {
                    LocalPosition = new Vector3(5.5f, 0, 20),
                    LocalScale = new Vector3(0.03f, 0.03f, 0.03f)
                },
                Layer = PhysicLayer.Enemy
            };
            enemyObject.AddComponent(new SoundEmitter(enemyObject, Sounds));
            enemyObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, enemyObject, Models["karaczan"]));
            enemyObject.AddComponent(new SphereCollider(enemyObject)
            {
                Center = new Vector3(0, 75f, 0),
                Radius = 37.5f,
                IsTrigger = true
            });
            enemyObject.AddComponent(new Rigidbody(enemyObject)
            {
                IsStatic = false,
                IsAffectedByGravity = false,
                IsKinematic = true,
            });
            enemyObject.AddComponent(new NavAgent(enemyObject)
            {
                PathCalculator = room1NavMesh,
                Speed = 1.0f
            });
            enemyObject.AddComponent(new EnemyMovement(enemyObject)
            {
                WanderRange = 6.5f
            });

            enemyObject.AddComponent(new EnemyHearing(enemyObject));
            enemyObject.AddComponent(new Enemy(enemyObject)
            {
                PlayerComponent = playerComponent
            });


            var enemyAnimator = new SkinnedAnimator(enemyObject) {IsLooped = true};
            enemyAnimator.InitializeTransforms(skinningData2);
            enemyAnimator.CopyAnimation(skinnedPandaAnimator);
            enemyAnimator.Play("walk");
            enemyObject.AddComponent(enemyAnimator);

            #endregion
            firstRoom.AddChild(enemyObject);

            scene.AddGameObject(enemyObject);

            #endregion

            #region Room_2

            GameObject roomObject2 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject2.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject2, Models["room2"]));
            roomObject2.GetComponent<MeshRenderer>().CastsShadow = false;
            roomObject2.AddComponent(new Rigidbody(roomObject2)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });
            roomObject2.AddComponent(new BoxCollider(roomObject2) //MIDDLE
            {
                Center = new Vector3(-17.25f, 0.0f, 3),
                Size = new Vector3(19.0f, 10.0f, 3.5f)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject2)
            {
                Center = new Vector3(-17.25f, 0.0f, -2.0f),
                Size = new Vector3(25.0f, 10.0f, 1.0f)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject2)
            {
                Center = new Vector3(-5, 0, -1.0f),
                Size = new Vector3(2, 10, 10)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject2)
            {
                Center = new Vector3(-5, 0, 15.0f),
                Size = new Vector3(2, 10, 10)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject2) // left from door
            {
                Center = new Vector3(-14.25f, 0.0f, 10.5f),
                Size = new Vector3(19.0f, 10.0f, 1.0f)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject2)
            {
                Center = new Vector3(-30, 0, 5.0f),
                Size = new Vector3(2, 10, 12)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject2) // left from door
            {
                Center = new Vector3(-29.0f, 0.0f, 10.5f),
                Size = new Vector3(6.0f, 10.0f, 1.0f)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject2)
            {
                Center = new Vector3(0, -0.1f, 0),
                Size = new Vector3(50, 0.1f, 50)
            });

            scene.AddGameObject(roomObject2);

            Room secondRoom = new Room() {model = Models["room2"], player = mainCamera};
            secondRoom.AddChild(roomObject2);

            PremadeNavMesh room2NavMesh = PremadeNavMesh.FromTriangles(NavMeshModels["room2"].GetTriangles().ToArray());
            
            #region Enemy2
            
            GameObject enemyObject2 = new GameObject
            {
                Transform =
                {
                    LocalPosition = new Vector3(-22.0f, 0f, 7.0f),
                    LocalScale = new Vector3(0.03f, 0.03f, 0.03f)
                },
                Layer = PhysicLayer.Enemy
            };
            enemyObject2.AddComponent(new SoundEmitter(enemyObject2, Sounds));
            enemyObject2.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, enemyObject2, Models["karaczan"]));
            enemyObject2.AddComponent(new SphereCollider(enemyObject2)
            {
                Center = new Vector3(0, 75f, 0),
                Radius = 37.5f,
                IsTrigger = true
            });
            enemyObject2.AddComponent(new Rigidbody(enemyObject2)
            {
                IsStatic = false,
                IsAffectedByGravity = false,
                IsKinematic = true,
            });
            enemyObject2.AddComponent(new NavAgent(enemyObject2)
            {
                PathCalculator = room2NavMesh,
                Speed = 1.0f
            });
            enemyObject2.AddComponent(new EnemyMovement(enemyObject2)
            {
                WanderRange = 6.5f
            });
            enemyObject2.AddComponent(new EnemyHearing(enemyObject2));
            enemyObject2.AddComponent(new Enemy(enemyObject2)
            {
                PlayerComponent = playerComponent
            });

            var enemyAnimator2 = new SkinnedAnimator(enemyObject2) {IsLooped = true};
            enemyAnimator2.InitializeTransforms(skinningData2);
            enemyAnimator2.CopyAnimation(skinnedPandaAnimator);
            enemyAnimator2.Play("walk");
            enemyObject2.AddComponent(enemyAnimator2);

            #endregion
            
            secondRoom.AddChild(enemyObject2);

            scene.AddGameObject(enemyObject2);

            #endregion

            #region Room 3

            GameObject roomObject3 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject3.AddComponent(new BoxCollider(roomObject3) // left from door
            {
                Center = new Vector3(-29.0f, 0.0f, 11.5f),
                Size = new Vector3(6.0f, 10.0f, 1.0f)
            });
            roomObject3.AddComponent(new BoxCollider(roomObject3) // left from door
            {
                Center = new Vector3(-14.25f, 0.0f, 11.5f),
                Size = new Vector3(19.0f, 10.0f, 1.0f)
            });
            roomObject3.AddComponent(new BoxCollider(roomObject3)
            {
                Center = new Vector3(-30, 0, 20.0f),
                Size = new Vector3(2, 10, 20)
            });
            roomObject3.AddComponent(new BoxCollider(roomObject3)
            {
                Center = new Vector3(-20, 0, 20.0f),
                Size = new Vector3(2, 10, 20)
            });
            roomObject3.AddComponent(new BoxCollider(roomObject3) // left from door
            {
                Center = new Vector3(-29.0f, 0.0f, 26.5f),
                Size = new Vector3(6.0f, 10.0f, 1.0f)
            });
            roomObject3.AddComponent(new BoxCollider(roomObject3) // left from door
            {
                Center = new Vector3(-14.25f, 0.0f, 26.5f),
                Size = new Vector3(19.0f, 10.0f, 1.0f)
            });
            roomObject3.AddComponent(new Rigidbody(roomObject3)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });
            roomObject3.AddComponent(new BoxCollider(roomObject3)
            {
                Center = new Vector3(0, -0.1f, 0),
                Size = new Vector3(50, 0.1f, 50)
            });
            roomObject3.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject3, Models["room3"]));
            roomObject3.GetComponent<MeshRenderer>().CastsShadow = false;

            scene.AddGameObject(roomObject3);

            Room thirdRoom = new Room() {model = Models["room3"], player = mainCamera};
            thirdRoom.AddChild(roomObject3);
            
            PremadeNavMesh room3NavMesh = PremadeNavMesh.FromTriangles(NavMeshModels["room3"].GetTriangles().ToArray());
            
            #region Enemy2
            
            GameObject enemyObject3 = new GameObject
            {
                Transform =
                {
                    LocalPosition = new Vector3(-24.0f, 0f, 19.0f),
                    LocalScale = new Vector3(0.03f, 0.03f, 0.03f)
                },
                Layer = PhysicLayer.Enemy
            };
            enemyObject3.AddComponent(new SoundEmitter(enemyObject3, Sounds));
            enemyObject3.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, enemyObject3, Models["karaczan"]));
            enemyObject3.AddComponent(new SphereCollider(enemyObject3)
            {
                Center = new Vector3(0, 75f, 0),
                Radius = 37.5f,
                IsTrigger = true
            });
            enemyObject3.AddComponent(new Rigidbody(enemyObject3)
            {
                IsStatic = false,
                IsAffectedByGravity = false,
                IsKinematic = true,
            });
            enemyObject3.AddComponent(new NavAgent(enemyObject3)
            {
                PathCalculator = room3NavMesh,
                Speed = 1.0f
            });
            enemyObject3.AddComponent(new EnemyMovement(enemyObject3)
            {
                WanderRange = 6.5f
            });
            enemyObject3.AddComponent(new EnemyHearing(enemyObject3));
            enemyObject3.AddComponent(new Enemy(enemyObject3)
            {
                PlayerComponent = playerComponent
            });

            var enemyAnimator3 = new SkinnedAnimator(enemyObject3) {IsLooped = true};
            enemyAnimator3.InitializeTransforms(skinningData2);
            enemyAnimator3.CopyAnimation(skinnedPandaAnimator);
            enemyAnimator3.Play("walk");
            enemyObject3.AddComponent(enemyAnimator3);

            #endregion

            thirdRoom.AddChild(enemyObject3);
            scene.AddGameObject(enemyObject3);

            #endregion

            #region Room 4

            GameObject roomObject4 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-24.75f, 0.0f, 32.75f),
                Size = new Vector3(12.0f, 10.0f, 1.0f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-24.75f, 0.0f, 36.75f),
                Size = new Vector3(10.0f, 10.0f, 1.0f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-19.25f, 0.0f, 42.25f),
                Size = new Vector3(1.0f, 10.0f, 12.0f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-30.25f, 0.0f, 42.25f),
                Size = new Vector3(1.0f, 10.0f, 12.0f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-28.0f, 0.0f, 47.75f),
                Size = new Vector3(3.5f, 10.0f, 1.0f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-21.5f, 0.0f, 47.75f),
                Size = new Vector3(3.5f, 10.0f, 1.0f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-26.5f, 0.0f, 40.5f),
                Size = new Vector3(0.5f, 10.0f, 6.5f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-23.0f, 0.0f, 40.5f),
                Size = new Vector3(0.5f, 10.0f, 6.5f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-32.5f, 0.0f, 51.25f),
                Size = new Vector3(0.5f, 10.0f, 3.0f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-16.707f, 0.0f, 51.203f), // xd
                Size = new Vector3(0.5f, 10.0f, 3.0f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-20.25f, 0.0f, 53.0f),
                Size = new Vector3(3.0f, 10.0f, 0.5f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-29.25f, 0.0f, 53.0f),
                Size = new Vector3(3.0f, 10.0f, 0.5f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-21.5f, 0.0f, 54.25f),
                Size = new Vector3(0.5f, 10.0f, 3.0f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4) // left from door
            {
                Center = new Vector3(-28.0f, 0.0f, 54.25f),
                Size = new Vector3(0.5f, 10.0f, 3.0f)
            });
            roomObject4.AddComponent(new BoxCollider(roomObject4)
            {
                Center = new Vector3(0, -0.1f, 0),
                Size = new Vector3(100, 0.1f, 100)
            });
            roomObject4.AddComponent(new Rigidbody(roomObject4)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });
            roomObject4.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject4, Models["room4"]));
            roomObject4.GetComponent<MeshRenderer>().CastsShadow = false;

            scene.AddGameObject(roomObject4);

            Room fourthRoom = new Room() {model = Models["room4"], player = mainCamera};
            fourthRoom.AddChild(roomObject4);
            
            PremadeNavMesh room4NavMesh = PremadeNavMesh.FromTriangles(NavMeshModels["room4"].GetTriangles().ToArray());
            
            #region Enemy4
            
            GameObject enemyObject4 = new GameObject
            {
                Transform =
                {
                    LocalPosition = new Vector3(-17.0f, 0f, 41.0f),
                    LocalScale = new Vector3(0.03f, 0.03f, 0.03f)
                },
                Layer = PhysicLayer.Enemy
            };
            enemyObject4.AddComponent(new SoundEmitter(enemyObject4, Sounds));
            enemyObject4.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, enemyObject4, Models["karaczan"]));
            enemyObject4.AddComponent(new SphereCollider(enemyObject4)
            {
                Center = new Vector3(0, 75f, 0),
                Radius = 37.5f,
                IsTrigger = true
            });
            enemyObject4.AddComponent(new Rigidbody(enemyObject4)
            {
                IsStatic = false,
                IsAffectedByGravity = false,
                IsKinematic = true,
            });
            enemyObject4.AddComponent(new NavAgent(enemyObject4)
            {
                PathCalculator = room4NavMesh,
                Speed = 1.0f
            });
            enemyObject4.AddComponent(new EnemyMovement(enemyObject4)
            {
                WanderRange = 6.5f
            });
            enemyObject4.AddComponent(new EnemyHearing(enemyObject4));
            enemyObject4.AddComponent(new Enemy(enemyObject4)
            {
                PlayerComponent = playerComponent
            });

            var enemyAnimator4 = new SkinnedAnimator(enemyObject4) {IsLooped = true};
            enemyAnimator4.InitializeTransforms(skinningData2);
            enemyAnimator4.CopyAnimation(skinnedPandaAnimator);
            enemyAnimator4.Play("walk");
            enemyObject4.AddComponent(enemyAnimator4);

            #endregion
            
            #region Enemy4,5
            
            GameObject enemyObject42 = new GameObject
            {
                Transform =
                {
                    LocalPosition = new Vector3(-32.0f, 0f, 41.0f),
                    LocalScale = new Vector3(0.03f, 0.03f, 0.03f)
                },
                Layer = PhysicLayer.Enemy
            };
            enemyObject42.AddComponent(new SoundEmitter(enemyObject42, Sounds));
            enemyObject42.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, enemyObject42, Models["karaczan"]));
            enemyObject42.AddComponent(new SphereCollider(enemyObject42)
            {
                Center = new Vector3(0, 75f, 0),
                Radius = 37.5f,
                IsTrigger = true
            });
            enemyObject42.AddComponent(new Rigidbody(enemyObject42)
            {
                IsStatic = false,
                IsAffectedByGravity = false,
                IsKinematic = true,
            });
            enemyObject42.AddComponent(new NavAgent(enemyObject42)
            {
                PathCalculator = room4NavMesh,
                Speed = 1.0f
            });
            enemyObject42.AddComponent(new EnemyMovement(enemyObject42)
            {
                WanderRange = 6.5f
            });
            enemyObject42.AddComponent(new EnemyHearing(enemyObject42));
            enemyObject42.AddComponent(new Enemy(enemyObject42)
            {
                PlayerComponent = playerComponent
            });

            var enemyAnimator42 = new SkinnedAnimator(enemyObject42) {IsLooped = true};
            enemyAnimator42.InitializeTransforms(skinningData2);
            enemyAnimator42.CopyAnimation(skinnedPandaAnimator);
            enemyAnimator42.Play("walk");
            enemyObject42.AddComponent(enemyAnimator42);

            #endregion

            fourthRoom.AddChild(enemyObject4);
            fourthRoom.AddChild(enemyObject42);
            scene.AddGameObject(enemyObject4);
            scene.AddGameObject(enemyObject42);

            #endregion

            #region Room 5

            GameObject roomObject5 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject5.AddComponent(new BoxCollider(roomObject5) // left from door
            {
                Center = new Vector3(-2.886f, 0.0f, 62.895f), //xd
                Size = new Vector3(9.0f, 10.0f, 1.0f)
            });
            roomObject5.AddComponent(new BoxCollider(roomObject5) // left from door
            {
                Center = new Vector3(0.407f, 0.0f, 73.75f),
                Size = new Vector3(1.0f, 10.0f, 7.0f)
            });
            roomObject5.AddComponent(new BoxCollider(roomObject5) // left from door
            {
                Center = new Vector3(4.2f, 0.0f, 62.25f),
                Size = new Vector3(1.0f, 10.0f, 6.0f)
            });
            roomObject5.AddComponent(new BoxCollider(roomObject5) // left from door
            {
                Center = new Vector3(6.2f, 0.0f, 64.75f), //xd
                Size = new Vector3(3.0f, 10.0f, 1.0f)
            });
            roomObject5.AddComponent(new BoxCollider(roomObject5)
            {
                Center = new Vector3(0, -0.1f, 0),
                Size = new Vector3(50, 0.1f, 50)
            });
            roomObject5.AddComponent(new Rigidbody(roomObject5)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });
            roomObject5.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject5, Models["room5"]));
            roomObject5.GetComponent<MeshRenderer>().CastsShadow = false;

            scene.AddGameObject(roomObject5);

            Room fifthRoom = new Room() {model = Models["room5"], player = mainCamera};
            fifthRoom.AddChild(roomObject5);
            
            PremadeNavMesh room5NavMesh = PremadeNavMesh.FromTriangles(NavMeshModels["room5"].GetTriangles().ToArray());
            
            #region Enemy5
            
            GameObject enemyObject5 = new GameObject
            {
                Transform =
                {
                    LocalPosition = new Vector3(6.0f, 0f, 71.0f),
                    LocalScale = new Vector3(0.03f, 0.03f, 0.03f)
                },
                Layer = PhysicLayer.Enemy
            };
            enemyObject5.AddComponent(new SoundEmitter(enemyObject5, Sounds));
            enemyObject5.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, enemyObject5, Models["karaczan"]));
            enemyObject5.AddComponent(new SphereCollider(enemyObject5)
            {
                Center = new Vector3(0, 75f, 0),
                Radius = 37.5f,
                IsTrigger = true
            });
            enemyObject5.AddComponent(new Rigidbody(enemyObject5)
            {
                IsStatic = false,
                IsAffectedByGravity = false,
                IsKinematic = true,
            });
            enemyObject5.AddComponent(new NavAgent(enemyObject5)
            {
                PathCalculator = room5NavMesh,
                Speed = 1.0f
            });
            enemyObject5.AddComponent(new EnemyMovement(enemyObject5)
            {
                WanderRange = 6.5f
            });
            enemyObject5.AddComponent(new EnemyHearing(enemyObject5));
            enemyObject5.AddComponent(new Enemy(enemyObject5)
            {
                PlayerComponent = playerComponent
            });

            var enemyAnimator5 = new SkinnedAnimator(enemyObject5) {IsLooped = true};
            enemyAnimator5.InitializeTransforms(skinningData2);
            enemyAnimator5.CopyAnimation(skinnedPandaAnimator);
            enemyAnimator5.Play("walk");
            enemyObject5.AddComponent(enemyAnimator5);

            #endregion

            fifthRoom.AddChild(enemyObject5);
            scene.AddGameObject(enemyObject5);

            #endregion

            #region Room 6

            GameObject roomObject6 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject6.AddComponent(new Rigidbody(roomObject6)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });
            roomObject6.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject6, Models["room6"]));
            roomObject6.GetComponent<MeshRenderer>().CastsShadow = false;

            scene.AddGameObject(roomObject6);

            Room sixthRoom = new Room() {model = Models["room6"], player = mainCamera, name = "sixth"};
            sixthRoom.AddChild(roomObject6);

            #endregion

            #endregion

            scene.AddGameObject(roomObject);


            #region Doors

            GameObject doorsParent = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                //Transform = { LocalPosition = new Vector3(-4.5f, -1.0f, 5.5f) }
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };

            GameObject doorsObject = new GameObject
            {
                Layer = PhysicLayer.Enviroment
            };
            doorsObject.Transform.Translate(new Vector3(-2.75f, 0.0f, 5.75f));
            doorsObject.Transform.Rotate(new Vector3(0, 90, 0));
            doorsObject.Transform.LocalScale = new Vector3(1, 1, 1);
            doorsObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsObject, Models["doors"]));
            doorsObject.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject.AddComponent(new BoxCollider(doorsObject)
            {
                Center = new Vector3(-2f, 0.0f, -0.0f),
                Size = new Vector3(1.0f, 10f, 2.5f),
                IsTrigger = false
            });
            doorsParent.AddComponent(new SphereCollider(doorsParent)
            {
                Center = new Vector3(-4.5f, -1.0f, 5.5f),
                Radius = 2.5f,
                IsTrigger = true
            });
            doorsParent.AddComponent(new SoundEmitter(doorsParent, Sounds));
            doorsParent.AddComponent(new DoorsOpenClose(doorsParent) {accessLevelRequired = AccessLevel.Medium});
            doorsParent.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsParent, Models["jamb1"]));
            doorsParent.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject.AddComponent(new Rigidbody(doorsObject)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false
            });

            #region AnimationClips

            AnimationClip doorsOpenClip
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 3f),
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            AnimationClip doorsCloseClip
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 3f),
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        //new AnimationFrame
                        //{
                        //    Transform = new Transform(doorsObject)
                        //    {
                        //        LocalScale = doorsObject.Transform.LocalScale,
                        //        LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 0.2f),
                        //        LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                        //    },
                        //    Time = TimeSpan.FromSeconds(0.5)
                        //},
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            Animator doorsAnimator = new Animator(doorsObject) {IsLooped = false};
            doorsAnimator.AnimationClips.Add("Open", doorsOpenClip);
            doorsAnimator.AnimationClips.Add("Close", doorsCloseClip);
            doorsObject.AddComponent(doorsAnimator);

            doorsParent.AddChild(doorsObject);

            #endregion

            #region DoorsText

            GameObject doorsTextObject = new GameObject();

            doorsTextObject.AddComponent(
                new TextRenderer(doorsTextObject, SpriteFonts["TobagoPoster"])
                {
                    IsOverlay = false,
                    Text = "Doors",
                    Color = Color.Red,
                    Pivot = new Vector2(0.5f, 1)
                });
            doorsTextObject.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            doorsTextObject.Transform.Translate(new Vector3(-4, 2, 5.75f));
            doorsTextObject.Transform.Rotate(new Vector3(0, 90, 0));

            #endregion

            #endregion

            scene.AddGameObject(doorsParent);
            scene.AddGameObject(doorsTextObject);

            #region Doors0

            GameObject doorsParent0 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                //Transform = { LocalPosition = new Vector3(-4.5f, -1.0f, 5.5f) }
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };

            GameObject doorsObject0 = new GameObject
            {
                Layer = PhysicLayer.Enviroment
            };
            doorsObject0.Transform.Translate(new Vector3(3.25f, 0.0f, -0.25f));
            doorsObject0.Transform.Rotate(new Vector3(0, 0, 0));
            doorsObject0.Transform.LocalScale = new Vector3(1, 1, 1);
            doorsObject0.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsObject0, Models["doors"]));
            doorsObject0.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject0.AddComponent(new BoxCollider(doorsObject0)
            {
                Center = new Vector3(0.0f, 0.0f, -2.0f),
                Size = new Vector3(2.0f, 10f, 0.75f),
                IsTrigger = false
            });
            doorsParent0.AddComponent(new SphereCollider(doorsParent0)
            {
                Center = new Vector3(3.25f, 0.0f, -2.0f),
                Radius = 2.5f,
                IsTrigger = true
            });
            doorsParent0.AddComponent(new SoundEmitter(doorsParent0, Sounds));
            doorsParent0.AddComponent(new DoorsOpenClose(doorsParent0) {accessLevelRequired = AccessLevel.Basic});
            doorsParent0.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsParent0, Models["jamb0"]));
            doorsParent0.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject0.AddComponent(new Rigidbody(doorsObject0)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false
            });

            #region AnimationClips

            AnimationClip doorsOpenClip0
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject0.Transform.LocalScale,
                                LocalPosition = doorsObject0.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject0.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject0.Transform.LocalScale,
                                LocalPosition = doorsObject0.Transform.LocalPosition + new Vector3(3f, 0, 0f),
                                LocalEulerAngles = doorsObject0.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            AnimationClip doorsCloseClip0
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject0.Transform.LocalScale,
                                LocalPosition = doorsObject0.Transform.LocalPosition + new Vector3(3f, 0, 0f),
                                LocalEulerAngles = doorsObject0.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        //new AnimationFrame
                        //{
                        //    Transform = new Transform(doorsObject)
                        //    {
                        //        LocalScale = doorsObject.Transform.LocalScale,
                        //        LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 0.2f),
                        //        LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                        //    },
                        //    Time = TimeSpan.FromSeconds(0.5)
                        //},
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject0.Transform.LocalScale,
                                LocalPosition = doorsObject0.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject0.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            Animator doorsAnimator0 = new Animator(doorsObject0) {IsLooped = false};
            doorsAnimator0.AnimationClips.Add("Open", doorsOpenClip0);
            doorsAnimator0.AnimationClips.Add("Close", doorsCloseClip0);
            doorsObject0.AddComponent(doorsAnimator0);

            doorsParent0.AddChild(doorsObject0);

            #endregion

            #region DoorsText

            GameObject doorsTextObject0 = new GameObject();

            doorsTextObject0.AddComponent(
                new TextRenderer(doorsTextObject0, SpriteFonts["TobagoPoster"])
                {
                    IsOverlay = false,
                    Text = "Doors",
                    Color = Color.Red,
                    Pivot = new Vector2(0.5f, 1)
                });
            doorsTextObject0.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            doorsTextObject0.Transform.Translate(new Vector3(-4, 2, 5.75f));
            doorsTextObject0.Transform.Rotate(new Vector3(0, 90, 0));

            #endregion

            #endregion

            scene.AddGameObject(doorsParent0);
            scene.AddGameObject(doorsTextObject0);

            #region Doors2

            GameObject doorsParent2 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                //Transform = { LocalPosition = new Vector3(-4.5f, -1.0f, 5.5f) }
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };

            GameObject doorsObject2 = new GameObject
            {
                Layer = PhysicLayer.Enviroment
            };
            doorsObject2.Transform.Translate(new Vector3(-24.75f, 0.0f, 12.75f));
            doorsObject2.Transform.Rotate(new Vector3(0, 0, 0));
            doorsObject2.Transform.LocalScale = new Vector3(1f, 1, 1);
            doorsObject2.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsObject2, Models["doors"]));
            doorsObject2.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject2.AddComponent(new BoxCollider(doorsObject2)
            {
                Center = new Vector3(0.0f, 0.0f, -2.0f),
                Size = new Vector3(2.0f, 10f, 0.75f),
                IsTrigger = false
            });
            doorsParent2.AddComponent(new SphereCollider(doorsParent2)
            {
                Center = new Vector3(-24.75f, -2.0f, 11.0f),
                Radius = 2.5f,
                IsTrigger = true
            });
            doorsParent2.AddComponent(new SoundEmitter(doorsParent2, Sounds));
            doorsParent2.AddComponent(new DoorsOpenClose(doorsParent2) {accessLevelRequired = AccessLevel.Basic});
            doorsParent2.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsParent2, Models["jamb2"]));
            doorsParent2.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject2.AddComponent(new Rigidbody(doorsObject2)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false
            });

            #region AnimationClips

            AnimationClip doorsOpenClip2
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject2)
                            {
                                LocalScale = doorsObject2.Transform.LocalScale,
                                LocalPosition = doorsObject2.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject2.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject2)
                            {
                                LocalScale = doorsObject2.Transform.LocalScale,
                                LocalPosition = doorsObject2.Transform.LocalPosition + new Vector3(3, 0, 0f),
                                LocalEulerAngles = doorsObject2.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            AnimationClip doorsCloseClip2
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject2)
                            {
                                LocalScale = doorsObject2.Transform.LocalScale,
                                LocalPosition = doorsObject2.Transform.LocalPosition + new Vector3(3, 0, 0f),
                                LocalEulerAngles = doorsObject2.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        //new AnimationFrame
                        //{
                        //    Transform = new Transform(doorsObject)
                        //    {
                        //        LocalScale = doorsObject.Transform.LocalScale,
                        //        LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 0.2f),
                        //        LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                        //    },
                        //    Time = TimeSpan.FromSeconds(0.5)
                        //},
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject2)
                            {
                                LocalScale = doorsObject2.Transform.LocalScale,
                                LocalPosition = doorsObject2.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject2.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            Animator doorsAnimator2 = new Animator(doorsObject2) {IsLooped = false};
            doorsAnimator2.AnimationClips.Add("Open", doorsOpenClip2);
            doorsAnimator2.AnimationClips.Add("Close", doorsCloseClip2);
            doorsObject2.AddComponent(doorsAnimator2);

            doorsParent2.AddChild(doorsObject2);

            #endregion

            #region DoorsText

            GameObject doorsTextObject2 = new GameObject();

            doorsTextObject2.AddComponent(
                new TextRenderer(doorsTextObject2, SpriteFonts["TobagoPoster"])
                {
                    IsOverlay = false,
                    Text = "Doors",
                    Color = Color.Red,
                    Pivot = new Vector2(0.5f, 1)
                });
            doorsTextObject2.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            doorsTextObject2.Transform.Translate(new Vector3(-4, 2, 5.75f));
            doorsTextObject2.Transform.Rotate(new Vector3(0, 90, 0));

            #endregion

            #endregion

            scene.AddGameObject(doorsParent2);
            scene.AddGameObject(doorsTextObject2);

            #region Doors3

            GameObject doorsParent3 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                //Transform = { LocalPosition = new Vector3(-4.5f, -1.0f, 5.5f) }
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };

            GameObject doorsObject3 = new GameObject
            {
                Layer = PhysicLayer.Enviroment
            };
            doorsObject3.Transform.Translate(new Vector3(-24.75f, 0.0f, 28.75f));
            doorsObject3.Transform.Rotate(new Vector3(0, 0, 0));
            doorsObject3.Transform.LocalScale = new Vector3(1f, 1, 1);
            doorsObject3.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsObject3, Models["doors"]));
            doorsObject3.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject3.AddComponent(new BoxCollider(doorsObject3)
            {
                Center = new Vector3(0.0f, 0.0f, -2.0f),
                Size = new Vector3(2.0f, 10f, 0.75f),
                IsTrigger = false
            });
            doorsParent3.AddComponent(new SphereCollider(doorsParent3)
            {
                Center = new Vector3(-24.75f, -1.0f, 26.5f),
                Radius = 2.5f,
                IsTrigger = true
            });
            doorsParent3.AddComponent(new SoundEmitter(doorsParent3, Sounds));
            doorsParent3.AddComponent(new DoorsOpenClose(doorsParent3) {accessLevelRequired = AccessLevel.Basic});
            doorsParent3.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsParent3, Models["jamb3"]));
            doorsParent3.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject3.AddComponent(new Rigidbody(doorsObject3)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false
            });

            #region AnimationClips

            AnimationClip doorsOpenClip3
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject3)
                            {
                                LocalScale = doorsObject3.Transform.LocalScale,
                                LocalPosition = doorsObject3.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject3.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject3)
                            {
                                LocalScale = doorsObject3.Transform.LocalScale,
                                LocalPosition = doorsObject3.Transform.LocalPosition + new Vector3(3f, 0, 0f),
                                LocalEulerAngles = doorsObject3.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            AnimationClip doorsCloseClip3
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject3)
                            {
                                LocalScale = doorsObject3.Transform.LocalScale,
                                LocalPosition = doorsObject3.Transform.LocalPosition + new Vector3(3f, 0, 0f),
                                LocalEulerAngles = doorsObject3.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        //new AnimationFrame
                        //{
                        //    Transform = new Transform(doorsObject)
                        //    {
                        //        LocalScale = doorsObject.Transform.LocalScale,
                        //        LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 0.2f),
                        //        LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                        //    },
                        //    Time = TimeSpan.FromSeconds(0.5)
                        //},
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject3)
                            {
                                LocalScale = doorsObject3.Transform.LocalScale,
                                LocalPosition = doorsObject3.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject3.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            Animator doorsAnimator3 = new Animator(doorsObject3) {IsLooped = false};
            doorsAnimator3.AnimationClips.Add("Open", doorsOpenClip3);
            doorsAnimator3.AnimationClips.Add("Close", doorsCloseClip3);
            doorsObject3.AddComponent(doorsAnimator3);

            doorsParent3.AddChild(doorsObject3);

            #endregion

            #region DoorsText

            GameObject doorsTextObject3 = new GameObject();

            doorsTextObject3.AddComponent(
                new TextRenderer(doorsTextObject3, SpriteFonts["TobagoPoster"])
                {
                    IsOverlay = false,
                    Text = "Doors",
                    Color = Color.Red,
                    Pivot = new Vector2(0.5f, 1)
                });
            doorsTextObject3.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            doorsTextObject3.Transform.Translate(new Vector3(-4, 2, 5.75f));
            doorsTextObject3.Transform.Rotate(new Vector3(0, 90, 0));

            #endregion

            #endregion

            scene.AddGameObject(doorsParent3);
            scene.AddGameObject(doorsTextObject3);

            #region Doors4

            GameObject doorsParent4 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                //Transform = { LocalPosition = new Vector3(-4.5f, -1.0f, 5.5f) }
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };

            GameObject doorsObject4 = new GameObject
            {
                Layer = PhysicLayer.Enviroment
            };
            doorsObject4.Transform.Translate(new Vector3(-24.75f, 0.0f, 60.25f));
            doorsObject4.Transform.Rotate(new Vector3(0, 0, 0));
            doorsObject4.Transform.LocalScale = new Vector3(1f, 1, 1);
            doorsObject4.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsObject4, Models["doors"]));
            doorsObject4.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject4.AddComponent(new BoxCollider(doorsObject4)
            {
                Center = new Vector3(0.0f, 0.0f, -2.0f),
                Size = new Vector3(2.0f, 10f, 0.75f),
                IsTrigger = false
            });
            doorsParent4.AddComponent(new SphereCollider(doorsParent4)
            {
                Center = new Vector3(-24.75f, -1.0f, 58.0f),
                Radius = 2.5f,
                IsTrigger = true
            });
            doorsParent4.AddComponent(new SoundEmitter(doorsParent4, Sounds));
            doorsParent4.AddComponent(new DoorsOpenClose(doorsParent4) {accessLevelRequired = AccessLevel.Medium});
            doorsParent4.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsParent4, Models["jamb4"]));
            doorsParent4.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject4.AddComponent(new Rigidbody(doorsObject4)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false
            });

            #region AnimationClips

            AnimationClip doorsOpenClip4
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject4)
                            {
                                LocalScale = doorsObject4.Transform.LocalScale,
                                LocalPosition = doorsObject4.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject4.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject4)
                            {
                                LocalScale = doorsObject4.Transform.LocalScale,
                                LocalPosition = doorsObject4.Transform.LocalPosition + new Vector3(3f, 0, 0f),
                                LocalEulerAngles = doorsObject4.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            AnimationClip doorsCloseClip4
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject4)
                            {
                                LocalScale = doorsObject4.Transform.LocalScale,
                                LocalPosition = doorsObject4.Transform.LocalPosition + new Vector3(3f, 0, 0f),
                                LocalEulerAngles = doorsObject4.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        //new AnimationFrame
                        //{
                        //    Transform = new Transform(doorsObject)
                        //    {
                        //        LocalScale = doorsObject.Transform.LocalScale,
                        //        LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 0.2f),
                        //        LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                        //    },
                        //    Time = TimeSpan.FromSeconds(0.5)
                        //},
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject4)
                            {
                                LocalScale = doorsObject4.Transform.LocalScale,
                                LocalPosition = doorsObject4.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject4.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            Animator doorsAnimator4 = new Animator(doorsObject4) {IsLooped = false};
            doorsAnimator4.AnimationClips.Add("Open", doorsOpenClip4);
            doorsAnimator4.AnimationClips.Add("Close", doorsCloseClip4);
            doorsObject4.AddComponent(doorsAnimator4);

            doorsParent4.AddChild(doorsObject4);

            #endregion

            #region DoorsText

            GameObject doorsTextObject4 = new GameObject();

            doorsTextObject4.AddComponent(
                new TextRenderer(doorsTextObject4, SpriteFonts["TobagoPoster"])
                {
                    IsOverlay = false,
                    Text = "Doors",
                    Color = Color.Red,
                    Pivot = new Vector2(0.5f, 1)
                });
            doorsTextObject4.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            doorsTextObject4.Transform.Translate(new Vector3(-4, 2, 5.75f));
            doorsTextObject4.Transform.Rotate(new Vector3(0, 90, 0));

            #endregion

            #endregion

            scene.AddGameObject(doorsParent4);
            scene.AddGameObject(doorsTextObject4);

            #region Doors5

            GameObject doorsParent5 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                //Transform = { LocalPosition = new Vector3(-4.5f, -1.0f, 5.5f) }
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };

            GameObject doorsObject5 = new GameObject
            {
                Layer = PhysicLayer.Enviroment
            };
            doorsObject5.Transform.Translate(new Vector3(-6.75f, 0.0f, 60.75f));
            doorsObject5.Transform.Rotate(new Vector3(0, 0, 0));
            doorsObject5.Transform.LocalScale = new Vector3(1f, 1, 1);
            doorsObject5.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsObject5, Models["doors"]));
            doorsObject5.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject5.AddComponent(new BoxCollider(doorsObject5)
            {
                Center = new Vector3(0.0f, 0.0f, -2.0f),
                Size = new Vector3(2.0f, 10f, 0.75f),
                IsTrigger = false
            });
            doorsParent5.AddComponent(new SphereCollider(doorsParent5)
            {
                Center = new Vector3(-6.75f, -1.0f, 58.5f),
                Radius = 2.5f,
                IsTrigger = true
            });
            doorsParent5.AddComponent(new SoundEmitter(doorsParent5, Sounds));
            doorsParent5.AddComponent(new DoorsOpenClose(doorsParent5) {accessLevelRequired = AccessLevel.Basic});
            doorsParent5.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsParent5, Models["jamb5"]));
            doorsParent5.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject5.AddComponent(new Rigidbody(doorsObject5)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false
            });

            #region AnimationClips

            AnimationClip doorsOpenClip5
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject5)
                            {
                                LocalScale = doorsObject5.Transform.LocalScale,
                                LocalPosition = doorsObject5.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject5.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject5)
                            {
                                LocalScale = doorsObject5.Transform.LocalScale,
                                LocalPosition = doorsObject5.Transform.LocalPosition + new Vector3(3f, 0, 0f),
                                LocalEulerAngles = doorsObject5.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            AnimationClip doorsCloseClip5
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject5)
                            {
                                LocalScale = doorsObject5.Transform.LocalScale,
                                LocalPosition = doorsObject5.Transform.LocalPosition + new Vector3(3f, 0, 0f),
                                LocalEulerAngles = doorsObject5.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        //new AnimationFrame
                        //{
                        //    Transform = new Transform(doorsObject)
                        //    {
                        //        LocalScale = doorsObject.Transform.LocalScale,
                        //        LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 0.2f),
                        //        LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                        //    },
                        //    Time = TimeSpan.FromSeconds(0.5)
                        //},
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject5)
                            {
                                LocalScale = doorsObject5.Transform.LocalScale,
                                LocalPosition = doorsObject5.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject5.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            Animator doorsAnimator5 = new Animator(doorsObject5) {IsLooped = false};
            doorsAnimator5.AnimationClips.Add("Open", doorsOpenClip5);
            doorsAnimator5.AnimationClips.Add("Close", doorsCloseClip5);
            doorsObject5.AddComponent(doorsAnimator5);

            doorsParent5.AddChild(doorsObject5);

            #endregion

            #region DoorsText

            GameObject doorsTextObject5 = new GameObject();

            doorsTextObject5.AddComponent(
                new TextRenderer(doorsTextObject5, SpriteFonts["TobagoPoster"])
                {
                    IsOverlay = false,
                    Text = "Doors",
                    Color = Color.Red,
                    Pivot = new Vector2(0.5f, 1)
                });
            doorsTextObject5.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            doorsTextObject5.Transform.Translate(new Vector3(-4, 2, 5.75f));
            doorsTextObject5.Transform.Rotate(new Vector3(0, 90, 0));

            #endregion

            #endregion

            scene.AddGameObject(doorsParent5);
            scene.AddGameObject(doorsTextObject5);

            #region Doors6

            GameObject doorsParent6 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                //Transform = { LocalPosition = new Vector3(-4.5f, -1.0f, 5.5f) }
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };

            GameObject doorsObject6 = new GameObject
            {
                Layer = PhysicLayer.Enviroment
            };
            doorsObject6.Transform.Translate(new Vector3(6.5f, 0.0f, 31.75f));
            doorsObject6.Transform.Rotate(new Vector3(0, 0, 0));
            doorsObject6.Transform.LocalScale = new Vector3(1f, 1, 1);
            doorsObject6.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsObject6, Models["doors"]));
            doorsObject6.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject6.AddComponent(new BoxCollider(doorsObject6)
            {
                Center = new Vector3(0.0f, 0.0f, -2f),
                Size = new Vector3(2.0f, 10f, 0.75f),
                IsTrigger = false
            });
            doorsParent6.AddComponent(new SphereCollider(doorsParent6)
            {
                Center = new Vector3(6.5f, -1.0f, 29.5f),
                Radius = 2.5f,
                IsTrigger = true
            });
            doorsParent6.AddComponent(new SoundEmitter(doorsParent6, Sounds));
            doorsParent6.AddComponent(new DoorsOpenClose(doorsParent6) {accessLevelRequired = AccessLevel.Basic});
            doorsParent6.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsParent6, Models["jamb6"]));
            doorsParent6.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject6.AddComponent(new Rigidbody(doorsObject6)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false
            });

            #region AnimationClips

            AnimationClip doorsOpenClip6
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject6)
                            {
                                LocalScale = doorsObject6.Transform.LocalScale,
                                LocalPosition = doorsObject6.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject6.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject6)
                            {
                                LocalScale = doorsObject6.Transform.LocalScale,
                                LocalPosition = doorsObject6.Transform.LocalPosition + new Vector3(3f, 0, 0f),
                                LocalEulerAngles = doorsObject6.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            AnimationClip doorsCloseClip6
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject6)
                            {
                                LocalScale = doorsObject6.Transform.LocalScale,
                                LocalPosition = doorsObject6.Transform.LocalPosition + new Vector3(3f, 0, 0f),
                                LocalEulerAngles = doorsObject6.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        //new AnimationFrame
                        //{
                        //    Transform = new Transform(doorsObject)
                        //    {
                        //        LocalScale = doorsObject.Transform.LocalScale,
                        //        LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 0.2f),
                        //        LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                        //    },
                        //    Time = TimeSpan.FromSeconds(0.5)
                        //},
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject6)
                            {
                                LocalScale = doorsObject6.Transform.LocalScale,
                                LocalPosition = doorsObject6.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject6.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            Animator doorsAnimator6 = new Animator(doorsObject6) {IsLooped = false};
            doorsAnimator6.AnimationClips.Add("Open", doorsOpenClip6);
            doorsAnimator6.AnimationClips.Add("Close", doorsCloseClip6);
            doorsObject6.AddComponent(doorsAnimator6);

            doorsParent6.AddChild(doorsObject6);

            #endregion

            #region DoorsText

            GameObject doorsTextObject6 = new GameObject();

            doorsTextObject6.AddComponent(
                new TextRenderer(doorsTextObject6, SpriteFonts["TobagoPoster"])
                {
                    IsOverlay = false,
                    Text = "Doors",
                    Color = Color.Red,
                    Pivot = new Vector2(0.5f, 1)
                });
            doorsTextObject6.Transform.Scale(new Vector3(0.6f, 0.5f, 0.5f));
            doorsTextObject6.Transform.Translate(new Vector3(-4, 2, 5.75f));
            doorsTextObject6.Transform.Rotate(new Vector3(0, 90, 0));

            #endregion

            #endregion

            scene.AddGameObject(doorsParent6);
            scene.AddGameObject(doorsTextObject6);


            //firstRoom.AddChild(doorsObject);
            zeroRoom.AddDoorsAndRoom(doorsParent0, firstRoom);
            firstRoom.AddDoorsAndRoom(doorsParent0, zeroRoom);

            firstRoom.AddDoorsAndRoom(doorsParent, secondRoom);
            secondRoom.AddDoorsAndRoom(doorsParent, firstRoom);

            secondRoom.AddDoorsAndRoom(doorsParent2, thirdRoom);
            thirdRoom.AddDoorsAndRoom(doorsParent2, secondRoom);

            thirdRoom.AddDoorsAndRoom(doorsParent3, fourthRoom);
            fourthRoom.AddDoorsAndRoom(doorsParent3, thirdRoom);

            fourthRoom.AddDoorsAndRoom(doorsParent4, fifthRoom);
            fifthRoom.AddDoorsAndRoom(doorsParent4, fourthRoom);

            fifthRoom.AddDoorsAndRoom(doorsParent5, sixthRoom);
            sixthRoom.AddDoorsAndRoom(doorsParent5, fifthRoom);

            sixthRoom.AddDoorsAndRoom(doorsParent6, firstRoom);
            firstRoom.AddDoorsAndRoom(doorsParent6, sixthRoom);

            scene.AddRoom(zeroRoom);
            scene.AddRoom(firstRoom);
            scene.AddRoom(secondRoom);
            scene.AddRoom(thirdRoom);
            scene.AddRoom(fourthRoom);
            scene.AddRoom(fifthRoom);
            scene.AddRoom(sixthRoom);

            #region Card

            GameObject card = new GameObject() {Layer = PhysicLayer.Enviroment};
            card.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, card, Models["box"]));
            card.AddComponent(new BoxCollider(card) {Size = new Vector3(20f, 20f, 20f), IsTrigger = true});
            card.Transform.Translate(new Vector3(-1f, 1f, 20f));
            card.Transform.Scale(new Vector3(0.5f, 1f, 0.1f));
            card.AddComponent(new AccessCard(card) {AccessLevel = AccessLevel.Medium, scene = scene});
            scene.AddGameObject(card);

            GameObject card1 = new GameObject() {Layer = PhysicLayer.Enviroment};
            card1.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, card1, Models["box"]));
            card1.AddComponent(new BoxCollider(card1) {Size = new Vector3(20f, 20f, 20f), IsTrigger = true});
            card1.Transform.Translate(new Vector3(-21.5f, 1f, 39f));
            card1.Transform.Scale(new Vector3(0.5f, 1f, 0.1f));
            card1.AddComponent(new AccessCard(card1) {AccessLevel = AccessLevel.Full, scene = scene});
            scene.AddGameObject(card1);

            #endregion


            #region YouDiedText

            GameObject youDiedTextObject = new GameObject();

            youDiedTextObject.AddComponent(new TextRenderer(youDiedTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Text = "You died",
                Pivot = new Vector2(0, 1)
            });
            youDiedTextObject.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            youDiedTextObject.Transform.Translate(
                new Vector3(200, GameManager.GraphicsDevice.Viewport.Height / 2.0f, 0));

            #endregion

            scene.AddGameObject(youDiedTextObject);


            #region PoisonCountText

            GameObject poisonCountTextObject = new GameObject();

            poisonCountTextObject.AddComponent(new TextRenderer(poisonCountTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Pivot = new Vector2(0, 1)
            });
            poisonCountTextObject.Transform.Scale(new Vector3(0.125f, 0.125f, 0.125f));
            poisonCountTextObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 210, 195,
                0));

            scene.AddGameObject(poisonCountTextObject);

            #endregion

            #region PoisonCountUI

            GameObject poisonVentsUIObject = new GameObject();

            poisonVentsUIObject.AddComponent(new ImageRenderer(poisonVentsUIObject, Textures2D["ventUI"])
            {
                IsOverlay = true,
                Pivot = new Vector2(0.5f, 0.5f)
            });


            poisonVentsUIObject.Transform.Scale(new Vector3(0.15f, 0.15f, 0.15f));
            poisonVentsUIObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 120, 100, 0));
            scene.AddGameObject(poisonVentsUIObject);

            #endregion


            #region NoiseIndicatorUI

            GameObject noiseIndicatorUIObject = new GameObject();
            noiseIndicatorUIObject.Transform.Translate(new Vector3(150, GameManager.GraphicsDevice.Viewport.Height - 150,
                0));
            noiseIndicatorUIObject.Transform.Scale(new Vector3(2, 2, 1));


//            noiseIndicatorUIObject.AddComponent(new ImageRenderer(noiseIndicatorUIObject, Textures2D["ventUI"])
//            {
//                IsOverlay = true,
//                Pivot = new Vector2(0.5f, 0.5f)
//            });
//
//
//            noiseIndicatorUIObject.Transform.Scale(new Vector3(0.018f, 0.018f, 0.018f));
//            noiseIndicatorUIObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 60, 50, 0));
//            scene.AddGameObject(noiseIndicatorUIObject);

            GameObject noiseIndicatorHandler = new GameObject();
            noiseIndicatorHandler.Transform.Scale(new Vector3(0.0085f, 0.0085f, 0.03f));

            //up
            GameObject upNoiseIndicator = new GameObject();
            //upNoiseIndicator.Transform.Scale(new Vector3(0.003f, 0.009f, 1));
            upNoiseIndicator.Transform.Scale(new Vector3(4, 4, 4));
            upNoiseIndicator.Transform.Translate(new Vector3(0, -1300, 0));
            upNoiseIndicator.Transform.LocalEulerAngles = new Vector3(0, 0, -90);

            upNoiseIndicator.AddComponent(
                new ImageRenderer(upNoiseIndicator, Textures2D["barNoise"])
                {
                    Pivot = new Vector2(0, 0.5f),
                    Filling = 1,
                    DrawOrder = 5
                });

            noiseIndicatorHandler.AddChild(upNoiseIndicator);

            //upright
            GameObject upRightNoiseIndicator = new GameObject();
            //upRightNoiseIndicator.Transform.Scale(new Vector3(0.01f, 0.01f, 0.01f));
            upRightNoiseIndicator.Transform.Scale(new Vector3(4, 4, 4));
            upRightNoiseIndicator.Transform.Translate(new Vector3(919.24f, -919.24f, 0));
            upRightNoiseIndicator.Transform.LocalEulerAngles = new Vector3(0, 0, -45);

            upRightNoiseIndicator.AddComponent(
                new ImageRenderer(upRightNoiseIndicator, Textures2D["barNoise"])
                {
                    Pivot = new Vector2(0, 0.5f),
                    Filling = 1,
                    DrawOrder = 5
                });

            noiseIndicatorHandler.AddChild(upRightNoiseIndicator);

            //right
            GameObject rightNoiseIndicator = new GameObject();
            rightNoiseIndicator.Transform.Translate(new Vector3(1300, 0, 0));
            rightNoiseIndicator.Transform.Scale(new Vector3(4, 4, 4));

            rightNoiseIndicator.AddComponent(
                new ImageRenderer(rightNoiseIndicator, Textures2D["barNoise"])
                {
                    Pivot = new Vector2(0, 0.5f),
                    Filling = 1,
                    DrawOrder = 5
                });

            noiseIndicatorHandler.AddChild(rightNoiseIndicator);

            //downright
            GameObject downRightNoiseIndicator = new GameObject();
            downRightNoiseIndicator.Transform.Scale(new Vector3(4, 4, 4));
            downRightNoiseIndicator.Transform.Translate(new Vector3(919.24f, 919.24f, 0));
            downRightNoiseIndicator.Transform.LocalEulerAngles = new Vector3(0, 0, 45);

            downRightNoiseIndicator.AddComponent(
                new ImageRenderer(downRightNoiseIndicator, Textures2D["barNoise"])
                {
                    Pivot = new Vector2(0, 0.5f),
                    Filling = 1,
                    DrawOrder = 5
                });

            noiseIndicatorHandler.AddChild(downRightNoiseIndicator);

            //down
            GameObject downNoiseIndicator = new GameObject();
            downNoiseIndicator.Transform.Scale(new Vector3(4, 4, 4));
            downNoiseIndicator.Transform.Translate(new Vector3(0, 1300, 0));
            downNoiseIndicator.Transform.LocalEulerAngles = new Vector3(0, 0, 90);

            downNoiseIndicator.AddComponent(
                new ImageRenderer(downNoiseIndicator, Textures2D["barNoise"])
                {
                    Pivot = new Vector2(0, 0.5f),
                    Filling = 1,
                    DrawOrder = 5
                });

            noiseIndicatorHandler.AddChild(downNoiseIndicator);

            //downleft
            GameObject downLeftNoiseIndicator = new GameObject();
            downLeftNoiseIndicator.Transform.Scale(new Vector3(4, 4, 4));
            downLeftNoiseIndicator.Transform.Translate(new Vector3(-919.24f, 919.24f, 0));
            downLeftNoiseIndicator.Transform.LocalEulerAngles = new Vector3(0, 0, 135);

            downLeftNoiseIndicator.AddComponent(
                new ImageRenderer(downLeftNoiseIndicator, Textures2D["barNoise"])
                {
                    Pivot = new Vector2(0, 0.5f),
                    Filling = 1,
                    DrawOrder = 5
                });

            noiseIndicatorHandler.AddChild(downLeftNoiseIndicator);

            //left
            GameObject leftNoiseIndicator = new GameObject();
            leftNoiseIndicator.Transform.Scale(new Vector3(4, 4, 4));
            leftNoiseIndicator.Transform.Translate(new Vector3(-1300, 0, 0));
            leftNoiseIndicator.Transform.LocalEulerAngles = new Vector3(0, 0, 180);

            leftNoiseIndicator.AddComponent(
                new ImageRenderer(leftNoiseIndicator, Textures2D["barNoise"])
                {
                    Pivot = new Vector2(0, 0.5f),
                    Filling = 1,
                    DrawOrder = 5
                });

            noiseIndicatorHandler.AddChild(leftNoiseIndicator);

            //upleft
            GameObject upLeftNoiseIndicator = new GameObject();
            upLeftNoiseIndicator.Transform.Scale(new Vector3(4, 4, 4));
            upLeftNoiseIndicator.Transform.Translate(new Vector3(-919.24f, -919.24f, 0));
            upLeftNoiseIndicator.Transform.LocalEulerAngles = new Vector3(0, 0, 225);

            upLeftNoiseIndicator.AddComponent(
                new ImageRenderer(upLeftNoiseIndicator, Textures2D["barNoise"])
                {
                    Pivot = new Vector2(0, 0.5f),
                    Filling = 1,
                    DrawOrder = 5
                });

            noiseIndicatorHandler.AddChild(upLeftNoiseIndicator);

            var noiseIndicator = new NoiseIndicator(noiseIndicatorUIObject)
            {
                FillingBars =
                {
                    [(int) Direction.Up] = upNoiseIndicator.GetComponent<ImageRenderer>(),
                    [(int) Direction.UpRight] = upRightNoiseIndicator.GetComponent<ImageRenderer>(),
                    [(int) Direction.Right] = rightNoiseIndicator.GetComponent<ImageRenderer>(),
                    [(int) Direction.DownRight] = downRightNoiseIndicator.GetComponent<ImageRenderer>(),
                    [(int) Direction.Down] = downNoiseIndicator.GetComponent<ImageRenderer>(),
                    [(int) Direction.DownLeft] = downLeftNoiseIndicator.GetComponent<ImageRenderer>(),
                    [(int) Direction.Left] = leftNoiseIndicator.GetComponent<ImageRenderer>(),
                    [(int) Direction.UpLeft] = upLeftNoiseIndicator.GetComponent<ImageRenderer>()
                }
            };

            noiseIndicatorUIObject.AddComponent(noiseIndicator);

            noiseIndicatorUIObject.AddChild(noiseIndicatorHandler);

            NoiseGenerator.NoiseIndicator = noiseIndicator;

            GameObject backgroundForNoiseIndicator = new GameObject();
            backgroundForNoiseIndicator.AddComponent(
                new ImageRenderer(backgroundForNoiseIndicator, Textures2D["noiseIndicator"])
                {
                    Pivot = new Vector2(0.5f, 0.5f),
                    DrawOrder = 4
                });
            backgroundForNoiseIndicator.Transform.Translate(Vector3.Left * 0.5f);
            backgroundForNoiseIndicator.Transform.Scale(new Vector3(0.12f, 0.12f, 0.03f));
//            
            noiseIndicatorUIObject.AddChild(backgroundForNoiseIndicator);

            scene.AddGameObject(noiseIndicatorUIObject);

            #endregion


            #region YouWonText

            GameObject youWonTextObject = new GameObject();

            youWonTextObject.AddComponent(new TextRenderer(youWonTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Text = "You won",
                Pivot = new Vector2(0, 1)
            });
            youWonTextObject.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            youWonTextObject.Transform.Translate(new Vector3(200, GameManager.GraphicsDevice.Viewport.Height / 2.0f,
                0));

            #endregion

            scene.AddGameObject(youWonTextObject);

            #region HUD

            GameObject hudObject = new GameObject();

            hudObject.AddComponent(new HeadUpDisplay(hudObject)
            {
                youDiedText = youDiedTextObject,
                youWonText = youWonTextObject,
                VentText = poisonCountTextObject,
                ventsPoisonedCountToWin = 3,
                VentUI = poisonVentsUIObject,
                ventImages = new List<Texture2D>()
                {
                    Textures2D["vent1"],
                    Textures2D["vent2"],
                    Textures2D["vent3"]
                }
            });

            #endregion

            scene.AddGameObject(hudObject);

            playerComponent.GameObject.GetComponent<Player>().headUpDisplay = hudObject.GetComponent<HeadUpDisplay>();


            #region Crosshair

            GameObject crosshairObject = new GameObject();

            crosshairObject.AddComponent(
                new ImageRenderer(crosshairObject, Textures2D["aimStand"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0.5f, 0.5f)
                });
            crosshairObject.Transform.Scale(new Vector3(0.008f, 0.008f, 0.008f));
            crosshairObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2.0f,
                GameManager.GraphicsDevice.Viewport.Height / 2.0f, 0));
            crosshairObject.AddComponent(
                new CrosshairTargetIndicator(crosshairObject));

            #endregion


            scene.AddGameObject(crosshairObject);


            playerController.ActiveCrosshair = crosshairObject;
            playerController.crosshairs =
                new List<Texture2D> {Textures2D["aimStand"], Textures2D["aimRun"], Textures2D["aimCrouch"]};

            #region Vent

            GameObject ventFillingBar = new GameObject();

            ventFillingBar.AddComponent(
                new ImageRenderer(ventFillingBar, Textures2D["bar"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            ventFillingBar.Transform.Scale(new Vector3(0.065f, 0.01f, 0.01f));
            ventFillingBar.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2.0f,
                GameManager.GraphicsDevice.Viewport.Height / 2.0f + 50, 0));

            GameObject ventTextObject = new GameObject();

            ventTextObject.AddComponent(new TextRenderer(ventTextObject, SpriteFonts["TobagoPoster"])
            {
                IsEnabled = false,
                IsOverlay = true,
                Text = "VENT - press F to use poison",
                Color = Color.Red,
                Pivot = new Vector2(0, 0)
            });
            ventTextObject.Transform.Scale(new Vector3(0.065f, 0.075f, 0.075f));

            ventTextObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2,
                GameManager.GraphicsDevice.Viewport.Height / 2, 0));

            GameObject ventObject = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            ventObject.Transform.Translate(new Vector3(-15, 0, 5));
            ventObject.Transform.Scale(new Vector3(0.5f, 1.0f, 0.5f));

            ventObject.Transform.Rotate(new Vector3(0.0f, 90, 0.0f));
            ventObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, ventObject, Models["Vent"]));
            ventObject.GetComponent<MeshRenderer>().CastsShadow = false;
            ventObject.AddComponent(new SphereCollider(ventObject) {Radius = 1.0f, IsTrigger = true});
            ventObject.AddComponent(new Vent(ventObject)
            {
                ventText = ventTextObject,
                headUpDisplay = hudObject.GetComponent<HeadUpDisplay>(),
                fillingBar = ventFillingBar
            });

            #endregion

            scene.AddGameObject(ventFillingBar);
            scene.AddGameObject(ventObject);
            scene.AddGameObject(ventTextObject);

            #region Vent2

            GameObject ventFillingBar2 = new GameObject();

            ventFillingBar2.AddComponent(
                new ImageRenderer(ventFillingBar2, Textures2D["bar"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            ventFillingBar2.Transform.Scale(new Vector3(0.065f, 0.01f, 0.01f));
            ventFillingBar2.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2.0f,
                GameManager.GraphicsDevice.Viewport.Height / 2.0f + 50, 0));

            GameObject ventTextObject2 = new GameObject();

            ventTextObject2.AddComponent(new TextRenderer(ventTextObject2, SpriteFonts["TobagoPoster"])
            {
                IsEnabled = false,
                IsOverlay = true,
                Text = "VENT - press F to use poison",
                Color = Color.Red,
                Pivot = new Vector2(0, 0)
            });
            ventTextObject2.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));

            ventTextObject2.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2,
                GameManager.GraphicsDevice.Viewport.Height / 2, 0));

            GameObject ventObject2 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            ventObject2.Transform.Translate(new Vector3(-28.5f, 0, 38.0f));
            ventObject2.Transform.Scale(new Vector3(0.5f, 1.0f, 0.5f));

            ventObject2.Transform.Rotate(new Vector3(0.0f, 90, 0.0f));
            ventObject2.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, ventObject2, Models["Vent"]));
            ventObject2.GetComponent<MeshRenderer>().CastsShadow = false;
            ventObject2.AddComponent(new SphereCollider(ventObject2) {Radius = 1.0f, IsTrigger = true, Center = new Vector3(-5,0,0)});
            ventObject2.AddComponent(new Vent(ventObject2)
            {
                ventText = ventTextObject2,
                headUpDisplay = hudObject.GetComponent<HeadUpDisplay>(),
                fillingBar = ventFillingBar2
            });

            #endregion

            scene.AddGameObject(ventFillingBar2);
            scene.AddGameObject(ventObject2);
            scene.AddGameObject(ventTextObject2);

            #region Vent3

            GameObject ventFillingBar3 = new GameObject();

            ventFillingBar3.AddComponent(
                new ImageRenderer(ventFillingBar3, Textures2D["bar"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            ventFillingBar3.Transform.Scale(new Vector3(0.075f, 0.01f, 0.01f));
            ventFillingBar3.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2.0f,
                GameManager.GraphicsDevice.Viewport.Height / 2.0f + 50, 0));

            GameObject ventTextObject3 = new GameObject();

            ventTextObject3.AddComponent(new TextRenderer(ventTextObject3, SpriteFonts["TobagoPoster"])
            {
                IsEnabled = false,
                IsOverlay = true,
                Text = "VENT - press F to use poison",
                Color = Color.Red,
                Pivot = new Vector2(0, 0)
            });
            ventTextObject3.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));

            ventTextObject3.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2,
                GameManager.GraphicsDevice.Viewport.Height / 2, 0));

            GameObject ventObject3 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            ventObject3.Transform.Translate(new Vector3(4.0f, 0, 62.25f));
            ventObject3.Transform.Scale(new Vector3(0.5f, 1.0f, 0.5f));

            ventObject3.Transform.Rotate(new Vector3(0.0f, 0, 0.0f));
            ventObject3.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, ventObject3, Models["Vent"]));
            ventObject3.GetComponent<MeshRenderer>().CastsShadow = false;
            ventObject3.AddComponent(new SphereCollider(ventObject3) {Radius = 1.0f, IsTrigger = true, Center = new Vector3(5,0,0)});
            ventObject3.AddComponent(new Vent(ventObject3)
            {
                ventText = ventTextObject3,
                headUpDisplay = hudObject.GetComponent<HeadUpDisplay>(),
                fillingBar = ventFillingBar3
            });

            #endregion

            scene.AddGameObject(ventFillingBar3);
            scene.AddGameObject(ventObject3);
            scene.AddGameObject(ventTextObject3);


            #region WinComputer

            GameObject winComputerTextObject = new GameObject();

            winComputerTextObject.AddComponent(new TextRenderer(winComputerTextObject, SpriteFonts["TobagoPoster"])
            {
                IsEnabled = false,
                IsOverlay = true,
                Text = "WIN COMPUTER",
                Color = Color.Blue,
                Pivot = new Vector2(0, 0)
            });
            winComputerTextObject.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));

            winComputerTextObject.Transform.Translate(new Vector3((GameManager.GraphicsDevice.Viewport.Width / 2 + 2),
                (GameManager.GraphicsDevice.Viewport.Height / 2 + 2), 0));

            GameObject winComputerObject = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            winComputerObject.Transform.Translate(new Vector3(6.25f, 0, -11));
            winComputerObject.Transform.Rotate(new Vector3(0.0f, 180.0f, 0.0f));
            winComputerObject.Transform.Scale(new Vector3(0.018f, 0.018f, 0.018f));
            winComputerObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, winComputerObject, Models["monitor"]));
            winComputerObject.GetComponent<MeshRenderer>().CastsShadow = false;
            winComputerObject.AddComponent(new SphereCollider(winComputerObject) {Radius = 50.0f, IsTrigger = true});
            winComputerObject.AddComponent(
                new WinComputer(winComputerObject) {headUpDisplay = hudObject.GetComponent<HeadUpDisplay>()});

            #endregion

            scene.AddGameObject(winComputerObject);
            scene.AddGameObject(winComputerTextObject);

         

            #region enviroment

            #region zeroRoom

            GameObject container1 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            container1.Transform.Translate(new Vector3(0f, 0f, -4.25f));
            container1.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            container1.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, container1, Models["container1"]));
            container1.AddComponent(new BoxCollider(container1) {Size = new Vector3(2f, 6, 3)});
            zeroRoom.AddChild(container1);

            scene.AddGameObject(container1);

            GameObject container2 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            container2.Transform.Translate(new Vector3(0f, 0.0f, -11f));
            container2.Transform.Rotate(new Vector3(0, -90, 0));
            container2.Transform.Scale(new Vector3(0.02f, 0.02f, 0.02f));
            container2.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, container2, Models["container2"]));
            container2.GetComponent<MeshRenderer>().CastsShadow = false;
            container2.AddComponent(new BoxCollider(container2)
            {
                Size = new Vector3(2.5f, 4, 1),
                Center = new Vector3(0.25f, 0, 0)
            });
            zeroRoom.AddChild(container2);

            scene.AddGameObject(container2);

            #endregion

            #region firstRoom

            GameObject tanks = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            tanks.Transform.Translate(new Vector3(4.5f, 1.5f, 4.25f));
            tanks.Transform.Rotate(new Vector3(-90, 0, 0));
            tanks.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            tanks.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, tanks, Models["tanks"]));
            tanks.AddComponent(new BoxCollider(tanks) {Size = new Vector3(3.0f, 1, 2)});
            firstRoom.AddChild(tanks);

            scene.AddGameObject(tanks);
            
            
            #endregion
            
            #region secondRoom
            
            GameObject locker = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            locker.Transform.Translate(new Vector3(-6.25f, 0f, 9.2f));
            locker.Transform.Rotate(new Vector3(0, 90, 0));
            locker.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            locker.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, locker, Models["locker"]));
            locker.GetComponent<MeshRenderer>().CastsShadow = false;
            //locker.AddComponent(new BoxCollider(locker) { Size = new Vector3(1.25f,6,1.25f)});
            secondRoom.AddChild(locker);

            scene.AddGameObject(locker);
            
            GameObject locker1 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            locker1.Transform.Translate(new Vector3(-7.5f, 0f, 9.2f));
            locker1.Transform.Rotate(new Vector3(0, 90, 0));
            locker1.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            locker1.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, locker1, Models["locker"]));
            locker1.GetComponent<MeshRenderer>().CastsShadow = false;
            //locker1.AddComponent(new BoxCollider(locker1) { Size = new Vector3(1.25f,6,1.25f)});
            secondRoom.AddChild(locker1);

            scene.AddGameObject(locker1);
            
            GameObject locker2 = new GameObject() //nowy
            {
                Layer = PhysicLayer.Enviroment
            };
            locker2.Transform.Translate(new Vector3(-8.75f, 0f, 9.2f));
            locker2.Transform.Rotate(new Vector3(0, 90, 0));
            locker2.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            locker2.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, locker2, Models["locker"]));
            locker2.GetComponent<MeshRenderer>().CastsShadow = false;
            //locker2.AddComponent(new BoxCollider(locker2) { Size = new Vector3(1.25f,6,1.25f)});
            secondRoom.AddChild(locker2);

            scene.AddGameObject(locker2);
            
            GameObject locker3 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            locker3.Transform.Translate(new Vector3(-10.0f, 0f, 9.2f));
            locker3.Transform.Rotate(new Vector3(0, 90, 0));
            locker3.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            locker3.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, locker3, Models["locker"]));
            locker3.GetComponent<MeshRenderer>().CastsShadow = false;
            //locker3.AddComponent(new BoxCollider(locker3) { Size = new Vector3(1.25f,6,1.25f)});
            secondRoom.AddChild(locker3);

            scene.AddGameObject(locker3);
            
            GameObject locker4 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            locker4.Transform.Translate(new Vector3(-11.25f, 0f, 9.2f));
            locker4.Transform.Rotate(new Vector3(0, 90, 0));
            locker4.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            locker4.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, locker4, Models["locker"]));
            locker4.GetComponent<MeshRenderer>().CastsShadow = false;
            //locker4.AddComponent(new BoxCollider(locker4) { Size = new Vector3(1.25f,6,1.25f)});
            secondRoom.AddChild(locker4);

            scene.AddGameObject(locker4);
            
            GameObject locker5 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            locker5.Transform.Translate(new Vector3(-12.5f, 0f, 9.2f));
            locker5.Transform.Rotate(new Vector3(0, 90, 0));
            locker5.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            locker5.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, locker5, Models["locker"]));
            locker5.GetComponent<MeshRenderer>().CastsShadow = false;
            locker5.AddComponent(new BoxCollider(locker5) { Size = new Vector3(14.75f,6,1.25f), Center = new Vector3(-0.3125f,0,0)});
            secondRoom.AddChild(locker5);

            scene.AddGameObject(locker5);
            
            GameObject locker6 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            locker6.Transform.Translate(new Vector3(-13.75f, 0f, 9.2f));
            locker6.Transform.Rotate(new Vector3(0, 90, 0));
            locker6.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            locker6.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, locker6, Models["locker"]));
            locker6.GetComponent<MeshRenderer>().CastsShadow = false;
            //locker6.AddComponent(new BoxCollider(locker6) { Size = new Vector3(1.25f,6,1.25f)});
            secondRoom.AddChild(locker6);

            scene.AddGameObject(locker6);
            
            GameObject locker7 = new GameObject() //nowy
            {
                Layer = PhysicLayer.Enviroment
            };
            locker7.Transform.Translate(new Vector3(-15.0f, 0f, 9.2f));
            locker7.Transform.Rotate(new Vector3(0, 90, 0));
            locker7.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            locker7.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, locker7, Models["locker"]));
            locker7.GetComponent<MeshRenderer>().CastsShadow = false;
            //locker7.AddComponent(new BoxCollider(locker7) { Size = new Vector3(1.25f,6,1.25f)});
            secondRoom.AddChild(locker7);

            scene.AddGameObject(locker7);
            
            GameObject locker8 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            locker8.Transform.Translate(new Vector3(-16.25f, 0f, 9.2f));
            locker8.Transform.Rotate(new Vector3(0, 90, 0));
            locker8.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            locker8.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, locker8, Models["locker"]));
            locker8.GetComponent<MeshRenderer>().CastsShadow = false;
            //locker8.AddComponent(new BoxCollider(locker8) { Size = new Vector3(1.25f,6,1.25f)});
            secondRoom.AddChild(locker8);

            scene.AddGameObject(locker8);
            
            GameObject locker9 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            locker9.Transform.Translate(new Vector3(-17.5f, 0f, 9.2f));
            locker9.Transform.Rotate(new Vector3(0, 90, 0));
            locker9.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            locker9.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, locker9, Models["locker"]));
            locker9.GetComponent<MeshRenderer>().CastsShadow = false;
            //locker9.AddComponent(new BoxCollider(locker9) { Size = new Vector3(1.25f,6,1.25f)});
            secondRoom.AddChild(locker9);

            scene.AddGameObject(locker9);
            
            GameObject locker10 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            locker10.Transform.Translate(new Vector3(-18.75f, 0f, 9.2f));
            locker10.Transform.Rotate(new Vector3(0, 90, 0));
            locker10.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            locker10.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, locker10, Models["locker"]));
            locker10.GetComponent<MeshRenderer>().CastsShadow = false;
            //locker10.AddComponent(new BoxCollider(locker10) { Size = new Vector3(1.25f,6,1.25f)});
            secondRoom.AddChild(locker10);

            scene.AddGameObject(locker10);
            
            GameObject locker11 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            locker11.Transform.Translate(new Vector3(-20.0f, 0f, 9.2f));
            locker11.Transform.Rotate(new Vector3(0, 90, 0));
            locker11.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            locker11.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, locker11, Models["locker"]));
            locker11.GetComponent<MeshRenderer>().CastsShadow = false;
            //locker11.AddComponent(new BoxCollider(locker11) { Size = new Vector3(1.25f,6,1.25f)});
            secondRoom.AddChild(locker11);

            scene.AddGameObject(locker11);
            
            #endregion
            
            #region thirdRoom

            GameObject case1 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            case1.Transform.Translate(new Vector3(-21.25f, 0.0f, 24.5f));
            case1.Transform.Rotate(new Vector3(90, 0, 0));
            case1.Transform.Scale(new Vector3(0.01f, 0.01f, 0.01f));
            case1.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, case1, Models["case1"]));
            case1.GetComponent<MeshRenderer>().CastsShadow = false;
            case1.AddComponent(new BoxCollider(case1) {Size = new Vector3(1f, 4, 2.5f)});
            thirdRoom.AddChild(case1);

            scene.AddGameObject(case1);

            GameObject case2 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            case2.Transform.Translate(new Vector3(-21.25f, 0.5f, 25.0f));
            case2.Transform.Rotate(new Vector3(0, -90, 0));
            case2.Transform.Scale(new Vector3(0.01f, 0.01f, 0.01f));
            case2.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, case2, Models["case2"]));
            case2.GetComponent<MeshRenderer>().CastsShadow = false;
            //case2.AddComponent(new BoxCollider(case2) { Size = new Vector3(1f,4,2.5f) });
            thirdRoom.AddChild(case2);

            scene.AddGameObject(case2);

            GameObject crate1 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            crate1.Transform.Translate(new Vector3(-28.0f, 0.0f, 22.5f));
            crate1.Transform.Rotate(new Vector3(0, 0, 0));
            crate1.Transform.Scale(new Vector3(0.01f, 0.01f, 0.01f));
            crate1.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, crate1, Models["crate1"]));
            crate1.GetComponent<MeshRenderer>().CastsShadow = false;
            crate1.AddComponent(new BoxCollider(crate1)
            {
                Size = new Vector3(1.5f, 4, 2.0f),
                Center = new Vector3(0, 0, -0.25f)
            });
            thirdRoom.AddChild(crate1);

            scene.AddGameObject(crate1);

            GameObject crate2 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            crate2.Transform.Translate(new Vector3(-28.0f, 0.7f, 22.65f));
            crate2.Transform.Rotate(new Vector3(0, 0, 0));
            crate2.Transform.Scale(new Vector3(0.01f, 0.01f, 0.01f));
            crate2.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, crate2, Models["crate1"]));
            crate2.GetComponent<MeshRenderer>().CastsShadow = false;
            //crate2.AddComponent(new BoxCollider(crate2) { Size = new Vector3(1f,4,2.5f) });
            thirdRoom.AddChild(crate2);

            scene.AddGameObject(crate2);

            GameObject crate3 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            crate3.Transform.Translate(new Vector3(-28.0f, 0.5f, 21.95f));
            crate3.Transform.Rotate(new Vector3(-75, 0, 0));
            crate3.Transform.Scale(new Vector3(0.01f, 0.01f, 0.01f));
            crate3.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, crate3, Models["crate1"]));
            crate3.GetComponent<MeshRenderer>().CastsShadow = false;
            //crate3.AddComponent(new BoxCollider(crate3) { Size = new Vector3(1f,4,2.5f) });
            thirdRoom.AddChild(crate3);

            scene.AddGameObject(crate3);
            
            
            GameObject crate21 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            crate21.Transform.Translate(new Vector3(-28.0f, 0.0f, 20.5f));
            crate21.Transform.Rotate(new Vector3(0, 0, 0));
            crate21.Transform.Scale(new Vector3(0.02f, 0.02f, 0.02f));
            crate21.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, crate21, Models["crate2"]));
            crate21.GetComponent<MeshRenderer>().CastsShadow = false;
            crate21.AddComponent(new BoxCollider(crate21) { Size = new Vector3(1.5f,4,1.0f), Center = new Vector3(0,0,0.0f)});
            thirdRoom.AddChild(crate21);

            scene.AddGameObject(crate21);
            
            GameObject crate22 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            crate22.Transform.Translate(new Vector3(-21.25f, 0.0f, 18.5f));
            crate22.Transform.Rotate(new Vector3(0, 0, 0));
            crate22.Transform.Scale(new Vector3(0.02f, 0.02f, 0.02f));
            crate22.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, crate22, Models["crate2"]));
            crate22.GetComponent<MeshRenderer>().CastsShadow = false;
            crate22.AddComponent(new BoxCollider(crate22) { Size = new Vector3(1.5f,4,2.25f), Center = new Vector3(0,0,-0.5f)});
            thirdRoom.AddChild(crate22);

            scene.AddGameObject(crate22);
            
            GameObject crate23 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            crate23.Transform.Translate(new Vector3(-21.25f, 0.0f, 17.25f));
            crate23.Transform.Rotate(new Vector3(0, 10, 0));
            crate23.Transform.Scale(new Vector3(0.02f, 0.02f, 0.02f));
            crate23.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, crate23, Models["crate2"]));
            crate23.GetComponent<MeshRenderer>().CastsShadow = false;
            thirdRoom.AddChild(crate23);

            scene.AddGameObject(crate23);
            
            GameObject crate24 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            crate24.Transform.Translate(new Vector3(-21.25f, 1.0f, 18.25f));
            crate24.Transform.Rotate(new Vector3(0, -10, 0));
            crate24.Transform.Scale(new Vector3(0.02f, 0.02f, 0.02f));
            crate24.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, crate24, Models["crate2"]));
            crate24.GetComponent<MeshRenderer>().CastsShadow = false;
            thirdRoom.AddChild(crate24);

            scene.AddGameObject(crate24);
            
            #endregion
            
            #region fifthRoom

            GameObject barrel1 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel1.Transform.Translate(new Vector3(-28.25f, 0f, 70.25f));
            barrel1.Transform.Rotate(new Vector3(0, 0, 0));
            barrel1.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel1.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel1, Models["barrel1"]));
            barrel1.AddComponent(new BoxCollider(barrel1) { Size = new Vector3(3.25f,4,2), Center = new Vector3(0.75f,0,-1.0f)});
            fifthRoom.AddChild(barrel1);

            scene.AddGameObject(barrel1);
            
            GameObject barrel2 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel2.Transform.Translate(new Vector3(-28.0f, 0f, 69.25f));
            barrel2.Transform.Rotate(new Vector3(0, 0, 0));
            barrel2.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel2.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel2, Models["barrel1"]));
            fifthRoom.AddChild(barrel2);

            scene.AddGameObject(barrel2);
            
            GameObject barrel3 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel3.Transform.Translate(new Vector3(-27.0f, 0f, 69.75f));
            barrel3.Transform.Rotate(new Vector3(0, 0, 0));
            barrel3.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel3.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel3, Models["barrel1"]));
            fifthRoom.AddChild(barrel3);

            scene.AddGameObject(barrel3);
            
            GameObject barrel4 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel4.Transform.Translate(new Vector3(-25.0f, 0.4f, 70.0f));
            barrel4.Transform.Rotate(new Vector3(0, 10, 90));
            barrel4.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel4.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel4, Models["barrel1"]));
            fifthRoom.AddChild(barrel4);

            scene.AddGameObject(barrel4);
            
            GameObject barrel5 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel5.Transform.Translate(new Vector3(-25.5f, 0.4f, 69.0f));
            barrel5.Transform.Rotate(new Vector3(0, -20, 90));
            barrel5.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel5.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel5, Models["barrel1"]));
            fifthRoom.AddChild(barrel5);

            scene.AddGameObject(barrel5);
            
            GameObject barrel21 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel21.Transform.Translate(new Vector3(-28.0f, 0.0f, 67.0f));
            barrel21.Transform.Rotate(new Vector3(0, 0, 0));
            barrel21.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel21.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel21, Models["barrel2"]));
            barrel21.AddComponent(new BoxCollider(barrel21) { Size = new Vector3(1.5f,4,1.5f)});
            fifthRoom.AddChild(barrel21);

            scene.AddGameObject(barrel21);
            
            GameObject barrel22 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel22.Transform.Translate(new Vector3(-21.5f, 0.0f, 60.5f));
            barrel22.Transform.Rotate(new Vector3(0, 0, 0));
            barrel22.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel22.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel22, Models["barrel2"]));
            barrel22.AddComponent(new BoxCollider(barrel22) { Size = new Vector3(1.5f,4,1.5f)});
            fifthRoom.AddChild(barrel22);

            scene.AddGameObject(barrel22);
            
            GameObject barrel23 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel23.Transform.Translate(new Vector3(-19.5f, 0.0f, 70.0f));
            barrel23.Transform.Rotate(new Vector3(0, 0, 0));
            barrel23.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel23.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel23, Models["barrel2"]));
            barrel23.AddComponent(new BoxCollider(barrel23) { Size = new Vector3(1.5f,4,1.5f)});
            fifthRoom.AddChild(barrel23);

            scene.AddGameObject(barrel23);
            
            GameObject barrel24 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel24.Transform.Translate(new Vector3(-17.5f, 0.0f, 70.0f));
            barrel24.Transform.Rotate(new Vector3(0, 0, 0));
            barrel24.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel24.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel24, Models["barrel2"]));
            barrel24.AddComponent(new BoxCollider(barrel24) { Size = new Vector3(1.5f,4,1.5f)});
            fifthRoom.AddChild(barrel24);

            scene.AddGameObject(barrel24);
            
            GameObject barrel31 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel31.Transform.Translate(new Vector3(-2.5f, 0.0f, 76.0f));
            barrel31.Transform.Rotate(new Vector3(0, 0, 0));
            barrel31.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel31.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel31, Models["barrel3"]));
            barrel31.AddComponent(new BoxCollider(barrel31) { Size = new Vector3(1.5f,4,1.5f)});
            fifthRoom.AddChild(barrel31);

            scene.AddGameObject(barrel31);
            
            GameObject barrel32 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel32.Transform.Translate(new Vector3(-0.5f, 0.0f, 74.0f));
            barrel32.Transform.Rotate(new Vector3(0, 0, 0));
            barrel32.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel32.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel32, Models["barrel3"]));
            barrel32.AddComponent(new BoxCollider(barrel32) { Size = new Vector3(1.5f,4,1.5f)});
            fifthRoom.AddChild(barrel32);

            scene.AddGameObject(barrel32);
            
            GameObject barrel33 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel33.Transform.Translate(new Vector3(-1.5f, 0.0f, 72.0f));
            barrel33.Transform.Rotate(new Vector3(0, 0, 0));
            barrel33.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel33.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel33, Models["barrel3"]));
            barrel33.AddComponent(new BoxCollider(barrel33) { Size = new Vector3(1.5f,4,1.5f)});
            fifthRoom.AddChild(barrel33);

            scene.AddGameObject(barrel33);
            
            GameObject barrel34 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel34.Transform.Translate(new Vector3(-2.0f, 0.0f, 74.5f));
            barrel34.Transform.Rotate(new Vector3(0, 0, 0));
            barrel34.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel34.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel34, Models["barrel3"]));
            barrel34.AddComponent(new BoxCollider(barrel34) { Size = new Vector3(1.5f,4,1.5f)});
            fifthRoom.AddChild(barrel34);

            scene.AddGameObject(barrel34);
            
            GameObject barrel35 = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            barrel35.Transform.Translate(new Vector3(2.0f, 0.0f, 76.0f));
            barrel35.Transform.Rotate(new Vector3(0, 0, 0));
            barrel35.Transform.Scale(new Vector3(0.03f, 0.03f, 0.03f));
            barrel35.AddComponent((MeshRenderer)Activator.CreateInstance(MeshRendererType, barrel35, Models["barrel3"]));
            barrel35.AddComponent(new BoxCollider(barrel35) { Size = new Vector3(1.5f,4,1.5f)});
            fifthRoom.AddChild(barrel35);

            scene.AddGameObject(barrel35);
            
            
            #endregion
            
            #endregion


            #region ScrewCarpet 

            GameObject carpet = new GameObject() //simulation of screws
            {
                Layer = PhysicLayer.Pullable
            };
            carpet.Transform.Translate(new Vector3(-1.0f, -0.01f, 0.0f));
            carpet.Transform.Scale(new Vector3(1.5f, 1.0f, 1.5f));
            carpet.AddComponent(new BoxCollider(carpet)
            {
                IsTrigger = true,
                Size = new Vector3(4.0f, 1.0f, 3.0f),
                Center = new Vector3(-0.25f, 0, 13)
            });
            carpet.AddComponent(new NoiseGenerator(carpet));
            carpet.AddComponent(new SoundEmitter(carpet, Sounds));
            carpet.AddComponent(new ScrewsSimulation(carpet));
            carpet.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, carpet, Models["ScrewCarpet"]));
            //carpet.AddChild(CreateScrew(new Vector3(0.0f, 0.0f, 0.0f), carpet, scene));
            firstRoom.AddChild(carpet);
            scene.AddGameObject(carpet);

            GameObject carpet1 = new GameObject() //simulation of screws
            {
                Layer = PhysicLayer.Pullable
            };
            carpet1.Transform.Translate(new Vector3(-24.0f, -0.01f, 22.0f));
            carpet1.Transform.Scale(new Vector3(1.5f, 1.0f, 1.5f));
            carpet1.AddComponent(new BoxCollider(carpet1)
            {
                IsTrigger = true,
                Size = new Vector3(4.0f, 1.0f, 0.5f),
                Center = new Vector3(-0.25f, 0, 13)
            });
            carpet1.AddComponent(new NoiseGenerator(carpet1));
            carpet1.AddComponent(new SoundEmitter(carpet1, Sounds));
            carpet1.AddComponent(new ScrewsSimulation(carpet1));
            carpet1.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, carpet1, Models["ScrewCarpet"]));
            //carpet1.AddChild(CreateScrew(new Vector3(0.0f, 0.0f, 0.0f), carpet1, scene));
            fourthRoom.AddChild(carpet1);
            scene.AddGameObject(carpet1);

            GameObject carpet2 = new GameObject() //simulation of screws
            {
                Layer = PhysicLayer.Pullable
            };
            carpet2.Transform.Translate(new Vector3(-14.0f, -0.01f, 28.0f));
            carpet2.Transform.Scale(new Vector3(1.5f, 1.0f, 1.5f));
            carpet2.AddComponent(new BoxCollider(carpet2)
            {
                IsTrigger = true,
                Size = new Vector3(2.0f, 1.0f, 3.0f),
                Center = new Vector3(-0.25f, 0, 13)
            });
            carpet2.AddComponent(new NoiseGenerator(carpet2));
            carpet2.AddComponent(new SoundEmitter(carpet2, Sounds));
            carpet2.AddComponent(new ScrewsSimulation(carpet2));
            carpet2.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, carpet2, Models["ScrewCarpet"]));
            //carpet2.AddChild(CreateScrew(new Vector3(0.0f, 0.0f, 0.0f), carpet2, scene));
            fourthRoom.AddChild(carpet2);
            scene.AddGameObject(carpet2);

            GameObject carpet3 = new GameObject() //simulation of screws
            {
                Layer = PhysicLayer.Pullable
            };
            carpet3.Transform.Translate(new Vector3(-35.0f, -0.01f, 28.0f));
            carpet3.Transform.Scale(new Vector3(1.5f, 1.0f, 1.5f));
            carpet3.AddComponent(new BoxCollider(carpet3)
            {
                IsTrigger = true,
                Size = new Vector3(1.0f, 1.0f, 3.0f),
                Center = new Vector3(-0.25f, 0, 13)
            });
            carpet3.AddComponent(new NoiseGenerator(carpet3));
            carpet3.AddComponent(new SoundEmitter(carpet3, Sounds));
            carpet3.AddComponent(new ScrewsSimulation(carpet3));
            carpet3.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, carpet3, Models["ScrewCarpet"]));
            //carpet3.AddChild(CreateScrew(new Vector3(0.0f, 0.0f, 0.0f), carpet3, scene));
            fourthRoom.AddChild(carpet3);
            scene.AddGameObject(carpet3);

            #endregion


            scene.AddDefaultLights();


            return scene;
        }

        private Scene FifthScene(bool isDebug)
        {
            Scene scene = new Scene(game, isDebug);

            Physic.SetLayersCollisionInteraction(PhysicLayer.UserDefined_4, PhysicLayer.UserDefined_3, false);

            #region Camera

            Camera mainCamera = new Camera(new Vector3(0, 180, 0))
            {
                Transform =
                {
                    LocalPosition = new Vector3(-1.5f, 1.75f, 1.5f)
                }
            };

            var cameraController = new CameraController(mainCamera);
            if (GameManager.IsDebug)
            {
                cameraController.LockY = false;
                cameraController.MoveSpeed = 10f;
                cameraController.MouseRotationSpeed = 3f;
            }

            mainCamera.AddComponent(new PlayerAccessTracker(mainCamera)); //DO NOT REMOVE :)

            mainCamera.AddComponent(cameraController);
            mainCamera.AddComponent(new BoxCollider(mainCamera)
            {
                Size = new Vector3(0.5f, 0.5f, 0.5f),
                Center = new Vector3(0, -0.875f, 0)
            });
            mainCamera.AddComponent(new SphereCollider(mainCamera) {Radius = 2.5f, IsTrigger = true});
            mainCamera.AddComponent(new Rigidbody(mainCamera)
            {
                IsAffectedByGravity = false,
                IsStatic = false,
                IsKinematic = true
            });

            GameObject gravityGun = new GameObject
            {
                Transform = {LocalPosition = new Vector3(-0.9f, -0.5f, -0.6f)}
            };
            gravityGun.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, gravityGun, Models["gravityGun"]));
            gravityGun.AddComponent(new GravityGun(gravityGun));
            gravityGun.Transform.Rotate(new Vector3(90, 90, 0));
            gravityGun.Transform.Scale(new Vector3(2, 2, 2));
            mainCamera.AddChild(gravityGun);

            GameObject taser = new GameObject
            {
                Transform = {LocalPosition = new Vector3(0.3f, -0.5f, -1.1f)}
            };
            taser.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, taser, Models["taser"]));

            taser.AddComponent(new Taser(taser));
            taser.Transform.Rotate(new Vector3(0, 90, 180));
            taser.Transform.Scale(new Vector3(2, 2, 2));
            mainCamera.AddChild(taser);

            GameObject lure = new GameObject
            {
                Transform = {LocalPosition = new Vector3(0.2f, -0.3f, -0.6f)}
            };
            lure.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, lure, Models["lure"]));

            lure.Transform.Scale(new Vector3(1, 1, 1));

            mainCamera.AddChild(lure);

            GameObject cameraHolder = new GameObject();

            cameraHolder.AddChild(mainCamera);

            PlayerController playerController = new PlayerController(cameraHolder);
            playerController.GameObject.AddComponent(new NoiseGenerator(playerController.GameObject));


            cameraHolder.AddComponent(playerController);
            if (!GameManager.IsDebug)
            {
                playerController.IsEnabled = false;
            }

            var playerComponent = new Player(mainCamera);
            mainCamera.AddComponent(playerComponent);


            ///// WEAPON SELECTOR

            #endregion

            scene.AddGameObject(cameraHolder);

            #region Rooms

            #region Room_1

            GameObject roomObject = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject, Models["room1"]));
            roomObject.GetComponent<MeshRenderer>().CastsShadow = false;
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(0, -0.1f, 0),
                Size = new Vector3(50, 0.1f, 50)
            });
            roomObject.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(0, 2.7f, 0),
                Size = new Vector3(50, 0.1f, 50)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //right wall
            {
                Center = new Vector3(2f, 0, 6.5f),
                Size = new Vector3(1, 10, 25)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //front wall
            {
                Center = new Vector3(0, 0, -2),
                Size = new Vector3(10, 10, 2)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //left wall
            {
                Center = new Vector3(-5, 0, -1.0f),
                Size = new Vector3(2, 10, 10)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //backleft wall
            {
                Center = new Vector3(-5, 0, 15.0f),
                Size = new Vector3(2, 10, 16)
            });
            roomObject.AddComponent(new BoxCollider(roomObject) //back wall
            {
                Center = new Vector3(-1.0f, 0, 23.0f),
                Size = new Vector3(5, 10, 2)
            });
            roomObject.AddComponent(new Rigidbody(roomObject)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });

            GameObject player = new GameObject() {Transform = {LocalPosition = new Vector3(0, 0, 0)}}; //for tests

            scene.Player = cameraHolder;

            Room firstRoom = new Room() {model = Models["room1"], player = mainCamera};
            firstRoom.AddChild(roomObject);

            #endregion

            #region Room_2

            GameObject roomObject2 = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };
            roomObject2.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, roomObject2, Models["room2"]));
            roomObject2.GetComponent<MeshRenderer>().CastsShadow = false;
            roomObject2.AddComponent(new Rigidbody(roomObject2)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false,
            });
            roomObject2.AddComponent(new BoxCollider(roomObject) //MIDDLE
            {
                Center = new Vector3(-17.25f, 0.0f, 3),
                Size = new Vector3(19.0f, 10.0f, 3.5f)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(-17.25f, 0.0f, -2.0f),
                Size = new Vector3(25.0f, 10.0f, 1.0f)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(-5, 0, -1.0f),
                Size = new Vector3(2, 10, 10)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(-5, 0, 15.0f),
                Size = new Vector3(2, 10, 10)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject) // left from door
            {
                Center = new Vector3(-14.25f, 0.0f, 10.5f),
                Size = new Vector3(19.0f, 10.0f, 1.0f)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject)
            {
                Center = new Vector3(-30, 0, 5.0f),
                Size = new Vector3(2, 10, 12)
            });
            roomObject2.AddComponent(new BoxCollider(roomObject) // left from door
            {
                Center = new Vector3(-29.0f, 0.0f, 10.5f),
                Size = new Vector3(6.0f, 10.0f, 1.0f)
            });

            scene.AddGameObject(roomObject2);

            Room secondRoom = new Room() {model = Models["room2"], player = mainCamera};
            secondRoom.AddChild(roomObject2);

            #endregion

            #endregion

            scene.AddGameObject(roomObject);

            #region Doors

            GameObject doorsParent = new GameObject
            {
                Layer = PhysicLayer.Enviroment,
                //Transform = { LocalPosition = new Vector3(-4.5f, -1.0f, 5.5f) }
                Transform = {LocalPosition = new Vector3(0, 0, 0)}
            };

            GameObject doorsObject = new GameObject
            {
                Layer = PhysicLayer.Enviroment
            };
            doorsObject.Transform.Translate(new Vector3(-4.5f, -1.0f, 5.5f));
            doorsObject.Transform.Rotate(new Vector3(0, 90, 0));
            doorsObject.Transform.LocalScale = new Vector3(2, 3, 3);
            doorsObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsObject, Models["doors"]));
            doorsObject.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject.AddComponent(new BoxCollider(doorsObject)
            {
                Center = new Vector3(0.0f, 0.0f, 0.25f),
                Size = new Vector3(0.75f, 10f, 2.0f),
                IsTrigger = false
            });
            doorsParent.AddComponent(new SphereCollider(doorsParent)
            {
                Center = new Vector3(-4.5f, -1.0f, 5.5f),
                Radius = 2.5f,
                IsTrigger = true
            });
            doorsParent.AddComponent(new DoorsOpenClose(doorsParent) {accessLevelRequired = AccessLevel.Basic});
            doorsParent.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, doorsParent, Models["jamb1"]));
            doorsParent.GetComponent<MeshRenderer>().CastsShadow = false;
            doorsObject.AddComponent(new Rigidbody(doorsObject)
            {
                IsStatic = true,
                IsAffectedByGravity = false,
                IsKinematic = false
            });

            #region AnimationClips

            AnimationClip doorsOpenClip
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 3f),
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            AnimationClip doorsCloseClip
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 3f),
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        //new AnimationFrame
                        //{
                        //    Transform = new Transform(doorsObject)
                        //    {
                        //        LocalScale = doorsObject.Transform.LocalScale,
                        //        LocalPosition = doorsObject.Transform.LocalPosition + new Vector3(0, 0, 0.2f),
                        //        LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                        //    },
                        //    Time = TimeSpan.FromSeconds(0.5)
                        //},
                        new AnimationFrame
                        {
                            Transform = new Transform(doorsObject)
                            {
                                LocalScale = doorsObject.Transform.LocalScale,
                                LocalPosition = doorsObject.Transform.LocalPosition,
                                LocalEulerAngles = doorsObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0.7)
                        }
                    },
                };

            Animator doorsAnimator = new Animator(doorsObject) {IsLooped = false};
            doorsAnimator.AnimationClips.Add("Open", doorsOpenClip);
            doorsAnimator.AnimationClips.Add("Close", doorsCloseClip);
            doorsObject.AddComponent(doorsAnimator);

            doorsParent.AddChild(doorsObject);

            #endregion

            #region DoorsText

            GameObject doorsTextObject = new GameObject();

            doorsTextObject.AddComponent(
                new TextRenderer(doorsTextObject, SpriteFonts["TobagoPoster"])
                {
                    IsOverlay = false,
                    Text = "Doors",
                    Color = Color.Red,
                    Pivot = new Vector2(0.5f, 1)
                });
            doorsTextObject.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            doorsTextObject.Transform.Translate(new Vector3(-4, 2, 5.75f));
            doorsTextObject.Transform.Rotate(new Vector3(0, 90, 0));

            #endregion

            #endregion


            scene.AddGameObject(doorsParent);
            scene.AddGameObject(doorsTextObject);


            //firstRoom.AddChild(doorsObject);
            firstRoom.AddDoorsAndRoom(doorsParent, secondRoom);
            secondRoom.AddDoorsAndRoom(doorsParent, firstRoom);

            scene.AddRoom(firstRoom);
            scene.AddRoom(secondRoom);

            #region Card

            GameObject boxObject = new GameObject() {Layer = PhysicLayer.Enviroment};
            boxObject.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, boxObject, Models["box"]));
            boxObject.AddComponent(new BoxCollider(boxObject) {Size = new Vector3(20f, 20f, 20f), IsTrigger = true});
            boxObject.Transform.Translate(new Vector3(-1f, 1f, 20f));
            boxObject.Transform.Scale(new Vector3(0.5f, 1f, 0.1f));
            boxObject.AddComponent(new AccessCard(boxObject) {AccessLevel = AccessLevel.Medium, scene = scene});

            #endregion

            scene.AddGameObject(boxObject);


            #region karaczan - Off

            SkinningData skinningData = Models["karaczan"].Tag as SkinningData;
            GameObject pandaObject = new GameObject
            {
                Transform =
                {
                    LocalScale = new Vector3(0.02f, 0.02f, 0.02f),
                    LocalPosition = new Vector3(0, 0.3f, -5)
                }
            };
            pandaObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, pandaObject, Models["karaczan"]));

            AnimationClip pandaRotateClip
                = new AnimationClip
                {
                    AnimationFrames = new List<AnimationFrame>
                    {
                        new AnimationFrame
                        {
                            Transform = new Transform(pandaObject)
                            {
                                LocalScale = pandaObject.Transform.LocalScale,
                                LocalPosition = pandaObject.Transform.LocalPosition,
                                LocalEulerAngles = pandaObject.Transform.LocalEulerAngles
                            },
                            Time = TimeSpan.FromSeconds(0)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(pandaObject)
                            {
                                LocalScale = pandaObject.Transform.LocalScale,
                                LocalPosition = pandaObject.Transform.LocalPosition,
                                LocalEulerAngles = new Vector3(0, 180, 0)
                            },
                            Time = TimeSpan.FromSeconds(2.5)
                        },
                        new AnimationFrame
                        {
                            Transform = new Transform(pandaObject)
                            {
                                LocalScale = pandaObject.Transform.LocalScale,
                                LocalPosition = pandaObject.Transform.LocalPosition,
                                LocalEulerAngles = new Vector3(0, 360, 0)
                            },
                            Time = TimeSpan.FromSeconds(5)
                        }
                    },
                };

            Animator pandaAnimator = new Animator(pandaObject) {IsLooped = true};
            pandaAnimator.AnimationClips.Add("Rotate", pandaRotateClip);
            pandaAnimator.Play("Rotate");
            pandaObject.AddComponent(pandaAnimator);

            SkinnedAnimator skinnedPandaAnimator = new SkinnedAnimator(pandaObject) {IsLooped = true};
            skinnedPandaAnimator.InitializeTransforms(skinningData);
            skinnedPandaAnimator.Play("Take 001");
            pandaObject.AddComponent(skinnedPandaAnimator);

            #endregion

            scene.AddGameObject(pandaObject);


            #region YouDiedText

            GameObject youDiedTextObject = new GameObject();

            youDiedTextObject.AddComponent(new TextRenderer(youDiedTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Text = "You died",
                Pivot = new Vector2(0, 1)
            });
            youDiedTextObject.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            youDiedTextObject.Transform.Translate(
                new Vector3(200, GameManager.GraphicsDevice.Viewport.Height / 2.0f, 0));

            #endregion

            scene.AddGameObject(youDiedTextObject);


            #region PoisonCountText

            GameObject poisonCountTextObject = new GameObject();

            poisonCountTextObject.AddComponent(new TextRenderer(poisonCountTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Pivot = new Vector2(0, 1)
            });
            poisonCountTextObject.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));
            poisonCountTextObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width - 200, 30,
                0));

            #endregion

            scene.AddGameObject(poisonCountTextObject);

            #region YouWonText

            GameObject youWonTextObject = new GameObject();

            youWonTextObject.AddComponent(new TextRenderer(youWonTextObject, SpriteFonts["TobagoPoster"])
            {
                IsOverlay = true,
                Text = "You won",
                Pivot = new Vector2(0, 1)
            });
            youWonTextObject.Transform.Scale(new Vector3(0.5f, 0.5f, 0.5f));
            youWonTextObject.Transform.Translate(new Vector3(200, GameManager.GraphicsDevice.Viewport.Height / 2.0f,
                0));

            #endregion

            scene.AddGameObject(youWonTextObject);

            #region HUD

            GameObject hudObject = new GameObject();

            hudObject.AddComponent(new HeadUpDisplay(hudObject)
            {
                youDiedText = youDiedTextObject,
                youWonText = youWonTextObject,
                VentText = poisonCountTextObject,
                ventsPoisonedCountToWin = 1
            });

            #endregion

            scene.AddGameObject(hudObject);

            playerComponent.GameObject.GetComponent<Player>().headUpDisplay = hudObject.GetComponent<HeadUpDisplay>();


            #region Standing

            GameObject standingObject = new GameObject();

            standingObject.AddComponent(
                new ImageRenderer(standingObject, Textures2D["standing"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            standingObject.Transform.Scale(new Vector3(0.25f, 0.25f, 0.25f));
            standingObject.Transform.Translate(new Vector3(0, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            scene.AddGameObject(standingObject);


            #region Crouching

            GameObject crouchingObject = new GameObject();

            crouchingObject.AddComponent(
                new ImageRenderer(crouchingObject, Textures2D["crouching"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            crouchingObject.Transform.Scale(new Vector3(0.25f, 0.25f, 0.25f));
            crouchingObject.Transform.Translate(new Vector3(0, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            scene.AddGameObject(crouchingObject);


            #region Running

            GameObject runningObject = new GameObject();

            runningObject.AddComponent(
                new ImageRenderer(runningObject, Textures2D["running"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            runningObject.Transform.Scale(new Vector3(0.25f, 0.25f, 0.25f));
            runningObject.Transform.Translate(new Vector3(0, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            scene.AddGameObject(runningObject);


            #region PlayerStateIndicator

            GameObject playerStateIndicatorObject = new GameObject();

            playerStateIndicatorObject.AddComponent(
                new PlayerStateIndicator(playerStateIndicatorObject)
                {
                    crouchingImage = crouchingObject,
                    runningImage = runningObject,
                    walkingImage = standingObject,
                    PlayerState = playerController.MovementState
                }
            );

            #endregion

            scene.AddGameObject(playerStateIndicatorObject);

            // playerController.playerStateIndicator = playerStateIndicatorObject.GetComponent<PlayerStateIndicator>();


            #region Crosshair

            GameObject crosshairObject = new GameObject();

            crosshairObject.AddComponent(
                new ImageRenderer(crosshairObject, Textures2D["crosshair"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0.5f, 0.5f)
                });
            crosshairObject.Transform.Scale(new Vector3(0.005f, 0.005f, 0.005f));
            crosshairObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2.0f,
                GameManager.GraphicsDevice.Viewport.Height / 2.0f, 0));
            crosshairObject.AddComponent(
                new CrosshairTargetIndicator(crosshairObject));

            #endregion

            scene.AddGameObject(crosshairObject);

            #region Vent

            GameObject ventTextObject = new GameObject();

            ventTextObject.AddComponent(new TextRenderer(ventTextObject, SpriteFonts["TobagoPoster"])
            {
                IsEnabled = false,
                IsOverlay = true,
                Text = "VENT",
                Color = Color.Red,
                Pivot = new Vector2(0, 0)
            });
            ventTextObject.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));

            ventTextObject.Transform.Translate(new Vector3(GameManager.GraphicsDevice.Viewport.Width / 2,
                GameManager.GraphicsDevice.Viewport.Height / 2, 0));

            GameObject ventObject = new GameObject()
            {
                Layer = PhysicLayer.Enviroment
            };
            ventObject.Transform.Translate(new Vector3(-15, 0, 5));
            ventObject.Transform.Scale(new Vector3(0.5f, 1.0f, 0.5f));

            ventObject.Transform.Rotate(new Vector3(0.0f, 90, 0.0f));
            ventObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, ventObject, Models["Vent"]));
            ventObject.AddComponent(new SphereCollider(ventObject) {Radius = 1.0f, IsTrigger = true});
            ventObject.AddComponent(new Vent(ventObject)
            {
                ventText = ventTextObject,
                headUpDisplay = hudObject.GetComponent<HeadUpDisplay>()
            });

            #endregion

            scene.AddGameObject(ventObject);
            scene.AddGameObject(ventTextObject);


            #region WinComputer

            GameObject winComputerTextObject = new GameObject();

            winComputerTextObject.AddComponent(new TextRenderer(winComputerTextObject, SpriteFonts["TobagoPoster"])
            {
                IsEnabled = false,
                IsOverlay = true,
                Text = "WIN COMPUTER",
                Color = Color.Blue,
                Pivot = new Vector2(0, 0)
            });
            winComputerTextObject.Transform.Scale(new Vector3(0.075f, 0.075f, 0.075f));

            winComputerTextObject.Transform.Translate(new Vector3((GameManager.GraphicsDevice.Viewport.Width / 2 + 2),
                (GameManager.GraphicsDevice.Viewport.Height / 2 + 2), 0));

            GameObject winComputerObject = new GameObject()
            {
                Layer = PhysicLayer.UserDefined_3
            };
            winComputerObject.Transform.Translate(new Vector3(-20, 0, 5));
            winComputerObject.Transform.Scale(new Vector3(0.5f, 1.0f, 0.5f));
            winComputerObject.AddComponent(
                (MeshRenderer) Activator.CreateInstance(MeshRendererType, winComputerObject, Models["box"]));
            winComputerObject.AddComponent(new SphereCollider(winComputerObject) {Radius = 1.0f, IsTrigger = true});
            winComputerObject.AddComponent(
                new WinComputer(winComputerObject) {headUpDisplay = hudObject.GetComponent<HeadUpDisplay>()});

            #endregion

            scene.AddGameObject(winComputerObject);
            scene.AddGameObject(winComputerTextObject);


            #region ScrewCarpet 

            GameObject carpet = new GameObject() //simulation of screws
            {
                Layer = PhysicLayer.Pullable
            };
            carpet.Transform.Translate(new Vector3(-1.0f, 0f, 0.0f));
            carpet.Transform.Scale(new Vector3(1.5f, 1.0f, 1.5f));
            carpet.AddComponent(new BoxCollider(carpet)
            {
                IsTrigger = true,
                Size = new Vector3(6.0f, 1.0f, 4.0f),
                Center = new Vector3(0, 0, 13)
            });
            carpet.AddComponent(new NoiseGenerator(carpet));
            carpet.AddComponent(new ScrewsSimulation(carpet));
            carpet.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, carpet, Models["Screw"]));
            //carpet.AddChild(CreateScrew(new Vector3(0.0f, 0.0f, 0.0f), carpet, scene));

            #endregion

            scene.AddGameObject(carpet);


            //var triangles = navMeshModels["nav_room1"].GetTriangles().ToArray();

            //PremadeNavMesh navMeshTess = PremadeNavMesh.FromTriangles(triangles);

            //try
            //{
            //    //PremadeNavMesh.LoadBakedObj("nav_room1.obj", navMeshTess);
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //    //PremadeNavMesh.BakeToObj("nav_room1.obj", navMeshTess);
            //}

            //#region Enemy

            //GameObject enemyObject = new GameObject
            //{
            //    Transform =
            //    {
            //        LocalPosition = new Vector3(5.5f, 0, 20),
            //        LocalScale = new Vector3(0.02f, 0.02f, 0.02f)
            //    },
            //    Layer = PhysicLayer.Enemy
            //};
            //enemyObject.AddComponent((MeshRenderer)Activator.CreateInstance(meshRendererType, enemyObject, models["karaczan"]));
            //enemyObject.AddComponent(new SphereCollider(enemyObject)
            //{
            //    Center = new Vector3(0, 75f, 0),
            //    Radius = 37.5f,
            //    IsTrigger = true
            //});
            //enemyObject.AddComponent(new Rigidbody(enemyObject)
            //{
            //    IsStatic = false,
            //    IsAffectedByGravity = false,
            //    IsKinematic = true,
            //});
            //enemyObject.AddComponent(new NavAgent(enemyObject)
            //{
            //    PathCalculator = navMeshTess,
            //    Speed = 1.0f
            //});
            //enemyObject.AddComponent(new EnemyMovement(enemyObject)
            //{
            //    WanderRange = 6.5f
            //});
            //enemyObject.AddComponent(new Enemy(enemyObject)
            //{
            //    PlayerComponent = playerComponent
            //});

            //var enemyAnimator = new SkinnedAnimator(enemyObject) { IsLooped = true };
            //enemyAnimator.InitializeTransforms(skinningData);
            //enemyAnimator.Play("Take 001");
            //enemyObject.AddComponent(enemyAnimator);

            //#endregion

            //scene.AddGameObject(enemyObject);

            #region TempProgressBar

            GameObject tempProgressBar = new GameObject();

            tempProgressBar.AddComponent(
                new ImageRenderer(tempProgressBar, Textures2D["bar"])
                {
                    IsOverlay = true,
                    Pivot = new Vector2(0, 1)
                });
            tempProgressBar.AddComponent(new ProgressBar(tempProgressBar));
            tempProgressBar.Transform.Scale(new Vector3(0.025f, 0.01f, 0.025f));

            tempProgressBar.Transform.Translate(new Vector3(350, GameManager.GraphicsDevice.Viewport.Height, 0));

            #endregion

            scene.AddGameObject(tempProgressBar);

            scene.AddDefaultLights();

            return scene;
        }

        #endregion

        GameObject CreateScrew(Vector3 position, GameObject parent, Scene scene)
        {
            GameObject gameObject = new GameObject();
            gameObject.AddComponent((MeshRenderer) Activator.CreateInstance(MeshRendererType, parent, Models["Screw"]));
            gameObject.Transform.Translate(position);
            scene.AddGameObject(gameObject);
            return gameObject;
        }
    }
}
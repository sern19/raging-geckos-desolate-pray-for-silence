#include "Common.fx"

float2 GBufferTargetSize;

texture FinalMap;
texture DepthMap;

//Texture samplers
sampler Final = sampler_state
{
       texture = <FinalMap>;
       MINFILTER = LINEAR;
       MAGFILTER = LINEAR;
       MIPFILTER = LINEAR;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};

sampler Depth = sampler_state
{
       texture = <DepthMap>;
       MINFILTER = POINT;
       MAGFILTER = POINT;
       MIPFILTER = POINT;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};

//Structures
struct VSI
{
    float3 Position: POSITION0;
    float2 UV: TEXCOORD0;
};

struct VSO
{
    float4 Position: POSITION0; //Because it has to be float32
    float2 UV: TEXCOORD0;
};


struct PSO
{
    float4 Color: COLOR0;
    float Depth: DEPTH;
};

//Vertex Shader
VSO VS(VSI input)
{
    VSO output;
    
    output.Position = float4(input.Position, 1);
    output.UV = input.UV - float2(1.0f / GBufferTargetSize);
    
    return output;
}

PSO PS(VSO input)
{
    PSO output;
    
    output.Depth = tex2D(Depth, input.UV).r;
    output.Color = tex2D(Final, input.UV);
    
    return output;
}

//Technique
technique Default
{
    pass p0
    {
        VertexShader = compile vs_4_0_level_9_3 VS();
        PixelShader = compile ps_4_0_level_9_3 PS();
    }
}
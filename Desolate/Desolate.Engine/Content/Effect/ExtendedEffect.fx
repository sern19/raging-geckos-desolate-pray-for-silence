//Textures
texture Texture;
texture NormalMap;
texture SpecularMap;
texture TransparencyMap;

//Parameters
float3 DiffuseColor;
float3 SpecularColor;
float SpecularPower;
float Alpha;

//Texture samplers
sampler DiffuseSampler = sampler_state
{
       texture = <Texture>;
       MINFILTER = POINT;
       MAGFILTER = POINT;
       MIPFILTER = POINT;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};
sampler NormalSampler = sampler_state
{
       texture = <NormalMap>;
       MINFILTER = POINT;
       MAGFILTER = POINT;
       MIPFILTER = POINT;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};
sampler SpecularSampler = sampler_state
{
       texture = <SpecularMap>;
       MINFILTER = POINT;
       MAGFILTER = POINT;
       MIPFILTER = POINT;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};
sampler TransparencySampler = sampler_state
{
       texture = <TransparencyMap>;
       MINFILTER = POINT;
       MAGFILTER = POINT;
       MIPFILTER = POINT;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};

//Vertex shader
float4 VS(float3 Position: POSITION0): POSITION0
{
    return float4(Position, 1);
}

float4 PS(): COLOR0
{
    //Because monogame compiler removes unsued fields (thats good but not in our case)
    float4 output = float4(DiffuseColor, 1) * tex2D(NormalSampler, float2(0, 0)) * tex2D(DiffuseSampler, float2(0, 0)) * tex2D(SpecularSampler, float2(0, 0)) * 
    tex2D(TransparencySampler, float2(0, 0));
    return output;
}

//Technique
technique Default
{
    pass p0
    {
        VertexShader = compile vs_4_0_level_9_3 VS();
        PixelShader = compile ps_4_0_level_9_3 PS();
    }
}
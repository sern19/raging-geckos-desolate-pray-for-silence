#include "Common.fx"

float4x4 DLViewProjection;
float4x4 InverseViewProjection;
float4x4 InverseView;

float3 CameraPosition;

float3 DLDirection;
float3 DLColor;
float3 DLSpecularColor;
float DLIntensity;
float3 DLPosition;
float DepthPrecision;
float DepthBias;

bool CastsShadow;

//GBuffer info
float2 GBufferTargetSize;
float2 ShadowMapSize;

//Textures
texture GBuffer0Texture;
texture GBuffer1Texture;
texture GBuffer2Texture;
texture2D ShadowMapTexture;

//Texture samplers
sampler GBuffer0 = sampler_state
{
       texture = <GBuffer0Texture>;
       MINFILTER = LINEAR;
       MAGFILTER = LINEAR;
       MIPFILTER = LINEAR;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};
sampler GBuffer1 = sampler_state
{
       texture = <GBuffer1Texture>;
       MINFILTER = LINEAR;
       MAGFILTER = LINEAR;
       MIPFILTER = LINEAR;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};
sampler GBuffer2 = sampler_state
{
       texture = <GBuffer2Texture>;
       MINFILTER = POINT;
       MAGFILTER = POINT;
       MIPFILTER = POINT;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};
sampler ShadowMap = sampler_state
{
       texture = <ShadowMapTexture>;
       MINFILTER = POINT;
       MAGFILTER = POINT;
       MIPFILTER = POINT;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};
SamplerComparisonState cmpSampler : register(s0);

//Structures
struct VSI
{
    float3 Position: POSITION0;
    float2 UV: TEXCOORD0;
};

struct VSO
{
    float4 Position: POSITION0; //Because it has to be float32
    float2 UV: TEXCOORD0;
};

float4 Phong(float3 position3d, float3 normal, float specularIntensity, float specularPower)
{
    //Reflection vector
    float3 reflection = normalize(reflect(DLDirection, normal));
    
    //Eye vector
    float3 eye = normalize(CameraPosition - position3d);
    
    //Normal - Light vector
    float3 nl = dot(normal, -DLDirection);
    
    float3 H = normalize(eye - DLDirection);
    
    float3 diffuse = nl * DLColor;
    //float specular = specularIntensity * pow(saturate(dot(reflection, eye)), specularPower); //Phong 
    float specular = specularIntensity * pow(saturate(dot(normal, H)), specularPower); //Blinn
     
    return DLIntensity * float4(diffuse + DLSpecularColor * specular.xxx, 1);
}

//Vertex Shader
VSO VS(VSI input)
{
    VSO output;
    
    output.Position = float4(input.Position, 1);
    output.UV = input.UV - float2(1.0f / GBufferTargetSize);
    
    return output;
}

//Pixel shader
float4 PS(VSO input): COLOR0
{
    half4 encodedNormal = tex2D(GBuffer1, input.UV);
    half3 normal = mul(decode(encodedNormal.xyz), (float3x3)InverseView);
    
    float specularIntensity = tex2D(GBuffer0, input.UV).w;
    float specularPower = encodedNormal.w * 255;
    
    float4 position3d = 1.0f;
    
    float depth = manualLinearSampler(GBuffer2, input.UV, GBufferTargetSize).x;
        
    position3d.x = input.UV.x * 2.0f - 1.0f;
    position3d.y = -(input.UV.y * 2.0f - 1.0f);
    position3d.z = depth;

    //Transform Position from Homogenous Space to World Space
    position3d = mul(position3d, InverseViewProjection);
    
    position3d /= position3d.w;    
    
    float shadow = 1.0f;
    
    if (CastsShadow)
    {
        float4 lightScreenPos = mul(position3d, DLViewProjection);
        lightScreenPos /= lightScreenPos.w;
        lightScreenPos.x = lightScreenPos.x / 2.0f + 0.5f; 
        lightScreenPos.y = lightScreenPos.y / -2.0f + 0.5f; 
        lightScreenPos.z -= 0.05f;
        
        if ((saturate(lightScreenPos.x) == lightScreenPos.x) && (saturate(lightScreenPos.y) == lightScreenPos.y))
        {      
            //PCF sampling for shadow map
            float sum = 0;
            float x, y;
         
            //perform PCF filtering on a 4 x 4 texel neighborhood
            for (y = -1.5; y <= 1.5; y += 1.0)
            {
                for (x = -1.5; x <= 1.5; x += 1.0)
                {
                    sum += ShadowMapTexture.SampleCmpLevelZero(cmpSampler, lightScreenPos.xy + texOffset(x, y, ShadowMapSize), lightScreenPos.z);
                }
            }
            shadow = sum / 16.0f;
            //shadow = shadowTexture;//(shadowTexture * exp(-(DepthPrecision * 0.5f) * (shadowDepth - DepthBias)));
        }
    }
    
    return shadow * Phong(position3d.xyz, normal, specularIntensity, specularPower);
}

//Technique
technique Default
{
    pass p0
    {
        VertexShader = compile vs_4_0 VS();
        PixelShader = compile ps_4_0 PS();
    }
}
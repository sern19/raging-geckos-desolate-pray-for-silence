#include "Common.fx"

float2 GBufferTargetSize;

//Textures
texture DiffuseTexture;
texture LightmapTexture;

//Texture samplers
sampler Diffuse = sampler_state
{
       texture = <DiffuseTexture>;
       MINFILTER = LINEAR;
       MAGFILTER = LINEAR;
       MIPFILTER = LINEAR;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};
sampler Lightmap = sampler_state
{
       texture = <LightmapTexture>;
       MINFILTER = LINEAR;
       MAGFILTER = LINEAR;
       MIPFILTER = LINEAR;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};

//Structures
struct VSI
{
    float3 Position: POSITION0;
    float2 UV: TEXCOORD0;
};

struct VSO
{
    float4 Position: POSITION0; //Because it has to be float32
    float2 UV: TEXCOORD0;
};

//Vertex Shader
VSO VS(VSI input)
{
    VSO output;
    
    output.Position = float4(input.Position, 1);
    output.UV = input.UV - float2(1.0f / GBufferTargetSize);
    
    return output;
}

//Pixel shader
 float4 PS(VSO input): COLOR0
 {
    float3 color = tex2D(Diffuse, input.UV).xyz;
    float4 lightning = tex2D(Lightmap, input.UV);
    
    float4 output = float4(color * lightning.xyz, 1);
    
    return output;
 }
 
 //Technique
 technique Default
 {
     pass p0
     {
         VertexShader = compile vs_4_0_level_9_3 VS();
         PixelShader = compile ps_4_0_level_9_3 PS();
     }
 }
//Normal Encoding
half3 encode(half3 n)
{
    n = normalize(n);
    n.xyz = 0.5f * (n.xyz + 1.0f);
    return n; 
}

//Normal Decoding
half3 decode(half3 enc)
{
    return (2.0f * enc.xyz- 1.0f);
}

//Decode RGBA
float RGBADecode(float4 value)
{
    const float4 bits = float4(1.0 / (256.0 * 256.0 * 256.0),
                               1.0 / (256.0 * 256.0),
                               1.0 / 256.0,
                               1);
    return dot(value.xyzw , bits);
}

//Samplers
float4 manualLinearSampler(sampler Sampler, float2 UV, float2 textureSize)
{
    float2 texelpos = textureSize * UV;
    float2 lerps = frac(texelpos);
    float2 texelSize = 1.0 / textureSize;
       
    float4 sourcevals[4];
    
    sourcevals[0] = tex2D(Sampler, UV);
    sourcevals[1] = tex2D(Sampler, UV + float2(texelSize.x, 0)); 
    sourcevals[2] = tex2D(Sampler, UV + float2(0, texelSize.y)); 
    sourcevals[3] = tex2D(Sampler, UV + texelSize);
    
    float4 interpolated = lerp(lerp(sourcevals[0], sourcevals[1], lerps.x), lerp(sourcevals[2], sourcevals[3], lerps.x ), lerps.y);
    
    return interpolated;
}


float2 texOffset(int u, int v, float2 shadowMapSize)
{
    return float2(u * 1.0f / shadowMapSize.x, v * 1.0f / shadowMapSize.y);
}
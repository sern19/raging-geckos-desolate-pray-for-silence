#include "Common.fx"

float3 AColor;
float AIntensity;

//Vertex Shader
float4 VS(float3 input: POSITION0): POSITION0
{
    return float4(input, 1);
}

float4 PS(float4 input: POSITION0): COLOR0
{   
    return float4(AColor * AIntensity, 1);
}

//Technique
technique Default
{
    pass p0
    {
        VertexShader = compile vs_4_0_level_9_3 VS();
        PixelShader = compile ps_4_0_level_9_3 PS();
    }
}
//Deferred GBuffer shader

#include "Common.fx"

//Basic parameters
float4x4 World;
float4x4 View;
float4x4 Projection;
float4x4 WorldInverseTranspose;

//Textures
texture Texture;
texture NormalMap;
texture SpecularMap;

bool UseDiffuseMap;
bool UseNormalMap;
bool UseSpecularMap;

//Parameters
float3 DiffuseColor;
float SpecularIntensity;
float SpecularPower;

//Texture samplers
sampler DiffuseSampler = sampler_state
{
       texture = <Texture>;
       MINFILTER = LINEAR;
       MAGFILTER = LINEAR;
       MIPFILTER = LINEAR;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};
sampler NormalSampler = sampler_state
{
       texture = <NormalMap>;
       MINFILTER = LINEAR;
       MAGFILTER = LINEAR;
       MIPFILTER = LINEAR;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};
sampler SpecularSampler = sampler_state
{
       texture = <SpecularMap>;
       MINFILTER = LINEAR;
       MAGFILTER = LINEAR;
       MIPFILTER = LINEAR;
       ADDRESSU = WRAP;
       ADDRESSV = WRAP;
};

//Structures
struct VSI
{
    float4 Position: POSITION0;
    float3 Normal: NORMAL0;
    float2 UV: TEXCOORD0;
    float3 Tangent: TANGENT0;
    float3 BiTangent: BINORMAL0;
};
struct VSO
{
    float4 Position: POSITION0;
    float2 UV: TEXCOORD0;
    float3 Depth: TEXCOORD1;
    float3x3 TBN: TEXCOORD2; //Tangent Bitangent Normal
};
struct PSO
{
       float4 Diffuse: COLOR0;
       float4 Normal: COLOR1;
       float4 Depth: COLOR2;
};

//Vertex shader
VSO VS(VSI input)
{
    VSO output;
    
    //Transform position
    float4 world = mul(input.Position, World);
    float4 view = mul(world, View);
    output.Position = mul(view, Projection);
    
    //Depth
    output.Depth.x = output.Position.z;
    output.Depth.y = output.Position.w;
    output.Depth.z = view.z;
    
    //TBN
    output.TBN[0] = normalize(mul(input.Tangent, (float3x3)WorldInverseTranspose));
    output.TBN[1] = normalize(mul(input.BiTangent, (float3x3)WorldInverseTranspose));
    output.TBN[2] = normalize(mul(input.Normal, (float3x3)WorldInverseTranspose));
    
    //UV
    output.UV = input.UV;
    
    return output;
}

//Pixel Shader
PSO PS(VSO input)
{
    PSO output;
    
    //Diffuse
    if (UseDiffuseMap)
        output.Diffuse = float4(DiffuseColor, 1.0f) * tex2D(DiffuseSampler, input.UV);
    else
        output.Diffuse = float4(DiffuseColor, 1.0f);
        
    //Normal
    if (UseNormalMap)
    {
        half3 normal = tex2D(NormalSampler, input.UV).xyz * 2.0f -1.0f;
        
        //Transform to WVP
        normal = normalize(mul(normal, input.TBN));
        
        output.Normal.xyz = encode(normal);
    }
    else
        output.Normal.xyz = encode(normalize(input.TBN[2]));
        
    //Depth
    output.Depth = input.Depth.x / input.Depth.y; //Screen space
    output.Depth.g = input.Depth.z; //View space
    
    //Specular
    if (UseSpecularMap)
        output.Diffuse.w = tex2D(SpecularSampler, input.UV).x;
    else 
        output.Diffuse.w = SpecularIntensity;
    output.Normal.w = SpecularPower;
    
    return output;
}


//Technique
technique Default
{
    pass p0
    {
        VertexShader = compile vs_4_0_level_9_3 VS();
        PixelShader = compile ps_4_0_level_9_3 PS();
    }
}
//Deferred GBuffer shader

#include "Common.fx"

#define SKINNED_EFFECT_MAX_BONES   72
float4x3 Bones[SKINNED_EFFECT_MAX_BONES];

//Basic parameters
float4x4 World;
float4x4 View;
float4x4 Projection;

float3 LPosition;

float DepthPrecision;

//Structures
struct VSI
{
    float4 Position: POSITION0;
    int4 Indices: BLENDINDICES0; 
    float4 Weights: BLENDWEIGHT0; 
};
struct VSO
{
    float4 Position: POSITION0;
    float4 WorldPosition: TEXCOORD0;
};

//Skinning
void Skin(inout VSI vin, uniform int boneCount)
{
    float4x3 skinning = 0;

    [unroll]
    for (int i = 0; i < boneCount; i++)
    {
        skinning += Bones[vin.Indices[i]] * vin.Weights[i];
    }

    vin.Position.xyz = mul(vin.Position, skinning);
}

//Vertex shader
VSO VS(VSI input)
{
    VSO output;
    
    Skin(input, 4);
    
    //Transform position
    float4 world = mul(input.Position, World);
    float4 view = mul(world, View);
    output.Position = mul(view, Projection);
    
    output.WorldPosition = output.Position;
    
    return output;
}

//Pixel Shader
float4 PS(VSO input): COLOR0
{
    //To float3
    input.WorldPosition /= input.WorldPosition.w;
    
    //Calculate depth from light
    float depth = input.WorldPosition.z;//max(0.01f, length(LPosition - input.WorldPosition.xyz))/DepthPrecision;
    
    //Encode
    return depth;//exp((DepthPrecision * 0.5f) * depth);
}


//Technique
technique Default
{
    pass p0
    {
        VertexShader = compile vs_4_0_level_9_3 VS();
        PixelShader = compile ps_4_0_level_9_3 PS();
    }
}
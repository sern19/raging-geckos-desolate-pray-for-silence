//Structures
struct PSO
{
       float4 Diffuse : COLOR0;
       float4 Normal : COLOR1;
       float4 Depth : COLOR2;
};

//Vertex shader
float4 VS(float3 Position: POSITION0): POSITION0
{
    return float4(Position, 1);
}

PSO PS()
{
    PSO output;
    
    output.Diffuse = 0;
    
    output.Normal.xyz = 0.5f;
    output.Normal.w = 0;
    
    output.Depth = 1;
    
    return output;
}

//Technique
technique Default
{
    pass p0
    {
        VertexShader = compile vs_4_0_level_9_3 VS();
        PixelShader = compile ps_4_0_level_9_3 PS();
    }
}
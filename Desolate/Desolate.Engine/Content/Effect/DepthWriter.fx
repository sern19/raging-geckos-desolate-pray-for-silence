//Deferred GBuffer shader

#include "Common.fx"

//Basic parameters
float4x4 World;
float4x4 View;
float4x4 Projection;

float3 LPosition;

float DepthPrecision;

//Structures
struct VSI
{
    float4 Position: POSITION0;
};
struct VSO
{
    float4 Position: POSITION0;
    float4 WorldPosition: TEXCOORD0;
};

//Vertex shader
VSO VS(VSI input)
{
    VSO output;
    
    //Transform position
    float4 world = mul(input.Position, World);
    float4 view = mul(world, View);
    output.Position = mul(view, Projection);
    
    output.WorldPosition = output.Position;
    
    return output;
}

//Pixel Shader
float4 PS(VSO input): COLOR0
{
    //To float3
    input.WorldPosition /= input.WorldPosition.w;
    
    //Calculate depth from light
    float depth = input.WorldPosition.z;//max(0.01f, length(LPosition - input.WorldPosition.xyz))/DepthPrecision;
    
    //Encode
    return depth;//exp((DepthPrecision * 0.5f) * depth);
}


//Technique
technique Default
{
    pass p0
    {
        VertexShader = compile vs_4_0_level_9_3 VS();
        PixelShader = compile ps_4_0_level_9_3 PS();
    }
}
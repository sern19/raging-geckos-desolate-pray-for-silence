﻿#define DEFERRED //Comment to use Forward rendering - as for now forward is not implemented

using System;
using Desolate.Core;
using Desolate.Engine.Managers;
using Desolate.Engine.Managers.Abstract;

namespace Desolate.Engine
{
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            ISceneLoader sceneLoader = new SceneManager();
            IContentLoader contentLoader = new ContentLoader();
#if DEFERRED
            GameManager.IsDeferred = true;
#endif
#if DEBUG
            using (var game = new KaraczanGame(sceneLoader, contentLoader, true))
#else
            using (var game = new KaraczanGame(sceneLoader, contentLoader))
#endif
                game.Run();
        }
    }
#endif
}
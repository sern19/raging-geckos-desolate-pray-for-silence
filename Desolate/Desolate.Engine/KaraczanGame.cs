﻿using System.Linq;
using Desolate.Core;
using Desolate.Core.Components;
using Desolate.Core.Components.Physics;
using Desolate.Engine.Managers.Abstract;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Desolate.Engine
{
    public class KaraczanGame : Game
    {
        private Scene currentScene;
        private readonly ISceneLoader sceneLoader;
        private readonly IContentLoader contentLoader;
        private GraphicsDeviceManager graphicsDeviceManager;
        private bool isDebugMode;

        public KaraczanGame(ISceneLoader sceneLoader, IContentLoader contentLoader, bool isDebugMode = false)
        {
            graphicsDeviceManager = new GraphicsDeviceManager(this);
            graphicsDeviceManager.PreferredBackBufferWidth = 1920;
            graphicsDeviceManager.PreferredBackBufferHeight = 1080;

            Content.RootDirectory = "Content";
            sceneLoader.Init(this);
            contentLoader.Init(Content);

            //Support for hardware PCF
            graphicsDeviceManager.GraphicsProfile = GraphicsProfile.HiDef;
            
            this.sceneLoader = sceneLoader;
            this.contentLoader = contentLoader;
            this.isDebugMode = isDebugMode;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
            if (GameManager.IsDebug)
                TooglePlayerCollisions();
            Physic.SetLayersCollisionInteraction(PhysicLayer.Enviroment, PhysicLayer.Enviroment, false);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            GameManager.SpriteBatch = new SpriteBatch(GraphicsDevice);

            LoadScene(4);
        }

        private void LoadScene(int sceneId)
        {
            contentLoader.LoadContent(sceneId);
            currentScene = sceneLoader.LoadScene(sceneId, isDebugMode);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            Content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (Keyboard.GetState().IsKeyDown(Keys.F5))
            {
                currentScene = sceneLoader.ReloadCurrentScene(isDebugMode);
            }

            HandleInputForDebug(currentScene);

            currentScene.Update(gameTime);
        }

        private static void HandleInputForDebug(Scene scene)
        {
            if (!GameManager.IsDebug)
                return;

            if ((Keyboard.GetState().IsKeyDown(Keys.F3)))
                TooglePlayerCollisions();

            var moveSpeed = Scene.MainCamera.GetComponent<CameraController>().MoveSpeed;

            if ((Keyboard.GetState().IsKeyDown(Keys.Add)))
                Scene.MainCamera.GetComponent<CameraController>().MoveSpeed =
                    MathHelper.Clamp(moveSpeed + 1, 0, float.MaxValue);

            if ((Keyboard.GetState().IsKeyDown(Keys.Subtract)))
                Scene.MainCamera.GetComponent<CameraController>().MoveSpeed =
                    MathHelper.Clamp(moveSpeed - 1, 0, float.MaxValue);
        }


        private static void TooglePlayerCollisions()
        {
            foreach (var collider in Scene.MainCamera.GetComponents<Collider>().Where(c => !c.IsTrigger))
            {
                collider.IsEnabled = !collider.IsEnabled;
            }
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
            GraphicsDevice.Clear(Color.CornflowerBlue);
            currentScene.Draw();
        }
    }
}
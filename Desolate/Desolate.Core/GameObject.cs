﻿using System;
using System.Collections.Generic;
using System.Linq;
using Desolate.Core.Components;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Audio;
using Desolate.Core.Components.Physics;
using Desolate.Core.Components.Rendering;
using Desolate.Core.Scripts;
using Desolate.Core.Scripts.Noise;
using Desolate.Core.Scripts.Weapons;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core
{
    public class GameObject
    {
        public static implicit operator bool(GameObject instance) => instance != null;

        public PhysicLayer Layer { get; set; } = PhysicLayer.Default;

        private HashSet<Collider> currentCollisions = new HashSet<Collider>();
        private HashSet<Collider> currentTriggers = new HashSet<Collider>();
        private bool isActive = true;

        #region Properties

        public GameObject Parent { get; set; }

        public List<GameObject> Children { get; } = new List<GameObject>();

        public List<SingleAttitude> Components { get; } = new List<SingleAttitude>();

        public bool IsActive
        {
            get => isActive;
            set
            {
                isActive = value;
                foreach (GameObject child in Children)
                    child.IsActive = isActive;
            }
        }

        public Transform Transform { get; private set; }

        #endregion

        public GameObject()
        {
            Transform = new Transform(this);
        }

        public T GetComponent<T>() where T : SingleAttitude => (T) Components.FirstOrDefault(c => c is T);
        public IEnumerable<T> GetComponents<T>() where T : SingleAttitude => Components.FindAll(c => c is T).Cast<T>();

        public T GetComponentInChildren<T>() where T : SingleAttitude
        {
            if (GetComponent<T>() is T component)
            {
                return component;
            }

            if (!Children.Any())
                return null;

            foreach (GameObject child in Children)
            {
                if (child.GetComponentInChildren<T>() is T componentInChildren)
                    return componentInChildren;
            }

            return null;
        }

        public IEnumerable<T> GetComponentsInChildren<T>() where T : SingleAttitude
        {
            List<T> listOfComponents = GetComponents<T>().ToList();

            if (!Children.Any())
                return listOfComponents;

            foreach (GameObject child in Children)
            {
                listOfComponents.AddRange(child.GetComponentsInChildren<T>());
            }

            return listOfComponents;
        }

        public IEnumerable<GameObject> GetAllChildren()
        {
            List<GameObject> gameObjects = new List<GameObject> {this};
            if (Children.Any())
            {
                foreach (GameObject child in Children)
                {
                    gameObjects.AddRange(child.GetAllChildren());
                }
            }

            return gameObjects;
        }

        public void AddChild(GameObject gameObject)
        {
            gameObject.Initialize();
            gameObject.Parent = this;
            Children.Add(gameObject);
        }

        public void AddComponent(SingleAttitude component)
        {
            component.Initialize();
            Components.Add(component);
        }

        internal virtual void Initialize()
        {
            //TODO Something better
            if (Transform == null)
                Transform = new Transform(this);

            foreach (GameObject child in Children)
            {
                child.Initialize();
            }

            foreach (SingleAttitude component in Components)
            {
                component.Initialize();
            }
        }

        public virtual void Update()
        {
            if (!IsActive)
                return;

            foreach (GameObject child in Children)
            {
                child.Update();
            }

            foreach (SingleAttitude component in Components)
            {
                if (component.IsEnabled)
                    component.Update();
            }
        }

        public virtual void LateUpdate()
        {
            if (!IsActive)
                return;

            foreach (GameObject child in Children)
            {
                child.LateUpdate();
            }

            foreach (SingleAttitude component in Components)
            {
                if (component.IsEnabled)
                    component.LateUpdate();
            }
        }

        public virtual void FixedUpdate()
        {
            if (!IsActive)
                return;
            foreach (var collision in currentCollisions)
            {
                Components.ForEach(c => c.OnCollisionStay(collision));
            }

            foreach (var trigger in currentTriggers)
            {
                Components.ForEach(c => c.OnTriggerStay(trigger));
            }

            foreach (GameObject child in Children)
            {
                child.FixedUpdate();
            }

            foreach (SingleAttitude component in Components)
            {
                component.FixedUpdate();
            }
        }

        public void DrawForShadows()
        {
            if (!IsActive)
                return;
            IEnumerable<MeshRenderer> renderers = GetComponents<MeshRenderer>();
            foreach (MeshRenderer renderer in renderers)
            {
                if (renderer.IsEnabled && renderer.CastsShadow)
                    renderer.DrawForShadows();
            }

            foreach (GameObject gameObject in Children)
            {
                gameObject.DrawForShadows();
            }
        }

        public void Draw()
        {
            if (!IsActive)
                return;
            IEnumerable<Renderer> renderers = GetComponents<Renderer>();
            foreach (Renderer renderer in renderers)
            {
                if (renderer.IsEnabled)
                    renderer.Draw();
            }

            foreach (GameObject gameObject in Children)
            {
                gameObject.Draw();
            }
        }

        public void LateDraw()
        {
            if (!IsActive)
                return;
            IEnumerable<Renderer> renderers = GetComponents<Renderer>();
            foreach (Renderer renderer in renderers)
            {
                if (renderer.IsEnabled)
                    renderer.LateDraw();
            }

            foreach (GameObject gameObject in Children)
            {
                gameObject.LateDraw();
            }

            //Reset states changed by SpriteBatch
            GameManager.GraphicsDevice.BlendState = BlendState.Opaque;
            GameManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GameManager.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
        }

        internal void AddCollision(Collider collider, bool isTrigger = false)
        {
            if (!isTrigger)
            {
                if (currentCollisions.Add(collider))
                    Components.ForEach(c => c.OnCollisionEnter(collider));
                else
                    Components.ForEach(c => c.OnCollisionStay(collider));
            }
            else
            {
                if (currentTriggers.Add(collider))
                    Components.ForEach(c => c.OnTriggerEnter(collider));
                else
                    Components.ForEach(c => c.OnTriggerStay(collider));
            }
        }

        internal void RemoveCollision(Collider collider, bool isTrigger = false)
        {
            if (!isTrigger)
            {
                if (currentCollisions.Remove(collider))
                    Components.ForEach(c => c.OnCollisionExit(collider));
            }
            else
            {
                if (currentTriggers.Remove(collider))
                    Components.ForEach(c => c.OnTriggerExit(collider));
            }
        }

        public GameObject Instantiate(Vector3 position, Scene scene, Model model,
            Dictionary<string, SoundEffect> sounds, bool isLure)
        {
            if (isLure)
            {
                GameObject newGameObject = (GameObject) Activator.CreateInstance(typeof(GameObject));
                newGameObject.AddComponent(
                    (MeshRenderer) Activator.CreateInstance(typeof(DeferredMeshRenderer), newGameObject, model));
                NoiseGenerator noiseGenerator =
                    (NoiseGenerator) Activator.CreateInstance(typeof(NoiseGenerator), new object[] {newGameObject});
                newGameObject.AddComponent(
                    (SoundEmitter) Activator.CreateInstance(typeof(SoundEmitter), newGameObject, sounds));
                newGameObject.AddComponent(
                    (LureNoise) Activator.CreateInstance(typeof(LureNoise), newGameObject, scene, noiseGenerator));

                newGameObject.AddComponent(noiseGenerator);
                newGameObject.Transform.Translate(position);
                scene.AddGameObjectInRuntime(newGameObject);
                newGameObject.Initialize();
                return newGameObject;
            }
            else
            {
                GameObject newGameObject = (GameObject) Activator.CreateInstance(typeof(GameObject));
                newGameObject.AddComponent(
                    (MeshRenderer) Activator.CreateInstance(typeof(DeferredMeshRenderer), newGameObject, model));
                NoiseGenerator noiseGenerator =
                    (NoiseGenerator) Activator.CreateInstance(typeof(NoiseGenerator), new object[] {newGameObject});
                newGameObject.AddComponent(
                    (SoundEmitter) Activator.CreateInstance(typeof(SoundEmitter), newGameObject, sounds));
                newGameObject.AddComponent(
                    (Screw) Activator.CreateInstance(typeof(Screw), newGameObject, noiseGenerator));
                newGameObject.AddComponent(
                    (BoxCollider) Activator.CreateInstance(typeof(BoxCollider), new object[] {newGameObject}));
                newGameObject.GetComponent<BoxCollider>().Size = new Vector3(0.1f, 0.1f, 0.1f);
                newGameObject.AddComponent(
                    (Rigidbody) Activator.CreateInstance(typeof(Rigidbody), new object[] {newGameObject}));
                newGameObject.GetComponent<Rigidbody>().IsKinematic = false;
                newGameObject.GetComponent<Rigidbody>().IsAffectedByGravity = true;
                newGameObject.GetComponent<Rigidbody>().Bounciness = 1;

                newGameObject.AddComponent(noiseGenerator);
                newGameObject.Transform.Translate(position);
                scene.AddGameObjectInRuntime(newGameObject);
                newGameObject.Initialize();
                return newGameObject;
            }
        }
    }
}
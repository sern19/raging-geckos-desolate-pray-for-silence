﻿using System;
using System.Collections.Generic;
using System.Linq;
using Desolate.Core.AI.Navigation;
using Microsoft.Xna.Framework;

namespace Desolate.Core.AI.PathFinding
{
    public class AstarSolver : IPathCalculator
    {
        private NavMesh navMesh;
        private List<double> distanceCosts = new List<double>();
        private Path currentPath;
        private Vector3 currentVertex;
        private PathSolution pathSolution;

        public AstarSolver(NavMesh _navMesh)
        {
            navMesh = _navMesh;
        }

        public NavPath Calculate(Vector3 startingPosition, Vector3 targetPosition)
        {
            Vector3 targetPositionZeroY = targetPosition;
            targetPositionZeroY.Y = 0;
            targetPositionZeroY = navMesh.ClampToNodeRange(targetPositionZeroY);
            Path startingPath = new Path(startingPosition);
            pathSolution = new PathSolution();
            var targetVertex = navMesh.GetNearestNode(targetPositionZeroY);
            Proceed(startingPath, targetVertex);
            if (!pathSolution.IsFound)
                return null;
            pathSolution.FinalPath.AddNode(targetPositionZeroY);
            return pathSolution.FinalPath;
        }

        private void Proceed(Path startingPath, Vector3 targetPosition)
        {
            Queue<Path> queue = new Queue<Path>();
            queue.Enqueue(startingPath);

            while (queue.Count != 0)
            {
                currentPath = queue.Dequeue();
                var x = currentVertex;
                currentVertex = currentPath.CurrentPosition;
                pathSolution.Visited.Add(currentPath);

                Vector3[] neighbours = navMesh.GetNeighbours(currentVertex)
                    // .Where(n => !(n.X == x.X && n.Y == x.Y && n.Z == x.Z))
                    .ToArray();
                //if (neighbours.Length == 0)
                //    neighbours = new[] {x};

                distanceCosts = new List<double>();

                List<Path> possibleNewPaths = new List<Path>();

                foreach (var neighbour in neighbours)
                {
                    var newPath = new Path(neighbour);
                    foreach (var node in currentPath.PathPositions)
                    {
                        newPath.AddNode(node);
                    }

                    newPath.AddNode(neighbour);

                    if (Vector3.Distance(newPath.CurrentPosition, targetPosition) < 0.1f)
                    {
                        pathSolution.FinalPath = newPath;
                        pathSolution.IsFound = true;
                        return;
                    }

                    if (pathSolution.Visited.SelectMany(v => v.PathPositions).Any(el => el == neighbour)) continue;
                    possibleNewPaths.Add(newPath);
                    pathSolution.Visited.Add(newPath);
                    distanceCosts.Add(RemainingDistance(newPath.CurrentPosition, targetPosition) +
                                      pathSolution.MovesCount);
                }

                List<double> costsSorted = new List<double>(distanceCosts.Count);
                costsSorted.AddRange(distanceCosts);
                costsSorted.Sort();

                var abc = possibleNewPaths.Select((t, i) => (t, distanceCosts[i])).OrderBy(a => a.Item2).ToList();

                foreach (var valueTuple in abc)
                {
                    queue.Enqueue(valueTuple.Item1);
                }
//
//                int index = 0;
//                while (index < distanceCosts.Count)
//                {
//                    double value = costsSorted[index];
//
//                    for (int i = 0; i < possibleNewPaths.Count; i++)
//                    {
//                        if (distanceCosts[i] == value)
//                        {
//                            queue.Enqueue(possibleNewPaths[i]);
//                            index++;
//                        }
//                    }
//                }

                pathSolution.Processed.Add(currentPath);
            }
        }

        private double RemainingDistance(Vector3 currentPosition, Vector3 targetPosition)
        {
            return Math.Sqrt((targetPosition.X - currentPosition.X) * (targetPosition.X - currentPosition.X)
                             + (targetPosition.Y - currentPosition.Y) * (targetPosition.Y - currentPosition.Y)
                             + (targetPosition.Z - currentPosition.Z) * (targetPosition.Z - currentPosition.Z));
        }
    }
}
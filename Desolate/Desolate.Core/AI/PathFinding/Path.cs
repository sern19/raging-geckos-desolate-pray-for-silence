﻿using System.Runtime.Serialization;
using Microsoft.Xna.Framework;

namespace Desolate.Core.AI.PathFinding
{
    [DataContract(Name = nameof(NavPath))]
    public class Path : NavPath
    {
        public Path(Vector3 currentVertex)
        {
            CurrentPosition = currentVertex;
        }

        public Vector3 CurrentPosition { get; set; }
    }
}
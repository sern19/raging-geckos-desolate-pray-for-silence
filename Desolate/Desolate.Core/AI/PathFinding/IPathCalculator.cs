﻿using Microsoft.Xna.Framework;

namespace Desolate.Core.AI.PathFinding
{
    public interface IPathCalculator
    {
        NavPath Calculate(Vector3 startingPosition, Vector3 targetPosition);
    }
}
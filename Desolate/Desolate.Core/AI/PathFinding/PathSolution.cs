﻿using System.Collections.Generic;

namespace Desolate.Core.AI.PathFinding
{
    public class PathSolution
    {
        public NavPath FinalPath { get; set; }
        public List<Path> Visited { get; set; } = new List<Path>();
        public List<Path> Processed { get; set; } = new List<Path>();
        public bool IsFound { get; set; } = false;
        public int MovesCount { get; internal set; }
    }
}

﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using Microsoft.Xna.Framework;

namespace Desolate.Core.AI.PathFinding
{
    public enum NavPathStatus
    {
        Complete,
        InProgress,
        Invalid,
    }

    [DataContract]
    [KnownType(typeof(NavPath))]
    public class NavPath
    {
        private HashSet<Vector3> pathPositions = new HashSet<Vector3>();
        public NavPathStatus Status { get; set; }

        [DataMember] public Vector3[] PathPositions => pathPositions.ToArray();

        public void AddNode(Vector3 node)
        {
            node.Y = 0;
            pathPositions.Add(node);
        }
    }
}
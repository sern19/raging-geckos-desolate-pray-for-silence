﻿using Desolate.Core.AI.PathFinding;
using Microsoft.Xna.Framework;

namespace Desolate.Core.AI.Navigation
{
    public abstract class NavMesh : IPathCalculator
    {
        public abstract Vector3[] GetNeighbours(Vector3 position);
        //public Vector3[] GetNeighbours(Vertex vertex) => GetNeighbours(vertex.Index);
        
        public abstract Vector3 GetNearestNode(Vector3 position);

        public abstract Vector3 ClampToNodeRange(Vector3 position);

        public abstract NavPath Calculate(Vector3 startingPosition, Vector3 targetPosition);
        
        public abstract bool IsBaked { get; }

    }
}
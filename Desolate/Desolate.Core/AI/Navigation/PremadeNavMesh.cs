﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Xml;
using Desolate.Core.AI.PathFinding;
using Desolate.Core.Components.Rendering.Helpers;
using Desolate.Core.Extensions;
using Microsoft.Xna.Framework;
using Path = Desolate.Core.AI.PathFinding.Path;

namespace Desolate.Core.AI.Navigation
{
    public class PremadeNavMesh : NavMesh
    {
        public static PremadeNavMesh FromTriangles(Triangle[] triangles, bool autobake = false)
        {
            PremadeNavMesh instance = new PremadeNavMesh(triangles);
            if (autobake)
                BakeToObj($"NavMesh_{triangles.GetHashCode()}.obj", instance);
            return instance;
        }

        private IPathCalculator pathCalculator;

        // instances of premade nav mesh should be created via static factory method
        private PremadeNavMesh(Triangle[] triangles)
        {
            this.triangles = triangles;
            vertices = triangles
                .SelectMany(t => t.GetVertices())
                .DistinctBy(v => v.Index)
                .ToArray();
            nodes = triangles.Select(t => t.Center).ToArray();
            pathCalculator = new AstarSolver(this);
        }

        private Triangle[] triangles;
        private Vector3[] nodes;
        private Vertex[] vertices;
        private NavPath[] prebakedPaths;

        public override Vector3[] GetNeighbours(Vector3 point)
        {
            try
            {
                var triangle = triangles.First(t => t.Cointans(point));

                //Finds triangles containing given vertex
                var neighbourTriangles = triangles
                    .Where(t => t.Touch(triangle))
                    .ToArray();

                return neighbourTriangles.Select(t => t.Center).Distinct().ToArray();
            }
            catch (Exception e)
            {
                return new Vector3[] {GetNearestNode(point)};
            }
        }

        public override Vector3 GetNearestNode(Vector3 position)
        {
            //Finds a vertex with minumum distance to given point
            return nodes.Aggregate((result, current) =>
                (Vector3.DistanceSquared(current, position) <
                 Vector3.DistanceSquared(result, position))
                    ? current
                    : result);
        }

        public override Vector3 ClampToNodeRange(Vector3 position)
        {
            //Finds a vertex with minumum distance to given point
            var nearestNode = GetNearestNode(position);
            var containingTriangle = triangles.FirstOrDefault(t => t.Center == nearestNode);
            return triangles.Any(t=>t.Cointans(position)) ? position : nearestNode;
        }

        public override NavPath Calculate(Vector3 startingPosition, Vector3 targetPosition)
        {
            if (prebakedPaths is null)
                return pathCalculator.Calculate(startingPosition, targetPosition);

            var a = GetNearestNode(startingPosition);
            var b = GetNearestNode(targetPosition);

            // var path = prebakedPaths.FirstOrDefault(p =>
            //    p.PathVertexes[0].Index == a.Index
            //    && p.PathVertexes.Last().Index == b.Index);

            return //path ?? 
                pathCalculator.Calculate(startingPosition, targetPosition);
        }

        public override bool IsBaked => prebakedPaths?.Any() ?? false;

        public static void BakeToObj(string path, PremadeNavMesh navMesh)
        {
            if (!path.EndsWith(".obj"))
                throw new ArgumentException("Path is an invalid .obj file name.", nameof(path));

            if (!navMesh.IsBaked)
                BakePaths(navMesh);

            var serializer = new DataContractSerializer(typeof(NavPath[]), knownTypes: new[] {typeof(Path)});
            using (var fs = new FileStream(path, FileMode.Create, FileAccess.Write))
            using (GZipStream zipStream = new GZipStream(fs, CompressionMode.Compress, false))
            using (var writer = XmlDictionaryWriter.CreateBinaryWriter(zipStream))
            {
                serializer.WriteObject(writer,
                    navMesh.prebakedPaths);
            }
        }

        private static void BakePaths(PremadeNavMesh navMesh)
        {
            List<NavPath> list = (from t1 in navMesh.triangles
                from vertex in t1.GetVertices()
                from vertex2 in navMesh.triangles.SelectMany(t => t.GetVertices())
                select navMesh.Calculate(vertex.Position, vertex2.Position)).ToList();

            navMesh.prebakedPaths = list.Where(p => !(p is null)).ToArray();
        }

        public static void LoadBakedObj(string path, PremadeNavMesh navMesh)
        {
            if (!File.Exists(path) || !path.EndsWith(".obj"))
                throw new ArgumentException("Path does not contain valid .obj file.", nameof(path));

            DataContractSerializer deserializer = new DataContractSerializer(typeof(NavPath[]));
            using (FileStream stream = new FileStream(path, FileMode.Open))
            using (GZipStream zip = new GZipStream(stream, CompressionMode.Decompress, true))
            using (XmlDictionaryReader reader = XmlDictionaryReader.CreateBinaryReader(zip,
                XmlDictionaryReaderQuotas.Max))
            {
                if (!(deserializer.ReadObject(reader) is NavPath[] navMeshPrebakedPaths))
                    throw new FileLoadException($"Could not paths from file: {path}.");

                navMesh.prebakedPaths = navMeshPrebakedPaths.Where(p => !(p is null)).ToArray();
            }
        }
    }
}
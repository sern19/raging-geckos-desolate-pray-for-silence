﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Rendering;

namespace Desolate.Core.Scripts.UI
{
    public class ProgressBar : SingleAttitude
    {
        private ImageRenderer imageRenderer;

        public ProgressBar(GameObject gameObject) : base(gameObject)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
            imageRenderer = GetComponent<ImageRenderer>();
            imageRenderer.Filling = 0;
        }

        public override void Update()
        {
            base.Update();
            imageRenderer.Filling += Time.DeltaTime;
            if (imageRenderer.Filling >= 1)
                imageRenderer.Filling = 0;
        }
    }
}
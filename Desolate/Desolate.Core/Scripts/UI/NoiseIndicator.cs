﻿using System;
using System.Collections.Generic;
using System.Linq;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Rendering;
using Desolate.Core.Extensions;
using Desolate.Core.Scripts.Noise;
using Desolate.Core.Scripts.PlayerBehaviour;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Scripts.UI
{
    public enum Direction
    {
        Up = 0,
        UpRight = 1,
        Right = 2,
        DownRight = 3,
        Down = 4,
        DownLeft = 5,
        Left = 6,
        UpLeft = 7
    }

    public class NoiseIndicator : SingleAttitude
    {
        class Noise
        {
            public NoiseGenerator Source;
            public Direction Direction;
            public float Value;
            public float TimeToOff;
        }

        public GameObject PlayerGameObject { get; set; }
        public ImageRenderer[] FillingBars { get; set; } = new ImageRenderer[8];
        private float[] fillingValues = new float[8];

        private List<Noise> activeNoises = new List<Noise>();

        private const float NoiseDurationInSeconds = 0.5f;
        private Random random = new Random();

        private float localTimer = 0.0f;

        public NoiseIndicator(GameObject gameObject) : base(gameObject)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
            for (var index = 0; index < FillingBars.Length; index++)
            {
                var imageRenderer = FillingBars[index];
                fillingValues[index] = imageRenderer.Filling;
            }

            localTimer = 0.0f;
        }

        private Vector3 GetDirectionVector(Direction direction)
        {
            Vector3 directionVector;
            switch (direction)
            {
                case Direction.Up:
                    return Scene.MainCamera.Transform.WorldForward;
                case Direction.UpRight:
                    directionVector = Vector3.Lerp(Scene.MainCamera.Transform.WorldForward,
                        Scene.MainCamera.Transform.WorldRight, 0.5f);
                    directionVector.Normalize();
                    return directionVector;
                case Direction.Right:
                    return Scene.MainCamera.Transform.WorldRight;
                case Direction.DownRight:
                    directionVector = Vector3.Lerp(Scene.MainCamera.Transform.WorldBackward,
                        Scene.MainCamera.Transform.WorldRight, 0.5f);
                    directionVector.Normalize();
                    return directionVector;
                case Direction.Down:
                    return Scene.MainCamera.Transform.Backward;
                case Direction.DownLeft:
                    directionVector = Vector3.Lerp(Scene.MainCamera.Transform.WorldBackward,
                        Scene.MainCamera.Transform.WorldLeft, 0.5f);
                    directionVector.Normalize();
                    return directionVector;
                case Direction.Left:
                    return Scene.MainCamera.Transform.WorldLeft;
                case Direction.UpLeft:
                    directionVector = Vector3.Lerp(Scene.MainCamera.Transform.WorldForward,
                        Scene.MainCamera.Transform.WorldLeft, 0.5f);
                    directionVector.Normalize();
                    return directionVector;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }
        }

        public void IndicateNoise(NoiseGenerator source, float loudness)
        {
            float[] angleToSource = new float[8];

            if (source.GetComponentInChildren<PlayerController>())
            {
                angleToSource = Enumerable.Repeat(1.0f, angleToSource.Length).ToArray();
            }
            else
            {
                for (var index = 0; index < angleToSource.Length; index++)
                {
                    var toSourceDirection = source.Transform.WorldPosition - Scene.MainCamera.Transform.WorldPosition;
                    toSourceDirection.Y = 0;
                    toSourceDirection.Normalize();

                    angleToSource[index] = Vector3Ext.Angle(GetDirectionVector((Direction) index),
                        toSourceDirection);
                }

                double dataMax = angleToSource.Max();
                double dataMin = angleToSource.Min();
                double range = dataMax - dataMin;

                angleToSource = angleToSource
                    .Select(d => (float) (Math.Abs((d - dataMax) / range)))
                    .ToArray();
            }

//            var sum = angleToSource.Sum();
//            angleToSource = angleToSource.Select(d => d / sum).ToArray();

            for (var index = 0; index < FillingBars.Length; index++)
            {
                Direction direction = (Direction) index;

                if (activeNoises.Exists(n => n.Source == source && n.Direction == direction))
                    continue;

                float localLoudness = angleToSource[index] > 0.75
                    ? loudness
                    : angleToSource[index] > 0.75
                        ? loudness * 0.5f
                        : loudness * angleToSource[index];

                var imageRenderer = FillingBars[index];
                imageRenderer.Filling += localLoudness;
                fillingValues[(int) direction] = imageRenderer.Filling;
                activeNoises.Add(new Noise()
                {
                    Source = source,
                    Direction = direction,
                    Value = localLoudness,
                    TimeToOff = localTimer + NoiseDurationInSeconds
                });
            }
        }

        public override void Update()
        {
            base.Update();
            localTimer += Time.DeltaTime;

            for (var index = 0; index < FillingBars.Length; index++)
            {
                var imageRenderer = FillingBars[index];
                var fillingValue = fillingValues[index];

                imageRenderer.Filling =
                    MathHelper.Clamp(imageRenderer.Filling + (float) (random.NextDouble() * (0.1) - 0.05),
                        fillingValue - 0.05f,
                        fillingValue + 0.05f);
            }
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();


            if (!activeNoises.Any())
            {
                for (var index = 0; index < FillingBars.Length; index++)
                {
                    FillingBars[index].Filling = 0;
                    fillingValues[index] = 0;
                }

                return;
            }

            for (var index = 0; index < activeNoises.Count; index++)
            {
                var noiseToCalm = activeNoises[index];
                if (localTimer >= noiseToCalm.TimeToOff)
                {
                    var imageRenderer = FillingBars[(int) noiseToCalm.Direction];
                    imageRenderer.Filling = fillingValues[(int) noiseToCalm.Direction] - noiseToCalm.Value;
                    fillingValues[(int) noiseToCalm.Direction] = imageRenderer.Filling;
                    activeNoises.Remove(noiseToCalm);
                }
            }
        }
    }
}
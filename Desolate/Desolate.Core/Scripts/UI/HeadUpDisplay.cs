﻿using System.Collections.Generic;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Scripts.UI
{
    public class HeadUpDisplay : SingleAttitude
    {

        public GameObject youDiedText;
        public GameObject VentText;
        public GameObject VentUI;
        public List<Texture2D> ventImages;

        private int poisonCount = 0;
        public int ventsPoisonedCountToWin = 3;

        public GameObject youWonText;

        public HeadUpDisplay(GameObject gameObject) : base(gameObject)
        {
        }

        public override void Initialize()
        {
            VentText.GetComponent<TextRenderer>().Color = new Color(new Vector3(66/255.0f, 182/255.0f, 255/255.0f));
            VentText.GetComponent<TextRenderer>().Text = "Vents";
            youDiedText.IsActive = false;
            youWonText.IsActive = false;
        }

        public void DisplayDeathScreen()
        {
            Time.TimeScale = 0;
            youDiedText.IsActive = true;
        }
        public void UpdatePoisonCount()
        {
            poisonCount++;
            VentUI.GetComponent<ImageRenderer>().Image = ventImages[poisonCount-1];
        }
        public int GetPoisonCount()
        {
            return poisonCount;
        }
        public void DisplayWinScreen()
        {
            Time.TimeScale = 0;
            youWonText.IsActive = true;
        }

    }
}

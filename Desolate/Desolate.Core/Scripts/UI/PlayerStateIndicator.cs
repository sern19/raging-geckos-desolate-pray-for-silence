﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Scripts.PlayerBehaviour;

namespace Desolate.Core.Scripts.UI
{
    public enum PlayerStateUI
    {
        Crouching,
        Standing
    }

    public class PlayerStateIndicator : SingleAttitude
    {
        public GameObject crouchingImage;
        public GameObject walkingImage;
        public GameObject runningImage;

        private MovementState playerState;

        public PlayerStateIndicator(GameObject gameObject) : base(gameObject)
        {
        }

        public MovementState PlayerState
        {
            get
            {
                return playerState;
            }
            set
            {
                playerState = value;

                switch (value)
                {
                    case MovementState.Crouching:
                        crouchingImage.IsActive = true;
                        walkingImage.IsActive = false;
                        runningImage.IsActive = false;
                        break;
                    case MovementState.Walking:
                        crouchingImage.IsActive = false;
                        walkingImage.IsActive = true;
                        runningImage.IsActive = false;
                        break;
                    case MovementState.Running:
                        crouchingImage.IsActive = false;
                        walkingImage.IsActive = false;
                        runningImage.IsActive = true;
                        break;
                }
            }
        }



        public override void Initialize()
        {
            PlayerState = MovementState.Walking;
        }
    }
}

﻿using System;
using Desolate.Core.AI.PathFinding;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.AI;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Scripts.Enemy
{
    public class EnemyMovement : SingleAttitude
    {
        public EnemyMovement(GameObject gameObject) : base(gameObject)
        {
        }

        public override void Initialize()
        {
            NavAgent = GameObject.GetComponent<NavAgent>();
            spawnPosition = Transform.WorldPosition; // will be used after searching and following implemented
            initialSpeed = NavAgent.Speed; // will be used to speed up when following player

            GoToRandomLocation();
        }

        public NavAgent NavAgent { get; set; }

        public float WanderRange
        {
            get => wanderRange;
            set => wanderRange = value;
        }

        private float wanderRange = 50.0f;
        private Vector3 spawnPosition;
        private float initialSpeed;

        private float followingMultiplier = 2.0f;
        private Vector3 randomRotation;
        private Vector3 startingRotation;
        private float timeRotation = 0.0f;
        private bool multiplySpeed = false;

        public Vector3 StartingRotation
        {
            get { return startingRotation; }
            set
            {
                startingRotation = value;
                randomRotation = new Vector3(0, RandomFromRange(-80, 80), 0) + startingRotation;
                timeRotation = 0.0f;
            }
        }

        public bool MultiplySpeed
        {
            get { return multiplySpeed; }
            set
            {
                multiplySpeed = value;
                NavAgent.Speed = multiplySpeed ? (initialSpeed * followingMultiplier) : initialSpeed;
            }
        }

        public float CalculateLengthToLocation(Vector3 location)
        {
            float length = 0.0f;
            bool shouldDisableMovement = false;
            NavPath navPath = new NavPath();

            if (!NavAgent.IsEnabled)
            {
                StartMovement();
                shouldDisableMovement = true;
            }

            if (NavAgent.CalculatePath(location, out navPath))
            {
                if (shouldDisableMovement)
                    StopMovement();
                if (navPath.Status != NavPathStatus.Complete)
                    return float.MaxValue;
                for (int i = 0; i < navPath.PathPositions.Length - 1; i++)
                {
                    length += Vector3.Distance(navPath.PathPositions[i], navPath.PathPositions[i + 1]);
                }
            }
            else
                return float.MaxValue;

            return length;
        }

        public void GoToRandomLocation(float shrinkRatio = 1.0f)
        {
            if (!IsEnabled)
                return;
            if (!NavAgent.IsEnabled)
                StartMovement();

            Vector3 randomLocation = InsideUnitSphere() * WanderRange / shrinkRatio + spawnPosition;
            randomLocation.Y = GameObject.Transform.WorldPosition.Y;

            //We dont want to wander outside of range, so we recalculate road
            if (CalculateLengthToLocation(randomLocation) > WanderRange)
            {
                //GoToRandomLocation();
                //return;
            }

            NavAgent.SetDestination(randomLocation);
        }

        public void GoToLocation(Vector3 location)
        {
            if (!NavAgent.IsEnabled)
                StartMovement();

            NavAgent.SetDestination(location);
        }

        public void GoToSpawnLocation()
        {
            if (!IsEnabled)
                return;
            if (!NavAgent.IsEnabled)
                StartMovement();
            NavAgent.SetDestination(spawnPosition);
        }

        public bool HasReachedDestination()
        {
            //So if the road is no longer valid enemy will go out of state
            if (!NavAgent.IsEnabled || !IsEnabled || NavAgent.Path.Status != NavPathStatus.Complete)
                return true;

            return (NavAgent.RemainingDistance < 0.1f);
        }

        public void Search()
        {
            if (!IsEnabled)
                IsEnabled = true;

            float angleDrag = 180 * Time.DeltaTime * (NavAgent.AngularSpeed);
            Transform.Rotate(new Vector3(0, angleDrag, 0));

            //if (Quaternion.Angle(transform.rotation, Quaternion.Euler(randomRotation)) < 5)
            //    StartingRotation = transform.rotation.eulerAngles;
            //timeRotation += Time.DeltaTime * (NavAgent.AngularSpeed / 360.0f);
            //GameObject.Transform.LocalRotation  = Quaternion.Lerp(Quaternion.Euler(startingRotation), Quaternion.Euler(randomRotation), timeRotation);
        }

        public void StartMovement()
        {
            if (!IsEnabled)
                IsEnabled = true;
            NavAgent.IsEnabled = true;
        }

        public void StopMovement()
        {
            if (!IsEnabled)
                IsEnabled = true;
            NavAgent.IsEnabled = false;
        }

        private Vector3 InsideUnitSphere()
        {
            Vector3 randomPoint = new Vector3();
            Random rand = new Random();
            randomPoint.X = rand.Next(-100, 100) / 100.0f;
            randomPoint.Y = rand.Next(-100, 100) / 100.0f;
            randomPoint.Z = rand.Next(-100, 100) / 100.0f;
            return randomPoint;
        }

        private float RandomFromRange(int min, int max)
        {
            Random rand = new Random();
            return rand.Next(min * 100, max * 100) / 100.0f;
        }
    }
}
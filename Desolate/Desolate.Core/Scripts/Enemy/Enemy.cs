﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Physics;
using Desolate.Core.Components.Rendering;
using Desolate.Core.Scripts.PlayerBehaviour;
using Desolate.Core.Components.Audio;
using Desolate.Core.Components.Rendering.Animation;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Scripts.Enemy

{
    public class Enemy : SingleAttitude
    {
        public enum EnemyState
        {
            Waiting,
            Wandering,
            Returning,
            Triggered,
            Following,
            Searching,
            Paralyzed
        }

        public enum EnemyType
        {
            BaseEnemy,
            InvisibleEnemy
        }

        private Player playerComponent;
        private float playerSearchRange = 5;
        private float playerSearchAngle;
        private float playerSearchTime = 10.0f;
        private float playerSearchTimer = 0.0f;

        private EnemyState initialState;
        private EnemyState currentState;
        private EnemyType typeOfEnemy;

        private EnemyMovement movementComponent;

        private float paralyzeTimer = 0.0f;
        private float timeOfBlink = 0.5f;
        private float breakBetweenBlinks = 3.0f;
        private float timeForCalculation = 3.0f;
        private bool stateOfBlink = false;
        private bool canDisappear = true;

        private float temporaryFixByTimer = 0.5f;
        private bool temporaryFix = false;

        private MeshRenderer meshRenderer;

        public Enemy(GameObject gameObject) : base(gameObject)
        {
        }

        public EnemyState CurrentState
        {

            get { return currentState; }
            set
            {
                currentState = value;
                switch (currentState)
                {
                    case (EnemyState.Returning):
                        GameObject.GetComponent<SoundEmitter>().Play("idleA",false);
                        movementComponent.GoToSpawnLocation();
                        //meshRenderer.material.color = Color.white;
                        break;
                    case (EnemyState.Wandering):
                        if (temporaryFix)
                        {
                            GameObject.GetComponent<SoundEmitter>().Play("idleA", false);
                        }
                        movementComponent.GoToRandomLocation();
                        //meshRenderer.material.color = Color.white;
                        break;
                    case (EnemyState.Triggered):
                        GameObject.GetComponent<SoundEmitter>().Play("triggerA",false);
                        movementComponent.StartMovement();
                        //meshRenderer.material.color = new Color(1.0f, 0.7f, 0.0f);
                        break;
                    case (EnemyState.Following):
                        GameObject.GetComponent<SoundEmitter>().Play("idleA", false);
                        movementComponent.StartMovement();
                        //meshRenderer.material.color = Color.red;
                        break;
                    case (EnemyState.Searching):
                        GameObject.GetComponent<SoundEmitter>().Play("triggerA", false);
                        movementComponent.StopMovement();
                        movementComponent.StartingRotation = GameObject.Transform.WorldEulerAngles;
                        // meshRenderer.material.color = Color.yellow;
                        break;
                    case (EnemyState.Paralyzed):
                        GameObject.GetComponent<SoundEmitter>().Play("paralyzedA",false);
                        movementComponent.StopMovement();
                        //meshRenderer.material.color = Color.blue;
                        break;
                    case (EnemyState.Waiting):
                        movementComponent.StopMovement();
                        // meshRenderer.material.color = Color.white;
                        break;
                }
            }
        }

        public float PlayerSearchRange
        {
            get { return playerSearchRange; }
        }

        public float PlayerSearchAngle
        {
            get { return playerSearchAngle; }
        }

        public PlayerBehaviour.Player PlayerComponent
        {
            get { return playerComponent; }
            set { playerComponent = value; }
        }

        //private void PlayAudioClip(AudioClip audioClip, bool loop = false, bool forceChange = false)
        //{
        //    if (audioSource.clip == audioClip)
        //        return;

        //    audioSource.Stop();
        //    audioSource.loop = loop;
        //    audioSource.clip = audioClip;
        //    audioSource.Play();
        //}

        //Maybe we should ignore floor layer - consider this if causes issues
        bool IsPlayerInRange()
        {
            //If player is in range
            if (Vector3.Distance(GameObject.Transform.WorldPosition, playerComponent.GameObject.Transform.WorldPosition) <= playerSearchRange)
            {
                //We check if is in vision angle
                Vector3 direction = (playerComponent.GameObject.Transform.WorldPosition -
                                     GameObject.Transform.WorldPosition);
                direction.Normalize();
                direction.Y = 0f;
                //if (Math.Abs(Vector3Ext.Angle(GameObject.Transform.Forward, direction)) <= playerSearchAngle / 2.0f)
               // {
                    //Final step is to check if nothing is on the way
                    var rayDirection =
                        (playerComponent.GameObject.Transform.WorldPosition - GameObject.Transform.WorldPosition);
                    rayDirection.Normalize();
                    Ray shootRay = new Ray
                    {
                        Position = GameObject.Transform.WorldPosition,
                        Direction = rayDirection
                    };

                    if (shootRay.Intersects(
                        playerComponent.GetComponentInChildren<SphereCollider>().BoundingSphere) is float distance)
                    {
                        return distance <= PlayerSearchRange;
                        //  }
                    }
            }

            return false;
        }

        public void Paralyze(float duration)
        {
            paralyzeTimer = duration;
            CurrentState = EnemyState.Paralyzed;
            if (GameObject.GetComponent<SkinnedAnimator>() != null)
            {
                GameObject.GetComponent<SkinnedAnimator>().Play("shout");
                GameObject.GetComponent<SkinnedAnimator>().IsLooped = false;
            }
        }

        private void SwitchInvisibility(bool visible)
        {
            //if (!visible)
            //{
            //    Color fixedAlpha = meshRenderer.material.color;
            //    fixedAlpha.A = 10;
            //    meshRenderer.material.color = fixedAlpha;
            //}
            //else
            //{
            //    Color fixedAlpha = meshRenderer.material.color;
            //    fixedAlpha.A = 100;
            //    meshRenderer.material.color = fixedAlpha;
            //}
        }

        private void Blink()
        {
            timeForCalculation -= Time.DeltaTime;
            if (timeForCalculation <= 0.0f)
            {
                if (stateOfBlink)
                {
                    timeForCalculation = breakBetweenBlinks;
                    canDisappear = true;
                    //meshRenderer.enabled = false;
                    SwitchInvisibility(false);
                    //
                    stateOfBlink = false;
                }
                else
                {
                    //PlayAudioClip(idleAudioClip);
                    timeForCalculation = timeOfBlink;
                    canDisappear = false;
                    //meshRenderer.enabled = true;
                    SwitchInvisibility(true);
                    //
                    stateOfBlink = true;
                }
            }
        }

        public override void OnTriggerEnter(Collider other)
        {
            if (other.GameObject == playerComponent.GameObject && currentState != EnemyState.Paralyzed)
            {
                Vector3 direction = (playerComponent.GameObject.Transform.WorldPosition -
                                     GameObject.Transform.WorldPosition);
                direction.Normalize();

                var rayDirection = (playerComponent.GameObject.Transform.WorldPosition -
                                    GameObject.Transform.WorldPosition);
                rayDirection.Normalize();
                Ray shootRay = new Ray
                {
                    Position = GameObject.Transform.WorldPosition,
                    Direction = rayDirection
                };

                if (shootRay.Intersects(
                    playerComponent.GetComponent<SphereCollider>().BoundingSphere) is float distance)
                {
                    if (distance <= Vector3.Distance(playerComponent.Transform.WorldPosition, Transform.WorldPosition))
                    {
                        playerComponent.Die();
                    }
                }
            }
        }

        public override void Initialize()
        {
            movementComponent = GameObject.GetComponent<EnemyMovement>();
            initialState = EnemyState.Wandering;
            //audioSource = GetComponent<AudioSource>();
            meshRenderer = GameObject.GetComponent<MeshRenderer>();
            if (!playerComponent)
            {
                //Debug.LogWarning(gameObject + " doesn't have player component set. Component will be disabled!");
                IsEnabled = false;
            }
            //if (idleAudioClip == null)
            //    Debug.LogWarning($"{nameof(idleAudioClip)}  is not initialized.", this);

            //if (movementAudioClip == null)
            //    Debug.LogWarning($"{nameof(movementAudioClip)} is not initialized.", this);

            //if (paralyzedAudioClip == null)
            //    Debug.LogWarning($"{nameof(paralyzedAudioClip)} is not initialized.", this);

            //if (triggerAudioClip == null)
            //    Debug.LogWarning($"{nameof(triggerAudioClip)} is not initialized.", this);
            
            CurrentState = initialState;
        }

        public override void Update()
        {
            if (!temporaryFix)
            {
                temporaryFixByTimer -= Time.DeltaTime;
                if (temporaryFixByTimer <= 0.0f)
                {
                    temporaryFix = true;
                }
            }

            GameObject.GetComponent<SoundEmitter>().Play("movementA", true);



            if (typeOfEnemy == EnemyType.InvisibleEnemy)
            {
                Blink();
            }

            switch (currentState)
            {
                case (EnemyState.Waiting):
                    if (typeOfEnemy == EnemyType.InvisibleEnemy && canDisappear)
                        SwitchInvisibility(false);
                    break;
                case (EnemyState.Wandering):
                    if (typeOfEnemy == EnemyType.InvisibleEnemy && canDisappear)
                        SwitchInvisibility(false);
                    if (movementComponent.HasReachedDestination())
                    {
                        if (GameObject.GetComponent<SkinnedAnimator>() != null)
                        {
                            GameObject.GetComponent<SkinnedAnimator>().Play("walk");
                        }
                        movementComponent.GoToRandomLocation();
                    }
                    break;
                case (EnemyState.Triggered):
                    if (typeOfEnemy == EnemyType.InvisibleEnemy)
                        SwitchInvisibility(true);
                    if (movementComponent.HasReachedDestination())
                    {
                        if (GameObject.GetComponent<SkinnedAnimator>() != null)
                        {
                            GameObject.GetComponent<SkinnedAnimator>().Play("idle");
                        }
                        CurrentState = EnemyState.Searching;
                    }
                    break;
                case (EnemyState.Following):
                    if (typeOfEnemy == EnemyType.InvisibleEnemy)
                        SwitchInvisibility(true);
                    if (!IsPlayerInRange())
                    {
                       if (GameObject.GetComponent<SkinnedAnimator>() != null)
                        {
                            GameObject.GetComponent<SkinnedAnimator>().Play("run");
                        }
                        CurrentState = EnemyState.Triggered;
                    }

                    if (!movementComponent.MultiplySpeed)
                        movementComponent.MultiplySpeed = true;
                    movementComponent.GoToLocation(playerComponent.GameObject.Transform.WorldPosition);
                    break;
                case (EnemyState.Searching):
                    if (typeOfEnemy == EnemyType.InvisibleEnemy)
                        SwitchInvisibility(true);
                    playerSearchTimer += Time.DeltaTime;
                    if (playerSearchTimer >= playerSearchTime)
                    {
                        playerSearchTimer = 0.0f;
                        if (GameObject.GetComponent<SkinnedAnimator>() != null)
                        {
                            GameObject.GetComponent<SkinnedAnimator>().Play("walk");
                        }
                        CurrentState = EnemyState.Returning;
                    }
                    else
                        movementComponent.Search();

                    break;
                case (EnemyState.Paralyzed):
                    if (typeOfEnemy == EnemyType.InvisibleEnemy)
                        SwitchInvisibility(true);
                    paralyzeTimer -= Time.DeltaTime;
                    if (paralyzeTimer < 0.0f)
                    {
                        paralyzeTimer = 0.0f;
                        if (GameObject.GetComponent<SkinnedAnimator>() != null)
                        {
                            GameObject.GetComponent<SkinnedAnimator>().Play("walk");
                        }
                        CurrentState = EnemyState.Searching;
                    }

                    break;
                case (EnemyState.Returning):
                    if (typeOfEnemy == EnemyType.InvisibleEnemy && canDisappear)
                        SwitchInvisibility(false);
                    if (movementComponent.HasReachedDestination())
                        CurrentState = initialState;
                    break;
            }

            if (currentState != EnemyState.Following && currentState != EnemyState.Paralyzed && IsPlayerInRange())
            {
                if (GameObject.GetComponent<SkinnedAnimator>() != null)
                {
                    GameObject.GetComponent<SkinnedAnimator>().Play("run");
                }
                CurrentState = EnemyState.Following;
            }
        }
    }
}
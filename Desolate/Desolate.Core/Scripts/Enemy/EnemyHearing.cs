﻿using Desolate.Core.Components.Abstract;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Scripts.Enemy
{
    public class EnemyHearing : SingleAttitude
    {
        public EnemyHearing(GameObject gameObject) : base(gameObject)
        {
        }

        public float HearingRangeMultiplier { get; } = 5.0f;

        private Enemy enemyComponent;
        private EnemyMovement movementComponent;

        public void FollowNoiseSource(Vector3 noiseSource, float noiseRange)
        {
            if (IsEnabled && enemyComponent.CurrentState != Enemy.EnemyState.Paralyzed &&
                enemyComponent.CurrentState != Enemy.EnemyState.Following)
            {
                if (movementComponent.CalculateLengthToLocation(noiseSource) <= noiseRange * HearingRangeMultiplier &&
                    enemyComponent.CurrentState != Enemy.EnemyState.Triggered &&
                    enemyComponent.CurrentState != Enemy.EnemyState.Following)
                {
                    enemyComponent.CurrentState = Enemy.EnemyState.Triggered;
                    movementComponent.GoToLocation(noiseSource);
                }
            }
        }

        public override void Initialize()
        {
            enemyComponent = GetComponent<Enemy>();
            movementComponent = GetComponent<EnemyMovement>();
        }
    }
}
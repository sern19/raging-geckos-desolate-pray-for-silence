﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Physics;
using Desolate.Core.Scripts.PlayerBehaviour;
using Desolate.Core.Scripts.UI;
using Microsoft.Xna.Framework.Input;

namespace Desolate.Core.Scripts
{
    public class WinComputer : SingleAttitude
    {
        public WinComputer(GameObject gameObject) : base(gameObject) { }
        //public Slider winComputerSlider;
        private float timer = 0;
        public float timeToWin = 1;
        private KeyboardState keyboardState;

        public HeadUpDisplay headUpDisplay;
        
        //public Text winComputerNotEnoughText;

        //public IEnumerable<GameObject> vents;

        //private void Start()
        //{
        //    //winComputerNotEnoughText.gameObject.SetActive(false);
        //    //winComputerSlider.gameObject.SetActive(false);
        //    //winComputerSlider.maxValue = timeToWin;
        //    //vents = FindObjectsOfType<Vent>();
        //}

        public override void Update()
        {
            base.Update();

            keyboardState = Keyboard.GetState();
        }

        public override void OnTriggerEnter(Collider collider)
        {
            //PlayerMapController playerMapController = collider.GetComponent<PlayerMapController>();
            //if (playerMapController)
            //{
            //    playerMapController.ShowMap(5);
            //    foreach (var vent in vents)
            //    {
            //        vent.GetComponentInChildren<MinimapIndicator>().Blink();
            //    }
            //}

            OnTriggerStay(collider);
        }

        public override void OnTriggerStay(Collider collider)
        {
            if (headUpDisplay.GetPoisonCount() >= headUpDisplay.ventsPoisonedCountToWin && collider.GameObject.Parent.GetComponentInChildren<PlayerController>() != null)
            {
                //winComputerSlider.gameObject.SetActive(true);
                if (keyboardState.IsKeyDown(Keys.F))
                {
                    timer += Time.DeltaTime;
                    //winComputerSlider.value = timer;
                    if (timer >= timeToWin)
                    {
                        timer = 0;
                        //winComputerSlider.gameObject.SetActive(false);
                        headUpDisplay.DisplayWinScreen();
                    }

                }
                if (keyboardState.IsKeyUp(Keys.F))
                {
                    timer = 0;
                    //winComputerSlider.value = timer;
                }
            }
            else if (collider.GameObject.Parent?.GetComponentInChildren<PlayerController>())
            {
                //winComputerNotEnoughText.gameObject.SetActive(true);
            }
        }

        //private void OnTriggerExit(Collider collider)
        //{
        //    winComputerSlider.gameObject.SetActive(false);
        //    winComputerNotEnoughText.gameObject.SetActive(false);
        //}
    }
}

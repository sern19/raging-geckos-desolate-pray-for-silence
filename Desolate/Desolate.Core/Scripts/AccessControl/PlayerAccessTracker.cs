﻿using System.Collections.Generic;
using Desolate.Core.Components.Abstract;

namespace Desolate.Core.Scripts.AccessControl
{
    public class PlayerAccessTracker : SingleAttitude
    {
        private List<AccessLevel> accessCardLevels { get; set; }

        public override void Initialize()
        {
            accessCardLevels = new List<AccessLevel>();
            accessCardLevels.Add(AccessLevel.Basic);
        }

        public bool HasAccess(AccessLevel accessLevel)
        {
            return accessCardLevels.Contains(accessLevel);
        }

        public void GainAccess(AccessLevel accessLevel)
        {
            if (!accessCardLevels.Contains(accessLevel))
            {
                accessCardLevels.Add(accessLevel);
            }
        }
        public PlayerAccessTracker(GameObject gameObject) : base(gameObject) { }
    }
}

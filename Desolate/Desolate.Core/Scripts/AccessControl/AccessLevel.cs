﻿namespace Desolate.Core.Scripts.AccessControl
{
    public enum AccessLevel
    {
        Basic,
        Medium,
        Full
    }
}

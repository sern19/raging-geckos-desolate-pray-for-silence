﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Physics;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Scripts.AccessControl
{
    public class AccessCard : SingleAttitude
    {
        public AccessLevel AccessLevel = AccessLevel.Basic;
        private PlayerAccessTracker playerAccessTracker = null;
        
        private float pickupDistance = 1;
        
        //private Text accessLevelMessage;

        private float rotationAngle;

        public Scene scene;

        //private IEnumerable<Door> doors;

        public override void Initialize()
        {
            //doors = FindObjectsOfType<Door>().Where(d => d.accessLevelRequired == AccessLevel);
        }

        public override void Update()
        {
            if (playerAccessTracker != null)
                TryToGrantAccessToPlayer();

            //rotationAngle = 50 * Time.DeltaTime;
            //GameObject.Transform.Rotate(new Vector3(0, rotationAngle, 0));
        }

        public override void OnTriggerEnter(Collider collider)
        {
            OnTriggerStay(collider);
        }

        public override void OnTriggerStay(Collider collider)
        {
            var collidedPlayer = collider.GameObject.GetComponent<PlayerAccessTracker>();
            if (collidedPlayer != null)
            {
                playerAccessTracker = collidedPlayer;
                //GetComponent<MeshRenderer>().material.color = Color.yellow;
                //accessLevelMessage.text = "Gain access level: " + AccessLevel;
            }
        }

        private void TryToGrantAccessToPlayer()
        {
            if (Vector3.Distance(playerAccessTracker.GameObject.Transform.WorldPosition, GameObject.Transform.WorldPosition) <= pickupDistance)
            {
                playerAccessTracker.GainAccess(AccessLevel);
                //playerAccessTracker.GetComponent<PlayerMapController>().ShowMap(5);
                //foreach (var door in doors)
                //{
                //    door.GetComponentInChildren<MinimapIndicator>().Blink();
                //    door.GetComponentInChildren<MinimapIndicator>().SetColor(Color.white);
                //}
                scene.DestroyGameObject(GameObject);
            }
        }

        public override void OnTriggerExit(Collider collider)
        {
            //if (collider.GameObject.GetComponent<PlayerBehaviour.Player>() != null)
            //{
            //    GetComponent<MeshRenderer>().material.color = Color.white;
            //    accessLevelMessage.text = "";
            //}
        }
        public AccessCard(GameObject gameObject) : base(gameObject) { }
    }
}

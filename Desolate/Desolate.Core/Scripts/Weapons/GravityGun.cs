﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Audio;
using Desolate.Core.Components.Physics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Desolate.Core.Scripts.Weapons
{
    public class GravityGun : SingleAttitude
    {
        public GravityGun(GameObject gameObject) : base(gameObject)
        {
        }

        public float speedOfThrow = 2.0f;
        public Scene scene;
        public Model model;
        public Dictionary<string, SoundEffect> sounds;
        private GameObject currentHit;
        private bool canBePulled = true;

        private AudioClip pullAudioClip;
        private AudioClip pushAudioClip;
        private AudioSource audioSource;

        public override void Initialize()
        {
            audioSource = GetComponent<AudioSource>();
            //layerMask = LayerMask.GetMask("Enviroment", "Enemy", "Pullable", "Floor", "Minimap");
        }

        public override void Update()
        {
            if (Mouse.GetState().LeftButton == ButtonState.Pressed && canBePulled)
                Catch();

            if (Mouse.GetState().RightButton == ButtonState.Pressed && !canBePulled)
                Throw();

            if (!canBePulled)
                currentHit.Transform.LocalPosition = GameObject.Parent.Transform.WorldPosition +
                                                     (GameObject.Parent.Transform.WorldForward * 2f);
        }

        void Catch()
        {
            Vector3 transformWorldForward = Scene.MainCamera.Transform.WorldForward;
            transformWorldForward.Normalize();
            Ray shootRay = new Ray
            {
                Position = Scene.MainCamera.Transform.WorldPosition,
                Direction = transformWorldForward
            };

            IEnumerable<GameObject> pullable = Scene.GetAllGameObjects().Where(p => p.Layer == PhysicLayer.Pullable);

            foreach (var pullableObj in pullable)
            {
                Vector3 x = pullableObj.Transform.WorldPosition - Transform.WorldPosition;
                x.Normalize();
                if (canBePulled && pullableObj.GetComponent<SphereCollider>() && shootRay
                        .Intersects(pullableObj.GetComponent<SphereCollider>().BoundingSphere).HasValue ||
                    pullableObj.GetComponent<BoxCollider>() &&
                    shootRay.Intersects(pullableObj.GetComponent<BoxCollider>().BoundingBox).HasValue)
                {
                    canBePulled = false;
                    // pullableObj.GetComponent<Items.IThrowable>().setWasThrown(false);
                    currentHit = GameObject.Instantiate(GameObject.Parent.Transform.WorldPosition + (GameObject.Parent.Transform.WorldForward * 2f),scene,model,sounds,false);
                    //currentHit.Layer = PhysicLayer.Throwable;
                    //isPulled = true;
                    //currentHit.GetComponent<Rigidbody>().IsEnabled = false;
                    //currentHit.GetComponent<BoxCollider>().IsEnabled = false;
                    //currentHit.GetComponent<SphereCollider>().IsEnabled = false;
                    //  audioSource.clip = pullAudioClip;
                    //  audioSource.Play();
                }
            }
        }

        void Throw()
        {
            canBePulled = true;
            //currentHit.GetComponent<Items.IThrowable>().setWasThrown(true);
            currentHit.GetComponent<Rigidbody>().Velocity = GameObject.Parent.Transform.WorldForward * speedOfThrow;
//            currentHit.GetComponent<Rigidbody>().IsEnabled = true;
//            currentHit.GetComponent<BoxCollider>().IsEnabled = true;
//            currentHit.GetComponent<SphereCollider>().IsEnabled = true;
            //canBePulled = false;
            //currentHit.Layer = PhysicLayer.Pullable;
            // audioSource.clip = pushAudioClip;
            // audioSource.Play();
        }
    }
}
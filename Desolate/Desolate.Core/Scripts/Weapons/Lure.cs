﻿using System.Collections.Generic;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Physics;
using Desolate.Core.Components.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Desolate.Core.Scripts.Weapons
{
    public class Lure : SingleAttitude
    {
        public Lure(GameObject gameObject) : base(gameObject)
        {
        }
        
        public float speedOfThrow = 30.0f;
        public int count;
        private int luresAvaliableAtStart = 5;
        public GameObject lurePrefab;
        public GameObject textCounter;
        public Scene scene;
        public Model model;
        private bool waitForRelease;
        public Dictionary<string, SoundEffect> sounds;

        //private Counter lureCounter;


        // Update is called once per frame
        public override void Update()
        {
            if (Mouse.GetState().LeftButton == ButtonState.Pressed)
                waitForRelease = true;
            if (Mouse.GetState().LeftButton == ButtonState.Released && waitForRelease)
            {
                if(count > 0)
                    Throw();
                waitForRelease = false;
            }

        }
        void Throw()
        {
            
            GameObject lure = GameObject.Instantiate(new Vector3(GameObject.Parent.Transform.LocalPosition.X,0.0f,GameObject.Parent.Transform.LocalPosition.Z), scene, model, sounds,true);
            count--;
            textCounter.GetComponent<TextRenderer>().Text = count + "/" + luresAvaliableAtStart;
            //            if (lureCounter.CurrentAmount <= 0)
            //            {
            //                gameObject.SetActive(false); // disable both to inform that it cannot be used anymore
            //                enabled = false;
            //            }
        }

        public override void Initialize()
        {
            count = luresAvaliableAtStart;
            textCounter.GetComponent<TextRenderer>().Text = count + "/" + luresAvaliableAtStart;
        }
    }
}
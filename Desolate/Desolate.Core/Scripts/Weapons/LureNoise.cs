﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Audio;
using Desolate.Core.Scripts.Noise;

namespace Desolate.Core.Scripts.Weapons
{
    public class LureNoise : SingleAttitude
    {
        public LureNoise(GameObject gameObject, Scene myScene, NoiseGenerator myNoiseGenerator) : base(gameObject)
        {
            scene = myScene;
            noiseGenerator = myNoiseGenerator;
        }
        
        private float noiseLevel = 20;
        private float noiseDelay = 5;
        private Scene scene;

        private float countdown = 5;

        private NoiseGenerator noiseGenerator;

        public override void Update()
        {
            noiseDelay -= Time.DeltaTime;

            if (noiseDelay <= 0.0f)
            {
                GetComponent<SoundEmitter>().Play("lure",true);
                noiseGenerator.MakeNoise(noiseLevel);
                countdown -= Time.DeltaTime;
                if (countdown <= 0.0f)
                {
                    GetComponent<SoundEmitter>().Stop("lure");
                    scene.DestroyGameObject(GameObject);
                }
                
            }
            
            
//            if (Mathf.CeilToInt(countdown)>=0)
//            {
//                switch (Mathf.CeilToInt(countdown))
//                {
//                    case 3:
//                    case 2:
//                        gameObject.GetComponentInChildren<Text>().color = new Color(255, 255, 0);
//                        break;
//                    case 1:
//                    case 0:
//                        gameObject.GetComponentInChildren<Text>().color = new Color(255, 0, 0);
//                        break;
//                    default:
//                        gameObject.GetComponentInChildren<Text>().color = new Color(0, 255, 0);
//                        break;
//
//                }
//                gameObject.GetComponentInChildren<Text>().text = (Mathf.CeilToInt(countdown)).ToString();
//            }
        }
    }
}
﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Desolate.Core.Scripts.Weapons
{
    public struct WeaponOption
    {
        public GameObject Weapon;
        public GameObject WeaponUI;

        public static bool operator ==(WeaponOption w1, WeaponOption w2) => w1.Equals(w2);

        public static bool operator !=(WeaponOption w1, WeaponOption w2) => !w1.Equals(w2);
    }

    public class WeaponSelector : SingleAttitude
    {
        public WeaponSelector(GameObject gameObject) : base(gameObject)
        {
        }

        public WeaponOption GravityGun { get; set; }

        public WeaponOption Taser { get; set; }

        public WeaponOption LureThrower { get; set; }

        public GameObject WeaponSelectorBackground { get; set; }

        private WeaponOption activeWeaponOption;

        private void ActivateWeapon(WeaponOption weapon, bool force = false)
        {
            if (!force && weapon == activeWeaponOption)
                return;

            SetActiveWeaponDisplay(weapon.WeaponUI, weapon);

            GravityGun.Weapon.IsActive = GravityGun == weapon;
            LureThrower.Weapon.IsActive = LureThrower == weapon;
            Taser.Weapon.IsActive = Taser == weapon;

            activeWeaponOption = weapon;
        }

        private void SetActiveWeaponDisplay(GameObject WeaponUI, WeaponOption Weapon)
        {
            activeWeaponOption.WeaponUI.GetComponent<ImageRenderer>().Color = new Color(70, 70, 70);
            WeaponSelectorBackground.Transform.LocalPosition = Weapon.WeaponUI.Transform.WorldPosition;
            Weapon.WeaponUI.GetComponent<ImageRenderer>().Color = Color.White;
        }

        public override void Initialize()
        {
            activeWeaponOption = Taser;
            ActivateWeapon(GravityGun, force: true);
        }

        public override void Update()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.D1))
            {
                ActivateWeapon(GravityGun);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.D2))
            {
                ActivateWeapon(Taser);
            }

            if (Keyboard.GetState().IsKeyDown(Keys.D3))
            {
                ActivateWeapon(LureThrower);
            }
        }
    }
}
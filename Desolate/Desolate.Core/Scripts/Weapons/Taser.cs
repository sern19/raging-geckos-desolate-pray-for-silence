﻿using System.Linq;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Physics;
using Desolate.Core.Components.Audio;
using Desolate.Core.Components.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Desolate.Core.Scripts.Weapons
{
    public class Taser : SingleAttitude
    {
        private float noiseRadius = 10;

        private bool isReloding;
        public float reloadTime = 15;
        private float timer = 0;
        public GameObject LoadingBar;

        public Taser(GameObject gameObject) : base(gameObject)
        {
        }

        // Use this for initialization
        public override void Initialize()
        {
            isReloding = false;
            LoadingBar.GetComponent<ImageRenderer>().Filling = 1;
        }

        // Update is called once per frame
        public override void Update()
        {
            base.Update();
            if (!isReloding && Mouse.GetState().LeftButton == ButtonState.Pressed)
            {
                Fire();
                GetComponent<AudioSource>().Play("taser",false,0.5f);
            }

            if (isReloding)
            {
                timer += Time.DeltaTime;

                //should last for reload time 
                if (LoadingBar.GetComponent<ImageRenderer>().Filling <= 1)
                    LoadingBar.GetComponent<ImageRenderer>().Filling = (timer/reloadTime);


                if (timer >= reloadTime)
                {
                    isReloding = false;
                    timer = 0;
                }
            }
        }

        private void Fire()
        {
            isReloding = true;
            LoadingBar.GetComponent<ImageRenderer>().Filling = 0;

            var transformWorldForward = Scene.MainCamera.Transform.WorldForward;
            transformWorldForward.Normalize();
            Ray shootRay = new Ray
            {
                Position = Scene.MainCamera.Transform.WorldPosition,
                Direction = transformWorldForward
            };

            var enemies = Scene.GetAllGameObjects().Where(g => g.GetComponent<Enemy.Enemy>());

            foreach (var enemy in enemies)
            {
                if (enemy.GetComponent<Enemy.Enemy>() is Enemy.Enemy enemyComponent)
                {
                    var x = enemyComponent.Transform.WorldPosition - Transform.WorldPosition;
                    x.Normalize();
                    if (shootRay.Intersects(enemyComponent.GetComponent<SphereCollider>().BoundingSphere).HasValue)
                        enemyComponent.Paralyze(5);
                }
            }
        }
    }
}
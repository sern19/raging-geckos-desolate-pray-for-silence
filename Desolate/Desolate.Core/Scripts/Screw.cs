﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Audio;
using Desolate.Core.Components.Physics;
using Desolate.Core.Scripts.Noise;

namespace Desolate.Core.Scripts
{
    public class Screw : SingleAttitude
    {
        public Screw(GameObject gameObject, NoiseGenerator myNoiseGenerator) : base(gameObject)
        {
            noiseGenerator = myNoiseGenerator;
        }
        
        public NoiseGenerator noiseGenerator;
        public float noiseLevel = 5.0f;

//        public override void Initialize()
//        {
//            base.Initialize();
//            //noiseGenerator = GetComponent<NoiseGenerator>();
//        }
        
        public override void OnCollisionEnter(Collider collider)
        {
            base.OnCollisionEnter(collider);
            
            noiseGenerator.MakeNoise(noiseLevel);
            GameObject.GetComponent<SoundEmitter>().Play("screw",false);
        }
    }
}
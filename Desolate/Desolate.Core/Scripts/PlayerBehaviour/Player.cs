﻿using Desolate.Core.Components;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Scripts.UI;

namespace Desolate.Core.Scripts.PlayerBehaviour
{
    public class Player : SingleAttitude
    {
        public HeadUpDisplay headUpDisplay;
        public Room room;

        public Player(GameObject gameObject) : base(gameObject)
        {
        }

        public override void Initialize()
        {
            base.Initialize();
            //headUpDisplay = this.GetComponent<HeadUpDisplay>();
        }

        public override void Update()
        {
            base.Update();
        }

        public void Die()
        {
           //headUpDisplay.DisplayDeathScreen();
        }

        public void changeRoom(Room newRoom)
        {
            room.isPlayerIn = false;
            room = newRoom;
            room.isPlayerIn = true;
        }
    }
}

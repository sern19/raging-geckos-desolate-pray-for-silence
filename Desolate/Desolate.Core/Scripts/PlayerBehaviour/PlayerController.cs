﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Desolate.Core.Components;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Audio;
using Desolate.Core.Components.Rendering;
using Desolate.Core.Scripts.Noise;
using Desolate.Core.Scripts.UI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Desolate.Core.Scripts.PlayerBehaviour
{
    public enum MovementState
    {
        Walking,
        Crouching,
        Running
    }

    public struct Movement
    {
        public float Speed;
        public float NoiseRange;
        public AudioClip AudioClip;
    }

    public class PlayerController : SingleAttitude
    {
        #region Camera configuration

        private Camera playerCamera;

        private float cameraSpeed = 2;

        private float cameraAngleLimit = 70;
        private float verticalRotation;

        #endregion


        //private PlayerNoiseIndicator playerNoiseIndicator;
        // public PlayerStateIndicator playerStateIndicator;
        private NoiseGenerator noiseGenerator;

        public GameObject ActiveCrosshair;
        public List<Texture2D> crosshairs;

        private float footstepNoiseIntervalInSeconds = 1;
        private float nextFootstepTime = 2;

        private float crouchHeightInPercentage = 30;

        private float walkingCameraY;

        private Movement walking = new Movement()
        {
            Speed = 5,
            NoiseRange = 4,
        };

        private Movement crouching = new Movement()
        {
            Speed = 3,
            NoiseRange = 2,
        };

        private Movement running = new Movement()
        {
            Speed = 10,
            NoiseRange = 8,
        };

        private Movement steady = new Movement() {AudioClip = null, Speed = 0, NoiseRange = 0};

        private MovementState movementState;

        private AudioSource audioSource;

        private Keys crouchingKey = Keys.LeftControl;
        private Keys runningKey = Keys.LeftShift;

        public Movement CurrentMovement
        {
            get
            {
                if (!IsMoving)
                    return steady;
                switch (movementState)
                {
                    case MovementState.Walking:
                        return walking;
                    case MovementState.Crouching:
                        return crouching;
                    case MovementState.Running:
                        return running;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public MovementState MovementState
        {
            get { return movementState; }
            private set
            {
                if (movementState == value)
                    return;

                movementState = value;

                var newCameraPosition = playerCamera.Transform.LocalPosition;
                newCameraPosition.Y = walkingCameraY;

                if (value == MovementState.Crouching)
                {
                    Transform.Translate(new Vector3(0,-0.5f,0));
                    //newCameraPosition.Y *= crouchHeightInPercentage / 100;
                    //Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("CrouchingIgnorable"), true);
                }
                else if(Transform.LocalPosition.Y  < 0)
                {
                    Transform.Translate(new Vector3(0, 0.5f,0));
                    //Physics.IgnoreLayerCollision(LayerMask.NameToLayer("Player"), LayerMask.NameToLayer("CrouchingIgnorable"), false);
                }

                //playerCamera.Transform.LocalPosition = newCameraPosition;

                //  playerStateIndicator.PlayerState = movementState;

                SwitchNoiseEffects();
            }
        }

        private bool isMoving;
        private bool isCrouchPressed;

        public bool IsMoving
        {
            get { return isMoving; }
            private set
            {
                if (isMoving == value)
                    return;
                isMoving = value;
                SwitchNoiseEffects();
            }
        }

        private void SwitchNoiseEffects()
        {
            if (!IsMoving)
            {
                GameObject.GetComponent<AudioSource>().StopAll();
                //playerNoiseIndicator.NoiseLevelUI = NoiseLevelUI.Minimum;
            }
            else
            {
                switch (MovementState)
                {
                    case MovementState.Walking:
                        GameObject.GetComponent<AudioSource>().StopAll();
                        GameObject.GetComponent<AudioSource>().Play("walking", true,0.7f);
                        ActiveCrosshair.GetComponent<ImageRenderer>().Image = crosshairs[0];
                        ActiveCrosshair.Transform.LocalScale = new Vector3(0.008f, 0.008f, 0.008f);
                        //playerNoiseIndicator.NoiseLevelUI = NoiseLevelUI.Medium;
                        break;
                    case MovementState.Crouching:
                        GameObject.GetComponent<AudioSource>().StopAll();
                        GameObject.GetComponent<AudioSource>().Play("crouching", true,0.2f);
                        ActiveCrosshair.GetComponent<ImageRenderer>().Image = crosshairs[2];
                        ActiveCrosshair.Transform.LocalScale = new Vector3(0.008f, 0.008f, 0.008f);
                        //playerNoiseIndicator.NoiseLevelUI = NoiseLevelUI.Low;
                        break;
                    case MovementState.Running:
                        GameObject.GetComponent<AudioSource>().StopAll();
                        GameObject.GetComponent<AudioSource>().Play("running", true,1.0f);
                        ActiveCrosshair.GetComponent<ImageRenderer>().Image = crosshairs[1];
                        ActiveCrosshair.Transform.LocalScale = new Vector3(0.01f, 0.01f, 0.01f);
                        //playerNoiseIndicator.NoiseLevelUI = NoiseLevelUI.High;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }

        public override void Initialize()
        {
            if (!playerCamera)
            {
                Debug.Write($"{nameof(playerCamera)} is not initialized.");
                playerCamera = GameObject.GetAllChildren().Single(s => s is Camera) as Camera;
            }

            if (!walking.AudioClip)
                Debug.Write($"{nameof(walking)} audioclip is not initialized.");

            if (!running.AudioClip)
                Debug.Write($"{nameof(running)} audioclip is not initialized.");

            if (!crouching.AudioClip)
                Debug.Write($"{nameof(crouching)} audioclip is not initialized.");

//            if (!playerNoiseIndicator)
//                Debug.Write($"{nameof(playerNoiseIndicator)} is not initialized.");
//
//            if (!playerStateIndicator)
            //               Debug.Write($"{nameof(playerStateIndicator)} is not initialized.");

            audioSource = GetComponent<AudioSource>();
            noiseGenerator = GetComponent<NoiseGenerator>();

            walkingCameraY = playerCamera.Transform.LocalPosition.Y;
            MovementState = MovementState.Walking;
        }


        // Update is called once per frame
        public override void Update()
        {
            ProcessMovementKeyboard();

            if (Time.GameTime.TotalGameTime.TotalSeconds >= nextFootstepTime && IsMoving)
            {
                //Debug.Log("NOISE");
                nextFootstepTime = (int) Math.Floor(Time.GameTime.TotalGameTime.TotalSeconds) + footstepNoiseIntervalInSeconds;
                noiseGenerator.MakeNoise(CurrentMovement.NoiseRange);
            }
        }

        private void ProcessMovementKeyboard()
        {
            var currentState = MovementState;
            if (Keyboard.GetState().IsKeyDown(crouchingKey))
                isCrouchPressed = true;

            if (Keyboard.GetState().IsKeyUp(crouchingKey) && isCrouchPressed)
            {
                currentState = currentState == MovementState.Crouching
                    ? MovementState.Walking
                    : MovementState.Crouching;
                isCrouchPressed = false;
            }
            else if (Keyboard.GetState().IsKeyDown(runningKey))
                currentState = MovementState.Running;
            else if (Keyboard.GetState().IsKeyUp(runningKey) && currentState != MovementState.Crouching)
                currentState = MovementState.Walking;

            if (currentState != MovementState.Crouching && currentState != MovementState.Running)
            {
                currentState = MovementState.Walking;
            }

            MovementState = currentState;
            playerCamera.GetComponent<CameraController>().MoveSpeed = CurrentMovement.Speed;
            IsMoving = Keyboard.GetState().IsKeyDown(Keys.W)
                       || Keyboard.GetState().IsKeyDown(Keys.S)
                       || Keyboard.GetState().IsKeyDown(Keys.A)
                       || Keyboard.GetState().IsKeyDown(Keys.D);
        }

        public PlayerController(GameObject gameObject) : base(gameObject)
        {
        }
    }
}
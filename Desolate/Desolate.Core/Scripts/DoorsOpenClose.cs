﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Physics;
using Desolate.Core.Components.Rendering.Animation;
using Desolate.Core.Scripts.AccessControl;
using Desolate.Core.Components.Audio;

namespace Desolate.Core.Scripts
{
    public enum DoorState
    {
        Opened,
        Opening,
        Closed,
        Closing
    }

    public class DoorsOpenClose : SingleAttitude
    {
        //private NoiseGenerator noiseGenerator;
        private float noiseValue = 3;
        
        public AccessLevel accessLevelRequired = AccessLevel.Full;
        
        private GameObject[] texts;

        private DoorState doorState = DoorState.Closed;
        public bool isClosed = true;
        public override void Update()
        {
            base.Update();

            if ((doorState == DoorState.Closing) && !(GameObject.GetComponentInChildren<Animator>().IsPlaying))
                isClosed = true;
            else if ((doorState == DoorState.Opening) && !(GameObject.GetComponentInChildren<Animator>().IsPlaying))
                isClosed = false;
        }
        public override void OnTriggerEnter(Collider collider)
        {
            PlayerAccessTracker playerAccessTracker = collider.GameObject.GetComponent<PlayerAccessTracker>();
            if (playerAccessTracker != null)
            {
                if (playerAccessTracker.HasAccess(accessLevelRequired))
                {
                    Open();
                    GameObject.GetComponent<SoundEmitter>().Play("open",false);
                    //foreach (GameObject x in texts)
                    //{
                    //    if (x.GetComponent<Image>())
                    //        x.GetComponent<Image>().color = Color.green;
                    //    if (x.GetComponentInChildren<Text>())
                    //    {
                    //        x.GetComponentInChildren<Text>().color = Color.green;
                    //        x.GetComponentInChildren<Text>().text = "Access Granted";
                    //    }
                    //}
                    //foreach (Renderer x in frames)
                    //    x.material.color = Color.green;
                }
                //else
                //{
                //    foreach (GameObject x in texts)
                //    {
                //        if (x.GetComponent<Image>())
                //            x.GetComponent<Image>().color = Color.red;
                //        if (x.GetComponentInChildren<Text>())
                //        {
                //            x.GetComponentInChildren<Text>().color = Color.red;
                //            x.GetComponentInChildren<Text>().text = "Access Denied";
                //        }
                //    }
                //    foreach (Renderer x in frames)
                //        x.material.color = Color.red;
                //}
            }
            base.OnTriggerEnter(collider);
        }


        public override void OnTriggerExit(Collider collider)
        {
            if (collider.GameObject.GetComponent<PlayerBehaviour.Player>() != null)
            {
                Close();
                PlayerAccessTracker playerAccessTracker = collider.GameObject.GetComponent<PlayerAccessTracker>();
                if (playerAccessTracker != null)
                {
                    if (playerAccessTracker.HasAccess(accessLevelRequired))
                    {
                        GameObject.GetComponent<SoundEmitter>().Play("close", false);
                    }
                }

                //foreach (GameObject x in texts)
                //{
                //    if (x.GetComponent<Image>())
                //        x.GetComponent<Image>().color = Color.blue;
                //    if (x.GetComponentInChildren<Text>())
                //    {
                //        x.GetComponentInChildren<Text>().color = Color.blue;
                //        x.GetComponentInChildren<Text>().text = "Door";
                //    }
                //}
                //foreach (Renderer x in frames)
                //    x.material.color = Color.blue;
            }
            base.OnTriggerExit(collider);
        }

        private void Open()
        {
            doorState = DoorState.Opening;
            isClosed = false;
            GameObject.GetComponentInChildren<Animator>().Play("Open");
        }

        private void Close()
        {
            if (doorState == DoorState.Opened || doorState == DoorState.Opening)
            {
                GameObject.GetComponentInChildren<Animator>().Play("Close");
                isClosed = false;
                doorState = DoorState.Closing;
            }
        }

        public DoorsOpenClose(GameObject gameObject) : base(gameObject) { }
    }
}
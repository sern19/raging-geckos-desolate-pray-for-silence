﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Physics;
using Desolate.Core.Components.Rendering;
using Desolate.Core.Scripts.PlayerBehaviour;
using Desolate.Core.Scripts.UI;
using Microsoft.Xna.Framework.Input;

namespace Desolate.Core.Scripts
{
    public class Vent : SingleAttitude
    {
        public Vent(GameObject gameObject) : base(gameObject) { }
        public GameObject ventText;
        public GameObject fillingBar;
        public HeadUpDisplay headUpDisplay;
        private bool isAlreadyUsed = false;
        private KeyboardState keyboardState;

        private float timer = 0;
        public float timeToUsePoison = 3;

        public override void Initialize()
        {
            fillingBar.GetComponent<ImageRenderer>().Filling = 0;
        }

        public override void Update()
        {
            base.Update();

            keyboardState = Keyboard.GetState();
        }

        public override void OnTriggerEnter(Collider collider)
        {
            OnTriggerStay(collider);
        }

        public override void OnTriggerStay(Collider collider)
        {
            if (!isAlreadyUsed && collider.GameObject.Parent?.GetComponentInChildren<PlayerController>())
            {
                ventText.GetComponent<TextRenderer>().IsEnabled = true;
                //ventSlider.gameObject.SetActive(true);
                if (keyboardState.IsKeyDown(Keys.F))
                {
                    timer += Time.DeltaTime;
                    if (fillingBar.GetComponent<ImageRenderer>().Filling <= 1)
                        fillingBar.GetComponent<ImageRenderer>().Filling = (timer / timeToUsePoison);
                    //ventSlider.value = timer;
                    if (timer >= timeToUsePoison)
                    {
                        isAlreadyUsed = true;
                        //GetComponentInChildren<MinimapIndicator>().gameObject.SetActive(false);
                        timer = 0;
                        //ventSlider.value = timer;
                        //ventSlider.gameObject.SetActive(false);
                        fillingBar.IsActive = false;
                        headUpDisplay.UpdatePoisonCount();
                    }

                }
                if (keyboardState.IsKeyUp(Keys.F))
                {
                    timer = 0;
                    fillingBar.GetComponent<ImageRenderer>().Filling = 0;
                    //ventSlider.value = timer;
                }
            }
        }

        public override void OnTriggerExit(Collider collider)
        {
            ventText.GetComponent<TextRenderer>().IsEnabled = false;
        }

    }
}

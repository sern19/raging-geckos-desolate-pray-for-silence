﻿using System.Linq;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Physics;
using Desolate.Core.Components.Rendering;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Scripts
{
    public class CrosshairTargetIndicator : SingleAttitude
    {
        public CrosshairTargetIndicator(GameObject gameObject) : base(gameObject)
        {
        }

        public ImageRenderer CrosshairHud { get; set; }

        // private RaycastHit rayHit;
        private GameObject oldTarget = null;


        void SetColor()
        {
            if (!oldTarget)
                CrosshairHud.Color = Color.White;
            else
            {
                switch (oldTarget.Layer)
                {
                    case PhysicLayer.Throwable:
                    case PhysicLayer.Pullable:
                        CrosshairHud.Color = Color.GreenYellow;
                        break;
                    case PhysicLayer.Enemy:
                        CrosshairHud.Color = Color.IndianRed;
                        break;
                    default:
                        CrosshairHud.Color = Color.White;
                        break;
                }
            }
        }

        public override void Initialize()
        {
            CrosshairHud = GetComponent<ImageRenderer>();
            CrosshairHud.Color = Color.White;
        }

        public override void Update()
        {
            var transformWorldForward = Scene.MainCamera.Transform.WorldForward;
            transformWorldForward.Normalize();
            Ray shootRay = new Ray
            {
                Position = Scene.MainCamera.Transform.WorldPosition,
                Direction = transformWorldForward
            };

            var maskedObjects = Scene.GetAllGameObjects().Where(g =>
                (g.Layer == PhysicLayer.Enemy || g.Layer == PhysicLayer.Throwable || g.Layer == PhysicLayer.Pullable) &&
                (g.GetComponent<SphereCollider>() || g.GetComponent<BoxCollider>()));

            var hit = maskedObjects.FirstOrDefault(g =>
                g.GetComponent<SphereCollider>() && shootRay.Intersects(g.GetComponent<SphereCollider>().BoundingSphere).HasValue ||
                g.GetComponent<BoxCollider>() && shootRay.Intersects(g.GetComponent<BoxCollider>().BoundingBox).HasValue);

            if (oldTarget != hit)
            {
                oldTarget = hit;
                SetColor();
            }
        }
    }
}
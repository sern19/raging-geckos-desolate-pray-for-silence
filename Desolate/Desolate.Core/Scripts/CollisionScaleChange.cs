﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Physics;
using Desolate.Core.Components.Rendering.Animation;

namespace Desolate.Core.Scripts
{
    public class CollisionScaleChange: SingleAttitude
    {
        public override void OnCollisionEnter(Collider collider)
        {
            GameObject.GetComponent<Animator>().Play("Rotate");
            base.OnCollisionEnter(collider);
        }

        public override void OnCollisionStay(Collider collider)
        {
            base.OnCollisionStay(collider);
        }

        public override void OnCollisionExit(Collider collider)
        {
            GameObject.GetComponent<Animator>().StopAnimation();
            base.OnCollisionExit(collider);
        }

        public CollisionScaleChange(GameObject gameObject) : base(gameObject) { }
    }
}
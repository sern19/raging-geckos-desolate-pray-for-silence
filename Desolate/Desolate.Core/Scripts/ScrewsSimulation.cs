﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Physics;
using Desolate.Core.Scripts.Noise;
using Desolate.Core.Components.Audio;

namespace Desolate.Core.Scripts
{
    public class ScrewsSimulation : SingleAttitude
    {
        public NoiseGenerator noiseGenerator;
        public float noiseLevel = 5.0f;
        public ScrewsSimulation(GameObject gameObject) : base(gameObject) { }

        public override void Initialize()
        {
            base.Initialize();
            noiseGenerator = GetComponent<NoiseGenerator>();
        }
        
        public override void OnTriggerEnter(Collider collider)
        {
            base.OnTriggerEnter(collider);
            if(collider.GameObject.Layer == PhysicLayer.Player)
            {
                noiseGenerator.MakeNoise(noiseLevel);
                GameObject.GetComponent<SoundEmitter>().Play("screw",false);
            }
        }
    }
}

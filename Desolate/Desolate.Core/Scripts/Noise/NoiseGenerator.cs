﻿using System.Collections.Generic;
using System.Linq;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Physics;
using Desolate.Core.Scripts.Enemy;
using Desolate.Core.Scripts.UI;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Scripts.Noise
{
    public class NoiseGenerator : SingleAttitude
    {
        public static float MaximumLoudness = 20;
        public static NoiseIndicator NoiseIndicator = null;

        public NoiseGenerator(GameObject gameObject) : base(gameObject)
        {
        }

        private IEnumerable<EnemyHearing> targetsInRange;

        public void MakeNoise(float radius, bool triggerEnemies = true)
        {
            if (radius <= 0)
                return;

            FindTargetsInRange(radius);
            PullTargets(radius);
            NoiseIndicator?.IndicateNoise(this, radius / MaximumLoudness);
        }

        private void PullTargets(float radius)
        {
            foreach (EnemyHearing x in targetsInRange)
            {
                x.FollowNoiseSource(GameObject.Transform.WorldPosition, radius);
            }
        }

        private void FindTargetsInRange(float radius)
        {
            targetsInRange = GetCollidersInRange(GameObject.Transform.WorldPosition, radius);
        }

        private IEnumerable<EnemyHearing> GetCollidersInRange(Vector3 position, float radius)
        {
            var enemyHearings = Scene.GetAllGameObjects()
                .Where(g => g.GetComponent<EnemyHearing>() && g.IsActive)
                .Select(g => g.GetComponent<EnemyHearing>())
                .ToArray();

            return enemyHearings.Where(gameObj => IsInRange(gameObj, position, radius)).ToList();
        }

        private static bool IsInRange(EnemyHearing gameObject, Vector3 spherePosition, float radius)
        {
            return Vector3.Distance(gameObject.Transform.WorldPosition, spherePosition) <
                   radius * gameObject.HearingRangeMultiplier;
        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Desolate.Core.Components;
using Desolate.Core.Components.Physics;
using Desolate.Core.Components.Rendering;
using Desolate.Core.Components.Rendering.Helpers;
using Desolate.Core.Components.Rendering.Lightning;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using DirectionalLight = Desolate.Core.Components.Rendering.Lightning.DirectionalLight;


namespace Desolate.Core
{
    public class Scene
    {
        public static Camera MainCamera { get; private set; }

        public static IEnumerable<GameObject> GetAllGameObjects()
        {
            List<GameObject> allGameObjects = new List<GameObject>();
            foreach (GameObject gameObject in gameObjects)
            {
                allGameObjects.AddRange(gameObject.GetAllChildren());
            }

            return allGameObjects;
        }

        public byte CheckPoint { get; set; }

        public GameObject Player { get; set; }

        public IEnumerable<Light> AllLights { get; private set; } = new List<Light>();

        private static List<GameObject> gameObjects = new List<GameObject>();

        private static IEnumerable<GameObject> gameObjectsSortedByDrawOrder;

        private List<GameObject> gameObjectsToDestroy = new List<GameObject>();
        private List<GameObject> gameObjectsToAdd = new List<GameObject>();

        private List<Room> rooms = new List<Room>();

        private bool shouldReloadLights = true;

        public Scene(Game game, bool isDebug = false)
        {
            GameManager.Game = game;
            GameManager.IsDebug = isDebug;
            GameManager.FullscreenQuad = new FullscreenQuad();
            if (GameManager.IsDeferred)
                DeferredMeshRenderer.InitializeGBufferTargets();
        }

        private void UpdateAllLights()
        {
            List<Light> lights = new List<Light>();

            foreach (GameObject gameObject in gameObjects)
                lights.AddRange(gameObject.GetComponentsInChildren<Light>());

            //"To olej te z pokoi" Janicki 2018
//            foreach (Room room in rooms)
//                lights.AddRange(room.GetComponentsInChildren<Light>());

            AllLights = lights;
            shouldReloadLights = false;
        }

        public void AddDefaultLights()
        {
            GameObject lights = new GameObject();
            lights.AddComponent(new DirectionalLight(lights)
            {
                Direction = new Vector3(-0.5265408f, -0.5735765f, -0.6275069f),
                Color = new Color(1f, 0.9607844f, 0.8078432f)
            });
            lights.AddComponent(new DirectionalLight(lights)
            {
                Direction = new Vector3(0.7198464f, 0.3420201f, 0.6040227f),
                Color = new Color(0.9647059f, 0.7607844f, 0.4078432f),
                SpecularColor = Color.Black,
                Intensity = 0.5f,
                CastsShadow = false
            });
            lights.AddComponent(new DirectionalLight(lights)
            {
                Direction = new Vector3(0.4545195f, -0.7660444f, 0.4545195f),
                Color = new Color(0.3231373f, 0.3607844f, 0.3937255f)
            });
            lights.AddComponent(new AmbientLight(lights)
            {
                Intensity = 0.05f
            });
            lights.Initialize();
            AddGameObject(lights);
        }

        public void AddGameObject(GameObject gameObject)
        {
            gameObjects.Add(gameObject);
            gameObjectsSortedByDrawOrder = gameObjects.OrderBy(g =>
                g.GetComponent<SpriteRenderer>() ? g.GetComponent<SpriteRenderer>().DrawOrder : int.MaxValue);
            shouldReloadLights = true;
        }

        public void DestroyGameObject(GameObject gameObject) => gameObjectsToDestroy.Add(gameObject);
        public void AddGameObjectInRuntime(GameObject gameObject) => gameObjectsToAdd.Add(gameObject);

        public void AddRoom(Room room)
        {
            rooms.Add(room);
            shouldReloadLights = true;
        }

        public void Initialize()
        {
            MainCamera = GetAllGameObjects().FirstOrDefault(g => g.GetType() == typeof(Camera)) as Camera;

            foreach (GameObject gameObject in gameObjects)
            {
                gameObject.Initialize();
            }

            foreach (Room room in rooms)
            {
                room.Initialize();
            }
        }

        public void Update(GameTime gameTime)
        {
            Time.GameTime = gameTime;

            if (Time.IsInFixedStep)
                FixedUpdate();

            foreach (GameObject gameObject in gameObjects)
                if (gameObject.IsActive)
                    gameObject.Update();

            foreach (GameObject gameObject in gameObjectsToDestroy)
            {
                gameObjects.Remove(gameObject);
                gameObjectsSortedByDrawOrder = gameObjects.OrderBy(g =>
                    g.GetComponent<SpriteRenderer>() ? g.GetComponent<SpriteRenderer>().DrawOrder : int.MaxValue);
            }


            foreach (GameObject gameObject in gameObjectsToAdd)
            {
                gameObjects.Add(gameObject);
                gameObjectsSortedByDrawOrder = gameObjects.OrderBy(g =>
                    g.GetComponent<SpriteRenderer>() ? g.GetComponent<SpriteRenderer>().DrawOrder : int.MaxValue);
            }

            if (gameObjectsToAdd.Count() != 0)
                gameObjectsToAdd.Clear();

            foreach (Room room in rooms)
            {
                room.Update();
            }

            LateUpdate();
        }

        public void FixedUpdate()
        {
            //Should always be called before gameObjects FixedUpdates to avoid Collision Stay and Exit in same frame
            ProcessCollisions();

            foreach (GameObject gameObject in gameObjects)
                if (gameObject.IsActive)
                    gameObject.FixedUpdate();
        }

        public void LateUpdate()
        {
            foreach (GameObject gameObject in gameObjects)
                if (gameObject.IsActive)
                    gameObject.LateUpdate();
        }

        private void ProcessCollisions()
        {
            IEnumerable<GameObject> allGameObjects = GetAllGameObjects().ToList();
            foreach (var gameObject in allGameObjects)
            {
                foreach (var collider in gameObject.GetComponents<Collider>().Where(c => c.IsEnabled))
                {
                    IEnumerable<GameObject> listenedGameObjects =
                        GetListenedForCollisionsGameObjects(gameObject, allGameObjects).ToArray();

                    foreach (var listenedGameObject in listenedGameObjects)
                    {
                        foreach (var listenedCollider in listenedGameObject.GetComponents<Collider>()
                            .Where(c => c.IsEnabled))
                        {
                            if (!collider.IsTrigger && !listenedCollider.IsTrigger &&
                                LayerMask.ContainsLayer(Physic.CollisionMatrix[gameObject.Layer],
                                    listenedGameObject.Layer))
                            {
                                if (collider.IsCollisionDetected(listenedCollider))
                                {
                                    gameObject.AddCollision(listenedCollider);
                                    listenedGameObject.AddCollision(collider);
                                }
                                else
                                {
                                    gameObject.RemoveCollision(listenedCollider);
                                    listenedGameObject.RemoveCollision(collider);
                                }
                            }
                            else if (collider.IsTrigger && listenedCollider.IsTrigger)
                            {
                                if (collider.IsTriggerDetected(listenedCollider))
                                {
                                    gameObject.AddCollision(listenedCollider, true);
                                    listenedGameObject.AddCollision(collider, true);
                                }
                                else
                                {
                                    gameObject.RemoveCollision(listenedCollider, true);
                                    listenedGameObject.RemoveCollision(collider, true);
                                }
                            }
                        }
                    }
                }
            }
        }

        private static IEnumerable<GameObject> GetListenedForCollisionsGameObjects(GameObject gameObject,
            IEnumerable<GameObject> allGameObjects)
        {
            return allGameObjects.Where(g =>
                g != gameObject && g.GetComponent<Collider>());
        }

        public void Draw()
        {
            if (shouldReloadLights)
                UpdateAllLights();

            //Prepare for geometry pass
            if (GameManager.IsDeferred)
            {
                DeferredMeshRenderer.ClearGBuffer();
                GameManager.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            }


            //Geometry pass
            foreach (GameObject gameObject in gameObjects)
                if (gameObject.IsActive)
                    gameObject.Draw();


            if (GameManager.IsDeferred)
            {
                //Shadow pass
                foreach (Light light in AllLights)
                {
                    if (!light.GameObject.IsActive || !light.IsEnabled || !light.CastsShadow) continue;
                    light.DrawShadowMap();
                    foreach (GameObject gameObject in gameObjects)
                        if (gameObject.IsActive)
                            gameObject.DrawForShadows();
                }


                //Lightning pass
                DeferredMeshRenderer.PrepareLightPass(MainCamera);
                foreach (Light light in AllLights)
                {
                    if (light.GameObject.IsActive && light.IsEnabled)
                        light.Draw();
                }

                DeferredMeshRenderer.FinalPass();
                //Postprocesses
                DeferredMeshRenderer.RestoreDepth(null);

//                if (GameManager.IsDebug)
//                    DeferredMeshRenderer.DrawDebug();
            }

            //UI draw

            foreach (GameObject gameObject in gameObjectsSortedByDrawOrder)
                if (gameObject.IsActive)
                    gameObject.LateDraw();
        }
    }
}
﻿using System;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Components
{
    public class Transform
    {
        #region Fields

        private Quaternion localRotation = Quaternion.Identity;
        private Vector3 localPosition = Vector3.Zero;
        private Vector3 localScale = Vector3.One;

        private Matrix ownCachedMatrix = Matrix.Identity;
        private Matrix cachedMatrixHierarchy;
        private readonly GameObject gameObject;
        private bool needUpdate = true;

        #endregion

        #region Properties

        #region Local

        #region Directions

        public Vector3 Forward
        {
            get
            {
                CheckAndUpdateCache();
                return ownCachedMatrix.Forward;
            }
        }

        public Vector3 Backward
        {
            get
            {
                CheckAndUpdateCache();
                return ownCachedMatrix.Backward;
            }
        }

        public Vector3 Left
        {
            get
            {
                CheckAndUpdateCache();
                return ownCachedMatrix.Left;
            }
        }

        public Vector3 Right
        {
            get
            {
                CheckAndUpdateCache();
                return ownCachedMatrix.Right;
            }
        }

        public Vector3 Up
        {
            get
            {
                CheckAndUpdateCache();
                return ownCachedMatrix.Up;
            }
        }

        public Vector3 Down
        {
            get
            {
                CheckAndUpdateCache();
                return ownCachedMatrix.Down;
            }
        }

        #endregion

        public Quaternion LocalRotation
        {
            get => localRotation;
            set
            {
                localRotation = value;
                NeedUpdate = true;
            }
        }

        public Vector3 LocalPosition
        {
            get => localPosition;
            set
            {
                localPosition = value;
                NeedUpdate = true;
            }
        }

        public Vector3 LocalScale
        {
            get => localScale;
            set
            {
                localScale = value;
                NeedUpdate = true;
            }
        }

        public Vector3 LocalEulerAngles
        {
            get
            {
                Vector3 eulerAngles = new Vector3
                {
                    X = MathHelper.ToDegrees((float) Math.Atan2(2 * (LocalRotation.Y * LocalRotation.Z + LocalRotation.W * LocalRotation.X),
                        Math.Pow(LocalRotation.W, 2) - Math.Pow(LocalRotation.X, 2) - Math.Pow(LocalRotation.Y, 2) + Math.Pow(LocalRotation.Z, 2))), //Pitch
                    Y = MathHelper.ToDegrees((float) Math.Asin(-2 * (LocalRotation.X * LocalRotation.Z - LocalRotation.W * LocalRotation.Y))), //Yaw
                    Z = MathHelper.ToDegrees((float) Math.Atan2(2 * (LocalRotation.X * LocalRotation.Y + LocalRotation.W * LocalRotation.Z),
                        Math.Pow(LocalRotation.W, 2) + Math.Pow(LocalRotation.X, 2) - Math.Pow(LocalRotation.Y, 2) - Math.Pow(LocalRotation.Z, 2))) //Roll
                };
                return eulerAngles;
            }
            set
            {
                LocalRotation = Quaternion.CreateFromYawPitchRoll(MathHelper.ToRadians(value.Y), MathHelper.ToRadians(value.X),
                    MathHelper.ToRadians(value.Z));
                NeedUpdate = true;
            }
        }

        #endregion

        #region World

        #region Directions

        public Vector3 WorldForward
        {
            get
            {
                CheckAndUpdateCache();
                return cachedMatrixHierarchy.Forward;
            }
        }

        public Vector3 WorldBackward
        {
            get
            {
                CheckAndUpdateCache();
                return cachedMatrixHierarchy.Backward;
            }
        }

        public Vector3 WorldLeft
        {
            get
            {
                CheckAndUpdateCache();
                return cachedMatrixHierarchy.Left;
            }
        }

        public Vector3 WorldRight
        {
            get
            {
                CheckAndUpdateCache();
                return cachedMatrixHierarchy.Right;
            }
        }

        public Vector3 WorldUp
        {
            get
            {
                CheckAndUpdateCache();
                return cachedMatrixHierarchy.Up;
            }
        }

        public Vector3 WorldDown
        {
            get
            {
                CheckAndUpdateCache();
                return cachedMatrixHierarchy.Down;
            }
        }

        #endregion

        public Quaternion WorldRotation
        {
            get
            {
                CheckAndUpdateCache();
                return cachedMatrixHierarchy.Rotation;
            }
        }

        public Vector3 WorldPosition
        {
            get
            {
                CheckAndUpdateCache();
                return cachedMatrixHierarchy.Translation;
            }
        }

        public Vector3 WorldScale
        {
            get
            {
                CheckAndUpdateCache();
                return gameObject.Parent != null ? gameObject.Parent.Transform.WorldScale * LocalScale : LocalScale;
            }
        }

        public Vector3 WorldEulerAngles
        {
            get
            {
                CheckAndUpdateCache();
                Vector3 eulerAngles = new Vector3
                {
                    X = MathHelper.ToDegrees((float) Math.Atan2(2 * (WorldRotation.Y * WorldRotation.Z + WorldRotation.W * WorldRotation.X),
                        Math.Pow(WorldRotation.W, 2) - Math.Pow(WorldRotation.X, 2) - Math.Pow(WorldRotation.Y, 2) + Math.Pow(WorldRotation.Z, 2))), //Pitch
                    Y = MathHelper.ToDegrees((float) Math.Asin(-2 * (WorldRotation.X * WorldRotation.Z - WorldRotation.W * WorldRotation.Y))), //Yaw
                    Z = MathHelper.ToDegrees((float) Math.Atan2(2 * (WorldRotation.X * WorldRotation.Y + WorldRotation.W * WorldRotation.Z),
                        Math.Pow(WorldRotation.W, 2) + Math.Pow(WorldRotation.X, 2) - Math.Pow(WorldRotation.Y, 2) - Math.Pow(WorldRotation.Z, 2))) //Roll
                };
                return eulerAngles;
            }
        }

        #endregion

        public Matrix TransformMatrix
        {
            get
            {
                CheckAndUpdateCache();
                return cachedMatrixHierarchy;
            }
        }

        private bool NeedUpdate
        {
            get => needUpdate;
            set
            {
                needUpdate = value;
                if (!needUpdate) return;

                foreach (GameObject x in gameObject.Children)
                    x.Transform.NeedUpdate = true;
            }
        }

        #endregion

        #region Methods

        private void CheckAndUpdateCache()
        {
            if (!NeedUpdate) return;

            ownCachedMatrix = Matrix.CreateFromQuaternion(LocalRotation) * Matrix.CreateScale(localScale) * Matrix.CreateTranslation(LocalPosition);

            NeedUpdate = false;
            cachedMatrixHierarchy = gameObject.Parent != null ? ownCachedMatrix * gameObject.Parent.Transform.TransformMatrix : ownCachedMatrix;
        }

        /// <summary>
        /// Rotate object using euler angles
        /// </summary>
        /// <param name="euler"></param>
        public void Rotate(Vector3 euler)
        {
            LocalRotation *= Quaternion.CreateFromYawPitchRoll(MathHelper.ToRadians(euler.Y), MathHelper.ToRadians(euler.X),
                MathHelper.ToRadians(euler.Z));
            NeedUpdate = true;
        }

        /// <summary>
        /// Rotate object using axis-angle
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="angle"></param>
        public void Rotate(Vector3 axis, float angle)
        {
            LocalRotation *= Quaternion.CreateFromAxisAngle(axis, angle);
            NeedUpdate = true;
        }

        /// <summary>
        /// Rotate object using quaternion
        /// </summary>
        /// <param name="rotation"></param>
        public void Rotate(Quaternion rotation)
        {
            LocalRotation *= rotation;
            NeedUpdate = true;
        }

        /// <summary>
        /// Move object
        /// </summary>
        /// <param name="translation"></param>
        public void Translate(Vector3 translation) => LocalPosition = LocalPosition + translation;
        
        /// <summary>
        /// Move object with scaled parameters
        /// </summary>
        /// <param name="translation"></param>
        public void TranslateWithScale(Vector3 translation) => LocalPosition = LocalPosition + translation * LocalScale;

        /// <summary>
        /// Scale object
        /// </summary>
        /// <param name="scale"></param>
        public void Scale(Vector3 scale) => LocalScale = LocalScale * scale;

        /// <summary>
        /// Looks at transform with free Up vector
        /// </summary>
        /// <param name="target"></param>
        public void LookAt(Transform target) =>
            LocalRotation = Matrix.CreateBillboard(target.WorldPosition, WorldPosition, WorldUp, target.WorldForward).Rotation;
        
        /// <summary>
        /// Looks at postition with free Up vector
        /// </summary>
        /// <param name="target"></param>
        public void LookAt(Vector3 target) =>
            LocalRotation = Matrix.CreateBillboard(target, WorldPosition, Vector3.Up, WorldPosition + target).Rotation;

        /// <summary>
        /// Looks at camera with locked Up vector
        /// </summary>
        /// <param name="target"></param>
        public void LookAtCamera(Transform target) =>
            LocalRotation = Matrix.CreateBillboard(target.WorldPosition, WorldPosition, Vector3.Up, target.WorldForward).Rotation;

        /// <summary>
        /// Looks at transform rotating only in Y axis
        /// </summary>
        /// <param name="target"></param>
        public void LookAtCylindrical(Transform target) => LocalRotation =
            Matrix.CreateConstrainedBillboard(target.WorldPosition, WorldPosition, Vector3.Up, target.WorldForward, WorldForward).Rotation;

        #endregion

        public Transform(GameObject gameObject) => this.gameObject = gameObject;
    }
}
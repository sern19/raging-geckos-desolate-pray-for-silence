﻿using Desolate.Core.Components.Abstract;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Desolate.Core.Components
{
    public class CameraController : SingleAttitude
    {
        private MouseState currentMouseState;
        private MouseState previousMouseState;
        private KeyboardState keyboardState;
        private Vector3 moveVector;
        private Vector2 mouseDelta;

        public CameraController(GameObject gameObject) : base(gameObject)
        {
        }

        public float MoveSpeed { get; set; } = 5.0f;

        public float MouseRotationSpeed { get; set; } = 2f;

        public bool LockY { get; set; } = true;

        public override void Initialize()
        {
            if (!(GameObject is Camera))
                IsEnabled = false;
        }

        public override void LateUpdate()
        {
            base.LateUpdate();

            #region GetStates

            keyboardState = Keyboard.GetState();
            currentMouseState = Mouse.GetState();

            #endregion

            #region MouseInput - Rotation

            if (currentMouseState != previousMouseState)
            {
                mouseDelta.X = GameManager.Game.GraphicsDevice.Viewport.Width / 2.0f - currentMouseState.X;
                mouseDelta.Y = GameManager.Game.GraphicsDevice.Viewport.Height / 2.0f - currentMouseState.Y;

                mouseDelta *= Time.DeltaTime * MouseRotationSpeed;

                ((Camera) GameObject).Rotate(new Vector3(mouseDelta.Y, mouseDelta.X, 0));
            }

            previousMouseState = currentMouseState;
            Mouse.SetPosition(GameManager.Game.GraphicsDevice.Viewport.Width / 2,
                GameManager.Game.GraphicsDevice.Viewport.Height / 2);

            #endregion

            #region KeyboardInput - Move

            if (keyboardState == null)
                return;

            moveVector = Vector3.Zero;
            if (keyboardState.IsKeyDown(Keys.W))
            {
                moveVector += GameObject.Transform.Forward;
            }

            if (keyboardState.IsKeyDown(Keys.S))
            {
                moveVector += GameObject.Transform.Backward;
            }

            if (keyboardState.IsKeyDown(Keys.A))
            {
                moveVector += GameObject.Transform.Left;
            }

            if (keyboardState.IsKeyDown(Keys.D))
            {
                moveVector += GameObject.Transform.Right;
            }

            if (LockY)
                moveVector.Y = 0.0f; //Because 'Wyższy Marcin' wanst cameraController behave more like PlayerController

            if (moveVector == Vector3.Zero) return;

            moveVector.Normalize();
            moveVector *= Time.DeltaTime * MoveSpeed;
            ((Camera) GameObject).Move(moveVector);

            #endregion
        }
    }
}
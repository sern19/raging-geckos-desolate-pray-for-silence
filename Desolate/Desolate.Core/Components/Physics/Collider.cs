﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Rendering;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Components.Physics
{
    public abstract class Collider : SingleAttitude
    {
        public abstract Vector3 Center { get; set; }
        public Vector3 WorldCenter => Center + GameObject.Transform.WorldPosition;

        public bool IsTrigger { get; set; }

        public bool IsTriggerDetected(Collider collider) => (IsTrigger & collider.IsTrigger) && Intersects(collider);

        public bool IsCollisionDetected(Collider collider) =>
            (!IsTrigger & !collider.IsTrigger) && Intersects(collider);

        protected Collider(GameObject gameObject) : base(gameObject)
        {
            if (!gameObject.GetComponent<ColliderRenderer>() && GameManager.IsDebug)
                gameObject.AddComponent(new ColliderRenderer(gameObject));
        }

        protected abstract bool Intersects(Collider collider);
        internal abstract bool Intersects(BoundingBox collision);
        internal abstract bool Intersects(BoundingSphere collision);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Desolate.Core.Components.Physics
{
    public static class Physic
    {
        public static readonly Dictionary<PhysicLayer, PhysicLayer> CollisionMatrix =
            new Dictionary<PhysicLayer, PhysicLayer>();

        static Physic()
        {
            foreach (var value in Enum.GetValues(typeof(PhysicLayer)).Cast<PhysicLayer>())
            {
                if (value != LayerMask.AllMask)
                    CollisionMatrix.Add(value, LayerMask.AllMask);
            }
        }

        public static PhysicLayer GetLayerCollisionMask(PhysicLayer layer) => CollisionMatrix[layer];

        public static void SetLayerCollisionMask(PhysicLayer layer, PhysicLayer mask)
        {
            CollisionMatrix[layer] = mask;
            DisableLayerInMaskedLayers(layer, mask);
        }

        private static void DisableLayerInMaskedLayers(PhysicLayer layer, PhysicLayer mask)
        {
            var collisionMatrixKeys = CollisionMatrix.Keys.ToList();
            foreach (var physicLayer in collisionMatrixKeys)
            {
                if (!mask.HasFlag(physicLayer))
                    CollisionMatrix[physicLayer] &= ~layer;
            }
        }

        public static void SetLayersCollisionInteraction(PhysicLayer first, PhysicLayer second, bool isActive)
        {
            if (isActive)
            {
                CollisionMatrix[first] |= second;
                CollisionMatrix[second] |= first;
            }
            else
            {
                CollisionMatrix[first] &= ~second;
                CollisionMatrix[second] &= ~first;
            }
        }
    }
}
﻿using System.Linq;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Components.Physics
{
    public class SphereCollider : Collider
    {
        #region Privates

        private BoundingSphere boundingSphere = new BoundingSphere(Vector3.Zero, 0);
        public BoundingSphere BoundingSphere
        {
            get
            {
                UpdateBoundingSphere();
                return boundingSphere;
            }
        }

        #endregion

        #region Properties

        public override Vector3 Center { get; set; }

        public float Radius { get; set; }

        #endregion

        public SphereCollider(GameObject gameObject) : base(gameObject)
        {
        }
        
        public SphereCollider(GameObject gameObject, BoundingSphere boundingSphere) : base(gameObject)
        {
            Center = boundingSphere.Center;
            Radius = boundingSphere.Radius;
        }

        private void UpdateBoundingSphere()
        {
            boundingSphere.Center = Vector3.Transform(Center, GameObject.Transform.TransformMatrix);

            //This is wrong for scale, length returns value calculated using pythagoras theorem
            //boundingSphere.Radius = Radius * GameObject.Transform.WorldScale.Length();

            //This is temporary workaround, should be changed asap - It'll produce good results if scale is uniform
            float[] scale =
            {
                GameObject.Transform.WorldScale.X,
                GameObject.Transform.WorldScale.Y,
                GameObject.Transform.WorldScale.Z
            };
            boundingSphere.Radius = Radius * scale.Max();
        }

        protected override bool Intersects(Collider collider)
        {
            UpdateBoundingSphere();
            return collider.Intersects(boundingSphere);
        }

        internal override bool Intersects(BoundingBox collision)
        {
            UpdateBoundingSphere();

            var containmentType = collision.Contains(boundingSphere);
            return containmentType == ContainmentType.Intersects || containmentType == ContainmentType.Contains ||
                   collision.Intersects(boundingSphere);
        }

        internal override bool Intersects(BoundingSphere collision)
        {
            UpdateBoundingSphere();

            var containmentType = collision.Contains(boundingSphere);
            return containmentType == ContainmentType.Intersects || containmentType == ContainmentType.Contains ||
                   collision.Intersects(boundingSphere);
        }
    }
}
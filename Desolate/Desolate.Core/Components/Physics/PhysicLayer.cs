﻿using System;

namespace Desolate.Core.Components.Physics
{
    [Flags]
    public enum PhysicLayer
    {
        Default = 1 << 0,
        UI = 1 << 1,
        Enviroment = 1 << 2,
        BuiltIn_1 = 1 << 3,
        BuiltIn_2 = 1 << 4,
        BuiltIn_3 = 1 << 5,
        BuiltIn_4 = 1 << 6,
        BuiltIn_5 = 1 << 7,
        BuiltIn_6 = 1 << 8,
        Throwable = 1 << 9,
        Enemy = 1 << 10,
        Pullable = 1 << 11,
        UserDefined_3 = 1 << 12,
        UserDefined_4 = 1 << 13,
        Player = 1 << 14,
        UserDefined_6 = 1 << 15,
        UserDefined_7 = 1 << 16,
        UserDefined_8 = 1 << 17,
        UserDefined_9 = 1 << 18,
        UserDefined_10 = 1 << 19,
        UserDefined_11 = 1 << 20,
        UserDefined_12 = 1 << 21,
        UserDefined_13 = 1 << 22,
        UserDefined_14 = 1 << 23,
        UserDefined_15 = 1 << 24,
        UserDefined_16 = 1 << 25,
        UserDefined_17 = 1 << 26,
        UserDefined_18 = 1 << 27,
        UserDefined_19 = 1 << 28,
        UserDefined_20 = 1 << 29,
        UserDefined_21 = 1 << 30
    }

    public static class PhysicLayerExtensions
    {
        public static bool HasFlagFast(this PhysicLayer value, PhysicLayer flag)
        {
            return (value & flag) != 0;
        }
    }
}
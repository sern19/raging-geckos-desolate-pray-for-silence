﻿using System;
using System.Linq;

namespace Desolate.Core.Components.Physics
{
    public static class LayerMask
    {
        public const PhysicLayer AllMask = (PhysicLayer) Int32.MaxValue;
        
        public static bool ContainsLayer(PhysicLayer mask, PhysicLayer layer) => mask.HasFlag(layer);

        public static PhysicLayer ToMask(PhysicLayer[] layers)
        {
            if (layers == null) throw new ArgumentNullException(nameof(layers));
            return layers.Aggregate<PhysicLayer, PhysicLayer>(0, (current, layer) => current | layer);
        }

        public static PhysicLayer ToIgnoreMask(PhysicLayer[] layers) => ~ToMask(layers);

        public static int Combine(int mask1, int mask2) => mask1 & mask2;
    }
}
﻿using System;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Components.Physics
{
    public class BoxCollider : Collider
    {
        #region Privates

        private BoundingBox boundingBox = new BoundingBox(Vector3.Zero, Vector3.Zero);

        public BoundingBox BoundingBox
        {
            get
            {
                CalculateGlobalBoundingBox();
                return boundingBox;
            }
        }

        /// <summary>
        /// X (Left/Right), Y (Down,Up), Z (Front,Back): LDF, RDF, RUF, LUF, LDB, RDB, RUB, LUB
        /// </summary>
        /// <returns></returns>
        public Vector3[] GetLocalPoints()
        {
            return new[]
            {
                LocalBoundingBoxCorners.Min,
                new Vector3(LocalBoundingBoxCorners.Max.X, LocalBoundingBoxCorners.Min.Y,
                    LocalBoundingBoxCorners.Min.Z),
                new Vector3(LocalBoundingBoxCorners.Max.X, LocalBoundingBoxCorners.Max.Y,
                    LocalBoundingBoxCorners.Min.Z),
                new Vector3(LocalBoundingBoxCorners.Min.X, LocalBoundingBoxCorners.Max.Y,
                    LocalBoundingBoxCorners.Min.Z),
                new Vector3(LocalBoundingBoxCorners.Min.X, LocalBoundingBoxCorners.Min.Y,
                    LocalBoundingBoxCorners.Max.Z),
                new Vector3(LocalBoundingBoxCorners.Max.X, LocalBoundingBoxCorners.Min.Y,
                    LocalBoundingBoxCorners.Max.Z),
                LocalBoundingBoxCorners.Max,
                new Vector3(LocalBoundingBoxCorners.Min.X, LocalBoundingBoxCorners.Max.Y,
                    LocalBoundingBoxCorners.Max.Z)
            };
        }

        #endregion

        #region Properties

        public override Vector3 Center { get; set; } = Vector3.Zero;
        public Vector3 Size { get; set; } = Vector3.One;

        private (Vector3 Min, Vector3 Max) LocalBoundingBoxCorners => (Center - 0.5f * Size, Center + 0.5f * Size);

        #endregion

        private void CalculateGlobalBoundingBox()
        {
            boundingBox.Min = LocalBoundingBoxCorners.Min + GameObject.Transform.WorldPosition;
            boundingBox.Max = LocalBoundingBoxCorners.Max + GameObject.Transform.WorldPosition;
        }


        protected override bool Intersects(Collider collider)
        {
            CalculateGlobalBoundingBox();
            return collider.Intersects(boundingBox);
        }

        internal override bool Intersects(BoundingBox collision)
        {
            CalculateGlobalBoundingBox();
            var containmentType = collision.Contains(boundingBox);
            return containmentType == ContainmentType.Intersects || containmentType == ContainmentType.Contains ||
                   collision.Intersects(boundingBox);
        }

        internal override bool Intersects(BoundingSphere collision)
        {
            CalculateGlobalBoundingBox();
            var containmentType = collision.Contains(boundingBox);
            return containmentType == ContainmentType.Intersects || containmentType == ContainmentType.Contains ||
                   collision.Intersects(boundingBox);
        }

        public BoxCollider(GameObject gameObject) : base(gameObject)
        {
        }

        public BoxCollider(GameObject gameObject, BoundingBox boundingBox) : base(gameObject)
        {
            Center = boundingBox.Max * 0.25f + boundingBox.Min * 0.25f;
            Size = boundingBox.Max - boundingBox.Min;
        }

        internal Vector3 GetPointOriantation(Vector3 point)
        {
            var x = point - WorldCenter;
            var y = BoxClapVector(x, true);
            return y;
        }

        internal Vector3 BoxClapVector(Vector3 vector, bool normalize = false)
        {
            if (normalize)
                vector /= Size; //normalize force by collider size

            //find most significant force or forces if equal

            Vector3 absVector = new Vector3
            {
                X = Math.Abs(vector.X),
                Y = Math.Abs(vector.Y),
                Z = Math.Abs(vector.Z)
            };

            Vector3 mask = new Vector3()
            {
                X = absVector.X >= absVector.Y && absVector.X >= absVector.Z ? 1 : 0,
                Y = absVector.Y >= absVector.X && absVector.Y >= absVector.Z ? 1 : 0,
                Z = absVector.Z >= absVector.X && absVector.Z >= absVector.Y ? 1 : 0,
            };

            var totalMaskSum = mask.X + mask.Y + mask.Z;
            if (totalMaskSum > 0)
            {
                mask /= mask.X + mask.Y + mask.Z; // equaly distibute force in each direction
                return vector * mask;
            }

            return Vector3.Zero;
        }
    }
}
﻿using Desolate.Core.Components.Abstract;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Components.Physics
{
    public class Rigidbody : SingleAttitude
    {
        public Vector3 Velocity { get; set; } = Vector3.Zero;

        public bool IsAffectedByGravity { get; set; } = true;
        public float GlobalAcceleration { get; set; } = 100f;
        public bool IsStatic { get; set; }
        public bool IsKinematic { get; set; }
        public float Bounciness { get; set; } = 1.3f;
        public float Friction { get; set; } = 0.5f;

        private Vector3 nonCollidedPosition;

        private Vector3 CurrentVelocity => IsAffectedByGravity
            ? Velocity + Vector3.Down * GlobalAcceleration * Time.DeltaTime
            : Velocity;

        private bool isInCollsion = false;

        public Rigidbody(GameObject gameObject) : base(gameObject)
        {
        }


        public override void OnCollisionEnter(Collider collider)
        {
            if (IsStatic) return;

            if (!(collider.GameObject.GetComponent<Rigidbody>() is Rigidbody colliderRigidbody)) return;

            isInCollsion = true;

            if (IsKinematic && !(colliderRigidbody.IsKinematic || colliderRigidbody.IsStatic))
                ImpactCollider(collider, colliderRigidbody);
            else
            {
                BounceOff(collider);
            }

            base.OnCollisionEnter(collider);
        }

        private void ImpactCollider(Collider collider, Rigidbody rigidbody)
        {
            var direction = GetCollisionPushbackDirection(collider);
            rigidbody.Velocity -= direction * rigidbody.Bounciness * Friction;
        }

        private void BounceOff(Collider collider)
        {
            var direction = GetCollisionPushbackDirection(collider);
            var dot = Vector3.Dot(CurrentVelocity, direction);
            Velocity += CurrentVelocity - 2 * direction * dot * Bounciness;
        }

        private Vector3 GetCollisionPushbackDirection(Collider collider)
        {
            var direction = nonCollidedPosition - GameObject.Transform.WorldPosition;
            if (collider is BoxCollider boxCollider)
                direction = boxCollider.BoxClapVector(direction, true);

            direction.Normalize();
            return direction;
        }

        public override void OnCollisionStay(Collider collider)
        {
            if (IsStatic) return;
            isInCollsion = true;
            if (IsKinematic)
            {
                if (collider is BoxCollider boxCollider)
                {
                    var direction = nonCollidedPosition - GameObject.Transform.WorldPosition;
                    var normalizedDirection = boxCollider.GetPointOriantation(GameObject.Transform.WorldPosition);
                    normalizedDirection.Normalize();
                    var distance = boxCollider.BoxClapVector(direction).Length();
                    GameObject.Transform.Translate(normalizedDirection * distance);
                }

                //TODO else
            }
            else if (collider.GetComponent<Rigidbody>())
            {
                collider.GetComponent<Rigidbody>().Velocity *= Friction * Time.DeltaTime;
            }

            base.OnCollisionStay(collider);
        }

        public override void OnCollisionExit(Collider collider)
        {
            if (IsStatic) return;
            isInCollsion = false;
            base.OnCollisionExit(collider);
        }

        public override void FixedUpdate()
        {
            base.FixedUpdate();
            if (!isInCollsion)
                nonCollidedPosition = GameObject.Transform.WorldPosition;
        }

        public override void LateUpdate()
        {
            base.LateUpdate();
            GameObject.Transform.Translate(CurrentVelocity * Time.DeltaTime);
        }
    }
}
﻿using Desolate.Core.Components.Abstract;

namespace Desolate.Core.Components
{
    public class CameraFacingBillboard: SingleAttitude
    {
        public CameraFacingBillboard(GameObject gameObject) : base(gameObject) {}

        public Camera Target { get; set; }

        public override void Initialize()
        {
            if (Target == null)
                Target = Scene.MainCamera;
            base.Initialize();
        }

        public override void Update()
        {
            if (Target != null)
                GameObject.Transform.LookAtCamera(Target.Transform);
            base.Update();
        }
    }
}
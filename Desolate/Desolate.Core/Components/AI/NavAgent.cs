﻿using System;
using System.Linq;
using Desolate.Core.AI.PathFinding;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Extensions;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Components.AI
{
    public class NavAgent : SingleAttitude
    {
        public NavAgent(GameObject gameObject) : base(gameObject)
        {
        }

        public NavPath Path { get; set; }
        public IPathCalculator PathCalculator { get; set; }

        public float Speed { get; set; } = 1;
        public float AngularSpeed { get; set; } = 0.2f;

        public float RemainingDistance => Vector3.Distance(GameObject.Transform.WorldPosition, finalDestination);

        private float PercentageOnCurrentEdge => Math.Abs(DistanceBetweenCurrentAndNextNode) <= 0
            ? 1.0f
            : Vector3.Distance(currentNode, Transform.WorldPosition) /
              DistanceBetweenCurrentAndNextNode;

        private Vector3 rotation;
        private Vector3 finalDestination;
        private Vector3 nextNode;
        private Vector3 currentNode;
        private Vector3 notLerpedPosition;
        private float DistanceBetweenCurrentAndNextNode => Vector3.Distance(currentNode, nextNode);

        public void SetDestination(Vector3 destination)
        {
            notLerpedPosition = Transform.WorldPosition;
            if (CalculatePath(destination, out NavPath navPath))
            {
                finalDestination = navPath.PathPositions.Last();
                currentNode = Transform.WorldPosition;
                nextNode = navPath.PathPositions[0];
                Path = navPath;
            }
            else
            {
                IsEnabled = false;
            }
        }

        public bool CalculatePath(Vector3 destination, out NavPath path)
        {
            destination.Y = Transform.WorldPosition.Y;
            path = PathCalculator.Calculate(Transform.WorldPosition, destination);
            return path != null;
        }

        public override void Update()
        {
            if (Path == null) return;

            if (PercentageOnCurrentEdge > 0.95)
                MoveToNextNode();

            if (currentNode == finalDestination)
            {
                IsEnabled = false;
                return;
            }

            Vector3 dirToNextNode = nextNode - currentNode;
            dirToNextNode.Y = 0;

            if (dirToNextNode.Length() <= 0)
            {
                IsEnabled = false;
                return;
            }

            dirToNextNode.Normalize();

            if (nextNode != finalDestination)
            {
                Vector3 dirToSecondNextNode =
                    (nextNode == finalDestination
                        ? dirToNextNode
                        : Path.PathPositions[Array.IndexOf(Path.PathPositions, nextNode) + 1] - currentNode);
                dirToSecondNextNode.Normalize();

                float exponentPercent = (float) Math.Exp(PercentageOnCurrentEdge * 4 - 2.0) / 10.0f;

                Vector3 lerpedDirection = Vector3.Lerp(dirToNextNode, dirToSecondNextNode, exponentPercent);
                lerpedDirection.Y = 0;
                lerpedDirection.Normalize();

                UpdatePosition(dirToNextNode, lerpedDirection);
                UpdateRotation(lerpedDirection);
            }
            else
            {
                UpdatePosition(dirToNextNode, dirToNextNode);
                UpdateRotation(dirToNextNode);
            }
        }

        private void MoveToNextNode()
        {
            currentNode = nextNode;

            if (currentNode == finalDestination)
            {
                IsEnabled = false;
                return;
            }

            if (nextNode == finalDestination)
            {
                nextNode = finalDestination;
                return;
            }

            nextNode = Path.PathPositions[Array.IndexOf(Path.PathPositions, nextNode) + 1];
        }

        private void UpdateRotation(Vector3 lerpedDirection)
        {
            float angle = -Vector3Ext.Angle(lerpedDirection, Transform.Backward);
            float angleDrag = angle * Time.DeltaTime * AngularSpeed;

            rotation.Y += angleDrag;
            Transform.LocalEulerAngles = rotation;
        }

        private void UpdatePosition(Vector3 direction, Vector3 lerpedDirection)
        {
            Vector3 translation = direction * Speed * Time.DeltaTime;
            translation.Y = 0;

            Vector3 lerpedTranslation = lerpedDirection * Speed * Time.DeltaTime;
            lerpedDirection.Y = 0;


            Transform.Translate(lerpedTranslation);
            notLerpedPosition += translation;
        }

        public override void Initialize()
        {
            rotation = Transform.LocalEulerAngles;
            base.Initialize();
        }
    }
}
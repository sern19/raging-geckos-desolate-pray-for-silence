﻿using System.Collections.Generic;
using Desolate.Core.Components.Physics;

namespace Desolate.Core.Components.Abstract
{
    public abstract class SingleAttitude
    {
        protected SingleAttitude(GameObject gameObject)
        {
            //Because I WANT TO BE SURE that this will be set
            GameObject = gameObject;
        }

        public readonly GameObject GameObject;
        public bool IsEnabled { get; set; } = true;

        public virtual void Initialize()
        {
        }

        public virtual void Update()
        {
        }

        public virtual void FixedUpdate()
        {
        }

        public virtual void LateUpdate()
        {
        }

        public virtual void OnTriggerEnter(Collider collider)
        {
        }

        public virtual void OnTriggerStay(Collider collider)
        {
        }

        public virtual void OnTriggerExit(Collider collider)
        {
        }

        public virtual void OnCollisionEnter(Collider collider)
        {
        }

        public virtual void OnCollisionStay(Collider collider)
        {
        }

        public virtual void OnCollisionExit(Collider collider)
        {
        }

        public static implicit operator bool(SingleAttitude instance) => instance != null;

        public T GetComponent<T>() where T : SingleAttitude => GameObject.GetComponent<T>();

        public T GetComponentInChildren<T>() where T : SingleAttitude => GameObject.GetComponentInChildren<T>();

        public IEnumerable<T> GetComponents<T>() where T : SingleAttitude => GameObject.GetComponents<T>();

        public IEnumerable<T> GetComponentsInChildren<T>() where T : SingleAttitude => GameObject.GetComponentsInChildren<T>();

        public Transform Transform => GameObject.Transform;
    }
}
﻿using System;
using System.Collections.Generic;
using Desolate.Core.Components.AI;
using Desolate.Core.Components.Physics;
using Desolate.Core.Components.Rendering;
using Desolate.Core.Scripts;
using Desolate.Core.Scripts.Enemy;
using Desolate.Core.Scripts.PlayerBehaviour;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Components
{
    public class Room : GameObject
    {
        private List<Tuple<GameObject, Room>> roomsAndDoors = new List<Tuple<GameObject, Room>>();

        public GameObject player;

        public Model model;

        private BoundingBox obb;

        public string name;

        public bool isPlayerIn = false;
        private bool isNeighboring;

        public void AddDoorsAndRoom(GameObject doors, Room room) => roomsAndDoors.Add(new Tuple<GameObject, Room>(doors, room));
        
        internal override void  Initialize()
        {
            IsActive = true;
            PointsToBox();
        }

        public override void Update()
        {
            foreach (Tuple<GameObject, Room> room in roomsAndDoors)
            {
                if (isPlayerIn)
                {
                    room.Item2.isNeighboring = CheckIfDoorsAreOpen(room.Item1);
                    room.Item2.TurnRenderers(room.Item2.isNeighboring);
                    room.Item2.TurnColliders(room.Item2.isNeighboring);
                }
            }
            
            if (!isNeighboring)
            {
                TurnRenderers(isPlayerIn);
                TurnColliders(isPlayerIn);
            }
            if (CheckPlayer())
            {
                player.GetComponent<Player>().changeRoom(this);
            }
        }

        public void TurnColliders(bool onOff)
        {
            foreach (Collider collider in GetComponentsInChildren<Collider>())
                collider.IsEnabled = onOff;
        }

        public void TurnRenderers(bool onOff)
        {
            foreach (Renderer renderer in GetComponentsInChildren<Renderer>())
                renderer.IsEnabled = onOff;
            foreach (Enemy enemy in GetComponentsInChildren<Enemy>())
                enemy.IsEnabled = onOff;
            foreach (EnemyMovement enemy in GetComponentsInChildren<EnemyMovement>())
                enemy.IsEnabled = onOff;
            foreach (EnemyHearing enemy in GetComponentsInChildren<EnemyHearing>())
                enemy.IsEnabled = onOff;
            foreach (NavAgent enemy in GetComponentsInChildren<NavAgent>())
                enemy.IsEnabled = onOff;
        }

        public void PointsToBox()
        {
            int i;
            Matrix[] modelTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(modelTransforms);
            List<Vector3> points = new List<Vector3>();

            foreach (ModelMesh modelMesh in model.Meshes)
            {
                Matrix boneTransform = modelTransforms[modelMesh.ParentBone.Index];
                foreach (ModelMeshPart point in modelMesh.MeshParts)
                {
                    Vector3[] vector = new Vector3[point.NumVertices];
                    int vertexStride = point.VertexBuffer.VertexDeclaration.VertexStride;

                    point.VertexBuffer.GetData<Vector3>(0, vector, 0, point.NumVertices, vertexStride);
                    for (i=0; i < vector.Length; i++)
                        vector[i] = Vector3.Transform(vector[i], boneTransform);
                    points.AddRange(vector);
                } 
            }

            obb = BoundingBox.CreateFromPoints(points);
        }

        public bool CheckPlayer() => (obb.Contains(player.Transform.WorldPosition) == ContainmentType.Contains);

        public bool CheckIfDoorsAreOpen(GameObject doors) => !(doors.GetComponent<DoorsOpenClose>().isClosed);
    }
}

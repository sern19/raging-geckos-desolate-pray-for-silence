﻿using System;
using System.Linq;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Components.Rendering.Helpers
{
    public struct Triangle
    {
        public Triangle(Vertex a, Vertex b, Vertex c)
        {
            C = c;
            B = b;
            A = a;

            Center = new Vector3
            {
                X = (a.Position.X + b.Position.X + c.Position.X) / 3.0f,
                Z = (a.Position.Z + b.Position.Z + c.Position.Z) / 3.0f
            };
        }

        public Vertex A { get; }
        public Vertex B { get; }
        public Vertex C { get; }

        public Vertex[] GetVertices() => new[] {A, B, C};

        public Vector3 Center { get; }

        /// <summary>
        /// Returns whether index is equal to one of triangle indices. 
        /// </summary>
        public bool Belongs(int index) => A.Index == index || B.Index == index || C.Index == index;

        public bool Touch(Triangle triangle)
        {
            if (triangle.Equals(this)) return false;
            var vertices = GetVertices();
//when indices are invalid
//            return vertices.Any(t => t.Position == triangle.A.Position) &&
//                   vertices.Any(t => t.Position == triangle.B.Position)
//                   || vertices.Any(t => t.Position == triangle.B.Position) &&
//                   vertices.Any(t => t.Position == triangle.C.Position)
//                   || vertices.Any(t => t.Position == triangle.C.Position) &&
//                   vertices.Any(t => t.Position == triangle.B.Position);
            return vertices.Contains(triangle.A) && vertices.Contains(triangle.B)
                   || vertices.Contains(triangle.B) && vertices.Contains(triangle.C)
                   || vertices.Contains(triangle.C) && vertices.Contains(triangle.A);
        }

        private static float Sign(Vector3 p1, Vector3 p2, Vector3 p3)
        {
            return (p1.X - p3.X) * (p2.Z - p3.Z) - (p2.X - p3.X) * (p1.Z - p3.Z);
        }

        static float Area(float x1, float y1, float x2,
            float y2, float x3, float y3)
        {
            return Math.Abs((x1 * (y2 - y3) +
                             x2 * (y3 - y1) +
                             x3 * (y1 - y2)) / 2.0f);
        }

        public bool Cointans(Vector3 pt)
        {
            bool b1 = Sign(pt, A.Position, B.Position) < 0.5f;
            bool b2 = Sign(pt, B.Position, C.Position) < 0.5f;
            bool b3 = Sign(pt, C.Position, A.Position) < 0.5f;

//            return Vector3.Distance(pt, Center) <= (Vector3.Distance(A.Position, Center))
//                   || Vector3.Distance(pt, Center) <= (Vector3.Distance(B.Position, Center))
//                   || Vector3.Distance(pt, Center) <= (Vector3.Distance(C.Position, Center));
            return ((b1 == b2) && (b2 == b3));

//            float a = ((B.Position.Z - C.Position.Z) * (pt.X - C.Position.X) +
//                       (A.Position.X - B.Position.X) * (pt.Z - C.Position.Z)) /
//                      ((B.Position.Z - C.Position.Z) * (A.Position.X - C.Position.X) +
//                       (C.Position.X - B.Position.X) * (A.Position.Z - C.Position.Z));
//            float b = ((C.Position.Z - A.Position.Z) * (pt.X - C.Position.X) +
//                       (A.Position.X - C.Position.X) * (pt.Z - C.Position.Z)) /
//                      ((B.Position.Z - C.Position.Z) * (A.Position.X - C.Position.X) +
//                       (C.Position.X - B.Position.X) * (A.Position.Z - C.Position.Z));
//            float c = 1 - a - b;

            //var cointans = ((0 <= a) && (a <= 1) && (0 <= b) && (b <= 1) && (0 <= c) && (c <= 1));

//            float Aarea = Area(A.Position.X, A.Position.Z, B.Position.X, B.Position.Z, C.Position.X, C.Position.Z);
//
//            /* Calculate area of triangle PBC */
//            float A1 = Area(pt.X, pt.Z, B.Position.X, B.Position.Z, C.Position.X, C.Position.Z);
//
//            /* Calculate area of triangle PAC */
//            float A2 = Area(A.Position.X, A.Position.Z, pt.X, pt.Z, C.Position.X, C.Position.Z);
//
//            /* Calculate area of triangle PAB */
//            float A3 = Area(A.Position.X, A.Position.Z, B.Position.X, B.Position.Z, pt.X, pt.Z);
//
//            bool cointans = (Math.Abs(Aarea - (A1 + A2 + A3)) < 1.1f);
//
//            return cointans;
        }
    }
}
﻿using System.Runtime.Serialization;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Components.Rendering.Helpers
{
    [DataContract]
    public struct Vertex
    {
        [DataMember]
        public int Index { get; set; }
        [DataMember]
        public Vector3 Position { get; set; }

        public Vertex(int index, Vector3 position, bool swapAxisYZ = false)
        {
            Index = index;
            if (swapAxisYZ)
            {
                Position = new Vector3()
                {
                    X = position.X,
                    Y = position.Z,
                    Z = position.Y
                };
            }
            else
            {
                Position = position;
            }
        }
    }
}
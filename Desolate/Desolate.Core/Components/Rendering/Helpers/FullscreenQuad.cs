﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Components.Rendering.Helpers
{
    public class FullscreenQuad
    {
        private readonly VertexBuffer vertexBuffer;
        private readonly IndexBuffer indexBuffer;

        public void Draw()
        {
            GameManager.GraphicsDevice.SetVertexBuffer(vertexBuffer);
            GameManager.GraphicsDevice.Indices = indexBuffer;
            
            GameManager.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, 0, 2);
            
            GameManager.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead; //
        }
        
        internal FullscreenQuad()
        {
            VertexPositionTexture[] verticles = new[]
            {
                new VertexPositionTexture(new Vector3(1, -1, 0), new Vector2(1, 1)), //Right down
                new VertexPositionTexture(new Vector3(-1, -1, 0), new Vector2(0, 1)), //Left down
                new VertexPositionTexture(new Vector3(-1, 1, 0), new Vector2(0, 0)), //Left up
                new VertexPositionTexture(new Vector3(1, 1, 0), new Vector2(1, 0)) //Right up
            };

            ushort[] indices = {0, 1, 2, 2, 3, 0};
            
            //Set buffers
            vertexBuffer = new VertexBuffer(GameManager.GraphicsDevice, VertexPositionTexture.VertexDeclaration, verticles.Length, BufferUsage.None);
            vertexBuffer.SetData(verticles);
            
            indexBuffer = new IndexBuffer(GameManager.GraphicsDevice, IndexElementSize.SixteenBits, indices.Length, BufferUsage.None);
            indexBuffer.SetData(indices);
        }
    }
}
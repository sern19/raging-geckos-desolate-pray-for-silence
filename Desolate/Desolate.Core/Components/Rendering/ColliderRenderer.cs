﻿using System;
using Desolate.Core.Components.Physics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Components.Rendering
{
    public class ColliderRenderer : SpriteRenderer
    {
        private readonly Texture2D texture;

        public ColliderRenderer(GameObject gameObject) : base(gameObject)
        {
            texture = new Texture2D(GameManager.SpriteBatch.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            BasicEffect.TextureEnabled = true;
            BasicEffect.Texture = texture;
            IsOverlay = false;
        }

        public override void LateDraw(Camera camera)
        {
            if (!GameManager.IsDebug) return;

            foreach (var collider in GameObject.GetComponents<Collider>())
            {
                if (collider is BoxCollider boxCollider)
                    DrawBoxCollider(camera, boxCollider);
            }
        }

        private void DrawBoxCollider(Camera camera, BoxCollider boxCollider)
        {
            texture.SetData(new[] {boxCollider.IsTrigger ? Color.Blue : Color.Green});
            BasicEffect.View = camera.View;
            BasicEffect.Projection = camera.Projection;

            #region Back Wall

            base.LateDraw(camera);

            BasicEffect.World =
                Matrix.CreateTranslation(boxCollider.WorldCenter + Vector3.Backward * (boxCollider.Size * 0.5f));

            DrawLine(
                new Vector2(boxCollider.GetLocalPoints()[0].X, boxCollider.GetLocalPoints()[0].Y),
                new Vector2(boxCollider.GetLocalPoints()[1].X, boxCollider.GetLocalPoints()[1].Y));

            DrawLine(new Vector2(boxCollider.GetLocalPoints()[1].X, boxCollider.GetLocalPoints()[1].Y),
                new Vector2(boxCollider.GetLocalPoints()[2].X, boxCollider.GetLocalPoints()[2].Y));

            DrawLine(new Vector2(boxCollider.GetLocalPoints()[2].X, boxCollider.GetLocalPoints()[2].Y),
                new Vector2(boxCollider.GetLocalPoints()[3].X, boxCollider.GetLocalPoints()[3].Y));

            DrawLine(new Vector2(boxCollider.GetLocalPoints()[3].X, boxCollider.GetLocalPoints()[3].Y),
                new Vector2(boxCollider.GetLocalPoints()[0].X, boxCollider.GetLocalPoints()[0].Y));

            GameManager.SpriteBatch.End();

            #endregion

            #region Front Wall

            base.LateDraw(camera);

            BasicEffect.World =
                Matrix.CreateTranslation(boxCollider.WorldCenter + Vector3.Forward * (boxCollider.Size * 0.5f));


            DrawLine(new Vector2(boxCollider.GetLocalPoints()[4].X, boxCollider.GetLocalPoints()[4].Y),
                new Vector2(boxCollider.GetLocalPoints()[5].X, boxCollider.GetLocalPoints()[5].Y));

            DrawLine(new Vector2(boxCollider.GetLocalPoints()[5].X, boxCollider.GetLocalPoints()[5].Y),
                new Vector2(boxCollider.GetLocalPoints()[6].X, boxCollider.GetLocalPoints()[6].Y));

            DrawLine(new Vector2(boxCollider.GetLocalPoints()[6].X, boxCollider.GetLocalPoints()[6].Y),
                new Vector2(boxCollider.GetLocalPoints()[7].X, boxCollider.GetLocalPoints()[7].Y));

            DrawLine(new Vector2(boxCollider.GetLocalPoints()[7].X, boxCollider.GetLocalPoints()[7].Y),
                new Vector2(boxCollider.GetLocalPoints()[4].X, boxCollider.GetLocalPoints()[4].Y));

            GameManager.SpriteBatch.End();

            #endregion

            #region Left Wall

            base.LateDraw(camera);

            BasicEffect.World = Matrix.CreateRotationY(MathHelper.ToRadians(-90)) *
                                Matrix.CreateTranslation(boxCollider.WorldCenter + Vector3.Left * (boxCollider.Size * 0.5f));

            DrawLine(new Vector2(boxCollider.GetLocalPoints()[0].Z, boxCollider.GetLocalPoints()[0].Y),
                new Vector2(boxCollider.GetLocalPoints()[5].Z, boxCollider.GetLocalPoints()[5].Y));

            DrawLine(new Vector2(boxCollider.GetLocalPoints()[3].Z, boxCollider.GetLocalPoints()[3].Y),
                new Vector2(boxCollider.GetLocalPoints()[7].Z, boxCollider.GetLocalPoints()[7].Y));

            GameManager.SpriteBatch.End();

            #endregion

            #region Right Wall

            base.LateDraw(camera);

            BasicEffect.World = Matrix.CreateRotationY(MathHelper.ToRadians(90)) *
                                Matrix.CreateTranslation(boxCollider.WorldCenter + Vector3.Right * (boxCollider.Size * 0.5f));

            DrawLine(new Vector2(boxCollider.GetLocalPoints()[6].Z, boxCollider.GetLocalPoints()[6].Y),
                new Vector2(boxCollider.GetLocalPoints()[2].Z, boxCollider.GetLocalPoints()[2].Y));

            DrawLine(new Vector2(boxCollider.GetLocalPoints()[5].Z, boxCollider.GetLocalPoints()[5].Y),
                new Vector2(boxCollider.GetLocalPoints()[1].Z, boxCollider.GetLocalPoints()[1].Y));

            GameManager.SpriteBatch.End();

            #endregion
        }

        private void DrawLine(Vector2 point1, Vector2 point2, float thickness = 0.02f)
        {
            var distance = Vector2.Distance(point1, point2);
            var angle = (float) Math.Atan2(point2.Y - point1.Y, point2.X - point1.X);
            DrawLine(point1, distance, angle, thickness);
        }

        private void DrawLine(Vector2 point, float length, float angle, float thickness)
        {
            var origin = new Vector2(0f, 0.5f);
            var scale = new Vector2(length, thickness);
            GameManager.SpriteBatch.Draw(texture, point, null, Color.Green, angle, origin, scale, SpriteEffects.None, 0);
        }
    }
}
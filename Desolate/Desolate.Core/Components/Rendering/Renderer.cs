﻿using Desolate.Core.Components.Abstract;

namespace Desolate.Core.Components.Rendering
{
    public abstract class Renderer : SingleAttitude
    {
        protected Transform TransformComponent;

        //Maybe here we can get all references to lights
        public abstract void Draw(Camera camera);

        public virtual void LateDraw(Camera camera)
        {
        }

        public void Draw()
        {
            Draw(Scene.MainCamera);
        }

        public void LateDraw()
        {
            LateDraw(Scene.MainCamera);
        }

        public override void Initialize()
        {
            TransformComponent = GameObject.Transform;
            base.Initialize();
        }

        protected Renderer(GameObject gameObject) : base(gameObject)
        {
        }
    }
}
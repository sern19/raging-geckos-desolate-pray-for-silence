﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Components.Rendering
{
    public abstract class SpriteRenderer : Renderer
    {
        public bool IsOverlay { get; set; } = true;
        protected readonly BasicEffect BasicEffect = new BasicEffect(GameManager.GraphicsDevice);
        protected Vector2 SpritePivot = new Vector2(0.5f, 0.5f);
        public int DrawOrder { get; set; } = 0;
        protected static Matrix OverlayProjection;

        public override void Initialize()
        {
            base.Initialize();
            OverlayProjection = Matrix.CreateTranslation(-0.5f, -0.5f, 0) *
                                Matrix.CreateOrthographicOffCenter(0, GameManager.GraphicsDevice.Viewport.Width,
                                    GameManager.GraphicsDevice.Viewport.Height, 0, 0, 1);
        }

        public override void Draw(Camera camera)
        {
        }

        public override void LateDraw(Camera camera)
        {
            GameManager.SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.AnisotropicWrap,
                IsOverlay ? DepthStencilState.DepthRead : DepthStencilState.Default,
                RasterizerState.CullNone, BasicEffect);
        }

        protected SpriteRenderer(GameObject gameObject) : base(gameObject)
        {
        }
    }
}
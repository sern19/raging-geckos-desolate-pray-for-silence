﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Components.Rendering.Animation.TransformAnimation
{
    public class AnimationClip
    {
        public List<AnimationFrame> AnimationFrames { get; set; } = new List<AnimationFrame>();

        private AnimationFrame CurrentFrame => iterator < AnimationFrames.Count - 1 ? AnimationFrames[iterator] : null;

        private AnimationFrame NextFrame =>
            iterator < AnimationFrames.Count ? AnimationFrames[iterator + 1] : null;


        public TimeSpan ClipDuration => AnimationFrames.ElementAt(AnimationFrames.Count - 1).Time;

        private int iterator;

        public void Initialize()
        {
            iterator = 0;
        }

        public Transform GetTransform(float time, GameObject gameObject)
        {
            if (time < AnimationFrames[0].Time.TotalSeconds)
                return AnimationFrames[0].Transform;


            if (time > NextFrame.Time.TotalSeconds)
            {
                iterator++;
                if (CurrentFrame == null)
                {
                    iterator = 0;
                    return AnimationFrames.Last().Transform;
                }

                return CurrentFrame.Transform;
            }

            float timeFraction = (float) ((time - CurrentFrame.Time.TotalSeconds) /
                                          (NextFrame.Time.TotalSeconds - CurrentFrame.Time.TotalSeconds));

            return new Transform(gameObject)
            {
                LocalPosition = Vector3.Lerp(CurrentFrame.Transform.LocalPosition, NextFrame.Transform.LocalPosition,
                    timeFraction),
                LocalRotation = Quaternion.Lerp(CurrentFrame.Transform.LocalRotation, NextFrame.Transform.LocalRotation,
                    timeFraction),
                LocalScale = Vector3.Lerp(CurrentFrame.Transform.LocalScale, NextFrame.Transform.LocalScale,
                    timeFraction)
            };
        }
    }
}
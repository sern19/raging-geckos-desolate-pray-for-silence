﻿using System;

namespace Desolate.Core.Components.Rendering.Animation.TransformAnimation
{
    public class AnimationFrame
    {
        public Transform Transform { get; set; }
        public TimeSpan Time { get; set; }
    }
}

﻿using System.Collections.Generic;
using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Rendering.Animation.TransformAnimation;

namespace Desolate.Core.Components.Rendering.Animation
{
    public class Animator : SingleAttitude
    {
        public Animator(GameObject gameObject) : base(gameObject)
        {
        }

        public Dictionary<string, AnimationClip> AnimationClips { get; set; } = new Dictionary<string, AnimationClip>();

        public AnimationClip CurrentClip { get; set; }
        public bool IsLooped { get; set; }

        private Transform animationStartTransform;
        private Transform currentTransform;

        private float animationTime;

        private bool isPlaying;
        private bool isPaused;

        public bool IsPlaying => isPlaying && !isPaused;

        public void Play(string clipName = null)
        {
            isPlaying = true;
            isPaused = false;

            if (!string.IsNullOrEmpty(clipName))
                CurrentClip = AnimationClips[clipName];

            animationStartTransform = GameObject.Transform;
            animationTime = 0;

            CurrentClip.Initialize();
        }

        public void PauseAnimation()
        {
            isPaused = true;
        }

        public void StopAnimation()
        {
            isPlaying = false;
            isPaused = false;

            GameObject.Transform.LocalPosition = animationStartTransform.LocalPosition;
            GameObject.Transform.LocalRotation = animationStartTransform.LocalRotation;
            GameObject.Transform.LocalScale = animationStartTransform.LocalScale;
        }

        public override void Update()
        {
            base.Update();

            if (!IsPlaying)
                return;

            animationTime += Time.DeltaTime;

            if (animationTime <= CurrentClip.ClipDuration.TotalSeconds)
            {
                currentTransform = CurrentClip.GetTransform(animationTime, GameObject);
                UpdateGameobjectTransform();
            }
            else
            {
                if (IsLooped) 
                    Play();
                else
                    StopAnimation();
            }
        }

        private void UpdateGameobjectTransform()
        {
            GameObject.Transform.LocalPosition = currentTransform.LocalPosition;
            GameObject.Transform.LocalRotation = currentTransform.LocalRotation;
            GameObject.Transform.LocalScale = currentTransform.LocalScale;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Desolate.Core.Components.Abstract;
using Microsoft.Xna.Framework;
using SkinnedModel;

namespace Desolate.Core.Components.Rendering.Animation
{
    public class SkinnedAnimator : SingleAttitude
    {
        int currentKeyframe;
        public Dictionary<string, AnimationClip> AnimationClips { get; set; } = new Dictionary<string, AnimationClip>();
        public AnimationClip CurrentClip { get; set; }
        public bool IsLooped { get; set; }

        public Transform animationStartTransform;
        public Transform currentTransform;

        protected float animationTime = 0;

        protected bool isPlaying;
        protected bool isPaused;

        public bool IsPlaying => isPlaying && !isPaused;
        public SkinningData SkinningDataValue { get; set; }

        Matrix[] boneTransforms;
        Matrix[] worldTransforms;
        Matrix[] skinTransforms;

        public SkinnedAnimator(GameObject gameObject) : base(gameObject)
        {
        }

        public void CopyAnimation(SkinnedAnimator skin)
        {
            AnimationClips = new Dictionary<string, AnimationClip>(skin.AnimationClips);
        }

        public void InitializeTransforms(SkinningData skinningData)
        {
            SkinningDataValue = skinningData;
            AnimationClips = SkinningDataValue.AnimationClips;
            boneTransforms = new Matrix[SkinningDataValue.BindPose.Count];
            worldTransforms = new Matrix[SkinningDataValue.BindPose.Count];
            skinTransforms = new Matrix[SkinningDataValue.BindPose.Count];
        }

        public void Play(string clipName = null)
        {
            isPlaying = true;
            isPaused = false;

            if (!string.IsNullOrEmpty(clipName))
                CurrentClip = AnimationClips[clipName];

            animationStartTransform = GameObject.Transform;
            animationTime = 0;

            currentKeyframe = 0;
            SkinningDataValue.BindPose.CopyTo(boneTransforms, 0);
        }

        public void PauseAnimation()
        {
            isPaused = true;
        }

        public void StopAnimation()
        {
            isPlaying = false;
            isPaused = false;

            GameObject.Transform.LocalPosition = animationStartTransform.LocalPosition;
            GameObject.Transform.LocalRotation = animationStartTransform.LocalRotation;
            GameObject.Transform.LocalScale = animationStartTransform.LocalScale;
        }

        public override void Update()
        {
            base.Update();

            if (!IsPlaying)
                return;

            animationTime += Time.DeltaTime;

            if (animationTime <= CurrentClip.Duration.TotalSeconds)
            {
                UpdateBoneTransforms();
                UpdateWorldTransforms();
                UpdateSkinTransforms();
            }
            else
            {
                if (IsLooped)
                    Play();
                else
                    StopAnimation();
            }
        }


        public void UpdateBoneTransforms()
        {
            if (CurrentClip == null)
                throw new InvalidOperationException(
                            "AnimationPlayer.Update was called before StartClip");


            // Read keyframe matrices.
            IList<Keyframe> keyframes = CurrentClip.Keyframes;

            while (currentKeyframe < keyframes.Count)
            {
                Keyframe keyframe = keyframes[currentKeyframe];

                // Stop when we've read up to the current time position.
                if (keyframe.Time.TotalSeconds > animationTime)
                    break;

                // Use this keyframe.
                boneTransforms[keyframe.Bone] = keyframe.Transform;

                currentKeyframe++;
            }
        }

        public void UpdateWorldTransforms()
        {
            worldTransforms[0] = boneTransforms[0];

            for (int bone = 1; bone < worldTransforms.Length; bone++)
            {
                int parentBone = SkinningDataValue.SkeletonHierarchy[bone];

                worldTransforms[bone] = boneTransforms[bone] *
                                             worldTransforms[parentBone];
            }
        }
        public void UpdateSkinTransforms()
        {
            for (int bone = 0; bone < skinTransforms.Length; bone++)
            {
                skinTransforms[bone] = SkinningDataValue.InverseBindPose[bone] *
                                            worldTransforms[bone];
            }
        }

        public Matrix[] GetBoneTransforms()
        {
            return boneTransforms;
        }

        public Matrix[] GetWorldTransforms()
        {
            return worldTransforms;
        }

        public Matrix[] GetSkinTransforms()
        {
            return skinTransforms;
        }

    }
}

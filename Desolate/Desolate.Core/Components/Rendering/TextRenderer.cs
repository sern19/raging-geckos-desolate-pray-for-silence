﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Components.Rendering
{
    public class TextRenderer : SpriteRenderer
    {
        private string text = "";
        private SpriteFont font;
        private Color color = Color.White;

        #region Properties

        public SpriteFont Font
        {
            get => font;
            set
            {
                font = value;
                BasicEffect.TextureEnabled = true;
                BasicEffect.Texture = Font.Texture;
            }
        }

        public string Text
        {
            get => text;
            set
            {
                text = value;
                UpdatePositionCompensation();
            }
        }

        public Vector2 Pivot
        {
            get => SpritePivot;
            set
            {
                SpritePivot.X = MathHelper.Clamp(value.X, 0, 1);
                SpritePivot.Y = MathHelper.Clamp(value.Y, 0, 1);
                UpdatePositionCompensation();
            }
        }

        public Color Color
        {
            get => color;
            set
            {
                color = value;
                BasicEffect.DiffuseColor = Color.ToVector3();
            }
        }

        #endregion

        private static readonly Matrix TextRotationCompensation =
            Matrix.CreateFromYawPitchRoll(MathHelper.ToRadians(180), MathHelper.ToRadians(0), MathHelper.ToRadians(180));

        private Matrix textPositionCompensation = Matrix.CreateTranslation(new Vector3(0, 0.5f, 0));

        private void UpdatePositionCompensation()
        {
            textPositionCompensation = IsOverlay
                ? textPositionCompensation =
                    Matrix.CreateTranslation(new Vector3(-Font.MeasureString(Text).X * Pivot.X, Font.MeasureString(Text).Y * -Pivot.Y, 0))
                : textPositionCompensation =
                    Matrix.CreateTranslation(new Vector3(-Font.MeasureString(Text).X * Pivot.X / Font.MeasureString(Text).Y, Pivot.Y, 0));
        }

        public override void LateDraw(Camera camera)
        {
            base.LateDraw(camera);

            if (IsOverlay)
            {
                BasicEffect.View = Matrix.Identity;
                BasicEffect.Projection = OverlayProjection;
                BasicEffect.World = textPositionCompensation * TransformComponent.TransformMatrix;
            }
            else
            {
                BasicEffect.View = camera.View;
                BasicEffect.Projection = camera.Projection;
                BasicEffect.World = TextRotationCompensation *
                                    Matrix.CreateScale(1.0f / Font.MeasureString(Text).Y, 1.0f / Font.MeasureString(Text).Y,
                                        1.0f / Font.MeasureString(Text).Y) * textPositionCompensation * TransformComponent.TransformMatrix;
            }

            GameManager.SpriteBatch.DrawString(Font, Text, Vector2.Zero, Color);

            GameManager.SpriteBatch.End();
        }

        public TextRenderer(GameObject gameObject, SpriteFont font) : base(gameObject)
        {
            Font = font;
            UpdatePositionCompensation();
        }
    }
}
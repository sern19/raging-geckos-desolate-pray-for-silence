﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Components.Rendering
{
    public class ImageRenderer : SpriteRenderer
    {
        private Texture2D image;
        private static readonly Matrix ImageScaleCompensation = Matrix.CreateScale(new Vector3(0.5f, 0.5f, 0.5f));
        private readonly Matrix imageSizeCompensation;
        private Color color = Color.White;
        private float filling = 1;

        public Color Color
        {
            get => color;
            set
            {
                if (color.ToVector4() == value.ToVector4()) return;
                color = value;
                BasicEffect.DiffuseColor = Color.ToVector3();
            }
        }

        public float Filling
        {
            get => filling;
            set => filling = MathHelper.Clamp(value, 0, 1);
        }

        #region Properties

        public Texture2D Image
        {
            get => image;
            set
            {
                image = value;
                BasicEffect.TextureEnabled = true;
                BasicEffect.Texture = Image;
            }
        }

        public Vector2 Pivot
        {
            get => SpritePivot;
            set
            {
                SpritePivot.X = MathHelper.Clamp(value.X, 0, 1);
                SpritePivot.Y = MathHelper.Clamp(value.Y, 0, 1);
            }
        }

        #endregion

        public override void LateDraw(Camera camera)
        {
            base.LateDraw(camera);
            if (IsOverlay)
            {
                BasicEffect.View = Matrix.Identity;
                BasicEffect.Projection = OverlayProjection;
                BasicEffect.World = ImageScaleCompensation * TransformComponent.TransformMatrix;
            }
            else
            {
                BasicEffect.View = camera.View;
                BasicEffect.Projection = camera.Projection;
                BasicEffect.World = imageSizeCompensation * ImageScaleCompensation * TransformComponent.TransformMatrix;
            }

            GameManager.SpriteBatch.Draw(Image,
                new Rectangle(-(int) (2 * Image.Width * Filling * Pivot.X), -(int) (2 * Image.Height * Pivot.Y),
                    (int) (2 * Image.Width * Filling),
                    2 * Image.Height),
                Color);

            GameManager.SpriteBatch.End();
        }

        public ImageRenderer(GameObject gameObject, Texture2D image) : base(gameObject)
        {
            Image = image;
            imageSizeCompensation = Matrix.CreateFromYawPitchRoll(MathHelper.ToRadians(180), MathHelper.ToRadians(0),
                                        MathHelper.ToRadians(180)) *
                                    Matrix.CreateScale(new Vector3(1.0f / Image.Width, 1.0f / Image.Height, 1));
        }
    }
}
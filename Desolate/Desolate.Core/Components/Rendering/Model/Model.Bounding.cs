﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Components.Rendering.Model
{
    public static class Model_Bounding
    {
        internal static IEnumerable<Vector3> GetAllPoints(this Microsoft.Xna.Framework.Graphics.Model model, Vector3 scale)
        {
            int i;

            Matrix scaleMatrix = Matrix.CreateScale(scale);

            Matrix[] modelTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(modelTransforms);
            List<Vector3> points = new List<Vector3>();

            foreach (ModelMesh modelMesh in model.Meshes)
            {
                Matrix boneTransform = modelTransforms[modelMesh.ParentBone.Index];
                foreach (ModelMeshPart point in modelMesh.MeshParts)
                {
                    Vector3[] vector = new Vector3[point.NumVertices];
                    int vertexStride = point.VertexBuffer.VertexDeclaration.VertexStride;

                    point.VertexBuffer.GetData(0, vector, 0, point.NumVertices, vertexStride);
                    for (i = 0; i < vector.Length; i++)
                        vector[i] = Vector3.Transform(vector[i], boneTransform * scaleMatrix);
                    points.AddRange(vector);
                }
            }

            return points;
        }

        public static BoundingBox CreateBoundingBox(this Microsoft.Xna.Framework.Graphics.Model model, Vector3 scale) =>
            BoundingBox.CreateFromPoints(model.GetAllPoints(scale));

        public static BoundingBox CreateBoundingBox(this Microsoft.Xna.Framework.Graphics.Model model) => CreateBoundingBox(model, Vector3.One);

        public static BoundingSphere CreateBoundingSphere(this Microsoft.Xna.Framework.Graphics.Model model, Vector3 scale) =>
            BoundingSphere.CreateFromPoints(model.GetAllPoints(scale));

        public static BoundingSphere CreateBoundingSphere(this Microsoft.Xna.Framework.Graphics.Model model) => CreateBoundingSphere(model, Vector3.One);
    }
}
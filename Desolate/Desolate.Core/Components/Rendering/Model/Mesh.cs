﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Rendering.Animation;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Components.Rendering.Model
{
    public class Mesh : SingleAttitude 
    {
        public Mesh(GameObject gameObject) : base(gameObject)
        {
        }
        
        public void RenderModel(GameTime gameTime, SkinnedAnimator animationController, Microsoft.Xna.Framework.Graphics.Model currentModel, GraphicsDeviceManager graphics)
        {
            Matrix[] bones = animationController.GetSkinTransforms();

            // Compute camera matrices.
            //Matrix view = Matrix.CreateTranslation(0, -40, 0) *
            //              Matrix.CreateRotationY(MathHelper.ToRadians(cameraRotation)) *
            //              Matrix.CreateRotationX(MathHelper.ToRadians(cameraArc)) *
            //              Matrix.CreateLookAt(new Vector3(0, 0, -cameraDistance),
            //                                  new Vector3(0, 0, 0), Vector3.Up);

            Matrix projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4,
                                                                    graphics.GraphicsDevice.Viewport.AspectRatio,
                                                                    1,
                                                                    10000);


            // Render the skinned mesh.
            foreach (ModelMesh mesh in currentModel.Meshes)
            {
                foreach (SkinnedEffect effect in mesh.Effects)
                {
                    effect.SetBoneTransforms(bones);

                    effect.View = Matrix.CreateTranslation(0, -40, 0);
                    effect.Projection = projection;

                    effect.EnableDefaultLighting();

                    effect.SpecularColor = new Vector3(0.25f);
                    effect.SpecularPower = 16;
                }

                mesh.Draw();
            }
        }
    }
}

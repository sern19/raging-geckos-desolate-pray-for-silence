﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Components.Rendering.Lightning
{
    public class DirectionalLight: Light
    {
        public Vector3 Direction { get; set; } = Vector3.Down;

        private Vector3 position = Vector3.Zero;
        private Matrix view = Matrix.Identity;
        private Matrix projection = Matrix.Identity;

        public override void DrawShadowMap()
        {
            int i;
            
            GameManager.GraphicsDevice.SetRenderTarget(ShadowMap);
            
            GameManager.GraphicsDevice.Clear(Color.Transparent);
            
//            // Matrix with that will rotate in points the direction of the light
//            Matrix lightRotation = Matrix.CreateLookAt(Vector3.Zero, -Direction, Vector3.Up);
//            
//            IEnumerable<GameObject> allGameObjects = Scene.GetAllGameObjects();
//            BoundingBox box = new BoundingBox();
//
//            box = (from gameObject in allGameObjects from meshRenderer in gameObject.GetComponents<MeshRenderer>() where meshRenderer.IsVisible() select meshRenderer).Aggregate(box, (current, meshRenderer) => BoundingBox.CreateMerged(current, BoundingBox.CreateFromSphere(meshRenderer.BoundingSphere)));
//
//            //box.Transform(lightRotation);
//            Vector3[] boxCorners = box.GetCorners();
//
////            for (i = 0; i < boxCorners.Length; i++)
////                boxCorners[i] = Vector3.Transform(boxCorners[i], lightRotation);
//
//            // Find the smallest box around the points
//            BoundingBox lightBox = BoundingBox.CreateFromPoints(boxCorners);
//
//            Vector3 boxSize = lightBox.Max - lightBox.Min;
//            Vector3 halfBoxSize = boxSize * 0.5f;
//
//            
//            
//            // The position of the light should be in the center of the back pannel of the box. 
//            position = lightBox.Min + halfBoxSize;
//            //position.Z = lightBox.Min.Z;
//
//            // We need the position back in world coordinates so we transform the light position by the inverse of the lights rotation
//            position = Vector3.Transform(position, Matrix.Invert(lightRotation));

            float range = GameManager.DirectionalShadowRenderingRange;
            GameManager.GraphicsDevice.SamplerStates[0] = ShadowMapSampler;
            
            position = Scene.MainCamera.Transform.WorldPosition;
            view = Matrix.CreateLookAt(position, position - Direction,  Vector3.Up);
            projection = Matrix.CreateOrthographic(range, range, -range, range);
            
            DeferredMeshRenderer.DepthWriterEffect.Parameters["View"].SetValue(view);
            DeferredMeshRenderer.DepthWriterEffect.Parameters["Projection"].SetValue(projection);
            DeferredMeshRenderer.DepthWriterEffect.Parameters["LPosition"].SetValue(position);
            DeferredMeshRenderer.DepthWriterEffect.Parameters["DepthPrecision"].SetValue(FarPlane);
            
            DeferredMeshRenderer.DepthWriterSkinnedEffect.Parameters["View"].SetValue(view);
            DeferredMeshRenderer.DepthWriterSkinnedEffect.Parameters["Projection"].SetValue(projection);
            DeferredMeshRenderer.DepthWriterSkinnedEffect.Parameters["LPosition"].SetValue(position);
            DeferredMeshRenderer.DepthWriterSkinnedEffect.Parameters["DepthPrecision"].SetValue(FarPlane);
        }

        public override void Draw()
        {
            if (SpecularColor!= null && SpecularColor == Color.Black) //Because black is bad for our shader
                SpecularColor = new Color(0.01f, 0.01f, 0.01f);
            DeferredMeshRenderer.DirectionalLightEffect.Parameters["DLViewProjection"].SetValue(view * projection);
            DeferredMeshRenderer.DirectionalLightEffect.Parameters["DLDirection"].SetValue(Vector3.Normalize(Direction));
            DeferredMeshRenderer.DirectionalLightEffect.Parameters["DLColor"].SetValue(Vector3.Normalize(Color.ToVector3()));
            DeferredMeshRenderer.DirectionalLightEffect.Parameters["DLSpecularColor"].SetValue(Vector3.Normalize((SpecularColor ?? Color).ToVector3()));
            DeferredMeshRenderer.DirectionalLightEffect.Parameters["DLPosition"].SetValue(position);
            DeferredMeshRenderer.DirectionalLightEffect.Parameters["DLIntensity"].SetValue(Intensity);
            DeferredMeshRenderer.DirectionalLightEffect.Parameters["CastsShadow"].SetValue(CastsShadow);
            DeferredMeshRenderer.DirectionalLightEffect.Parameters["DepthPrecision"].SetValue(FarPlane);
            DeferredMeshRenderer.DirectionalLightEffect.Parameters["DepthBias"].SetValue(DepthBias);
            DeferredMeshRenderer.DirectionalLightEffect.Parameters["ShadowMapTexture"].SetValue(ShadowMap);
            DeferredMeshRenderer.DirectionalLightEffect.Parameters["ShadowMapSize"].SetValue(GameManager.ShadowMapResolution);
            
            DeferredMeshRenderer.DirectionalLightEffect.CurrentTechnique.Passes[0].Apply();
            GameManager.FullscreenQuad.Draw();
        }
        
        public DirectionalLight(GameObject gameObject) : base(gameObject) { }
    }
}
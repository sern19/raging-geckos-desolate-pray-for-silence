﻿using Desolate.Core.Components.Abstract;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Components.Rendering.Lightning
{
    public abstract class Light : SingleAttitude
    {
        protected Light(GameObject gameObject) : base(gameObject)
        {
            CastsShadow = CastsShadow && GameManager.EnableShadows;
        }

        public Color Color { get; set; } = Color.White;
        public Color? SpecularColor { get; set; }
        public float Intensity { get; set; } = 1.0f;
        
        public bool CastsShadow { get; set; } = true;
        public float DepthBias { get; set; } = 1.0f / 2000.0f;
        public float NearPlane { get; set; } = 1.0f;
        public float FarPlane { get; set; } = 100.0f; //Maybe use camera far for this

        protected RenderTarget2D ShadowMap;
        
        protected static readonly SamplerState ShadowMapSampler = new SamplerState
        {
            AddressU = TextureAddressMode.Clamp,
            AddressV = TextureAddressMode.Clamp,
            AddressW = TextureAddressMode.Clamp,
            Filter = TextureFilter.Linear,
            ComparisonFunction = CompareFunction.GreaterEqual,
            FilterMode = TextureFilterMode.Comparison 
        };

        public override void Initialize()
        {
            if (CastsShadow)
                ShadowMap = new RenderTarget2D(GameManager.GraphicsDevice, (int)GameManager.ShadowMapResolution.X, (int)GameManager.ShadowMapResolution.Y, false,
                    SurfaceFormat.Single, DepthFormat.Depth24Stencil8);
        }

        public abstract void DrawShadowMap();

        /// <summary>
        /// Used only in deferred rendering.
        /// </summary>
        public abstract void Draw();
    }
}
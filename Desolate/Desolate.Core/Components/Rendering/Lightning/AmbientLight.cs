﻿using Microsoft.Xna.Framework;

namespace Desolate.Core.Components.Rendering.Lightning
{
    public class AmbientLight: Light
    {
        public AmbientLight(GameObject gameObject) : base(gameObject)
        {
            CastsShadow = false;
        }

        /// <summary>
        /// Ambient light doesn't have shadows
        /// </summary>
        public override void DrawShadowMap() {}

        /// <summary>
        /// Adds fixed value to light contribiution
        /// </summary>
        public override void Draw()
        {
            DeferredMeshRenderer.AmbientLightEffect.Parameters["AColor"].SetValue(Vector3.Normalize(Color.ToVector3()));
            DeferredMeshRenderer.AmbientLightEffect.Parameters["AIntensity"].SetValue(Intensity);
            
            DeferredMeshRenderer.AmbientLightEffect.CurrentTechnique.Passes[0].Apply();
            GameManager.FullscreenQuad.Draw();
        }
    }
}
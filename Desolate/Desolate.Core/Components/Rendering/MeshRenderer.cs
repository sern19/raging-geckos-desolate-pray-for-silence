﻿using Desolate.Core.Components.Rendering.Animation;
using Desolate.Core.Components.Rendering.Model;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Components.Rendering
{
    public abstract class MeshRenderer : Renderer
    {
        protected readonly Microsoft.Xna.Framework.Graphics.Model Model;
        protected Matrix[] ModelTransforms;
        protected Matrix[] Bones = null;

        protected bool IsSkinned = false;

        public BoundingSphere BoundingSphere { get; set; }
        public bool CastsShadow { get; set; } = true;
        public bool IsVisible(Camera camera) => IsEnabled && GameObject.IsActive && camera.InView(BoundingSphere.Transform(Transform.TransformMatrix));
        public bool IsVisible() => IsVisible(Scene.MainCamera);
        
        public override void Initialize()
        {
            base.Initialize();
            ModelTransforms = new Matrix[Model.Bones.Count];
            Model.CopyAbsoluteBoneTransformsTo(ModelTransforms);

            if (!(GameObject.GetComponent<SkinnedAnimator>() is SkinnedAnimator skinnedAnimator)) return;

            Bones = skinnedAnimator.GetSkinTransforms();
            IsSkinned = true;
        }

        public abstract void DrawForShadows();

        protected MeshRenderer(GameObject gameObject, Microsoft.Xna.Framework.Graphics.Model model) : base(gameObject)
        {
            Model = model;
            BoundingSphere = model.CreateBoundingSphere();
            CastsShadow = CastsShadow && GameManager.EnableShadows;
        }
    }
}
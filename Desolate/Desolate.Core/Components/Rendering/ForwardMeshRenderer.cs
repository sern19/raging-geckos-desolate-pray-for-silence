﻿using System;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Components.Rendering
{
    [Obsolete]
    public class ForwardMeshRenderer : MeshRenderer
    {
        private Effect customEffect = null;

        public override void Initialize()
        {
            base.Initialize();
            foreach (ModelMesh mesh in Model.Meshes)
            foreach (Effect effect in mesh.Effects)
                if (!(effect is BasicEffect || effect is SkinnedEffect))
                {
                    customEffect = IsSkinned? (Effect)new SkinnedEffect(GameManager.GraphicsDevice) : new BasicEffect(GameManager.GraphicsDevice);
                    return;
                }
        }

        public override void DrawForShadows() //TODO
        {
            throw new NotImplementedException();
        }

        public override void Draw(Camera camera)
        {
            if (!camera.InView(BoundingSphere.Transform(Transform.TransformMatrix)))
                return;
            foreach (ModelMesh mesh in Model.Meshes)
            {
                if (customEffect != null)
                {
                    foreach (ModelMeshPart part in mesh.MeshParts)
                    {
                        GameManager.GraphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                        GameManager.GraphicsDevice.Indices = part.IndexBuffer;
                        
                        if (IsSkinned)
                        {
                            SkinnedEffect skinnedEffect = (SkinnedEffect) customEffect;
                            skinnedEffect.Projection = camera.Projection;
                            skinnedEffect.View = camera.View;
                            skinnedEffect.SetBoneTransforms(Bones);
                            skinnedEffect.World = TransformComponent.TransformMatrix;

                            //Here we should also setup all lights, pain in te ass, maybe there is other way around it
                            skinnedEffect.PreferPerPixelLighting = true;
                            skinnedEffect.EnableDefaultLighting();

                            skinnedEffect.DiffuseColor = part.Effect.Parameters["DiffuseColor"].GetValueVector3();
                            skinnedEffect.Alpha = part.Effect.Parameters["Alpha"].GetValueSingle();
                            skinnedEffect.Texture = part.Effect.Parameters["Texture"].GetValueTexture2D();
                        }
                        else
                        {
                            BasicEffect basicEffect = (BasicEffect) customEffect;
                            basicEffect.Projection = camera.Projection;
                            basicEffect.View = camera.View;
                            basicEffect.World = ModelTransforms[mesh.ParentBone.Index] *
                                                TransformComponent.TransformMatrix;

                            //Here we should also setup all lights, pain in te ass, maybe there is other way around it
                            basicEffect.PreferPerPixelLighting = true;
                            basicEffect.EnableDefaultLighting();
                            
                            basicEffect.DiffuseColor = part.Effect.Parameters["DiffuseColor"].GetValueVector3();
                            basicEffect.Alpha = part.Effect.Parameters["Alpha"].GetValueSingle();
                            basicEffect.Texture = part.Effect.Parameters["Texture"].GetValueTexture2D();
                            basicEffect.TextureEnabled = basicEffect.Texture != null;
                        }
                        
                        customEffect.Parameters["SpecularColor"].SetValue(part.Effect.Parameters["SpecularColor"].GetValueVector3());
                        customEffect.Parameters["SpecularPower"].SetValue(part.Effect.Parameters["SpecularPower"].GetValueSingle());

                        customEffect.CurrentTechnique.Passes[0].Apply();
                        GameManager.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, part.StartIndex, part.PrimitiveCount);
                    }
                }
                else
                {
                    foreach (Effect effect in mesh.Effects)
                    {
                        //Not exacly sure if this will play nicely with SkinnedEffect - needs testing
                        //If not, I'll probably have to write two draw methods for effects
                        switch (effect)
                        {
                            case BasicEffect basicEffect:
                                basicEffect.Projection = camera.Projection;
                                basicEffect.View = camera.View;
                                basicEffect.World = ModelTransforms[mesh.ParentBone.Index] *
                                                    TransformComponent.TransformMatrix;

                                //Here we should also setup all lights, pain in te ass, maybe there is other way around it
                                basicEffect.PreferPerPixelLighting = true;
                                basicEffect.EnableDefaultLighting();
                                break;
                            case SkinnedEffect skinnedEffect:
                                skinnedEffect.Projection = camera.Projection;
                                skinnedEffect.View = camera.View;
                                if (IsSkinned)
                                {
                                    skinnedEffect.SetBoneTransforms(Bones);
                                    skinnedEffect.World = TransformComponent.TransformMatrix;
                                }
                                else
                                    skinnedEffect.World = ModelTransforms[mesh.ParentBone.Index] *
                                                          TransformComponent.TransformMatrix;

                                //Here we should also setup all lights, pain in te ass, maybe there is other way around it
                                skinnedEffect.PreferPerPixelLighting = true;
                                skinnedEffect.EnableDefaultLighting();
                                break;
                        }
                    }

                    mesh.Draw();
                }
            }
        }

        public ForwardMeshRenderer(GameObject gameObject, Microsoft.Xna.Framework.Graphics.Model model) : base(gameObject, model)
        {}
    }
}
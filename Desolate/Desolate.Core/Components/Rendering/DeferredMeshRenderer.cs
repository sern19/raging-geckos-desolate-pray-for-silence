﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Components.Rendering
{
    public class DeferredMeshRenderer : MeshRenderer
    {
        private static RenderTargetBinding[] gBufferTargets;
        private static RenderTargetBinding lightmapTarget;
        private static RenderTargetBinding finalTarget;
        private static Vector2 gBufferTargetSize;

        private Effect currentGBufferEffect;
        private Effect currentDepthWriterEffect;

        public static Effect ClearEffect;
        public static Effect GBufferEffect;
        public static Effect GBufferSkinnedEffect;
        public static Effect AmbientLightEffect;
        public static Effect DirectionalLightEffect;
        public static Effect PointLightEffect;
        public static Effect SpotLightEffect;
        public static Effect CompositionEffect;
        public static Effect DepthWriterEffect;
        public static Effect DepthWriterSkinnedEffect;
        public static Effect RestoreDepthEffect;

        private static BlendState lightmapBlendState;

        public override void Initialize()
        {
            base.Initialize();
            currentGBufferEffect = IsSkinned ? GBufferSkinnedEffect : GBufferEffect;
            currentDepthWriterEffect = IsSkinned ? DepthWriterSkinnedEffect : DepthWriterEffect;
        }

        #region Static Methods

        /// <summary>
        /// Used to initialize GBuffer textures. Should be called when window size is changed
        /// </summary>
        public static void InitializeGBufferTargets()
        {
            gBufferTargetSize = GameManager.ShouldUseScreenResolution
                ? new Vector2(GameManager.GraphicsDevice.Viewport.Width, GameManager.GraphicsDevice.Viewport.Height)
                : GameManager.RenderResolution;

            gBufferTargets = new RenderTargetBinding[3];
            gBufferTargets[0] = new RenderTargetBinding(new RenderTarget2D(GameManager.GraphicsDevice, (int) gBufferTargetSize.X, (int) gBufferTargetSize.Y,
                false, SurfaceFormat.Rgba64, DepthFormat.Depth24Stencil8));
            gBufferTargets[1] = new RenderTargetBinding(new RenderTarget2D(GameManager.GraphicsDevice, (int) gBufferTargetSize.X, (int) gBufferTargetSize.Y,
                false, SurfaceFormat.Rgba64, DepthFormat.Depth24Stencil8));
            gBufferTargets[2] = new RenderTargetBinding(new RenderTarget2D(GameManager.GraphicsDevice, (int) gBufferTargetSize.X, (int) gBufferTargetSize.Y,
                false, SurfaceFormat.Vector2, DepthFormat.Depth24Stencil8));
            lightmapTarget = new RenderTargetBinding(new RenderTarget2D(GameManager.GraphicsDevice, (int) gBufferTargetSize.X, (int) gBufferTargetSize.Y,
                false, SurfaceFormat.Rgba64, DepthFormat.Depth24Stencil8));
            finalTarget = new RenderTargetBinding(new RenderTarget2D(GameManager.GraphicsDevice, (int) gBufferTargetSize.X, (int) gBufferTargetSize.Y,
                true, SurfaceFormat.Color, DepthFormat.Depth24Stencil8));

            DirectionalLightEffect.Parameters["GBufferTargetSize"].SetValue(gBufferTargetSize);
            //PointLightEffect.Parameters["GBufferTargetSize"].SetValue(gBufferTargetSize);
            //SpotLightEffect.Parameters["GBufferTargetSize"].SetValue(gBufferTargetSize);
            CompositionEffect.Parameters["GBufferTargetSize"].SetValue(gBufferTargetSize);
            RestoreDepthEffect.Parameters["GBufferTargetSize"].SetValue(gBufferTargetSize);

            lightmapBlendState = new BlendState
            {
                ColorSourceBlend = Blend.One,
                ColorDestinationBlend = Blend.One,
                ColorBlendFunction = BlendFunction.Add,
                AlphaSourceBlend = Blend.One,
                AlphaDestinationBlend = Blend.One,
                AlphaBlendFunction = BlendFunction.Add
            };
        }

        /// <summary>
        /// Clears GBuffer and prepares for rendering
        /// </summary>
        public static void ClearGBuffer()
        {
            GameManager.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;
            GameManager.GraphicsDevice.SetRenderTargets(gBufferTargets);

            ClearEffect.CurrentTechnique.Passes[0].Apply();

            GameManager.FullscreenQuad.Draw();
        }

        /// <summary>
        /// Sets targets, samplers, calculates camera matrices
        /// </summary>
        public static void PrepareLightPass(Camera camera)
        {
            GameManager.GraphicsDevice.SetRenderTargets(lightmapTarget);

            GameManager.GraphicsDevice.Clear(Color.Transparent);

            GameManager.GraphicsDevice.BlendState = lightmapBlendState;
            GameManager.GraphicsDevice.DepthStencilState = DepthStencilState.DepthRead;

            #region Samplers

            DirectionalLightEffect.Parameters["GBuffer0Texture"].SetValue(gBufferTargets[0].RenderTarget);
            DirectionalLightEffect.Parameters["GBuffer1Texture"].SetValue(gBufferTargets[1].RenderTarget);
            DirectionalLightEffect.Parameters["GBuffer2Texture"].SetValue(gBufferTargets[2].RenderTarget);

            #endregion

            Matrix inverseView = Matrix.Invert(camera.View);
            Matrix inverseViewProjection = Matrix.Invert(camera.View * camera.Projection);

            #region Set parameters

            DirectionalLightEffect.Parameters["InverseView"].SetValue(inverseView);
            DirectionalLightEffect.Parameters["InverseViewProjection"].SetValue(inverseViewProjection);
            DirectionalLightEffect.Parameters["CameraPosition"].SetValue(camera.Transform.WorldPosition);

//            PointLightEffect.Parameters["InverseView"].SetValue(inverseView);
//            PointLightEffect.Parameters["InverseViewProjection"].SetValue(inverseViewProjection);
//            PointLightEffect.Parameters["CameraPosition"].SetValue(camera.Transform.WorldPosition);
//            
//            SpotLightEffect.Parameters["InverseView"].SetValue(inverseView);
//            SpotLightEffect.Parameters["InverseViewProjection"].SetValue(inverseViewProjection);
//            SpotLightEffect.Parameters["CameraPosition"].SetValue(camera.Transform.WorldPosition);

            #endregion
        }

        /// <summary>
        /// Renders combined output
        /// </summary>
        public static void FinalPass()
        {
            GameManager.GraphicsDevice.SetRenderTargets(finalTarget);

            GameManager.GraphicsDevice.Clear(Color.Transparent);

            GameManager.GraphicsDevice.BlendState = BlendState.Opaque;
            GameManager.GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GameManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            
            CompositionEffect.Parameters["DiffuseTexture"].SetValue(gBufferTargets[0].RenderTarget);
            CompositionEffect.Parameters["LightmapTexture"].SetValue(lightmapTarget.RenderTarget);

            CompositionEffect.CurrentTechnique.Passes[0].Apply();

            GameManager.FullscreenQuad.Draw();
        }

        /// <summary>
        /// Prepares for UI/Transparency passes
        /// </summary>
        /// <param name="output"></param>
        public static void RestoreDepth(RenderTarget2D output)
        {
            GameManager.GraphicsDevice.SetRenderTarget(output);

            GameManager.GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GameManager.GraphicsDevice.RasterizerState = RasterizerState.CullNone;
            GameManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            
            RestoreDepthEffect.Parameters["FinalMap"].SetValue(finalTarget.RenderTarget);
            RestoreDepthEffect.Parameters["DepthMap"].SetValue(gBufferTargets[2].RenderTarget);

            RestoreDepthEffect.CurrentTechnique.Passes[0].Apply();

            GameManager.FullscreenQuad.Draw();
        }

        /// <summary>
        /// Draws GBuffer targets content
        /// </summary>
        public static void DrawDebug()
        {
            GameManager.GraphicsDevice.SetRenderTarget(null);
            GameManager.SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.Opaque, SamplerState.LinearClamp);

            int width = GameManager.GraphicsDevice.Viewport.Width / 2;
            int height = GameManager.GraphicsDevice.Viewport.Height / 2;
            Rectangle rect = new Rectangle(0, 0, width, height);

            //Draw GBuffers
            GameManager.SpriteBatch.Draw((Texture2D) gBufferTargets[0].RenderTarget, rect, Color.White);

            rect.X += width;
            GameManager.SpriteBatch.Draw((Texture2D) gBufferTargets[1].RenderTarget, rect, Color.White);

            rect.X = 0;
            rect.Y += height;
            GameManager.SpriteBatch.Draw((Texture2D) gBufferTargets[2].RenderTarget, rect, Color.White);

            rect.X += width;
            GameManager.SpriteBatch.Draw((Texture2D) lightmapTarget.RenderTarget, rect, Color.White);

            GameManager.SpriteBatch.End();
        }

        #endregion

        public override void DrawForShadows()
        {
            //Set states
            GameManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GameManager.GraphicsDevice.BlendState = BlendState.Opaque;
            
            foreach (ModelMesh mesh in Model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    //Init buffers
                    GameManager.GraphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    GameManager.GraphicsDevice.Indices = part.IndexBuffer;
                    
                    if (IsSkinned)
                    {
                        currentDepthWriterEffect.Parameters["World"].SetValue(TransformComponent.TransformMatrix);
                        currentDepthWriterEffect.Parameters["Bones"].SetValue(Bones);
                    }
                    else
                        currentDepthWriterEffect.Parameters["World"].SetValue(ModelTransforms[mesh.ParentBone.Index] * TransformComponent.TransformMatrix);
                    
                    currentDepthWriterEffect.CurrentTechnique.Passes[0].Apply();
                    GameManager.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, part.StartIndex, part.PrimitiveCount);
                }
            }
        }
        
        public override void Draw(Camera camera)
        {
            //Set states
            GameManager.GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GameManager.GraphicsDevice.BlendState = BlendState.Opaque;
            
            if (!IsVisible(camera))
                return;
            
            //Set V, P matrix
            currentGBufferEffect.Parameters["Projection"].SetValue(camera.Projection);
            currentGBufferEffect.Parameters["View"].SetValue(camera.View);

            foreach (ModelMesh mesh in Model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
//                    if (part.Effect.Parameters["Alpha"].GetValueSingle() < 1.0f || part.Effect.Parameters["TransparencyMap"]?.GetValueTexture2D() != null)
//                        return; //We'll draw transparency in later pass
                    //Init buffers
                    GameManager.GraphicsDevice.SetVertexBuffer(part.VertexBuffer, part.VertexOffset);
                    GameManager.GraphicsDevice.Indices = part.IndexBuffer;

                    currentGBufferEffect.Parameters["DiffuseColor"].SetValue(part.Effect.Parameters["DiffuseColor"].GetValueVector3());
                    currentGBufferEffect.Parameters["SpecularIntensity"].SetValue(part.Effect.Parameters["SpecularColor"].GetValueVector3().X);
                    currentGBufferEffect.Parameters["SpecularPower"].SetValue(part.Effect.Parameters["SpecularPower"].GetValueSingle());


                    //Set parameters
                    if (IsSkinned)
                    {
                        currentGBufferEffect.Parameters["World"].SetValue(TransformComponent.TransformMatrix);
                        currentGBufferEffect.Parameters["WorldInverseTranspose"]
                            .SetValue(Matrix.Transpose(Matrix.Invert(TransformComponent.TransformMatrix * camera.View)));
                        currentGBufferEffect.Parameters["Bones"].SetValue(Bones);
                    }
                    else
                    {
                        currentGBufferEffect.Parameters["World"].SetValue(ModelTransforms[mesh.ParentBone.Index] * TransformComponent.TransformMatrix); //
                        currentGBufferEffect.Parameters["WorldInverseTranspose"]
                            .SetValue(
                                Matrix.Transpose(Matrix.Invert(ModelTransforms[mesh.ParentBone.Index] * TransformComponent.TransformMatrix * camera.View)));
                    }

                    //Set textures
                    if (part.Effect.Parameters["Texture"]?.GetValueTexture2D() != null)
                    {
                        currentGBufferEffect.Parameters["Texture"].SetValue(part.Effect.Parameters["Texture"].GetValueTexture2D());
                        currentGBufferEffect.Parameters["UseDiffuseMap"].SetValue(true);
                    }
                    else currentGBufferEffect.Parameters["UseDiffuseMap"].SetValue(false);

                    if (part.Effect.Parameters["NormalMap"]?.GetValueTexture2D() != null)
                    {
                        currentGBufferEffect.Parameters["NormalMap"].SetValue(part.Effect.Parameters["NormalMap"].GetValueTexture2D());
                        currentGBufferEffect.Parameters["UseNormalMap"].SetValue(true);
                    }
                    else currentGBufferEffect.Parameters["UseNormalMap"].SetValue(false);

                    if (part.Effect.Parameters["SpecularMap"]?.GetValueTexture2D() != null)
                    {
                        currentGBufferEffect.Parameters["SpecularMap"].SetValue(part.Effect.Parameters["SpecularMap"].GetValueTexture2D());
                        currentGBufferEffect.Parameters["UseSpecularMap"].SetValue(true);
                    }
                    else currentGBufferEffect.Parameters["UseSpecularMap"].SetValue(false);

                    //Draw
                    currentGBufferEffect.CurrentTechnique.Passes[0].Apply();
                    GameManager.GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList, 0, part.StartIndex, part.PrimitiveCount);
                }
            }
        }

        public DeferredMeshRenderer(GameObject gameObject, Microsoft.Xna.Framework.Graphics.Model model) : base(gameObject, model)
        {
        }
    }
}
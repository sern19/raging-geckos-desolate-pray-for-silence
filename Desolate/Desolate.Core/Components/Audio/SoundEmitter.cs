﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Desolate.Core.Components.Abstract;
using Microsoft.Xna.Framework.Audio;

namespace Desolate.Core.Components.Audio
{
    public class SoundEmitter : SingleAttitude
    {

        Dictionary<string, SoundEffect> sounds;
        Dictionary<string, SoundEffectInstance> soundInstances;
        AudioEmitter emitter;
        public void Stop(string name)
        {
            if (soundInstances.ContainsKey(name))
            {
                soundInstances[name].Stop();
            }
        }
        public void StopAll()
        {
            foreach (KeyValuePair<string, SoundEffectInstance> instance in soundInstances)
                instance.Value.Stop();
        }


        public void Play(string name,bool loopNeeded)
        {
            if (!soundInstances.ContainsKey(name))
            {
                soundInstances.Add(name, sounds[name].CreateInstance());
                soundInstances[name].IsLooped = loopNeeded;
            }
            emitter.Position = GameObject.Transform.WorldPosition;
            soundInstances[name].Apply3D(Scene.MainCamera.GetComponent<SoundListener>().GetListener(), emitter);
            soundInstances[name].Play();
        }

        public SoundEmitter(GameObject gameObject, Dictionary<string, SoundEffect> soundEffects) : base(gameObject)
        {
            sounds = new Dictionary<string, SoundEffect>(soundEffects);
            soundInstances = new Dictionary<string, SoundEffectInstance>();
            emitter = new AudioEmitter();
        }

    }
}

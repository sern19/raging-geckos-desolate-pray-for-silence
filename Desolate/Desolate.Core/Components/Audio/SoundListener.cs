﻿using Desolate.Core.Components.Abstract;
using Microsoft.Xna.Framework.Audio;

namespace Desolate.Core.Components.Audio
{
    public class SoundListener : SingleAttitude
    {
        AudioListener audioListener;

        public AudioListener GetListener()
        {
            audioListener.Position = GameObject.Transform.WorldPosition;
            return audioListener;
        }

        public SoundListener(GameObject gameObject) : base(gameObject)
        {
            audioListener = new AudioListener();
        }
    }
}

﻿using Desolate.Core.Components.Abstract;
using Microsoft.Xna.Framework.Audio;
using System.Collections.Generic;

namespace Desolate.Core.Components.Audio
{
    public class AudioSource : SingleAttitude
    {

        Dictionary<string, SoundEffect> sounds;
        Dictionary<string, SoundEffectInstance> soundInstances;


        public void Stop(string name)
        {
            if (soundInstances.ContainsKey(name))
            {
                soundInstances[name].Stop();
            }
        }
        public void StopAll()
        {
            foreach (KeyValuePair<string, SoundEffectInstance> instance in soundInstances)
                instance.Value.Stop();
        }

        public void Play(string name, bool loopNeeded, float volume)
        {
            if (!soundInstances.ContainsKey(name))
            {
                soundInstances.Add(name, sounds[name].CreateInstance());
                soundInstances[name].IsLooped = loopNeeded;
                soundInstances[name].Volume = volume;
            }
            soundInstances[name].Play();
        }

        public AudioSource(GameObject gameObject, Dictionary<string, SoundEffect> soundEffects) : base(gameObject)
        {
            sounds = new Dictionary<string, SoundEffect>(soundEffects);
            soundInstances = new Dictionary<string, SoundEffectInstance>();
        }
    }
}

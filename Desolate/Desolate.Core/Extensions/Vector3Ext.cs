﻿using System;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Extensions
{
    public static class Vector3Ext
    {
        /// <summary>
        /// Returns angle between two vectors.
        /// </summary>
        public static float Angle(Vector3 vector1, Vector3 vector2, bool radians = false)
        {
            var norm1 = vector1;
            var norm2 = vector2;
            norm1.Normalize();
            norm2.Normalize();
            
            float angleInRadians = (float) Math.Acos(Vector3.Dot(norm1, norm2));

            return radians ? angleInRadians : MathHelper.ToDegrees(angleInRadians);
        }
    }
}
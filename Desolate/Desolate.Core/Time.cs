﻿using System;
using Microsoft.Xna.Framework;

namespace Desolate.Core
{
    public static class Time
    {
        private static GameTime gameTime = new GameTime();

        public static GameTime GameTime
        {
            get => gameTime;
            set
            {
                UnscaledGameTime = value;
                if (gameTime != null)
                {
                    gameTime.ElapsedGameTime =
                        TimeSpan.FromSeconds(value.ElapsedGameTime.TotalSeconds * TimeScale);
                    gameTime.TotalGameTime +=
                        TimeSpan.FromSeconds(value.TotalGameTime.TotalSeconds * TimeScale);
                }

                if (UnscaledGameTime.TotalGameTime.TotalSeconds >= FixedGameTime + FixedStep)
                {
                    IsInFixedStep = true;
                    FixedDeltaTime = FixedStep;
                    FixedGameTime += FixedStep;
                }
                else
                    IsInFixedStep = false;
            }
        }

        public static GameTime UnscaledGameTime { get; private set; }

        public static float UnscaledTime => (float)(UnscaledGameTime?.ElapsedGameTime.TotalSeconds ?? 0.0);

        private static float FixedGameTime { get; set; }

        public static float FixedStep { get; set; } = 0.01f;

        public static bool IsInFixedStep { get; private set; }

        public static float TimeScale { get; set; } = 1.0f;

        public static float DeltaTime => (float) GameTime.ElapsedGameTime.TotalSeconds;

        public static float UnscaledDeltaTime => (float) UnscaledGameTime.ElapsedGameTime.TotalSeconds;

        public static float FixedDeltaTime { get; private set; } = 0;
    }
}
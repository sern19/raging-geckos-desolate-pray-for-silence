﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core
{
    public class Camera : GameObject
    {
        public enum ProjectionType
        {
            Ortographic,
            Perspective
        }

        private ProjectionType typeOfProjection;

        private Vector3 cameraRot;

        private Matrix previousTransform;
        
        public (float Min, float Max) Clipping { get; set; } = (0.1f, 100.0f);

        public float FieldOfView { get; set; } = 60.0f;

        public Effect PostProcessingEffect { get; set; }

        public Matrix View { get; set; }

        public Matrix Projection { get; set; }
        
        public BoundingFrustum CameraFrustum { get; private set; } = new BoundingFrustum(Matrix.Identity);

        public ProjectionType TypeOfProjection
        {
            get => typeOfProjection;
            set
            {
                if (value == ProjectionType.Perspective)
                {
                    Projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(FieldOfView),
                        GameManager.Game.GraphicsDevice.Viewport.AspectRatio,
                        Clipping.Min, Clipping.Max);
                }
                else
                {
                    Projection = Matrix.CreateOrthographic(GameManager.Game.GraphicsDevice.Viewport.Width,
                        GameManager.Game.GraphicsDevice.Viewport.Height, Clipping.Min,
                        Clipping.Max);
                }

                typeOfProjection = value;
            }
        }

        internal override void Initialize()
        {
            base.Initialize();
            TypeOfProjection = ProjectionType.Perspective;
            Transform.LocalEulerAngles = cameraRot;
            previousTransform = Transform.TransformMatrix;
            View = Matrix.CreateLookAt(Transform.WorldPosition, Transform.WorldPosition + Transform.Forward, Transform.Up);
            CameraFrustum.Matrix = View * Projection;
        }

        public void Move(Vector3 translation)
        {
            Transform.Translate(translation);
            View = Matrix.CreateLookAt(Transform.WorldPosition, Transform.WorldPosition + Transform.Forward, Transform.Up);
            CameraFrustum.Matrix = View * Projection;
        }

        public void Rotate(Vector3 rotation)
        {
            cameraRot += rotation;
            cameraRot.X = MathHelper.Clamp(cameraRot.X, -90f, 90f);
            Transform.LocalEulerAngles = cameraRot;
            View = Matrix.CreateLookAt(Transform.WorldPosition, Transform.WorldPosition + Transform.Forward, Transform.Up);
            CameraFrustum.Matrix = View * Projection;
        }

        public override void Update()
        {
            if (previousTransform != Transform.TransformMatrix)
            {
                previousTransform = Transform.TransformMatrix;
                View = Matrix.CreateLookAt(Transform.WorldPosition, Transform.WorldPosition + Transform.Forward, Transform.Up);
                CameraFrustum.Matrix = View * Projection;
            }
            base.Update();
        }

        public bool InView(BoundingSphere boundingSphere) => CameraFrustum.Intersects(boundingSphere);
        
        public Camera() {}
        public Camera(Vector3 cameraRot) => this.cameraRot = cameraRot;
    }
}
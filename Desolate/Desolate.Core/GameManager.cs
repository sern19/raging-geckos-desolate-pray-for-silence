﻿using Desolate.Core.Components.Rendering.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core
{
    public static class GameManager
    {
        public static GraphicsDevice GraphicsDevice => Game.GraphicsDevice;
        public static SpriteBatch SpriteBatch { get; set; }
        public static Game Game { get; internal set; }
        public static FullscreenQuad FullscreenQuad { get; internal set; }
        public static bool IsDebug { get; internal set; } = false;

        #region Rendering
        
        public static bool IsDeferred { get; set; } = true;

        public static float DirectionalShadowRenderingRange = 30.0f;
        
        public static bool EnableShadows = true; 
        public static Vector2 ShadowMapResolution { get; internal set; } = new Vector2(3072, 3072);
        
        #region Deferred

        public static bool ShouldUseScreenResolution { get; internal set; } = true;
        public static Vector2 RenderResolution { get; internal set; } = new Vector2(1600, 960); //Default is 800x480

        #endregion
        

        #endregion
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Desolate.Core.Components.Rendering.Helpers;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Desolate.Core.Managers
{
    public static class GeometryExtensions
    {
        public static IEnumerable<Triangle> GetTriangles(this Model model)
        {
            var modelTransforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(modelTransforms);

            var triangles = new List<Triangle>();

            var modelMeshParts = model.Meshes.SelectMany(m => m.MeshParts).ToArray();
            foreach (var mesh in model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    VertexDeclaration declaration = part.VertexBuffer.VertexDeclaration;

                    VertexElement vertexPosition = new VertexElement();

                    // This where we store the vertices until transformed 
                    Vector3[] allVertex = new Vector3[part.NumVertices];

                    // Read the vertices from the buffer in to the array 
                    part.VertexBuffer.GetData(
                        part.VertexOffset * declaration.VertexStride + vertexPosition.Offset,
                        allVertex,
                        0,
                        part.NumVertices,
                        declaration.VertexStride);


                    // Each primitive is a triangle 
                    short[] indexElements = new short[part.PrimitiveCount * 3];

                    part.IndexBuffer.GetData(part.StartIndex * 2, indexElements, 0, part.PrimitiveCount * 3);

                    for (int i = 2; i < indexElements.Length; i += 3)
                    {
                        Vertex a = new Vertex(indexElements[i - 2],
                            Vector3.Transform(allVertex[indexElements[i - 2]], modelTransforms[mesh.ParentBone.Index]));
                        Vertex b = new Vertex(indexElements[i - 1],
                            Vector3.Transform(allVertex[indexElements[i - 1]], modelTransforms[mesh.ParentBone.Index]));
                        Vertex c = new Vertex(indexElements[i],
                            Vector3.Transform(allVertex[indexElements[i]], modelTransforms[mesh.ParentBone.Index]));
                        triangles.Add(new Triangle(a, b, c));
                    }
                }
            }

            return triangles;
        }
    }
}
﻿using System.Linq;
using Desolate.Core.AI.Navigation;
using Desolate.Core.Components.Rendering.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Tests.AI
{
    [TestClass]
    public class PremadeNavMeshTests
    {
        private static readonly Vertex[] MockVertices =
        {
            // Row - 0
            new Vertex(00, new Vector3(0, 0, 0)),
            new Vertex(01, new Vector3(1, 0, 0)),
            new Vertex(02, new Vector3(2, 0, 0)),
            new Vertex(03, new Vector3(3, 0, 0)),

            // Row - 1
            new Vertex(04, new Vector3(0, 0, 1)),
            new Vertex(05, new Vector3(1, 0, 1)),
            new Vertex(06, new Vector3(2, 0, 1)),
            new Vertex(07, new Vector3(3, 0, 1)),

            // Row - 2
            new Vertex(08, new Vector3(0, 0, 2)),
            new Vertex(09, new Vector3(1, 0, 2)),
            new Vertex(10, new Vector3(2, 0, 2)),
            new Vertex(11, new Vector3(3, 0, 2)),

            // Row - 3
            new Vertex(12, new Vector3(0, 0, 3)),
            new Vertex(13, new Vector3(1, 0, 3)),
            new Vertex(14, new Vector3(2, 0, 3)),
            new Vertex(15, new Vector3(3, 0, 3)),
        };

        private static readonly Triangle[] MockTriangles =
        {
            // Row - 0; Square - 0
            new Triangle(MockVertices[0], MockVertices[1], MockVertices[4]),
            new Triangle(MockVertices[1], MockVertices[4], MockVertices[5]),

            // Row - 0; Square - 1
            new Triangle(MockVertices[1], MockVertices[2], MockVertices[5]),
            new Triangle(MockVertices[2], MockVertices[5], MockVertices[6]),

            // Row - 0; Square - 2
            new Triangle(MockVertices[2], MockVertices[3], MockVertices[6]),
            new Triangle(MockVertices[3], MockVertices[6], MockVertices[7]),


            // Row - 1; Square - 0
            new Triangle(MockVertices[4], MockVertices[5], MockVertices[8]),
            new Triangle(MockVertices[5], MockVertices[8], MockVertices[9]),

            // Row - 1; Square - 1
            new Triangle(MockVertices[5], MockVertices[6], MockVertices[9]),
            new Triangle(MockVertices[6], MockVertices[9], MockVertices[10]),

            // Row - 1; Square - 2
            new Triangle(MockVertices[6], MockVertices[7], MockVertices[10]),
            new Triangle(MockVertices[7], MockVertices[10], MockVertices[11]),


            // Row - 2; Square - 0
            new Triangle(MockVertices[8], MockVertices[9], MockVertices[12]),
            new Triangle(MockVertices[9], MockVertices[12], MockVertices[13]),

            // Row - 2; Square - 1
            new Triangle(MockVertices[9], MockVertices[10], MockVertices[13]),
            new Triangle(MockVertices[10], MockVertices[13], MockVertices[14]),

            // Row - 2; Square - 2
            new Triangle(MockVertices[10], MockVertices[11], MockVertices[14]),
            new Triangle(MockVertices[11], MockVertices[14], MockVertices[15]),
        };

        [TestMethod]
        public void FromTrianglesTest()
        {
            // Arrange

            // Act
            NavMesh navMesh = PremadeNavMesh.FromTriangles(MockTriangles);

            // Assert
            Assert.IsNotNull(navMesh);
            Assert.IsInstanceOfType(navMesh, typeof(PremadeNavMesh));
        }

        [TestMethod]
        public void GetNearestNodeTest()
        {
            // Arrange

            #region settings

            var settings = new[]
            {
                new {Point = new Vector3(0.1f, 0.0f, 0.1f), ClosestVertex = MockTriangles[0].Center},
                new {Point = new Vector3(0.8f, 0.0f, 0.8f), ClosestVertex = MockTriangles[1].Center},
                new {Point = new Vector3(2.8f, 0.0f, 2.8f), ClosestVertex = MockTriangles[17].Center},
                new {Point = new Vector3(5.8f, 0.0f, 5.8f), ClosestVertex = MockTriangles[17].Center},
            };

            #endregion

            NavMesh navMesh = PremadeNavMesh.FromTriangles(MockTriangles);

            // Act
            foreach (var setting in settings)
            {
                Vector3 result = navMesh.GetNearestNode(setting.Point);

                // Assert
                Assert.AreEqual(setting.ClosestVertex.X, result.X, 0.0001f);
                Assert.AreEqual(setting.ClosestVertex.Y, result.Y, 0.0001f);
                Assert.AreEqual(setting.ClosestVertex.Z, result.Z, 0.0001f);
            }
        }

        [TestMethod]
        public void GetNeighboursByVertex()
        {
            // Arrange

            #region settings

            var settings = new[]
            {
                new
                {
                    TestVertex = new Vector3(0, 0, 0),
                    NeighboursVertices = new[]
                    {
                        MockTriangles[1].Center
                    },
                },
                new
                {
                    TestVertex = new Vector3(1.7f, 0, 1.7f),
                    NeighboursVertices = new[]
                    {
                        MockTriangles[8].Center,
                        MockTriangles[10].Center,
                        MockTriangles[14].Center,
                    },
                },
            };

            #endregion

            NavMesh navMesh = PremadeNavMesh.FromTriangles(MockTriangles);

            // Act
            foreach (var setting in settings)
            {
                Vector3[] result = navMesh.GetNeighbours(setting.TestVertex).OrderBy(r => r.Z).ThenBy(r => r.X)
                    .ToArray();

                // Assert
                Assert.AreEqual(setting.NeighboursVertices.Length, result.Length,
                    $"Neighbours to vertex {setting.TestVertex} does not match prediction.");

                for (var index = 0; index < result.Length; index++)
                {
                    Assert.AreEqual(setting.NeighboursVertices[index].X, result[index].X, 0.0001f,
                        $"Neighbours to vertex {setting.TestVertex} does not match prediction.");
                    Assert.AreEqual(setting.NeighboursVertices[index].Y, result[index].Y, 0.0001f,
                        $"Neighbours to vertex {setting.TestVertex} does not match prediction.");
                    Assert.AreEqual(setting.NeighboursVertices[index].Z, result[index].Z, 0.0001f,
                        $"Neighbours to vertex {setting.TestVertex} does not match prediction.");
                }
            }
        }
    }
}
﻿using System.Linq;
using Desolate.Core.AI.Navigation;
using Desolate.Core.AI.PathFinding;
using Desolate.Core.Components.Rendering.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Tests.AI
{
    [TestClass]
    public class PathCalculatorTest
    {
        private static readonly Vertex[] MockVertices =
           {
            // Row - 0
            new Vertex(00, new Vector3(0, 0, 0)),
            new Vertex(01, new Vector3(1, 0, 0)),
            new Vertex(02, new Vector3(2, 0, 0)),
            new Vertex(03, new Vector3(3, 0, 0)),

            // Row - 1
            new Vertex(04, new Vector3(0, 0, 1)),
            new Vertex(05, new Vector3(1, 0, 1)),
            new Vertex(06, new Vector3(2, 0, 1)),
            new Vertex(07, new Vector3(3, 0, 1)),

            // Row - 2
            new Vertex(08, new Vector3(0, 0, 2)),
            new Vertex(09, new Vector3(1, 0, 2)),
            new Vertex(10, new Vector3(2, 0, 2)),
            new Vertex(11, new Vector3(3, 0, 2)),

            // Row - 3
            new Vertex(12, new Vector3(0, 0, 3)),
            new Vertex(13, new Vector3(1, 0, 3)),
            new Vertex(14, new Vector3(2, 0, 3)),
            new Vertex(15, new Vector3(3, 0, 3)),
        };

        private static readonly Triangle[] MockTriangles =
        {
            // Row - 0; Square - 0
            new Triangle(MockVertices[0], MockVertices[1], MockVertices[4]),
            new Triangle(MockVertices[1], MockVertices[4], MockVertices[5]),

            // Row - 0; Square - 1
            new Triangle(MockVertices[1], MockVertices[2], MockVertices[5]),
            new Triangle(MockVertices[2], MockVertices[5], MockVertices[6]),

            // Row - 0; Square - 2
            new Triangle(MockVertices[2], MockVertices[3], MockVertices[6]),
            new Triangle(MockVertices[3], MockVertices[6], MockVertices[7]),


            // Row - 1; Square - 0
            new Triangle(MockVertices[4], MockVertices[5], MockVertices[8]),
            new Triangle(MockVertices[5], MockVertices[8], MockVertices[9]),

            // Row - 1; Square - 1
            new Triangle(MockVertices[5], MockVertices[6], MockVertices[9]),
            new Triangle(MockVertices[6], MockVertices[9], MockVertices[10]),

            // Row - 1; Square - 2
            new Triangle(MockVertices[6], MockVertices[7], MockVertices[10]),
            new Triangle(MockVertices[7], MockVertices[10], MockVertices[11]),


            // Row - 2; Square - 0
            new Triangle(MockVertices[8], MockVertices[9], MockVertices[12]),
            new Triangle(MockVertices[9], MockVertices[12], MockVertices[13]),

            // Row - 2; Square - 1
            new Triangle(MockVertices[9], MockVertices[10], MockVertices[13]),
            new Triangle(MockVertices[10], MockVertices[13], MockVertices[14]),

            // Row - 2; Square - 2
            new Triangle(MockVertices[10], MockVertices[11], MockVertices[14]),
            new Triangle(MockVertices[11], MockVertices[14], MockVertices[15]),
        };


        private static readonly Triangle[] MockTrianglesNotInSquare =
        {
            // Row - 0; Square - 0
            new Triangle(MockVertices[0], MockVertices[1], MockVertices[4]),
            new Triangle(MockVertices[1], MockVertices[4], MockVertices[5]),

            //// Row - 0; Square - 1
            //new Triangle(MockVertices[1], MockVertices[2], MockVertices[5]),
            //new Triangle(MockVertices[2], MockVertices[5], MockVertices[6]),

            // Row - 0; Square - 2
            //new Triangle(MockVertices[2], MockVertices[3], MockVertices[6]),
            //new Triangle(MockVertices[3], MockVertices[6], MockVertices[7]),


            // Row - 1; Square - 0
            new Triangle(MockVertices[4], MockVertices[5], MockVertices[8]),
            new Triangle(MockVertices[5], MockVertices[8], MockVertices[9]),

            //// Row - 1; Square - 1
            //new Triangle(MockVertices[5], MockVertices[6], MockVertices[9]),
            //new Triangle(MockVertices[6], MockVertices[9], MockVertices[10]),

            // Row - 1; Square - 2
            //new Triangle(MockVertices[6], MockVertices[7], MockVertices[10]),
            //new Triangle(MockVertices[7], MockVertices[10], MockVertices[11]),


            // Row - 2; Square - 0
            new Triangle(MockVertices[8], MockVertices[9], MockVertices[12]),
            new Triangle(MockVertices[9], MockVertices[12], MockVertices[13]),

            // Row - 2; Square - 1
            new Triangle(MockVertices[9], MockVertices[10], MockVertices[13]),
            new Triangle(MockVertices[10], MockVertices[13], MockVertices[14]),

            // Row - 2; Square - 2
            new Triangle(MockVertices[10], MockVertices[11], MockVertices[14]),
            new Triangle(MockVertices[11], MockVertices[14], MockVertices[15]),
        };

        [TestMethod]
        public void GenerateSimplePathTest()
        {
            NavMesh navMesh = PremadeNavMesh.FromTriangles(MockTriangles);
            Vector3 startingPosition = new Vector3(0, 0, 0);
            Vector3 targetPosition = new Vector3(1, 0, 0);

            AstarSolver pathCalculator = new AstarSolver(navMesh);

            NavPath simplePath = pathCalculator.Calculate(startingPosition, targetPosition);

            Assert.AreEqual(3, simplePath.PathPositions.Length);
            Assert.AreEqual(startingPosition, simplePath.PathPositions[0]);
            Assert.AreEqual(targetPosition, simplePath.PathPositions[1]);

        }

        [TestMethod]
        public void GenerateMoreComplicatedPathTest()
        {
            NavMesh navMesh = PremadeNavMesh.FromTriangles(MockTriangles);
            Vector3 startingPosition = new Vector3(0, 0, 0);
            Vector3 targetPosition = new Vector3(3, 0, 3);

            AstarSolver pathCalculator = new AstarSolver(navMesh);

            NavPath simplePath = pathCalculator.Calculate(startingPosition, targetPosition);

            Assert.AreEqual(10, simplePath.PathPositions.Length);
            Assert.AreEqual(startingPosition, simplePath.PathPositions[0]);
            Assert.AreEqual(targetPosition, simplePath.PathPositions.Last());
        }
//
//        [TestMethod]
//        public void GenerateDifferentPathTest()
//        {
//            NavMesh navMesh = PremadeNavMesh.FromTriangles(MockTrianglesNotInSquare);
//            Vector3 startingPosition = new Vector3(0, 0, 0);
//            Vector3 targetPosition = new Vector3(3, 0, 3);
//
//            AstarSolver pathCalculator = new AstarSolver(navMesh);
//
//            NavPath simplePath = pathCalculator.Calculate(startingPosition, targetPosition);
//
//            Assert.AreEqual(startingPosition, simplePath.PathVertexes[0].Position);
//            Assert.AreEqual(targetPosition, simplePath.PathVertexes[simplePath.PathVertexes.Count - 1].Position);
//        }

    }
}

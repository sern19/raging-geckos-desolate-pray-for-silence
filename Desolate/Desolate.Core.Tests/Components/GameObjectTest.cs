﻿using System.Linq;
using Desolate.Core.Components.Physics;
using Desolate.Core.Components.Rendering.Model;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Desolate.Core.Tests.Components
{
    /// <summary>
    /// Opis podsumowujący elementu GetComponentsTest
    /// </summary>
    [TestClass]
    public class GameObjectTest
    {
        [TestMethod]
        public void GetComponentsTest()
        {
            GameObject gameObject = new GameObject();
            Mesh mesh1 = new Mesh(gameObject);
            Mesh mesh2 = new Mesh(gameObject);
            BoxCollider collider = new BoxCollider(gameObject);
            gameObject.AddComponent(mesh1);
            gameObject.AddComponent(mesh2);
            gameObject.AddComponent(collider);
            gameObject.AddComponent(new BoxCollider(gameObject));
            gameObject.Initialize();

            Assert.IsTrue(gameObject.GetComponents<Collider>().Contains(collider));
            Assert.IsTrue(gameObject.GetComponents<Mesh>().Contains(mesh1));
            Assert.IsTrue(gameObject.GetComponents<Mesh>().Contains(mesh2));
        }
    }
}
﻿using Desolate.Core.Components.Physics;
using Desolate.Core.Tests.Components.HelperClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Tests.Components.Physics
{
    [TestClass]
    public class SceneCollisionTest
    {
        [TestMethod]
        public void CollisionNotDetectedTest()
        {
            //Arrange
            Scene scene = new Scene(null);
            GameObject first = new GameObject { Layer = PhysicLayer.Default };
            first.Initialize();
            first.AddComponent(new BoxCollider(first) { Center = Vector3.Zero, Size = Vector3.One });
            first.AddComponent(new TestComponent(first));

            GameObject second = new GameObject { Layer = PhysicLayer.Default };
            second.Initialize();
            second.Transform.LocalPosition = Vector3.One * 5;
            second.AddComponent(new BoxCollider(second) { Center = Vector3.Zero, Size = Vector3.One });
            second.AddComponent(new TestComponent(second));

            scene.AddGameObject(first);
            scene.AddGameObject(second);

            //Act
            scene.FixedUpdate();

            //Assert
            Assert.IsFalse(first.GetComponent<TestComponent>().CollisionEntered);
            Assert.IsFalse(second.GetComponent<TestComponent>().CollisionEntered);
        }

        [TestMethod]
        public void CollisionEnterTest()
        {
            //Arrange
            Scene scene = new Scene(null);
            GameObject first = new GameObject { Layer = PhysicLayer.Default };
            first.Initialize();
            first.AddComponent(new BoxCollider(first) { Center = Vector3.Zero, Size = Vector3.One });
            first.AddComponent(new TestComponent(first));

            GameObject second = new GameObject { Layer = PhysicLayer.Default };
            second.Initialize();
            second.AddComponent(new BoxCollider(second) { Center = Vector3.Zero, Size = Vector3.One });
            second.AddComponent(new TestComponent(second));
            second.Transform.LocalPosition = Vector3.One * 5;

            scene.AddGameObject(first);
            scene.AddGameObject(second);

            //Act            
            scene.FixedUpdate();

            second.Transform.LocalPosition = Vector3.Zero;

            scene.FixedUpdate();

            //Assert
            Assert.IsTrue(first.GetComponent<TestComponent>().CollisionEntered);
            Assert.IsTrue(second.GetComponent<TestComponent>().CollisionEntered);
        }

        [TestMethod]
        public void CollisionStayTest()
        {
            //Arrange
            Scene scene = new Scene(null);
            GameObject first = new GameObject { Layer = PhysicLayer.Default };
            first.Initialize();
            first.AddComponent(new BoxCollider(first) { Center = Vector3.Zero, Size = Vector3.One });
            first.AddComponent(new TestComponent(first));

            GameObject second = new GameObject { Layer = PhysicLayer.Default };
            second.Initialize();
            second.Transform.LocalPosition = Vector3.One * 5;
            second.AddComponent(new BoxCollider(second) { Center = Vector3.Zero, Size = Vector3.One });
            second.AddComponent(new TestComponent(second));

            scene.AddGameObject(first);
            scene.AddGameObject(second);

            //Act

            scene.FixedUpdate();

            second.Transform.LocalPosition = Vector3.Zero;

            scene.FixedUpdate();

            //no changes in transform

            scene.FixedUpdate();

            //Assert

            Assert.IsTrue(first.GetComponent<TestComponent>().CollisionStayed);
            Assert.IsTrue(second.GetComponent<TestComponent>().CollisionStayed);
        }

        [TestMethod]
        public void CollisionExitTest()
        {
            //Arrange
            Scene scene = new Scene(null);
            GameObject first = new GameObject { Layer = PhysicLayer.Default };
            first.AddComponent(new BoxCollider(first) { Center = Vector3.Zero, Size = Vector3.One });
            first.AddComponent(new TestComponent(first));

            GameObject second = new GameObject { Layer = PhysicLayer.Default };
            second.AddComponent(new BoxCollider(second) { Center = Vector3.Zero, Size = Vector3.One });
            second.AddComponent(new TestComponent(second));

            scene.AddGameObject(first);
            scene.AddGameObject(second);
            
            scene.Initialize();

            //Act

            scene.FixedUpdate();

            second.Transform.LocalPosition = Vector3.One * 10;

            scene.FixedUpdate();

            //Assert

            Assert.IsTrue(first.GetComponent<TestComponent>().CollisionExited);
            Assert.IsTrue(second.GetComponent<TestComponent>().CollisionExited);
        }

        [TestMethod]
        public void CollisionDetectedButIgnoredTest()
        {
            //Arrange
            Scene scene = new Scene(null);
            GameObject first = new GameObject { Layer = PhysicLayer.Throwable };
            first.Initialize();
            first.AddComponent(new BoxCollider(first) { Center = Vector3.Zero, Size = Vector3.One });
            first.AddComponent(new TestComponent(first));

            GameObject second = new GameObject { Layer = PhysicLayer.Enemy };
            second.Initialize();
            second.AddComponent(new BoxCollider(second) { Center = Vector3.Zero, Size = Vector3.One });
            second.AddComponent(new TestComponent(second));

            scene.AddGameObject(first);
            scene.AddGameObject(second);

            //Act
            Physic.CollisionMatrix[PhysicLayer.Enemy] = LayerMask.ToIgnoreMask(new[] { PhysicLayer.Throwable });
            Physic.CollisionMatrix[PhysicLayer.Throwable] = LayerMask.ToIgnoreMask(new[] { PhysicLayer.Enemy });

            scene.FixedUpdate();

            //Assert
            Assert.IsFalse(first.GetComponent<TestComponent>().CollisionEntered);
            Assert.IsFalse(second.GetComponent<TestComponent>().CollisionEntered);
        }
    }
}

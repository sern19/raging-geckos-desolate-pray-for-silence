﻿using System;
using System.Collections.Generic;
using System.Linq;
using Desolate.Core.Components.Physics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Desolate.Core.Tests.Components.Physics
{
    [TestClass]
    public class LayerMaskTest
    {
        [TestMethod]
        public void LayerBinaryValueTest()
        {
            //Arrange

            //Act

            //Assert
            Assert.AreEqual((PhysicLayer) 1, PhysicLayer.Default);
            Assert.AreEqual((PhysicLayer) 2, PhysicLayer.UI);
            Assert.AreEqual((PhysicLayer) Math.Pow(2, 30), PhysicLayer.UserDefined_21);
        }

        [TestMethod]
        public void LayerBinarySumTest()
        {
            //Arrange

            //Act
            IEnumerable<PhysicLayer> allLayers = Enum.GetValues(typeof(PhysicLayer)).Cast<PhysicLayer>().Select(l => l);

            PhysicLayer sum = allLayers.Aggregate<PhysicLayer, PhysicLayer>(0, (current, value) => current | value);

            //Assert
            Assert.AreEqual(LayerMask.AllMask, sum);
        }

        [TestMethod]
        public void LayerBinaryMaskTest()
        {
            //Arrange

            //Act

            //Assert
            Assert.AreEqual((PhysicLayer)3, (PhysicLayer.Default | PhysicLayer.UI));
        }

        [TestMethod]
        public void MaskContainsLayerTest()
        {
            //Arrange

            //Act
            PhysicLayer mask = (PhysicLayer.UI | PhysicLayer.Enviroment);

            //Assert
            Assert.IsTrue(LayerMask.ContainsLayer(mask, PhysicLayer.UI));
            Assert.IsTrue(LayerMask.ContainsLayer(mask, PhysicLayer.Enviroment));

            foreach (var value in Enum.GetValues(typeof(PhysicLayer)).Cast<PhysicLayer>())
            {
                PhysicLayer layer = value;
                if (layer != PhysicLayer.UI && layer != PhysicLayer.Enviroment)
                    Assert.IsFalse(LayerMask.ContainsLayer(mask, layer));
            }
        }

        [TestMethod]
        public void ToMaskTest()
        {
            //Arrange

            //Act
            PhysicLayer mask = LayerMask.ToMask(new[] {PhysicLayer.UI, PhysicLayer.Enviroment});

            //Assert
            Assert.AreEqual((PhysicLayer.UI | PhysicLayer.Enviroment), mask);
        }

        [TestMethod]
        public void ToIgnoreMaskTest()
        {
            //Arrange

            //Act
            PhysicLayer ignoreMask = LayerMask.ToIgnoreMask(new[] {PhysicLayer.UI, PhysicLayer.Enviroment});

            //Assert
            Assert.IsFalse(LayerMask.ContainsLayer(ignoreMask, PhysicLayer.UI));
            Assert.IsFalse(LayerMask.ContainsLayer(ignoreMask, PhysicLayer.Enviroment));

            foreach (var value in Enum.GetValues(typeof(PhysicLayer)).Cast<PhysicLayer>())
            {
                PhysicLayer layer = value;
                if (layer != PhysicLayer.UI && layer != PhysicLayer.Enviroment)
                    Assert.IsTrue(LayerMask.ContainsLayer(ignoreMask, layer));
            }
        }
    }
}
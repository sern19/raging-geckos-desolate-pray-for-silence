﻿using Desolate.Core.Components.Physics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Tests.Components.Physics
{
    [TestClass]
    public class BoxColliderTest
    {
        [TestMethod]
        public void BoundingBoxHiddenCalculationTest()
        {
            //Arrange
            Vector3 center = new Vector3(1.12f, -7.77f, 34.4f);
            Vector3 size = new Vector3(-12.5f, 3.93f, 0.7f);

            //Act
            BoxCollider collider = new BoxCollider(new GameObject())
            {
                Center = center,
                Size = size
            };

            //Assert
            Assert.AreEqual(center.X, collider.Center.X, 0.0001f);
            Assert.AreEqual(center.Y, collider.Center.Y, 0.0001f);
            Assert.AreEqual(center.Z, collider.Center.Z, 0.0001f);

            Assert.AreEqual(size.X, collider.Size.X, 0.0001f);
            Assert.AreEqual(size.Y, collider.Size.Y, 0.0001f);
            Assert.AreEqual(size.Z, collider.Size.Z, 0.0001f);
        }

        [TestMethod]
        public void SetCenterTest()
        {
            //Arrange
            Vector3 center = new Vector3(1.12f, -7.77f, 34.4f);
            Vector3 size = new Vector3(-12.5f, 3.93f, 0.7f);
            BoxCollider collider = new BoxCollider(new GameObject())
            {
                Center = center,
                Size = size
            };

            Vector3 newCenter = new Vector3(5.35f, -1.2f, 3.4f);
            //Act
            collider.Center = newCenter;

            //Assert
            Assert.AreEqual(newCenter.X, collider.Center.X, 0.0001f);
            Assert.AreEqual(newCenter.Y, collider.Center.Y, 0.0001f);
            Assert.AreEqual(newCenter.Z, collider.Center.Z, 0.0001f);
        }


        [TestMethod]
        public void SetSizeTest()
        {
            //Arrange
            Vector3 center = new Vector3(1.12f, -7.77f, 34.4f);
            Vector3 size = new Vector3(-12.5f, 3.93f, 0.7f);
            BoxCollider collider = new BoxCollider(new GameObject())
            {
                Center = center,
                Size = size
            };

            Vector3 newSize = new Vector3(5.35f, -1.2f, 3.4f);
            //Act
            collider.Size = newSize;

            //Assert

            Assert.AreEqual(newSize.X, collider.Size.X, 0.0001f);
            Assert.AreEqual(newSize.Y, collider.Size.Y, 0.0001f);
            Assert.AreEqual(newSize.Z, collider.Size.Z, 0.0001f);
        }


        [TestMethod]
        public void SetSizeAndCenterTest()
        {
            //Arrange
            Vector3 center = new Vector3(1.12f, -7.77f, 34.4f);
            Vector3 size = new Vector3(-12.5f, 3.93f, 0.7f);
            BoxCollider collider = new BoxCollider(new GameObject())
            {
                Center = center,
                Size = size
            };

            Vector3 newSize = new Vector3(5.35f, -1.2f, 3.4f);
            Vector3 newCenter = new Vector3(5.35f, -1.2f, 3.4f);
            //Act
            collider.Size = newSize;
            collider.Center = newCenter;

            //Assert
            Assert.AreEqual(newCenter.X, collider.Center.X, 0.0001f);
            Assert.AreEqual(newCenter.Y, collider.Center.Y, 0.0001f);
            Assert.AreEqual(newCenter.Z, collider.Center.Z, 0.0001f);

            Assert.AreEqual(newSize.X, collider.Size.X, 0.0001f);
            Assert.AreEqual(newSize.Y, collider.Size.Y, 0.0001f);
            Assert.AreEqual(newSize.Z, collider.Size.Z, 0.0001f);
        }
    }
}

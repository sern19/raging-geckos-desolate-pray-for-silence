﻿using Desolate.Core.Components.Physics;
using Desolate.Core.Tests.Components.HelperClasses;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Tests.Components.Physics
{
    [TestClass]
    public class GameObjectCollisionTest
    {

        [TestMethod]
        public void OnCollisionEnterTest()
        {
            //Arrange
            BoxCollider collider = new BoxCollider(new GameObject()) { Center = Vector3.Zero, Size = Vector3.One };

            GameObject gameObject = new GameObject();
            TestComponent component = new TestComponent(gameObject);
            gameObject.AddComponent(component);

            //Act
            gameObject.AddCollision(collider);

            //Assert
            Assert.IsTrue(component.CollisionEntered);
            Assert.IsFalse(component.CollisionStayed);
            Assert.IsFalse(component.CollisionExited);

            Assert.IsFalse(component.TriggerEntered);
            Assert.IsFalse(component.TriggerStayed);
            Assert.IsFalse(component.TriggerExited);
        }

        [TestMethod]
        public void OnCollisionStayTest()
        {
            //Arrange
            BoxCollider collider = new BoxCollider(new GameObject()) { Center = Vector3.Zero, Size = Vector3.One };

            GameObject gameObject = new GameObject();
            TestComponent component = new TestComponent(gameObject);
            gameObject.AddComponent(component);

            //Act
            gameObject.AddCollision(collider);
            gameObject.AddCollision(collider); // Add it second time

            //Assert
            Assert.IsTrue(component.CollisionEntered);
            Assert.IsTrue(component.CollisionStayed);
            Assert.IsFalse(component.CollisionExited);

            Assert.IsFalse(component.TriggerEntered);
            Assert.IsFalse(component.TriggerStayed);
            Assert.IsFalse(component.TriggerExited);
        }

        [TestMethod]
        public void OnCollisionExitTest()
        {
            //Arrange
            BoxCollider collider = new BoxCollider(new GameObject()) { Center = Vector3.Zero, Size = Vector3.One };

            GameObject gameObject = new GameObject();
            TestComponent component = new TestComponent(gameObject);
            gameObject.AddComponent(component);

            //Act
            gameObject.AddCollision(collider);
            gameObject.RemoveCollision(collider);

            //Assert
            Assert.IsTrue(component.CollisionEntered);
            Assert.IsFalse(component.CollisionStayed);
            Assert.IsTrue(component.CollisionExited);

            Assert.IsFalse(component.TriggerEntered);
            Assert.IsFalse(component.TriggerStayed);
            Assert.IsFalse(component.TriggerExited);
        }

        [TestMethod]
        public void OnTriggerEnterTest()
        {
            //Arrange
            BoxCollider collider = new BoxCollider(new GameObject()) { Center = Vector3.Zero, Size = Vector3.One };

            GameObject gameObject = new GameObject();
            TestComponent component = new TestComponent(gameObject);
            gameObject.AddComponent(component);

            //Act
            gameObject.AddCollision(collider, isTrigger: true);

            //Assert
            Assert.IsFalse(component.CollisionEntered);
            Assert.IsFalse(component.CollisionStayed);
            Assert.IsFalse(component.CollisionExited);

            Assert.IsTrue(component.TriggerEntered);
            Assert.IsFalse(component.TriggerStayed);
            Assert.IsFalse(component.TriggerExited);
        }

        [TestMethod]
        public void OnTriggerStayTest()
        {
            //Arrange
            BoxCollider collider = new BoxCollider(new GameObject()) { Center = Vector3.Zero, Size = Vector3.One };

            GameObject gameObject = new GameObject();
            TestComponent component = new TestComponent(gameObject);
            gameObject.AddComponent(component);

            //Act
            gameObject.AddCollision(collider, isTrigger: true);
            gameObject.AddCollision(collider, isTrigger: true); // Add it second time

            //Assert
            Assert.IsFalse(component.CollisionEntered);
            Assert.IsFalse(component.CollisionStayed);
            Assert.IsFalse(component.CollisionExited);

            Assert.IsTrue(component.TriggerEntered);
            Assert.IsTrue(component.TriggerStayed);
            Assert.IsFalse(component.TriggerExited);
        }

        [TestMethod]
        public void OnTriggerExitTest()
        {
            //Arrange
            BoxCollider collider = new BoxCollider(new GameObject()) { Center = Vector3.Zero, Size = Vector3.One };

            GameObject gameObject = new GameObject();
            TestComponent component = new TestComponent(gameObject);
            gameObject.AddComponent(component);

            //Act
            gameObject.AddCollision(collider, isTrigger: true);
            gameObject.RemoveCollision(collider);

            //Assert
            Assert.IsFalse(component.CollisionEntered);
            Assert.IsFalse(component.CollisionStayed);
            Assert.IsFalse(component.CollisionExited);
        }
    }
}

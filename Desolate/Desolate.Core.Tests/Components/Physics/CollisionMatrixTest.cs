﻿using Desolate.Core.Components.Physics;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Desolate.Core.Tests.Components.Physics
{
    [TestClass]
    public class CollisionMatrixTest
    {
        [TestMethod]
        public void SetLayerCollisionMaskTest()
        {
            //Arrange
            PhysicLayer ignoreMask = LayerMask.ToIgnoreMask(new[] {PhysicLayer.UI, PhysicLayer.Enviroment});

            //Act
            Physic.SetLayerCollisionMask(PhysicLayer.Throwable, ignoreMask);

            //Assert
            Assert.AreEqual(ignoreMask, Physic.GetLayerCollisionMask(PhysicLayer.Throwable));

            Assert.IsTrue(Physic.GetLayerCollisionMask(PhysicLayer.Throwable).HasFlag(PhysicLayer.Throwable));
            
            Assert.IsFalse(Physic.GetLayerCollisionMask(PhysicLayer.Throwable).HasFlag(PhysicLayer.UI));
            Assert.IsFalse(Physic.GetLayerCollisionMask(PhysicLayer.Throwable).HasFlag(PhysicLayer.Enviroment));
            Assert.IsFalse(Physic.GetLayerCollisionMask(PhysicLayer.UI).HasFlag(PhysicLayer.Throwable));
            Assert.IsFalse(Physic.GetLayerCollisionMask(PhysicLayer.Enviroment).HasFlag(PhysicLayer.Throwable));
        }

        [TestMethod]
        public void SetLayersCollisionInteractionFalseTest()
        {
            //Arrange

            //Act
            Physic.SetLayersCollisionInteraction(PhysicLayer.Throwable, PhysicLayer.BuiltIn_1, false);
            Physic.SetLayersCollisionInteraction(PhysicLayer.Throwable, PhysicLayer.Default, false);

            //Assert
            Assert.IsTrue(Physic.GetLayerCollisionMask(PhysicLayer.Throwable).HasFlag(PhysicLayer.Throwable));
            
            Assert.IsFalse(Physic.GetLayerCollisionMask(PhysicLayer.Throwable).HasFlag(PhysicLayer.BuiltIn_1));
            Assert.IsFalse(Physic.GetLayerCollisionMask(PhysicLayer.Throwable).HasFlag(PhysicLayer.Default));

            Assert.IsFalse(Physic.GetLayerCollisionMask(PhysicLayer.Default).HasFlag(PhysicLayer.Throwable));
            Assert.IsFalse(Physic.GetLayerCollisionMask(PhysicLayer.BuiltIn_1).HasFlag(PhysicLayer.Throwable));
        }
        
        [TestMethod]
        public void SetLayersCollisionInteractionTrueTest()
        {
            //Arrange
            PhysicLayer ignoreMask = LayerMask.ToIgnoreMask(new[] {PhysicLayer.UI, PhysicLayer.Enviroment});

            //Act
            Physic.SetLayerCollisionMask(PhysicLayer.Throwable, ignoreMask);
            Physic.SetLayersCollisionInteraction(PhysicLayer.Throwable, PhysicLayer.UI, true);

            //Assert
            Assert.IsTrue(Physic.GetLayerCollisionMask(PhysicLayer.Throwable).HasFlag(PhysicLayer.Throwable));
            
            Assert.IsTrue(Physic.GetLayerCollisionMask(PhysicLayer.Throwable).HasFlag(PhysicLayer.UI));
            Assert.IsTrue(Physic.GetLayerCollisionMask(PhysicLayer.UI).HasFlag(PhysicLayer.Throwable));
            
            Assert.IsFalse(Physic.GetLayerCollisionMask(PhysicLayer.Throwable).HasFlag(PhysicLayer.Enviroment));
            Assert.IsFalse(Physic.GetLayerCollisionMask(PhysicLayer.Enviroment).HasFlag(PhysicLayer.Throwable));
        }
    }
}
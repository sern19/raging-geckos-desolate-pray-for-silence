﻿using Desolate.Core.Components.Physics;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Tests.Components.Physics
{
    [TestClass]
    public class ColliderTest
    {
        private GameObject firstGameObject; 
        private GameObject secondGameObject; 

        [TestInitialize]
        public void TestInitialize()
        {
            firstGameObject = new GameObject();
            firstGameObject.Initialize();

            secondGameObject = new GameObject();
            secondGameObject.Initialize();
        }

        [TestMethod]
        public void IntersectionBoxWithBoxDetectedTest()
        {
            //Arrange
            BoxCollider colliderFirst = new BoxCollider(firstGameObject) { Center = Vector3.Zero, Size = Vector3.One * 2 };
            BoxCollider colliderSecond = new BoxCollider(secondGameObject) { Center = Vector3.One, Size = Vector3.One };

            //Act
            bool colisionDetected = colliderFirst.IsCollisionDetected(colliderSecond);

            //Assert
            Assert.IsTrue(colisionDetected);
        }


        [TestMethod]
        public void IntersectionBoxWithBoxNotDetectedTest()
        {
            //Arrange
            BoxCollider colliderFirst = new BoxCollider(firstGameObject) { Center = Vector3.Zero, Size = Vector3.One * 0.5f };
            BoxCollider colliderSecond = new BoxCollider(secondGameObject) { Center = Vector3.One, Size = Vector3.One };

            //Act
            bool colisionDetected = colliderFirst.IsCollisionDetected(colliderSecond);

            //Assert
            Assert.IsFalse(colisionDetected);
        }


        [TestMethod]
        public void IntersectionBoxWithSphereDetectedTest()
        {
            //Arrange
            BoxCollider colliderFirst = new BoxCollider(firstGameObject) { Center = Vector3.Zero, Size = Vector3.One };
            SphereCollider colliderSecond = new SphereCollider(secondGameObject) { Center = Vector3.One, Radius = 3 };

            //Act
            bool colisionDetected = colliderFirst.IsCollisionDetected(colliderSecond);

            //Assert
            Assert.IsTrue(colisionDetected);
        }


        [TestMethod]
        public void IntersectionBoxWithSphereNotDetectedTest()
        {
            //Arrange
            BoxCollider colliderFirst = new BoxCollider(firstGameObject) { Center = Vector3.Zero, Size = Vector3.One };
            SphereCollider colliderSecond = new SphereCollider(secondGameObject) { Center = Vector3.One * 3, Radius = 1 };

            //Act
            bool colisionDetected = colliderFirst.IsCollisionDetected(colliderSecond);

            //Assert
            Assert.IsFalse(colisionDetected);
        }

        [TestMethod]
        public void ColliderWithColliderColisionDetectedTest()
        {
            //Arrange
            BoxCollider colliderFirst = new BoxCollider(firstGameObject) { Center = Vector3.Zero, Size = Vector3.One };
            SphereCollider colliderSecond = new SphereCollider(secondGameObject) { Center = Vector3.Zero, Radius = 1 };

            //Act
            bool colisionDetected = colliderFirst.IsCollisionDetected(colliderSecond);

            //Assert
            Assert.IsTrue(colisionDetected);
        }


        [TestMethod]
        public void ColliderWithTriggerColisionNotDetectedTest()
        {
            //Arrange
            BoxCollider colliderFirst = new BoxCollider(firstGameObject) { Center = Vector3.Zero, Size = Vector3.One };
            SphereCollider colliderSecond = new SphereCollider(secondGameObject) { Center = Vector3.Zero, Radius = 1, IsTrigger = true };

            //Act
            bool colisionDetected = colliderFirst.IsCollisionDetected(colliderSecond);

            //Assert
            Assert.IsFalse(colisionDetected);
        }


        [TestMethod]
        public void TriggerWithTriggerColisionNotDetectedTest()
        {
            //Arrange
            BoxCollider colliderFirst = new BoxCollider(firstGameObject) { Center = Vector3.Zero, Size = Vector3.One };
            SphereCollider colliderSecond = new SphereCollider(secondGameObject) { Center = Vector3.Zero, Radius = 1, IsTrigger = true };

            //Act
            bool colisionDetected = colliderFirst.IsCollisionDetected(colliderSecond);

            //Assert
            Assert.IsFalse(colisionDetected);
        }


        [TestMethod]
        public void ColliderWithColliderTriggerNotDetectedTest()
        {
            //Arrange
            BoxCollider colliderFirst = new BoxCollider(firstGameObject) { Center = Vector3.Zero, Size = Vector3.One };
            SphereCollider colliderSecond = new SphereCollider(secondGameObject) { Center = Vector3.Zero, Radius = 1 };

            //Act
            bool triggerDetected = colliderFirst.IsTriggerDetected(colliderSecond);

            //Assert
            Assert.IsFalse(triggerDetected);
        }


        [TestMethod]
        public void ColliderWithTriggerTriggerDetectedTest()
        {
            //Arrange
            BoxCollider colliderFirst = new BoxCollider(firstGameObject) { Center = Vector3.Zero, Size = Vector3.One };
            SphereCollider colliderSecond = new SphereCollider(secondGameObject) { Center = Vector3.Zero, Radius = 1, IsTrigger = true };

            //Act
            bool triggerDetected = colliderFirst.IsTriggerDetected(colliderSecond);

            //Assert
            Assert.IsTrue(triggerDetected);
        }


        [TestMethod]
        public void TriggerWithTriggerTriggerDetectedTest()
        {
            //Arrange
            BoxCollider colliderFirst = new BoxCollider(firstGameObject) { Center = Vector3.Zero, Size = Vector3.One, IsTrigger =  true};
            SphereCollider colliderSecond = new SphereCollider(secondGameObject) { Center = Vector3.Zero, Radius = 1, IsTrigger = true };

            //Act
            bool triggerDetected = colliderFirst.IsTriggerDetected(colliderSecond);

            //Assert
            Assert.IsTrue(triggerDetected);
        }
    }
}

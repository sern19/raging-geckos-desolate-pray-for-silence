﻿using Desolate.Core.Components.Abstract;
using Desolate.Core.Components.Physics;

namespace Desolate.Core.Tests.Components.HelperClasses
{
    public class TestComponent : SingleAttitude
    {
        public TestComponent(GameObject gameObject) : base(gameObject)
        {
        }

        public bool CollisionEntered { get; set; }
        public bool CollisionStayed { get; set; }
        public bool CollisionExited { get; set; }

        public bool TriggerEntered { get; set; }
        public bool TriggerStayed { get; set; }
        public bool TriggerExited { get; set; }


        public override void OnCollisionEnter(Collider collider)
        {
            base.OnCollisionEnter(collider);
            CollisionEntered = true;
        }

        public override void OnCollisionStay(Collider collider)
        {
            base.OnCollisionStay(collider);
            CollisionStayed = true;
        }

        public override void OnCollisionExit(Collider collider)
        {
            base.OnCollisionExit(collider);
            CollisionExited = true;
        }

        public override void OnTriggerEnter(Collider collider)
        {
            base.OnTriggerEnter(collider);
            TriggerEntered = true;
        }

        public override void OnTriggerStay(Collider collider)
        {
            base.OnTriggerStay(collider);
            TriggerStayed = true;
        }

        public override void OnTriggerExit(Collider collider)
        {
            base.OnTriggerExit(collider);
            TriggerExited = true;
        }
    }
}

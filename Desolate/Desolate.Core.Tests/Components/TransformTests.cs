﻿using System.Linq;
using Desolate.Core.Components;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Tests.Components
{
    [TestClass]
    public class TransformTests
    {
        private readonly GameObject dummyObject = new GameObject();

        [TestMethod]
        public void TransformTranslateTest()
        {
            Transform transform = new Transform(dummyObject);

            Assert.AreEqual(transform.TransformMatrix, Matrix.Identity);

            transform.Translate(Vector3.One);
            Assert.AreEqual(transform.TransformMatrix, Matrix.CreateTranslation(Vector3.One));

            transform.Translate(-Vector3.One);
            Assert.AreEqual(transform.TransformMatrix, Matrix.Identity);
        }

        [TestMethod]
        public void TransformTranslateHierarchyTest()
        {
            GameObject parent = new GameObject();
            parent.Initialize();
            parent.AddChild(new GameObject());

            Assert.AreEqual(parent.Children.First().Transform.TransformMatrix, Matrix.Identity);

            parent.Transform.Translate(Vector3.One);
            Assert.AreEqual(parent.Children.First().Transform.TransformMatrix, Matrix.CreateTranslation(Vector3.One));

            parent.Children.First().Transform.Translate(-Vector3.One);
            Assert.AreEqual(parent.Transform.TransformMatrix, Matrix.CreateTranslation(Vector3.One));
            Assert.AreEqual(parent.Children.First().Transform.TransformMatrix, Matrix.CreateTranslation(Vector3.Zero));

            parent.Transform.Translate(-Vector3.One);
            Assert.AreEqual(parent.Transform.TransformMatrix, Matrix.CreateTranslation(Vector3.Zero));
            Assert.AreEqual(parent.Children.First().Transform.TransformMatrix, Matrix.CreateTranslation(-Vector3.One));
        }

        [TestMethod]
        public void TransformRotateTest()
        {
            Transform transform = new Transform(dummyObject);

            Assert.AreEqual(transform.TransformMatrix, Matrix.Identity);

            transform.Rotate(new Vector3(0, 90, 0));
            Assert.AreEqual(transform.LocalEulerAngles.Y, 90, 0.5);

            transform.LocalEulerAngles = Vector3.Zero;
            Assert.AreEqual(transform.TransformMatrix, Matrix.Identity);
        }

        [TestMethod]
        public void TransformRotateHierarchyTest()
        {
            GameObject parent = new GameObject();
            parent.Initialize();
            parent.AddChild(new GameObject());

            Assert.AreEqual(parent.Children.First().Transform.TransformMatrix, Matrix.Identity);

            Transform transform = new Transform(dummyObject);
            transform.Rotate(new Vector3(0, 0, 90));

            parent.Transform.Rotate(new Vector3(0, 0, 90));
            Assert.AreEqual(parent.Children.First().Transform.TransformMatrix, transform.TransformMatrix);
        }

        [TestMethod]
        public void TransformScaleTest()
        {
            Transform transform = new Transform(dummyObject);

            Assert.AreEqual(transform.TransformMatrix, Matrix.Identity);

            transform.LocalScale = 2 * Vector3.One;
            Assert.AreEqual(transform.TransformMatrix, Matrix.CreateScale(2 * Vector3.One));

            transform.LocalScale = Vector3.One;
            Assert.AreEqual(transform.TransformMatrix, Matrix.Identity);
        }

        [TestMethod]
        public void TransformScaleHierarchyTest()
        {
            GameObject parent = new GameObject();
            parent.Initialize();
            parent.AddChild(new GameObject());

            Assert.AreEqual(parent.Children.First().Transform.TransformMatrix, Matrix.Identity);

            parent.Transform.Rotate(new Vector3(0, 80, 0));

            parent.Transform.LocalScale = 2 * Vector3.One;
            Assert.AreEqual(parent.Children.First().Transform.WorldScale, 2 * Vector3.One);

            parent.Children.First().Transform.LocalScale = 2 * Vector3.One;
            Assert.AreEqual(parent.Transform.WorldScale, 2 * Vector3.One);
            Assert.AreEqual(parent.Children.First().Transform.WorldScale, 4 * Vector3.One);

            parent.Transform.LocalScale = Vector3.One;
            Assert.AreEqual(parent.Transform.WorldScale, Vector3.One);
            Assert.AreEqual(parent.Children.First().Transform.WorldScale, 2 * Vector3.One);
        }
    }
}
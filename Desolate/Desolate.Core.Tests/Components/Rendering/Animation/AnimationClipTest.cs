﻿using System;
using Desolate.Core.Components;
using Desolate.Core.Components.Rendering.Animation.TransformAnimation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Tests.Components.Rendering.Animation
{
    [TestClass]
    public class AnimationClipTest
    {
        private GameObject gameObject;

        [TestInitialize]
        public void TestInitialize()
        {
            gameObject = new GameObject();
            gameObject.Initialize();
        }

        [TestMethod]
        public void GetTransformNotLoopedTest()
        {
            //Arrange
            AnimationClip animationClip = new AnimationClip();

            AnimationFrame first = new AnimationFrame
            {
                Transform = new Transform(gameObject) {LocalPosition = Vector3.Zero},
                Time = TimeSpan.FromSeconds(0)
            };

            AnimationFrame second = new AnimationFrame
            {
                Transform = new Transform(gameObject) {LocalPosition = Vector3.One},
                Time = TimeSpan.FromSeconds(5)
            };

            animationClip.AnimationFrames.Add(first);
            animationClip.AnimationFrames.Add(second);


            //Act

            //Assert
            Assert.AreEqual(first.Transform.LocalPosition, animationClip.GetTransform(0, gameObject).LocalPosition);
            Assert.AreEqual(Vector3.One * 0.25f, animationClip.GetTransform(1.25f, gameObject).LocalPosition);
            Assert.AreEqual(Vector3.One * 0.5f, animationClip.GetTransform(2.5f, gameObject).LocalPosition);
            Assert.AreEqual(Vector3.One * 0.75f, animationClip.GetTransform(3.75f, gameObject).LocalPosition);
            Assert.AreEqual(second.Transform.LocalPosition, animationClip.GetTransform(5, gameObject).LocalPosition);
            Assert.AreEqual(second.Transform.LocalPosition,
                animationClip.GetTransform(10000, gameObject).LocalPosition);
        }
    }
}
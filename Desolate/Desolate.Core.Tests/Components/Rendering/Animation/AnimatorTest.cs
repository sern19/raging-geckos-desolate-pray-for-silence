﻿using System;
using Desolate.Core.Components;
using Desolate.Core.Components.Rendering.Animation;
using Desolate.Core.Components.Rendering.Animation.TransformAnimation;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.Xna.Framework;

namespace Desolate.Core.Tests.Components.Rendering.Animation
{
    [TestClass]
    public class AnimatorTest
    {
        private GameObject gameObject;

        [TestInitialize]
        public void TestInitialize()
        {
            gameObject = new GameObject();
            gameObject.Initialize();
        }

        [TestMethod]
        public void SetCurrentClipTest()
        {
            //Arrange
            AnimationClip firstClip = new AnimationClip();
            AnimationClip secondClip = new AnimationClip();

            Animator animator = new Animator(new GameObject());
            animator.AnimationClips.Add("first", firstClip);
            animator.AnimationClips.Add("second", secondClip);

            //Act
            animator.Play("first");

            //Assert
            Assert.AreSame(firstClip, animator.CurrentClip);
            Assert.IsTrue(animator.IsPlaying);
        }


        [TestMethod]
        public void UpdateAndStopAnimationTest()
        {
            //Arrange  
            Vector3 basePosition = gameObject.Transform.LocalPosition;

            AnimationFrame first = new AnimationFrame
            {
                Transform = new Transform(gameObject) {LocalPosition = Vector3.Zero},
                Time = TimeSpan.FromSeconds(0)
            };

            AnimationFrame second = new AnimationFrame
            {
                Transform = new Transform(gameObject) {LocalPosition = Vector3.One},
                Time = TimeSpan.FromSeconds(5)
            };

            AnimationClip firstClip = new AnimationClip();

            firstClip.AnimationFrames.Add(first);
            firstClip.AnimationFrames.Add(second);

            gameObject.Transform.LocalPosition = first.Transform.LocalPosition;
            gameObject.Transform.LocalEulerAngles = first.Transform.LocalEulerAngles;
            gameObject.Transform.LocalScale = first.Transform.LocalScale;

            Animator animator = new Animator(gameObject);

            animator.AnimationClips.Add("first", firstClip);

            GameTime baseGameTime = new GameTime();


            //Act
            animator.Play("first");
            animator.Update();
            baseGameTime.ElapsedGameTime = TimeSpan.FromSeconds(5);
            animator.Update();

            //Assert
            Assert.AreEqual(first.Transform.LocalPosition, gameObject.Transform.LocalPosition);

            animator.StopAnimation();

            Assert.AreEqual(basePosition, gameObject.Transform.LocalPosition);
        }

        [TestMethod]
        public void AnimatorLoopedClipTest()
        {
            //Arrange
            AnimationClip animationClip = new AnimationClip();

            AnimationFrame first = new AnimationFrame
            {
                Transform = new Transform(gameObject) {LocalPosition = Vector3.Zero},
                Time = TimeSpan.FromSeconds(0)
            };

            AnimationFrame second = new AnimationFrame
            {
                Transform = new Transform(gameObject) {LocalPosition = Vector3.One},
                Time = TimeSpan.FromSeconds(3)
            };

            animationClip.AnimationFrames.Add(first);
            animationClip.AnimationFrames.Add(second);

            Animator animator = new Animator(gameObject) {IsLooped = true};

            animator.AnimationClips.Add("first", animationClip);

            GameTime baseGameTime = new GameTime();

            //Act
            animator.Play("first");
            animator.Update();
            baseGameTime.ElapsedGameTime = TimeSpan.FromSeconds(5);
            animator.Update();

            //Assert
            Assert.IsTrue(animator.IsPlaying);
            Assert.AreEqual(animationClip, animator.CurrentClip);
        }

        [TestMethod]
        public void AnimatorNotLoopedClipTest()
        {
            //Arrange
            AnimationClip animationClip = new AnimationClip();

            AnimationFrame first = new AnimationFrame
            {
                Transform = new Transform(gameObject) {LocalPosition = Vector3.Zero},
                Time = TimeSpan.FromSeconds(0)
            };

            AnimationFrame second = new AnimationFrame
            {
                Transform = new Transform(gameObject) {LocalPosition = Vector3.One},
                Time = TimeSpan.FromSeconds(3)
            };

            animationClip.AnimationFrames.Add(first);
            animationClip.AnimationFrames.Add(second);

            Animator animator = new Animator(gameObject);

            animator.AnimationClips.Add("first", animationClip);

            GameTime baseGameTime = new GameTime();
            Time.GameTime = baseGameTime;
            
            //Act
            animator.Play("first");
            animator.Update();
            
            baseGameTime.ElapsedGameTime = TimeSpan.FromSeconds(5);
            Time.GameTime = baseGameTime;
            
            animator.Update();

            //Assert
            Assert.IsFalse(animator.IsPlaying);
            Assert.AreEqual(animationClip, animator.CurrentClip);
        }
    }
}
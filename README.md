## PBL
### Solution build order:
1. ModelPipeline solution (release)
2. Desolate solution

### Requires Monogame pipeline tool 3.7

### Authors:
* Beata Czekaj
* Bartosz Janicki
* Marcin Kotlicki
* Krystian Owoc
* Marcin Warszawski
